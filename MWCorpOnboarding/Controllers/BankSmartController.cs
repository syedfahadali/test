﻿using System.Collections.Generic;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CoreHandler;
using DBHandler.Helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using DBHandler.Repositories;
using Microsoft.Extensions.Configuration;
using DBHandler.Enum;
using CommonModel;
using Newtonsoft.Json.Linq;
using Swashbuckle.AspNetCore.Annotations;
using System;
//using RuleEngineHandler;
using DBHandler.CreditProgramSearchByCIFModel;
using DBHandler.CreditProgramUpdateModel;
using DBHandler.PostChargesModel;
using DBHandler.Model.Loan;
using DBHandler.Model.Gurantee;
using System.Globalization;
using HttpHandler;
using DigiBancMiddleware.Helper;

namespace VendorApi.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class BankSmartController : ControllerBase
    {
        private readonly HttpHandler.HttpHandler httpHandler;
        private readonly IUserChannelsRepository _userChannels;
        private readonly ILogRepository _logs;
        private readonly IConfiguration _config;
        private Level level = Level.Info;
        private Exception myEx;
        private ControllerBase _controllerBase;
        private Exception excetionForLog;
        

        public BankSmartController(IConfiguration config, IUserChannelsRepository userChannels, IHttpClientFactory httpClientFactory, ILogRepository logs, ControllerBase controllerbase = null)
        {
            _controllerBase = controllerbase;
            _config = config;
            _userChannels = userChannels;
            _logs = logs;
            httpHandler = new HttpHandler.HttpHandler(httpClientFactory);
            
        }


        [Route("BalanceInquriy")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Balance>", typeof(ActiveResponseSucces<Balance>))]
        public async Task<IActionResult> BalanceInquriy([FromBody] BalanceInquiryModel Model)
        {
            
            

            ActiveResponseSucces<Balance> response = new ActiveResponseSucces<Balance>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:BalanceInquriy"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<Balance>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    string specialChar = _config["GlobalSettings:SpecialChar"];
                    if (HttpContext.Request.Headers["ChannelId"] == _config["GlobalSettings:ECSCannel"])
                    {
                        response.Content.Name = response.Content.Name != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Name, specialChar) : null;
                        response.Content.ProductType = response.Content.ProductType != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ProductType, specialChar) : null;
                        response.Content.ProductDesc = response.Content.ProductDesc != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ProductDesc, specialChar) : null;
                    }

                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }
        
        [Route("CreateCorporateAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Content>", typeof(ActiveResponseSucces<Content>))]
        public async Task<IActionResult> CreateCorporateAccount([FromBody] CreateCorporateAccount Model)
        {
            
            
            ActiveResponseSucces<Content> response = new ActiveResponseSucces<Content>();

            if (Model != null)
            {
               
                Model.Acc.OurBranchID = HttpContext.Request.Headers["BranchId"];
                Model.Acc.CreateBy = HttpContext.Request.Headers["ChannelId"];
            }

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:CreateCorporateAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<Content>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    if (string.IsNullOrEmpty(response.Content.status))
                    {
                        response.Content.status = "Active";
                    }
                    else if (response.Content.status == "I")
                    {
                        response.Content.status = "InActive";
                    }
                    else if (response.Content.status == "T")
                    {
                        response.Content.status = "Dormant";
                    }
                    else if (response.Content.status == "X")
                    {
                        response.Content.status = "Deceased";
                    }
                    else if (response.Content.status == "D")
                    {
                        response.Content.status = "Blocked";
                    }
                    else if (response.Content.status == "C")
                    {
                        response.Content.status = "Closed";
                    }
                    else
                    {
                        response.Content.status = "Undefined";
                    }

                    return CreatedAtAction(null, response);

                }
            }
            return BadRequest(response);
        }

        [Route("GetCorporateAccountDetailEBos")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.GetCorporateAccountDetailEBosModel.GetCorporateAccountDetailEBosModelResponse>", typeof(ActiveResponseSucces<DBHandler.GetCorporateAccountDetailEBosModel.GetCorporateAccountDetailEBosModelResponse>))]
        public async Task<IActionResult> GetCorporateAccountDetailEBos([FromBody] DBHandler.GetCorporateAccountDetailEBosModel.GetCorporateAccountDetailEBosModel Model)
        {
            
            
            ActiveResponseSucces<DBHandler.GetCorporateAccountDetailEBosModel.GetCorporateAccountDetailEBosModelResponse> response = new ActiveResponseSucces<DBHandler.GetCorporateAccountDetailEBosModel.GetCorporateAccountDetailEBosModelResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:GetCorporateAccountDetailEBos"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.GetCorporateAccountDetailEBosModel.GetCorporateAccountDetailEBosModelResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        //#region Arfeen

        [Route("UpdateCorporateAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Content>", typeof(ActiveResponseSucces<Content>))]
        public async Task<IActionResult> UpdateCorporateAccount([FromBody] UpdateCorporateAccountModel Model)
        {
            
            
            ActiveResponseSucces<Content> response = new ActiveResponseSucces<Content>();

            if (Model != null)
            {
                
                Model.Acc.OurBranchID = HttpContext.Request.Headers["BranchId"];
                Model.Acc.CreateBy = HttpContext.Request.Headers["ChannelId"];
            }

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:UpdateCorporateAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);

            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<Content>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    if (string.IsNullOrEmpty(response.Content.status))
                    {
                        response.Content.status = "Active";
                    }
                    else if (response.Content.status == "I")
                    {
                        response.Content.status = "InActive";
                    }
                    else if (response.Content.status == "T")
                    {
                        response.Content.status = "Dormant";
                    }
                    else if (response.Content.status == "X")
                    {
                        response.Content.status = "Deceased";
                    }
                    else if (response.Content.status == "D")
                    {
                        response.Content.status = "Blocked";
                    }
                    else if (response.Content.status == "C")
                    {
                        response.Content.status = "Closed";
                    }
                    else
                    {
                        response.Content.status = "Undefined";
                    }

                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }
             

        //todo this need to fix as account model need to have validations

        [Route("CloseAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<Balance>>", typeof(ActiveResponseSucces<List<Balance>>))]
        public async Task<IActionResult> CloseAccount([FromBody] BalanceInquiryModel Model)
        {
            
            
            ActiveResponseSucces<List<Balance>> response = new ActiveResponseSucces<List<Balance>>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:CloseAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<Balance>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }
            
        [Route("GetTransactionListCorporate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<TransactionView>>", typeof(ActiveResponseSucces<List<TransactionView>>))]
        public async Task<IActionResult> GetTransactionListCorporate([FromBody] GetTransactionModel Model)
        {
            
            
            ActiveResponseSucces<List<TransactionView>> response = new ActiveResponseSucces<List<TransactionView>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:GetTransactionListCorporate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<TransactionView>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }
                                                       
        [Route("GetCorporateAccountDetail")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<AccountCorporateView>", typeof(ActiveResponseSucces<AccountCorporateView>))]
        public async Task<IActionResult> GetCorporateAccountDetail([FromBody] BalanceIBANInquiryModel Model)
        {
            
            
            ActiveResponseSucces<AccountCorporateView> response = new ActiveResponseSucces<AccountCorporateView>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:GetCorporateAccountDetail"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                try
                {
                    response = JsonConvert.DeserializeObject<ActiveResponseSucces<AccountCorporateView>>(result.httpResult.httpResponse);
                }
                catch (Exception ex)
                {
                    int i = 0;
                }
                if (result != null && result.httpResult?.severity == "Success")
                {

                    if (string.IsNullOrEmpty(response.Content.status))
                    {
                        response.Content.status = "Active";
                    }
                    else if (response.Content.status == "I")
                    {
                        response.Content.status = "InActive";
                    }
                    else if (response.Content.status == "T")
                    {
                        response.Content.status = "Dormant";
                    }
                    else if (response.Content.status == "X")
                    {
                        response.Content.status = "Deceased";
                    }
                    else if (response.Content.status == "D")
                    {
                        response.Content.status = "Blocked";
                    }
                    else if (response.Content.status == "C")
                    {
                        response.Content.status = "Closed";
                    }
                    else
                    {
                        response.Content.status = "Undefined";
                    }
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }
               
        [Route("CreateCorporateCIF")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CorporateInfo> ", typeof(ActiveResponseSucces<CorporateInfo>))]
        public async Task<IActionResult> CreateCorporateCIF([FromBody] DBHandler.Model.Dtos.CustomerModel Model)
        {
            
            
            ActiveResponseSucces<CorporateInfo> response = new ActiveResponseSucces<CorporateInfo>();

            if (Model != null)
            {
                Model.Cust.CreateBy = HttpContext.Request.Headers["ChannelId"];
                Model.Cust.ReasonACorp = Model.Cust.FATCANoReasonCorp;
            }

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:CreateCorporateCIF"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CorporateInfo>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("UpdateCorporateClient")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateCorporateClient([FromBody] UpdateCorporateCIF Model)
        {
            
            
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            if (Model != null)
            {
                
                Model.Cust.OurBranchID = HttpContext.Request.Headers["BranchId"];
                Model.Cust.CreateBy = HttpContext.Request.Headers["ChannelId"];
            }
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:UpdateCorporateClient"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("UpdateCorporateCifWithChilds")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CorporateInfo>", typeof(ActiveResponseSucces<CorporateInfo>))]
        public async Task<IActionResult> UpdateCorporateCifWithChilds([FromBody] DBHandler.Model.Dtos.UpdateCorporateCifWithChildsModel Model)
        {
            
            
            ActiveResponseSucces<CorporateInfo> response = new ActiveResponseSucces<CorporateInfo>();

            if (Model != null)
            {
                Model.Cust.CreateBy = HttpContext.Request.Headers["ChannelId"];
            }
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:UpdateCorporateCifWithChilds"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CorporateInfo>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("CreateFIKycLiteCustomerV2")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<DBHandler.Model.CreateCorporateCIFV2Response>))]
        public async Task<IActionResult> CreateFIKycLiteCustomerV2([FromBody] DBHandler.Model.CreateFIKycLiteCustomerV2Model Model)
        {
            ActiveResponseSucces<DBHandler.Model.CreateCorporateCIFV2Response> response = new ActiveResponseSucces<DBHandler.Model.CreateCorporateCIFV2Response>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:CreateFIKycLiteCustomerV2"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.CreateCorporateCIFV2Response>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        //[Route("CreateFIKycLiteCustomer")]
        //[Authorize(AuthenticationSchemes = "BasicAuthentication")]
        //[ProducesResponseType(typeof(ActiveResponse), 400)]
        //[HttpPost]
        //[SwaggerResponse(200, "ActiveResponseSucces<CorporateInfo>", typeof(ActiveResponseSucces<CorporateInfo>))]
        //public async Task<IActionResult> CreateFIKycLiteCustomer([FromBody] DBHandler.Model.Dtos.CreateFIKycLiteCustomerModel Model)
        //{
            
            
        //    ActiveResponseSucces<CorporateInfo> response = new ActiveResponseSucces<CorporateInfo>();

        //    if (Model != null)
        //    {
        //        Model.Cust.CreateBy = HttpContext.Request.Headers["ChannelId"];
        //    }
        //    InternalResult result = await APIRequestHandler.GetResponse(Model,
        //                                                                _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
        //                                                                _config["BSCorpOnboardingAPI:CreateFIKycLiteCustomer"].ToString(),
        //                                                                level,
        //                                                                _logs,
        //                                                                _userChannels,
        //                                                                this,
        //                                                                _config,
        //                                                                httpHandler, 1);
        //    response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
        //    response.LogId = HttpContext.Request.Headers["LogId"];
        //    response.Status = result.Status;
        //    if (result.Status.Code == null)
        //    {
        //        response = JsonConvert.DeserializeObject<ActiveResponseSucces<CorporateInfo>>(result.httpResult.httpResponse);
        //        if (result != null && result.httpResult?.severity == "Success")
        //        {
        //            return CreatedAtAction(null, response);
        //        }
        //    }
        //    return BadRequest(response);
        //}


        [Route("GetCorporateCIFDetail")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.Dtos.CustomerCorporateView>", typeof(ActiveResponseSucces<DBHandler.Model.Dtos.CustomerCorporateView>))]
        public async Task<IActionResult> GetCorporateCIFDetail([FromBody] RimInquiry Model)
        {
            
            
            ActiveResponseSucces<DBHandler.Model.Dtos.CustomerCorporateView> response = new ActiveResponseSucces<DBHandler.Model.Dtos.CustomerCorporateView>();


            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:GetCorporateCIFDetail"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                string reaons = "";

                var temp = JObject.Parse(result.httpResult.httpResponse);
                if (temp["content"].Count() > 0)
                {
                    JObject channel = (JObject)temp["content"]["cust"];
                    reaons = channel.Property("reasonACorp")?.Value.ToString();
                    if (reaons != null)
                        channel.Property("reasonACorp").Remove();
                }
                string json = temp.ToString(); // backing result to json
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.Dtos.CustomerCorporateView>>(json);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Content.Cust.FATCANoReasonCorp = reaons;
                    if (response.Content.Cust.KYCReviewDateCorp != null)
                    {
                        DateTime d = Convert.ToDateTime(response.Content.Cust.KYCReviewDateCorp);
                        response.Content.Cust.KYCReviewDateCorp = new DateTime(d.Year, d.Month, d.Day, d.Hour, d.Minute, d.Second);
                    }

                    string specialChar = _config["GlobalSettings:SpecialChar"];
                    if (HttpContext.Request.Headers["ChannelId"] == _config["GlobalSettings:ECSCannel"])
                    {
                        response.Content.Cust.Name = response.Content.Cust.Name != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.Name, specialChar) : null;
                        response.Content.Cust.RManager = response.Content.Cust.RManager != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.RManager, specialChar) : null;
                        response.Content.Cust.EntityNameCorp = response.Content.Cust.EntityNameCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.EntityNameCorp, specialChar) : null;
                        response.Content.Cust.EntityTypeCorp = response.Content.Cust.EntityTypeCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.EntityTypeCorp, specialChar) : null;
                        response.Content.Cust.RegisteredAddressCorp = response.Content.Cust.RegisteredAddressCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.RegisteredAddressCorp, specialChar) : null;
                        response.Content.Cust.ContactName1Corp = response.Content.Cust.ContactName1Corp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.ContactName1Corp, specialChar) : null;
                        response.Content.Cust.ContactID1Corp = response.Content.Cust.ContactID1Corp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.ContactID1Corp, specialChar) : null;
                        response.Content.Cust.Address1Corp = response.Content.Cust.Address1Corp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.Address1Corp, specialChar) : null;
                        response.Content.Cust.ContactName2Corp = response.Content.Cust.ContactName2Corp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.ContactName2Corp, specialChar) : null;
                        response.Content.Cust.ContactID2Corp = response.Content.Cust.ContactID2Corp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.ContactID2Corp, specialChar) : null;
                        response.Content.Cust.Address2Corp = response.Content.Cust.Address2Corp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.Address2Corp, specialChar) : null;
                        response.Content.Cust.BusinessActivity = response.Content.Cust.BusinessActivity != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.BusinessActivity, specialChar) : null;
                        response.Content.Cust.TradeLicenseNumberCorp = response.Content.Cust.TradeLicenseNumberCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.TradeLicenseNumberCorp, specialChar) : null;
                        response.Content.Cust.CountryofIncorporationCorp = response.Content.Cust.CountryofIncorporationCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.CountryofIncorporationCorp, specialChar) : null;
                        response.Content.Cust.TradeLicenseIssuanceAuthorityCorp = response.Content.Cust.TradeLicenseIssuanceAuthorityCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.TradeLicenseIssuanceAuthorityCorp, specialChar) : null;
                        response.Content.Cust.SponsorNameCorp = response.Content.Cust.SponsorNameCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.SponsorNameCorp, specialChar) : null;
                        response.Content.Cust.NFFECorp = response.Content.Cust.NFFECorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.NFFECorp, specialChar) : null;
                        response.Content.Cust.RegAddCityCorp = response.Content.Cust.RegAddCityCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.RegAddCityCorp, specialChar) : null;
                        response.Content.Cust.RegAddCountryCorp = response.Content.Cust.RegAddCountryCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.RegAddCountryCorp, specialChar) : null;
                        response.Content.Cust.RegAddStateCorp = response.Content.Cust.RegAddStateCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.RegAddStateCorp, specialChar) : null;
                        response.Content.Cust.Add1CountryCorp = response.Content.Cust.Add1CountryCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.Add1CountryCorp, specialChar) : null;
                        response.Content.Cust.Add1StateCorp = response.Content.Cust.Add1StateCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.Add1StateCorp, specialChar) : null;
                        response.Content.Cust.Add1CityCorp = response.Content.Cust.Add1CityCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.Add1CityCorp, specialChar) : null;
                        response.Content.Cust.Add2CountryCorp = response.Content.Cust.Add2CountryCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.Add2CountryCorp, specialChar) : null;
                        response.Content.Cust.Add2StateCorp = response.Content.Cust.Add2StateCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.Add2StateCorp, specialChar) : null;
                        response.Content.Cust.Add2CityCorp = response.Content.Cust.Add2CityCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.Add2CityCorp, specialChar) : null;
                        response.Content.Cust.TradeLicenseIssuanceAuthorityCorp = response.Content.Cust.TradeLicenseIssuanceAuthorityCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.TradeLicenseIssuanceAuthorityCorp, specialChar) : null;
                        response.Content.Cust.TradeLicenseIssuanceAuthorityCorp = response.Content.Cust.TradeLicenseIssuanceAuthorityCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.TradeLicenseIssuanceAuthorityCorp, specialChar) : null;
                        response.Content.Cust.TradeLicenseIssuanceAuthorityCorp = response.Content.Cust.TradeLicenseIssuanceAuthorityCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.TradeLicenseIssuanceAuthorityCorp, specialChar) : null;
                        response.Content.Cust.TradeLicenseIssuanceAuthorityCorp = response.Content.Cust.TradeLicenseIssuanceAuthorityCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.TradeLicenseIssuanceAuthorityCorp, specialChar) : null;
                        response.Content.Cust.TradeLicenseIssuanceAuthorityCorp = response.Content.Cust.TradeLicenseIssuanceAuthorityCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.TradeLicenseIssuanceAuthorityCorp, specialChar) : null;

                        for (int i = 0; i < response.Content.ShareHolders.Count; i++)
                        {
                            response.Content.ShareHolders[i].ShareHolderName = response.Content.ShareHolders[i].ShareHolderName != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ShareHolders[i].ShareHolderName, specialChar) : null;
                            response.Content.ShareHolders[i].ShareHolderAddress = response.Content.ShareHolders[i].ShareHolderAddress != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ShareHolders[i].ShareHolderAddress, specialChar) : null;
                            response.Content.ShareHolders[i].CountryofIncorporation = response.Content.ShareHolders[i].CountryofIncorporation != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ShareHolders[i].CountryofIncorporation, specialChar) : null;
                            response.Content.ShareHolders[i].AuthorisedSignatory = response.Content.ShareHolders[i].AuthorisedSignatory != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ShareHolders[i].AuthorisedSignatory, specialChar) : null;
                            response.Content.ShareHolders[i].BeneficialOwner = response.Content.ShareHolders[i].BeneficialOwner != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ShareHolders[i].BeneficialOwner, specialChar) : null;
                            response.Content.ShareHolders[i].CountryofResidence = response.Content.ShareHolders[i].CountryofResidence != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ShareHolders[i].CountryofResidence, specialChar) : null;
                            response.Content.ShareHolders[i].SHCity = response.Content.ShareHolders[i].SHCity != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ShareHolders[i].SHCity, specialChar) : null;
                            response.Content.ShareHolders[i].SHState = response.Content.ShareHolders[i].SHState != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ShareHolders[i].SHState, specialChar) : null;
                            response.Content.ShareHolders[i].SHCountry = response.Content.ShareHolders[i].SHCountry != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ShareHolders[i].SHCountry, specialChar) : null;
                        }

                        for (int i = 0; i < response.Content.Authignatories.Count; i++)
                        {
                            response.Content.Authignatories[i].AuthorisedsignatoriesName = response.Content.Authignatories[i].AuthorisedsignatoriesName != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Authignatories[i].AuthorisedsignatoriesName, specialChar) : null;
                            response.Content.Authignatories[i].CorrespondenceAddress = response.Content.Authignatories[i].CorrespondenceAddress != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Authignatories[i].CorrespondenceAddress, specialChar) : null;
                            response.Content.Authignatories[i].Remarks = response.Content.Authignatories[i].Remarks != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Authignatories[i].Remarks, specialChar) : null;
                            response.Content.Authignatories[i].ASCity = response.Content.Authignatories[i].ASCity != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Authignatories[i].ASCity, specialChar) : null;
                            response.Content.Authignatories[i].ASState = response.Content.Authignatories[i].ASState != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Authignatories[i].ASState, specialChar) : null;
                            response.Content.Authignatories[i].ASCountry = response.Content.Authignatories[i].ASCountry != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Authignatories[i].ASCountry, specialChar) : null;
                        }

                        for (int i = 0; i < response.Content.BoardDirectors.Count; i++)
                        {
                            response.Content.BoardDirectors[i].BoardDirectorName = response.Content.BoardDirectors[i].BoardDirectorName != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BoardDirectors[i].BoardDirectorName, specialChar) : null;
                            response.Content.BoardDirectors[i].Designation = response.Content.BoardDirectors[i].Designation != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BoardDirectors[i].Designation, specialChar) : null;
                            response.Content.BoardDirectors[i].BoardDirectorAddress = response.Content.BoardDirectors[i].BoardDirectorAddress != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BoardDirectors[i].BoardDirectorAddress, specialChar) : null;
                            response.Content.BoardDirectors[i].BDCity = response.Content.BoardDirectors[i].BDCity != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BoardDirectors[i].BDCity, specialChar) : null;
                            response.Content.BoardDirectors[i].BDState = response.Content.BoardDirectors[i].BDState != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BoardDirectors[i].BDState, specialChar) : null;
                            response.Content.BoardDirectors[i].BDCountry = response.Content.BoardDirectors[i].BDCountry != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BoardDirectors[i].BDCountry, specialChar) : null;
                        }

                        for (int i = 0; i < response.Content.BeneficialOwners.Count; i++)
                        {
                            response.Content.BeneficialOwners[i].BeneficialName = response.Content.BeneficialOwners[i].BeneficialName != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BeneficialOwners[i].BeneficialName, specialChar) : null;
                            response.Content.BeneficialOwners[i].BeneficialAddress = response.Content.BeneficialOwners[i].BeneficialAddress != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BeneficialOwners[i].BeneficialAddress, specialChar) : null;
                            response.Content.BeneficialOwners[i].BOCity = response.Content.BeneficialOwners[i].BOCity != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BeneficialOwners[i].BOCity, specialChar) : null;
                            response.Content.BeneficialOwners[i].BOState = response.Content.BeneficialOwners[i].BOState != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BeneficialOwners[i].BOState, specialChar) : null;
                            response.Content.BeneficialOwners[i].BOCountry = response.Content.BeneficialOwners[i].BOCountry != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BeneficialOwners[i].BOCountry, specialChar) : null;
                        }
                    }
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }
              
        [Route("InquireFIKycLiteCustomer")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.Dtos.CustomerCorporateView>", typeof(ActiveResponseSucces<DBHandler.Model.Dtos.CustomerCorporateView>))]
        public async Task<IActionResult> InquireFIKycLiteCustomer([FromBody] RimInquiry Model)
        {
            
            
            ActiveResponseSucces<DBHandler.Model.Dtos.CustomerCorporateView> response = new ActiveResponseSucces<DBHandler.Model.Dtos.CustomerCorporateView>();


            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:InquireFIKycLiteCustomer"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                string reaons = "";

                var temp = JObject.Parse(result.httpResult.httpResponse);
                if (temp["content"].Count() > 0)
                {
                    JObject channel = (JObject)temp["content"]["cust"];
                    reaons = channel.Property("reasonACorp").Value.ToString();
                    channel.Property("reasonACorp").Remove();
                }
                string json = temp.ToString(); // backing result to json
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.Dtos.CustomerCorporateView>>(json);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Content.Cust.FATCANoReasonCorp = reaons;
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetShareHolders")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<ShareHolder>>", typeof(ActiveResponseSucces<List<ShareHolder>>))]
        public async Task<IActionResult> GetShareHolders([FromBody] CIFDetailInquiry Model)
        {
            
            
            ActiveResponseSucces<List<ShareHolder>> response = new ActiveResponseSucces<List<ShareHolder>>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:GetShareHolders"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<ShareHolder>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("GetAuthorisedSignatories")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<AuthorisedSignatoriesView>>", typeof(ActiveResponseSucces<List<AuthorisedSignatoriesView>>))]
        public async Task<IActionResult> GetAuthorisedSignatories([FromBody] CIFDetailInquiry Model)
        {
            
            
            ActiveResponseSucces<List<AuthorisedSignatoriesView>> response = new ActiveResponseSucces<List<AuthorisedSignatoriesView>>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:GetAuthorisedSignatories"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<AuthorisedSignatoriesView>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("GetBeneficialOwners")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<BeneficialOwners>>", typeof(ActiveResponseSucces<List<BeneficialOwners>>))]
        public async Task<IActionResult> GetBeneficialOwners([FromBody] CIFDetailInquiry Model)
        {
            
            
            ActiveResponseSucces<List<BeneficialOwners>> response = new ActiveResponseSucces<List<BeneficialOwners>>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:GetBeneficialOwners"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<BeneficialOwners>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("GetBoardDirectors")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<BoardDirectors>>", typeof(ActiveResponseSucces<List<BoardDirectors>>))]
        public async Task<IActionResult> GetBoardDirectors([FromBody] CIFDetailInquiry Model)
        {
            
            
            ActiveResponseSucces<List<BoardDirectors>> response = new ActiveResponseSucces<List<BoardDirectors>>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:GetBoardDirectors"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<BoardDirectors>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("DeleteShareHolders")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> DeleteShareHolders([FromBody] DeleteRequestForClientDetail Model)
        {
            
            
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:DeleteShareHolders"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("UpdateGroupAndSignature")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateGroupAndSignature([FromBody] UpdateSignatureModel Model)
        {
            
            
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:UpdateGroupAndSignature"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("DeleteAuthSignatory")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> DeleteAuthSignatory([FromBody] DeleteRequestForClientDetail Model)
        {
            
            
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:DeleteAuthSignatory"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("DeleteBeneficialOwners")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> DeleteBeneficialOwners([FromBody] DeleteRequestForClientDetail Model)
        {
            
            
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:DeleteBeneficialOwners"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("DeleteBoardDirectors")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> DeleteBoardDirectors([FromBody] DeleteRequestForClientDetail Model)
        {
            
            
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:DeleteBoardDirectors"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }


            //UserName:erc1,Password:Cbt@1786
            #region RimInquiryByRim
            /// <summary>
            /// Rim Inquiry.
            /// </summary>
            /// <remarks>
            /// Sample request:
            ///
            ///     POST /Todo
            ///     {
            ///         "signOnRq":{ 
            ///        "LogId": "e0ccbe1a-dfdd-4753-a4c7-acc6fb0665e3",
            ///          "DateTime":"03-06-201",
            ///          "ChannelId":"Swagger",
            ///         "OurBranchId":"079"},
            ///       
            ///        "RimNo": "00000155",
            ///     }
            ///
            /// </remarks>
            /// <response code="400">If any Error Occur</response>            

            
            #endregion

            #region RimInquiryByCpr
            /// <summary>
            /// Rim Inquiry.
            /// </summary>
            /// <remarks>
            /// Sample request:
            ///
            ///     POST /Todo
            ///     {
            ///         "signOnRq":{ 
            ///        "LogId": "e0ccbe1a-dfdd-4753-a4c7-acc6fb0665e3",
            ///          "DateTime":"03-06-201",
            ///          "ChannelId":"Swagger",
            ///         "OurBranchId":"079"},
            ///       
            ///        "CprNumber": "3840366576395",
            ///     }
            ///
            /// </remarks>
            /// <response code="400">If any Error Occur</response>            

            

            #endregion
                

            [Route("GetAllManageGroups")]
            [Authorize(AuthenticationSchemes = "BasicAuthentication")]
            [HttpPost]
            [SwaggerResponse(200, "ActiveResponseSucces<List<DBHandler.Model.GetAllManageGroups.GetAllManageGroupsResponse>>>", typeof(ActiveResponseSucces<List<DBHandler.Model.GetAllManageGroups.GetAllManageGroupsResponse>>))]
            public async Task<IActionResult> GetAllManageGroups([FromBody] DBHandler.Model.GetAllManageGroups.GetAllManageGroupsModel Model)
            {
                
                
                ActiveResponseSucces<List<DBHandler.Model.GetAllManageGroups.GetAllManageGroupsResponse>> response = new ActiveResponseSucces<List<DBHandler.Model.GetAllManageGroups.GetAllManageGroupsResponse>>();

                InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                            _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                            _config["BSCorpOnboardingAPI:GetAllManageGroups"].ToString(),
                                                                            level,
                                                                            _logs,
                                                                            _userChannels,
                                                                            this,
                                                                            _config,
                                                                            httpHandler, 1);
                response.Status = result.Status;
                response.LogId = HttpContext.Request.Headers["LogId"];
                response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
                if (result.Status.Code == null)
                {
                    response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<DBHandler.Model.GetAllManageGroups.GetAllManageGroupsResponse>>>(result.httpResult.httpResponse);
                    if (result != null && result.httpResult?.severity == "Success")
                    {
                        return CreatedAtAction(null, response);
                    }
                }

                return BadRequest(response);
            }

            [Route("GetManageGroup")]
            [Authorize(AuthenticationSchemes = "BasicAuthentication")]
            [HttpPost]
            [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.GetManageGroup.GetManageGroupResponse>", typeof(ActiveResponseSucces<DBHandler.Model.GetManageGroup.GetManageGroupResponse>))]
            public async Task<IActionResult> GetManageGroup([FromBody] DBHandler.Model.GetManageGroup.GetManageGroupModel Model)
            {
                
                
                ActiveResponseSucces<DBHandler.Model.GetManageGroup.GetManageGroupResponse> response = new ActiveResponseSucces<DBHandler.Model.GetManageGroup.GetManageGroupResponse>();

                InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                            _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                            _config["BSCorpOnboardingAPI:GetManageGroup"].ToString(),
                                                                            level,
                                                                            _logs,
                                                                            _userChannels,
                                                                            this,
                                                                            _config,
                                                                            httpHandler, 1);
                response.Status = result.Status;
                response.LogId = HttpContext.Request.Headers["LogId"];
                response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
                if (result.Status.Code == null)
                {
                    response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.GetManageGroup.GetManageGroupResponse>>(result.httpResult.httpResponse);
                    if (result != null && result.httpResult?.severity == "Success")
                    {
                        return CreatedAtAction(null, response);
                    }
                }

                return BadRequest(response);
            }

            [Route("SaveManageGroup")]
            [Authorize(AuthenticationSchemes = "BasicAuthentication")]
            [HttpPost]
            [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.SaveManageGroup.SaveManageGroupResponse>", typeof(ActiveResponseSucces<DBHandler.Model.SaveManageGroup.SaveManageGroupResponse>))]
            public async Task<IActionResult> SaveManageGroup([FromBody] DBHandler.Model.SaveManageGroup.SaveManageGroupModel Model)
            {
                
                
                ActiveResponseSucces<DBHandler.Model.SaveManageGroup.SaveManageGroupResponse> response = new ActiveResponseSucces<DBHandler.Model.SaveManageGroup.SaveManageGroupResponse>();

                InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                            _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                            _config["BSCorpOnboardingAPI:SaveManageGroup"].ToString(),
                                                                            level,
                                                                            _logs,
                                                                            _userChannels,
                                                                            this,
                                                                            _config,
                                                                            httpHandler, 1);
                response.Status = result.Status;
                response.LogId = HttpContext.Request.Headers["LogId"];
                response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
                if (result.Status.Code == null)
                {
                    response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.SaveManageGroup.SaveManageGroupResponse>>(result.httpResult.httpResponse);
                    if (result != null && result.httpResult?.severity == "Success")
                    {
                        return CreatedAtAction(null, response);
                    }
                }

                return BadRequest(response);
            }

            [Route("DeleteManageGroup")]
            [Authorize(AuthenticationSchemes = "BasicAuthentication")]
            [HttpPost]
            [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.DeleteManageGroup.DeleteManageGroupResponse>", typeof(ActiveResponseSucces<DBHandler.Model.DeleteManageGroup.DeleteManageGroupResponse>))]
            public async Task<IActionResult> DeleteManageGroup([FromBody] DBHandler.Model.DeleteManageGroup.DeleteManageGroupModel Model)
            {
                
                
                ActiveResponseSucces<DBHandler.Model.DeleteManageGroup.DeleteManageGroupResponse> response = new ActiveResponseSucces<DBHandler.Model.DeleteManageGroup.DeleteManageGroupResponse>();

                InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                            _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                            _config["BSCorpOnboardingAPI:DeleteManageGroup"].ToString(),
                                                                            level,
                                                                            _logs,
                                                                            _userChannels,
                                                                            this,
                                                                            _config,
                                                                            httpHandler, 1);
                response.Status = result.Status;
                response.LogId = HttpContext.Request.Headers["LogId"];
                response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
                if (result.Status.Code == null)
                {
                    response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.DeleteManageGroup.DeleteManageGroupResponse>>(result.httpResult.httpResponse);
                    if (result != null && result.httpResult?.severity == "Success")
                    {
                        return CreatedAtAction(null, response);
                    }
                }

                return BadRequest(response);
            }

            [Route("AddUpdateBorrowMandate")]
            [Authorize(AuthenticationSchemes = "BasicAuthentication")]
            [HttpPost]
            [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.AddUpdateBorrowManDate.AddUpdateBorrowManDateResponse>", typeof(ActiveResponseSucces<DBHandler.Model.AddUpdateBorrowManDate.AddUpdateBorrowManDateResponse>))]
            public async Task<IActionResult> AddUpdateBorrowMandate([FromBody] DBHandler.Model.AddUpdateBorrowManDate.AddUpdateBorrowManDateModel Model)
            {
                ActiveResponseSucces<DBHandler.Model.AddUpdateBorrowManDate.AddUpdateBorrowManDateResponse> response = new ActiveResponseSucces<DBHandler.Model.AddUpdateBorrowManDate.AddUpdateBorrowManDateResponse>();

                InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                            _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                            _config["BSCorpOnboardingAPI:AddUpdateBorrowManDate"].ToString(),
                                                                            level,
                                                                            _logs,
                                                                            _userChannels,
                                                                            this,
                                                                            _config,
                                                                            httpHandler, 1);
                response.Status = result.Status;
                response.LogId = HttpContext.Request.Headers["LogId"];
                response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
                if (result.Status.Code == null)
                {
                    response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.AddUpdateBorrowManDate.AddUpdateBorrowManDateResponse>>(result.httpResult.httpResponse);
                    if (result != null && result.httpResult?.severity == "Success")
                    {
                    return CreatedAtAction(null, response);
                }
                }

                return BadRequest(response);
            }

            [Route("GetBorrowMandate")]
            [Authorize(AuthenticationSchemes = "BasicAuthentication")]
            [HttpPost]
            [SwaggerResponse(200, "ActiveResponseSucces<List<DBHandler.Model.GetBorrowManDate.GetBorrowManDateResponse>>", typeof(ActiveResponseSucces<List<DBHandler.Model.GetBorrowManDate.GetBorrowManDateResponse>>))]
            public async Task<IActionResult> GetBorrowMandate([FromBody] DBHandler.Model.GetBorrowManDate.GetBorrowManDateModel Model)
            {
                ActiveResponseSucces<List<DBHandler.Model.GetBorrowManDate.GetBorrowManDateResponse>> response = new ActiveResponseSucces<List<DBHandler.Model.GetBorrowManDate.GetBorrowManDateResponse>>();

                InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                            _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                            _config["BSCorpOnboardingAPI:GetBorrowManDate"].ToString(),
                                                                            level,
                                                                            _logs,
                                                                            _userChannels,
                                                                            this,
                                                                            _config,
                                                                            httpHandler, 1);
                response.Status = result.Status;
                response.LogId = HttpContext.Request.Headers["LogId"];
                response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
                if (result.Status.Code == null)
                {
                    response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<DBHandler.Model.GetBorrowManDate.GetBorrowManDateResponse>>>(result.httpResult.httpResponse);
                    if (result != null && result.httpResult?.severity == "Success")
                    {
                        return CreatedAtAction(null, response);
                    }
                }

                return BadRequest(response);
            }

            [Route("DeleteBorrowMandate")]
            [Authorize(AuthenticationSchemes = "BasicAuthentication")]
            [HttpPost]
            [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.DeleteBorrowManDate.DeleteBorrowManDateResponse>", typeof(ActiveResponseSucces<DBHandler.Model.DeleteBorrowManDate.DeleteBorrowManDateResponse>))]
            public async Task<IActionResult> DeleteBorrowMandate([FromBody] DBHandler.Model.DeleteBorrowManDate.DeleteBorrowManDateModel Model)
            {
                ActiveResponseSucces<DBHandler.Model.DeleteBorrowManDate.DeleteBorrowManDateResponse> response = new ActiveResponseSucces<DBHandler.Model.DeleteBorrowManDate.DeleteBorrowManDateResponse>();

                InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                            _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                            _config["BSCorpOnboardingAPI:DeleteBorrowManDate"].ToString(),
                                                                            level,
                                                                            _logs,
                                                                            _userChannels,
                                                                            this,
                                                                            _config,
                                                                            httpHandler, 1);
                response.Status = result.Status;
                response.LogId = HttpContext.Request.Headers["LogId"];
                response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
                if (result.Status.Code == null)
                {
                    response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.DeleteBorrowManDate.DeleteBorrowManDateResponse>>(result.httpResult.httpResponse);
                    if (result != null && result.httpResult?.severity == "Success")
                    {
                        return CreatedAtAction(null, response);
                    }
                }

                return BadRequest(response);
            }

        [Route("UpdateCifStatus")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<UpdateCIFStatusDTO>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateCifStatus([FromBody] UpdateCIFStatusDTO Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:UpdateCIF"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime =DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetSignature")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<SignatureModel>", typeof(ActiveResponseSucces<SignatureModel>))]
        public async Task<IActionResult> GetSignature([FromBody] DeleteRequestForClientDetail Model)
        {
            ActiveResponseSucces<SignatureModel> response = new ActiveResponseSucces<SignatureModel>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:GetSignature"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<SignatureModel>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("AddShareHolder")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> AddShareHolder([FromBody] CIFShareHolderDetiilDTO Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:AddUpdateShareHolder"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("AddAuthorisedSignatories")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> AddAuthorisedSignatories([FromBody] CIFAuthorisedSignatoriesDTO Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:AddUpdateAuthorisedSignatories"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("AddBeneficialOwners")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> AddBeneficialOwners([FromBody] CIFBeneficialOwnersDTO Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:AddUpdateBeneficialOwners"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("AddBoardDirectors")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> AddBoardDirectors([FromBody] CIFBoardDirectorsDTO Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:AddUpdateBoardDirectors"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("UpdateShareHolder")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateShareHolder([FromBody] CIFShareHolderDetiilDTO Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:AddUpdateShareHolder"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("UpdateAuthorisedSignatories")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateAuthorisedSignatories([FromBody] CIFAuthorisedSignatoriesDTO Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:AddUpdateAuthorisedSignatories"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("UpdateBeneficialOwners")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateBeneficialOwners([FromBody] CIFBeneficialOwnersDTO Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse( Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:AddUpdateBeneficialOwners"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("UpdateBoardDirectors")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateBoardDirectors([FromBody] CIFBoardDirectorsDTO Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:AddUpdateBoardDirectors"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("GetRelationshipManagers")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<RelationshipOfficer>>", typeof(ActiveResponseSucces<List<RelationshipOfficer>>))]
        public async Task<IActionResult> GetRelationshipManagers([FromBody] RelationshipOfficerModel Model)
        {


            ActiveResponseSucces<List<RelationshipOfficer>> response = new ActiveResponseSucces<List<RelationshipOfficer>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:GetRelationshipManagers"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<RelationshipOfficer>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("CreateCorporateCIFV2")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.CreateCorporateCIFV2Response>", typeof(ActiveResponseSucces<DBHandler.Model.CreateCorporateCIFV2Response>))]
        public async Task<IActionResult> CreateCorporateCIFV2([FromBody] DBHandler.Model.CreateCorporateCIFV2Model Model)
        {
            ActiveResponseSucces<DBHandler.Model.CreateCorporateCIFV2Response> response = new ActiveResponseSucces<DBHandler.Model.CreateCorporateCIFV2Response>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:CreateCorporateCIFV2"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.CreateCorporateCIFV2Response>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("UpdateCorporateClientV2")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateCorporateClientV2([FromBody] DBHandler.Model.UpdateCorporateClientV2Model Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:UpdateCorporateClientV2"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }
            
        [Route("UpdateCorporateCifWithChildsV2")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.CreateCorporateCIFV2Response>", typeof(ActiveResponseSucces<DBHandler.Model.CreateCorporateCIFV2Response>))]
        public async Task<IActionResult> UpdateCorporateCifWithChildsV2([FromBody] DBHandler.Model.UpdateCorporateCifWithChildsV2Model Model)
        {
            ActiveResponseSucces<DBHandler.Model.CreateCorporateCIFV2Response> response = new ActiveResponseSucces<DBHandler.Model.CreateCorporateCIFV2Response>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:UpdateCorporateCifWithChildsV2"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.CreateCorporateCIFV2Response>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("AddEditOnlineUser")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.AddEditOnlineUserResponse>", typeof(ActiveResponseSucces<DBHandler.Model.AddEditOnlineUserResponse>))]
        public async Task<IActionResult> AddEditOnlineUser([FromBody] DBHandler.Model.AddEditOnlineUserModel Model)
        {
            ActiveResponseSucces<DBHandler.Model.AddEditOnlineUserResponse> response = new ActiveResponseSucces<DBHandler.Model.AddEditOnlineUserResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:AddEditOnlineUser"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.AddEditOnlineUserResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }
        [Route("ViewOnlineUser")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.ViewOnlineUserResponse>", typeof(ActiveResponseSucces<DBHandler.Model.ViewOnlineUserResponse>))]
        public async Task<IActionResult> ViewOnlineUser([FromBody] DBHandler.Model.ViewOnlineUserModel Model)
        {
            ActiveResponseSucces<DBHandler.Model.ViewOnlineUserResponse> response = new ActiveResponseSucces<DBHandler.Model.ViewOnlineUserResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:ViewOnlineUser"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.ViewOnlineUserResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("DeleteOnlineUser")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.DeleteOnlineUserResponse>", typeof(ActiveResponseSucces<DBHandler.Model.DeleteOnlineUserResponse>))]
        public async Task<IActionResult> DeleteOnlineUser([FromBody] DBHandler.Model.DeleteOnlineUserModel Model)
        {
            ActiveResponseSucces<DBHandler.Model.DeleteOnlineUserResponse> response = new ActiveResponseSucces<DBHandler.Model.DeleteOnlineUserResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:DeleteOnlineUser"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.DeleteOnlineUserResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetAllOnlineUser")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DBHandler.Model.GetAllOnlineUserResponse>>", typeof(ActiveResponseSucces<List<DBHandler.Model.GetAllOnlineUserResponse>>))]
        public async Task<IActionResult> GetAllOnlineUser([FromBody] DBHandler.Model.GetAllOnlineUserModel Model)
        {
            ActiveResponseSucces<List<DBHandler.Model.GetAllOnlineUserResponse>> response = new ActiveResponseSucces<List<DBHandler.Model.GetAllOnlineUserResponse>>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:GetAllOnlineUser"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<DBHandler.Model.GetAllOnlineUserResponse>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("InquireCorporateCifV2")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.InquireCorporateCifV2Response>", typeof(ActiveResponseSucces<DBHandler.Model.InquireCorporateCifV2Response>))]
        public async Task<IActionResult> InquireCorporateCifV2([FromBody] DBHandler.Model.InquireCorporateCifV2Model Model)
        {
            ActiveResponseSucces<DBHandler.Model.InquireCorporateCifV2Response> response = new ActiveResponseSucces<DBHandler.Model.InquireCorporateCifV2Response>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:InquireCorporateCifV2"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.InquireCorporateCifV2Response>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("AddEditCIF")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.AddEditCIFResponse>", typeof(ActiveResponseSucces<DBHandler.Model.AddEditCIFResponse>))]
        public async Task<IActionResult> AddEditCIF([FromBody] DBHandler.Model.AddEditCIFDTO Model)
        {
            ActiveResponseSucces<DBHandler.Model.AddEditCIFResponse> response = new ActiveResponseSucces<DBHandler.Model.AddEditCIFResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:AddEditCIF"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.AddEditCIFResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("AddEditAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.AddEditAccountResponse>", typeof(ActiveResponseSucces<DBHandler.Model.AddEditAccountResponse>))]
        public async Task<IActionResult> AddEditAccount([FromBody] DBHandler.Model.AddEditAccountDTO Model)
        {
            ActiveResponseSucces<DBHandler.Model.AddEditAccountResponse> response = new ActiveResponseSucces<DBHandler.Model.AddEditAccountResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:AddEditAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.AddEditAccountResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("InquireNostroCIF")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.InquireNostroCIFRespnse>", typeof(ActiveResponseSucces<DBHandler.Model.InquireNostroCIFRespnse>))]
        public async Task<IActionResult> InquireNostroCIF([FromBody] DBHandler.Model.InquireNostroCIFDTO Model)
        {
            ActiveResponseSucces<DBHandler.Model.InquireNostroCIFRespnse> response = new ActiveResponseSucces<DBHandler.Model.InquireNostroCIFRespnse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:InquireNostroCIF"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.InquireNostroCIFRespnse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }
             
        [Route("InquireNostroAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.InquireNostroAccountResponse>", typeof(ActiveResponseSucces<DBHandler.Model.InquireNostroAccountResponse>))]
        public async Task<IActionResult> InquireNostroAccount([FromBody] DBHandler.Model.InquireNostroAccountDTO Model)
        {
            ActiveResponseSucces<DBHandler.Model.InquireNostroAccountResponse> response = new ActiveResponseSucces<DBHandler.Model.InquireNostroAccountResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:InquireNostroAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.InquireNostroAccountResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetCorporateCIFDetailV2")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.CustomerCorporateViewV2>", typeof(ActiveResponseSucces<DBHandler.Model.CustomerCorporateViewV2>))]
        public async Task<IActionResult> GetCorporateCIFDetailV2([FromBody] RimInquiryV2 Model)
        {
            ActiveResponseSucces<DBHandler.Model.CustomerCorporateViewV2> response = new ActiveResponseSucces<DBHandler.Model.CustomerCorporateViewV2>();


            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:GetCorporateCIFDetailV2"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                string reaons = "";

                var temp = JObject.Parse(result.httpResult.httpResponse);
                if (temp["content"].Count() > 0)
                {
                    JObject channel = (JObject)temp["content"]["cust"];
                    reaons = channel.Property("reasonACorp")?.Value.ToString();
                    if (reaons != null)
                        channel.Property("reasonACorp").Remove();
                }
                string json = temp.ToString(); // backing result to json
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.CustomerCorporateViewV2>>(json);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Content.Cust.FATCANoReasonCorp = reaons;
                    if (response.Content.Cust.KYCReviewDateCorp != null)
                    {
                        DateTime d = Convert.ToDateTime(response.Content.Cust.KYCReviewDateCorp);
                        response.Content.Cust.KYCReviewDateCorp = new DateTime(d.Year, d.Month, d.Day, d.Hour, d.Minute, d.Second);
                    }

                    string specialChar = _config["GlobalSettings:SpecialChar"];
                    if (HttpContext.Request.Headers["ChannelId"] == _config["GlobalSettings:ECSCannel"])
                    {
                        response.Content.Cust.Name = response.Content.Cust.Name != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.Name, specialChar) : null;
                        response.Content.Cust.RManager = response.Content.Cust.RManager != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.RManager, specialChar) : null;
                        response.Content.Cust.EntityNameCorp = response.Content.Cust.EntityNameCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.EntityNameCorp, specialChar) : null;
                        response.Content.Cust.EntityTypeCorp = response.Content.Cust.EntityTypeCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.EntityTypeCorp, specialChar) : null;
                        response.Content.Cust.RegisteredAddressCorp = response.Content.Cust.RegisteredAddressCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.RegisteredAddressCorp, specialChar) : null;
                        response.Content.Cust.ContactName1Corp = response.Content.Cust.ContactName1Corp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.ContactName1Corp, specialChar) : null;
                        response.Content.Cust.ContactID1Corp = response.Content.Cust.ContactID1Corp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.ContactID1Corp, specialChar) : null;
                        response.Content.Cust.Address1Corp = response.Content.Cust.Address1Corp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.Address1Corp, specialChar) : null;
                        response.Content.Cust.ContactName2Corp = response.Content.Cust.ContactName2Corp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.ContactName2Corp, specialChar) : null;
                        response.Content.Cust.ContactID2Corp = response.Content.Cust.ContactID2Corp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.ContactID2Corp, specialChar) : null;
                        response.Content.Cust.Address2Corp = response.Content.Cust.Address2Corp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.Address2Corp, specialChar) : null;
                        response.Content.Cust.BusinessActivity = response.Content.Cust.BusinessActivity != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.BusinessActivity, specialChar) : null;
                        response.Content.Cust.TradeLicenseNumberCorp = response.Content.Cust.TradeLicenseNumberCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.TradeLicenseNumberCorp, specialChar) : null;
                        response.Content.Cust.CountryofIncorporationCorp = response.Content.Cust.CountryofIncorporationCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.CountryofIncorporationCorp, specialChar) : null;
                        response.Content.Cust.TradeLicenseIssuanceAuthorityCorp = response.Content.Cust.TradeLicenseIssuanceAuthorityCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.TradeLicenseIssuanceAuthorityCorp, specialChar) : null;
                        response.Content.Cust.SponsorNameCorp = response.Content.Cust.SponsorNameCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.SponsorNameCorp, specialChar) : null;
                        response.Content.Cust.NFFECorp = response.Content.Cust.NFFECorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.NFFECorp, specialChar) : null;
                        response.Content.Cust.RegAddCityCorp = response.Content.Cust.RegAddCityCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.RegAddCityCorp, specialChar) : null;
                        response.Content.Cust.RegAddCountryCorp = response.Content.Cust.RegAddCountryCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.RegAddCountryCorp, specialChar) : null;
                        response.Content.Cust.RegAddStateCorp = response.Content.Cust.RegAddStateCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.RegAddStateCorp, specialChar) : null;
                        response.Content.Cust.Add1CountryCorp = response.Content.Cust.Add1CountryCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.Add1CountryCorp, specialChar) : null;
                        response.Content.Cust.Add1StateCorp = response.Content.Cust.Add1StateCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.Add1StateCorp, specialChar) : null;
                        response.Content.Cust.Add1CityCorp = response.Content.Cust.Add1CityCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.Add1CityCorp, specialChar) : null;
                        response.Content.Cust.Add2CountryCorp = response.Content.Cust.Add2CountryCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.Add2CountryCorp, specialChar) : null;
                        response.Content.Cust.Add2StateCorp = response.Content.Cust.Add2StateCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.Add2StateCorp, specialChar) : null;
                        response.Content.Cust.Add2CityCorp = response.Content.Cust.Add2CityCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.Add2CityCorp, specialChar) : null;
                        response.Content.Cust.TradeLicenseIssuanceAuthorityCorp = response.Content.Cust.TradeLicenseIssuanceAuthorityCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.TradeLicenseIssuanceAuthorityCorp, specialChar) : null;
                        response.Content.Cust.TradeLicenseIssuanceAuthorityCorp = response.Content.Cust.TradeLicenseIssuanceAuthorityCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.TradeLicenseIssuanceAuthorityCorp, specialChar) : null;
                        response.Content.Cust.TradeLicenseIssuanceAuthorityCorp = response.Content.Cust.TradeLicenseIssuanceAuthorityCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.TradeLicenseIssuanceAuthorityCorp, specialChar) : null;
                        response.Content.Cust.TradeLicenseIssuanceAuthorityCorp = response.Content.Cust.TradeLicenseIssuanceAuthorityCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.TradeLicenseIssuanceAuthorityCorp, specialChar) : null;
                        response.Content.Cust.TradeLicenseIssuanceAuthorityCorp = response.Content.Cust.TradeLicenseIssuanceAuthorityCorp != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Cust.TradeLicenseIssuanceAuthorityCorp, specialChar) : null;

                        for (int i = 0; i < response.Content.ShareHolders.Count; i++)
                        {
                            response.Content.ShareHolders[i].ShareHolderName = response.Content.ShareHolders[i].ShareHolderName != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ShareHolders[i].ShareHolderName, specialChar) : null;
                            response.Content.ShareHolders[i].ShareHolderAddress = response.Content.ShareHolders[i].ShareHolderAddress != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ShareHolders[i].ShareHolderAddress, specialChar) : null;
                            response.Content.ShareHolders[i].CountryofIncorporation = response.Content.ShareHolders[i].CountryofIncorporation != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ShareHolders[i].CountryofIncorporation, specialChar) : null;
                            response.Content.ShareHolders[i].AuthorisedSignatory = response.Content.ShareHolders[i].AuthorisedSignatory != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ShareHolders[i].AuthorisedSignatory, specialChar) : null;
                            response.Content.ShareHolders[i].BeneficialOwner = response.Content.ShareHolders[i].BeneficialOwner != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ShareHolders[i].BeneficialOwner, specialChar) : null;
                            response.Content.ShareHolders[i].CountryofResidence = response.Content.ShareHolders[i].CountryofResidence != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ShareHolders[i].CountryofResidence, specialChar) : null;
                            response.Content.ShareHolders[i].SHCity = response.Content.ShareHolders[i].SHCity != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ShareHolders[i].SHCity, specialChar) : null;
                            response.Content.ShareHolders[i].SHState = response.Content.ShareHolders[i].SHState != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ShareHolders[i].SHState, specialChar) : null;
                            response.Content.ShareHolders[i].SHCountry = response.Content.ShareHolders[i].SHCountry != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ShareHolders[i].SHCountry, specialChar) : null;
                        }

                        for (int i = 0; i < response.Content.Authignatories.Count; i++)
                        {
                            response.Content.Authignatories[i].AuthorisedsignatoriesName = response.Content.Authignatories[i].AuthorisedsignatoriesName != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Authignatories[i].AuthorisedsignatoriesName, specialChar) : null;
                            response.Content.Authignatories[i].CorrespondenceAddress = response.Content.Authignatories[i].CorrespondenceAddress != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Authignatories[i].CorrespondenceAddress, specialChar) : null;
                            response.Content.Authignatories[i].Remarks = response.Content.Authignatories[i].Remarks != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Authignatories[i].Remarks, specialChar) : null;
                            response.Content.Authignatories[i].ASCity = response.Content.Authignatories[i].ASCity != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Authignatories[i].ASCity, specialChar) : null;
                            response.Content.Authignatories[i].ASState = response.Content.Authignatories[i].ASState != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Authignatories[i].ASState, specialChar) : null;
                            response.Content.Authignatories[i].ASCountry = response.Content.Authignatories[i].ASCountry != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Authignatories[i].ASCountry, specialChar) : null;
                        }

                        for (int i = 0; i < response.Content.BoardDirectors.Count; i++)
                        {
                            response.Content.BoardDirectors[i].BoardDirectorName = response.Content.BoardDirectors[i].BoardDirectorName != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BoardDirectors[i].BoardDirectorName, specialChar) : null;
                            response.Content.BoardDirectors[i].Designation = response.Content.BoardDirectors[i].Designation != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BoardDirectors[i].Designation, specialChar) : null;
                            response.Content.BoardDirectors[i].BoardDirectorAddress = response.Content.BoardDirectors[i].BoardDirectorAddress != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BoardDirectors[i].BoardDirectorAddress, specialChar) : null;
                            response.Content.BoardDirectors[i].BDCity = response.Content.BoardDirectors[i].BDCity != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BoardDirectors[i].BDCity, specialChar) : null;
                            response.Content.BoardDirectors[i].BDState = response.Content.BoardDirectors[i].BDState != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BoardDirectors[i].BDState, specialChar) : null;
                            response.Content.BoardDirectors[i].BDCountry = response.Content.BoardDirectors[i].BDCountry != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BoardDirectors[i].BDCountry, specialChar) : null;
                        }

                        for (int i = 0; i < response.Content.BeneficialOwners.Count; i++)
                        {
                            response.Content.BeneficialOwners[i].BeneficialName = response.Content.BeneficialOwners[i].BeneficialName != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BeneficialOwners[i].BeneficialName, specialChar) : null;
                            response.Content.BeneficialOwners[i].BeneficialAddress = response.Content.BeneficialOwners[i].BeneficialAddress != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BeneficialOwners[i].BeneficialAddress, specialChar) : null;
                            response.Content.BeneficialOwners[i].BOCity = response.Content.BeneficialOwners[i].BOCity != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BeneficialOwners[i].BOCity, specialChar) : null;
                            response.Content.BeneficialOwners[i].BOState = response.Content.BeneficialOwners[i].BOState != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BeneficialOwners[i].BOState, specialChar) : null;
                            response.Content.BeneficialOwners[i].BOCountry = response.Content.BeneficialOwners[i].BOCountry != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.BeneficialOwners[i].BOCountry, specialChar) : null;
                        }
                    }
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("InquireFIKycLiteCustomerV2")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.InquireFIKycCustomerCorporateViewV2>", typeof(ActiveResponseSucces<DBHandler.Model.InquireFIKycCustomerCorporateViewV2>))]
        public async Task<IActionResult> InquireFIKycLiteCustomerV2([FromBody] InquireFIKycRimInquiryV2 Model)

        {
            ActiveResponseSucces<DBHandler.Model.InquireFIKycCustomerCorporateViewV2> response = new ActiveResponseSucces<DBHandler.Model.InquireFIKycCustomerCorporateViewV2>();


            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:InquireFIKycLiteCustomerV2"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                string reaons = "";

                var temp = JObject.Parse(result.httpResult.httpResponse);
                if (temp["content"].Count() > 0)
                {
                    JObject channel = (JObject)temp["content"]["cust"];
                    reaons = channel.Property("reasonACorp").Value.ToString();
                    channel.Property("reasonACorp").Remove();
                }
                string json = temp.ToString(); // backing result to json
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.InquireFIKycCustomerCorporateViewV2>>(json);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Content.Cust.FATCANoReasonCorp = reaons;
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("AddUpdateShareHolderV2")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> AddUpdateShareHolderV2([FromBody] CIFShareHolderDetiilDTOV2 Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:AddUpdateShareHolderV2"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("GetCustomerIDExpiry")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<GetCustomerIDExpiryResponse>>", typeof(ActiveResponseSucces<List<GetCustomerIDExpiryResponse>>))]
        public async Task<IActionResult> GetCustomerIDExpiry([FromBody] GetCustomerIDExpiryDto Model)
        {
            ActiveResponseSucces<List<GetCustomerIDExpiryResponse>> response = new ActiveResponseSucces<List<GetCustomerIDExpiryResponse>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:GetCustomerIDExpiry"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<GetCustomerIDExpiryResponse>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("AddSweep")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<AddSweepResponse>", typeof(ActiveResponseSucces<AddSweepResponse>))]
        public async Task<IActionResult> AddSweep([FromBody] AddSweepDTO Model)
        {
            ActiveResponseSucces<AddSweepResponse> response = new ActiveResponseSucces<AddSweepResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:AddSweep"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<AddSweepResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("GetAllSweeps")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<GetAllSweepsResp>", typeof(ActiveResponseSucces<GetAllSweepsResp>))]
        public async Task<IActionResult> GetAllSweeps([FromBody] GetAllSweepDTO Model)
        {
            ActiveResponseSucces<GetAllSweepsResp> response = new ActiveResponseSucces<GetAllSweepsResp>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:GetAllSweeps"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<GetAllSweepsResp>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("EditSweep")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> EditSweep([FromBody] EditSweepDTO Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:EditSweep"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("CancelSweep")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> CancelSweep([FromBody] CancelSweepDTO Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:CancelSweep"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }



    }
}




