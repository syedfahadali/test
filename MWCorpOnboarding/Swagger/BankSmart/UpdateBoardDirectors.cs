﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWCorpOnboarding.Swagger.BankSmart.UpdateBoardDirectors
{
    
    //REQUEST
    public class UpdateBoardDirectorsRequest : IExamplesProvider<CIFBoardDirectorsDTO>
    {
        public CIFBoardDirectorsDTO GetExamples()
        {
            return new CIFBoardDirectorsDTO()
            {
                ClientID = "000000139",
                BoardDirectors=new BoardDirectors() 
                {
                    BoardDirector =56,
                    BoardDirectorName = "fahad",
                    Designation ="president",
                    BoardDirectorAddress ="4/c",
                    BoardDirectorIDExpirydate = DateTime.Now,
                    ResidentNonResident ="N",
                    CountryofResidence ="ARE",
                    BoardDirectorIDtype ="DLL",
                    BoardDirectorIDNo ="123123",
                    Blacklist ="Y",
                    PEP ="Y",
                    FATCARelevant ="Y",
                    CRSRelevant ="Y",
                    AreyouaUSCitizen ="Y",
                    AreyouaUSTaxResident ="Y",
                    WereyoubornintheUS ="Y",
                    USAORUAE ="Y",
                    TINAvailable ="",
                    TinNo1 ="",
                    TaxCountry1 =null,
                    TinNo2 ="",
                    TaxCountry2 = null,
                    TinNo3 ="",
                    TaxCountry3 = null,
                    TinNo4 ="",
                    TaxCountry4 = null,
                    TinNo5 ="",
                    TaxCountry5 = null,
                    FATCANoReason ="Reason A",
                    ReasonB ="",
                    BDCity ="00012",
                    BDState ="005",
                    BDCountry ="ARE",
                    IDGender ="M",
                    DateOfBirth =DateTime.Now,
                    IDNationality ="ARE",


                }
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}

}
