﻿using DBHandler.Helper;
using DBHandler.Model;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWCorpOnboarding.Swagger.BankSmart.UpdateShareHolder
{
    //REQUEST
    public class UpdateShareHolderRequest : IExamplesProvider<CIFShareHolderDetiilDTOV2>
    {
        public CIFShareHolderDetiilDTOV2 GetExamples()
        {
            return new CIFShareHolderDetiilDTOV2()
            {
                ClientID = "000222811",
                ShareHolder = new DBHandler.Model.AddUpdateShareHolderV2()
                {
                    ShareHolderID = 11,
                    ShareHolderName = "salman",
                    ShareHolderAddress = "5c",
                    ShareholderType = "COM",
                    ShareholdersIDtype = "DLL",
                    ShareholdersIDNo = "11",
                    ShareholdersIDExpirydate = DateTime.Now,
                    CountryofIncorporation = "",
                    BoardMember = "Y",
                    AuthorisedSignatory = "Y",
                    BeneficialOwner = "Y",
                    ResidentNonResident = "N",
                    CountryofResidence = "",
                    OwnershipPercentage = 123,
                    Blacklist = "Y",
                    PEP = "Y",
                    FATCARelevant = "Y",
                    CRSRelevant = "Y",
                    USCitizen = "Y",
                    AreyouaUSTaxResident = "Y",
                    WereyoubornintheUS = "Y",
                    USAEntity = "",
                    FFI = "",
                    FFICategory = "",
                    GIINNo = "",
                    GIINNA = "",
                    SponsorName = "",
                    SponsorGIIN = "",
                    NFFE = "",
                    StockExchange = "",
                    TradingSymbol = "",
                    USAORUAE = "Y",
                    TINAvailable = "",
                    TinNo1 = "",
                    TaxCountry1 = "",
                    TinNo2 = "",
                    TaxCountry2 = "",
                    TinNo3 = "",
                    TaxCountry3 = "",
                    TinNo4 = "",
                    TaxCountry4 = "",
                    TinNo5 = "",
                    TaxCountry5 = "",
                    FATCANoReason = "Reason A",
                    ReasonB = "",
                    SHIDIssuanceCountry = "ARE",
                    IDGender = "M",
                    DateOfBirth = DateTime.Now,
                    IDNationality = "",
                    SHCity = "",
                    SHState = "006",
                    SHCountry = "ARE",
                    JurEmiratesId = null,
                    JurTypeId = null,
                    JurAuthorityId = null,
                    POBox = null,
                    JurOther = null,
                    ParentShareHolderId = null,
                    EntityType = null,
                    ResidenceShareHolderCountryID = "ARE",
                    PlaceofBirth = "khi",
                    GroupCodeType = null
                    
                }
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}

}
