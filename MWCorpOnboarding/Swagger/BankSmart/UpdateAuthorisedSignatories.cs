﻿using DBHandler.Helper;
using DBHandler.Model;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWCorpOnboarding.Swagger.BankSmart.UpdateAuthorisedSignatories
{

    //REQUEST
    public class UpdateAuthorisedSignatoriesRequest : IExamplesProvider<CIFAuthorisedSignatoriesDTO>
    {
        public CIFAuthorisedSignatoriesDTO GetExamples()
        {
            return new CIFAuthorisedSignatoriesDTO()
            {
                ClientID = "",

                AuthorisedSignatories = new AuthorisedSignatories()
                {
                    
                    AuthorisedSignatoriesID = 0,
                    AuthorisedsignatoriesName = "string",
                    PhoneNumber = "string",
                    EmailID = "string",
                    CorrespondenceAddress = "string",
                    AuthorisedsignatoryIDtype1 = "string",
                    AuthorisedsignatoryIDNumber1 = "string",
                    AuthorisedsignatoryIDExpirydate1 = DateTime.Now,
                    AuthorisedsignatoryIDIssueCountry1 = "string",
                    AuthorisedsignatoryIDtype2 = "string",
                    AuthorisedsignatoryIDNumber2 = "string",
                    AuthorisedsignatoryIDExpirydate2 = DateTime.Now,
                    AuthorisedsignatoryIDIssueCountry2 = "string",
                    AuthorisedsignatoryVisaIssuedby = "string",
                    AuthorisedsignatoryVisaNumber = "string",
                    AuthorisedsignatoryVisaExpiry = DateTime.Now,
                    Remarks = "string",
                    Blacklist = "string",
                    PEP = "string",
                    FATCARelevant = "string",
                    CRSRelevant = "string",
                    AreyouaUSCitizen = "string",
                    AreyouaUSTaxResident = "string",
                    WereyoubornintheUS = "string",
                    USAORUAE = "string",
                    TINAvailable = "string",
                    TinNo1 = "string",
                    TaxCountry1 = "string",
                    TinNo2 = "string",
                    TaxCountry2 = "string",
                    TinNo3 = "string",
                    TaxCountry3 = "string",
                    TinNo4 = "string",
                    TaxCountry4 = "string",
                    TinNo5 = "string",
                    TaxCountry5 = "string",
                    FATCANoReason = "string",
                    ReasonB = "string",
                    IDGender = "string",
                    DateOfBirth = DateTime.Now,
                    IDNationality = "string",
                    ASCity = "string",
                    ASState = "string",
                    ASCountry = "string",
                    GroupID = "string"

                }
            };
        }
    }

    //RESPONSE
    //public class BalanceInquiryResponse : IExamplesProvider<ActiveResponseSucces<Balance>>
    //{
    //    public ActiveResponseSucces<Balance> GetExamples()
    //    {
    //        return new ActiveResponseSucces<Balance>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new Balance()
    //            {
    //                AccountId = "0315100003348801"

    //            }

    //        };
    //    }
    //}
}
