﻿using DBHandler.Helper;
using DBHandler.Model;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWCorpOnboarding.Swagger.BankSmart.GetRelationshipManagers
{

    //REQUEST
    public class GetRelationshipManagersRequest : IExamplesProvider<RelationshipOfficerModel>
    {
        public RelationshipOfficerModel GetExamples()
        {
            return new RelationshipOfficerModel()
            {
                
            };
        }
    }

    //RESPONSE
    //public class BalanceInquiryResponse : IExamplesProvider<ActiveResponseSucces<Balance>>
    //{
    //    public ActiveResponseSucces<Balance> GetExamples()
    //    {
    //        return new ActiveResponseSucces<Balance>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new Balance()
    //            {
    //                AccountId = "0315100003348801"

    //            }

    //        };
    //    }
    //}
}
