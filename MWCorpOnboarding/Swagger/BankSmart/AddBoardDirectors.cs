﻿using DBHandler.Helper;
using DBHandler.Model;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWCorpOnboarding.Swagger.BankSmart.AddBoardDirectors
{

    //REQUEST
    public class AddBoardDirectorsRequest : IExamplesProvider<CIFBoardDirectorsDTO>
    {
        public CIFBoardDirectorsDTO GetExamples()
        {
            return new CIFBoardDirectorsDTO()
            {
                ClientID = "",
                BoardDirectors = new BoardDirectors()
                {
                    BoardDirector = 0,
                    BoardDirectorName = "string",
                    Designation = "string",
                    BoardDirectorAddress = "string",
                    BoardDirectorIDExpirydate = DateTime.Now,
                    ResidentNonResident = "string",
                    CountryofResidence = "string",
                    BoardDirectorIDtype = "string",
                    BoardDirectorIDNo = "string",
                    Blacklist = "string",
                    PEP = "string",
                    FATCARelevant = "string",
                    CRSRelevant = "string",
                    AreyouaUSCitizen = "string",
                    AreyouaUSTaxResident = "string",
                    WereyoubornintheUS = "string",
                    USAORUAE = "string",
                    TINAvailable = "string",
                    TinNo1 = "string",
                    TaxCountry1 = "string",
                    TinNo2 = "string",
                    TaxCountry2 = "string",
                    TinNo3 = "string",
                    TaxCountry3 = "string",
                    TinNo4 = "string",
                    TaxCountry4 = "string",
                    TinNo5 = "string",
                    TaxCountry5 = "string",
                    FATCANoReason = "string",
                    ReasonB = "string",
                    IDGender = "string",
                    DateOfBirth = DateTime.Now,
                    IDNationality = "string",
                    BDCity = "string",
                    BDState = "string",
                    BDCountry = "string"
                }
            };
        }
    }

    //RESPONSE
    //public class BalanceInquiryResponse : IExamplesProvider<ActiveResponseSucces<Balance>>
    //{
    //    public ActiveResponseSucces<Balance> GetExamples()
    //    {
    //        return new ActiveResponseSucces<Balance>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new Balance()
    //            {
    //                AccountId = "0315100003348801"

    //            }

    //        };
    //    }
    //}
}
