﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWCorpOnboarding.Swagger.BankSmart.UpdateCorporateAccount
{
    //REQUEST
    public class UpdateCorporateAccountRequest : IExamplesProvider<UpdateCorporateAccountModel>
    {
        public UpdateCorporateAccountModel GetExamples()
        {
            return new UpdateCorporateAccountModel()
            {
                Acc = new UpdateAccountCorporate()
                {
                    CreateBy = "20",
                    OurBranchID = "03",
                    AccountID = "0310100000013302",
                    ClientID = "000000133",
                    Address = "address",
                    CountryID = "ARE",
                    StateID = "002",
                    CityID = null,
                    StatementFrequency = "W",
                    HoldMail = 0,
                    ZakatExemption = 0,
                    ContactPersonName = null,
                    Designation = null,
                    Phone1 = null,
                    Phone2 = null,
                    MobileNo = null,
                    Fax = null,
                    EmailID = null,
                    IntroducerAccountNo = null,
                    IntroducedBy = null,
                    IntroducerAddress = null,
                    IntroducerCityID = null,
                    IntroducerStateID = null,
                    IntroducerCountryID = null,
                    ModeOfOperation = "Company Mandate",
                    Reminder = null,
                    Notes = null,
                    NatureID = "0102",
                    RelationshipCode = "CAD",
                    AllowCreditTransaction = 0,
                    AllowDebitTransaction = 0,
                    NotServiceCharges = 0,
                    NotStopPaymentCharges = 0,
                    NotChequeBookCharges = 0,
                    TurnOver = "Below 1M",
                    NoOfDrTrx = null,
                    NoOfCrTrx = null,
                    DrThresholdLimit = null,
                    CrThresholdLimit = null,
                    ProductCash = 0,
                    ProductClearing = 0,
                    ProductCollection = 0,
                    ProductRemittance = 0,
                    ProductCross = 0,
                    ProductOthers = 0,
                    ProductOthersDesc = null
                }
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
