﻿using DBHandler.Helper;
using DBHandler.Model;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWCorpOnboarding.Swagger.BankSmart.CreateCorporateCIFV2
{
    //REQUEST
    public class CreateCorporateCIFV2Request : IExamplesProvider<DBHandler.Model.CreateCorporateCIFV2Model>
    {
        public DBHandler.Model.CreateCorporateCIFV2Model GetExamples()
        {
            return new DBHandler.Model.CreateCorporateCIFV2Model()
            {
                
                Cust = new CreateCorporateCIFCustomerV2()
                {
                    
                    Name = "dmeo",
                    CategoryId = "Corporate",
                    GroupCode = "",
                    RManager = "",
                    RiskProfile = "Low",
                    EntityNameCorp = "FUTURE EYE TECHNOLOGY L.L.C",
                    EntityTypeCorp = "LLC",
                    YearofestablishmentCorp = DateTime.Now,
                    RegisteredAddressCorp = "ABC",
                    ContactName1Corp = "Michel",
                    ContactID1Corp = "",
                    IDTypeCorp = null,
                    Address1Corp = "",
                    PhoneNumberCorp = "0852369741",
                    AlternatePhonenumberCorp = "",
                    EmailIDCorp = "Contact@Fi.Com",
                    ContactName2Corp = "",
                    ContactID2Corp = "",
                    IDType2 = null,
                    Address2Corp = "",
                    PhoneNumber2Corp = 0852369741,
                    AlternatePhonenumber2Corp = 123456789,
                    EmailID2Corp = "",
                    BusinessActivity = "6499",
                    SICCodesCorp = "6499",
                    IndustryCorp = "Fia",
                    PresenceinCISCountryCorp = "",
                    DealingwithCISCountryCorp = "",
                    TradeLicenseNumberCorp = "23045665",
                    CountryofIncorporationCorp = "And",
                    TradeLicenseIssuedateCorp = DateTime.Now,
                    TradeLicenseExpirydateCorp = DateTime.Now,
                    TradeLicenseIssuanceAuthorityCorp = "Uae",
                    TRNNoCorp = "",
                    PEPCorp = "N",
                    BlackListCorp = "N",
                    KYCReviewDateCorp = DateTime.Now,
                    FATCARelevantCorp = "N",
                    CRSRelevantCorp = "N",
                    USAEntityCorp = "",
                    FFICorp = "N",
                    FFICategoryCorp = null,
                    GIINNoCorp = "",
                    GIINNACorp = null,
                    SponsorNameCorp = "",
                    SponsorGIIN = "",
                    NFFECorp = null,
                    StockExchangeCorp = "",
                    TradingSymbolCorp = "",
                    USAORUAECorp = "N",
                    TINAvailableCorp = "",
                    TinNo1Corp = "",
                    TaxCountry1Corp = "",
                    TinNo2Corp = "",
                    TaxCountry2Corp = "",
                    TinNo3Corp = "",
                    TaxCountry3Corp = "",
                    TinNo4Corp = "",
                    TaxCountry4Corp = "",
                    TinNo5Corp = "",
                    TaxCountry5Corp = "",
                    FATCANoReasonCorp = "",
                    ReasonBCorp = "",
                    CompanyWebsite = "",
                    CompCashIntensive = "",
                    PubLimCompCorp = "N",
                    RegAddCountryCorp = "And",
                    RegAddStateCorp = "002",
                    RegAddCityCorp = "00005",
                    Add1CountryCorp = null,
                    Add1CityCorp = null,
                    Add1StateCorp = null,
                    Add2CityCorp = null,
                    Add2CountryCorp = null,
                    Add2StateCorp = null,
                    ClientID = "",
                    ReasonACorp = null,
                    VATRegistered = null,
                    CreateBy = "Admin",
                    JurAuthorityID = "001",
                    JurEmiratesID = "008",
                    JurTypeID = "001",
                    POBox = "2013526",
                    JurOther = "And",
                    GroupID = null,
                    ParentClientID = null,
                    LicenseType = "Test",
                    CountryID = "",
                    MOAMOU = DateTime.Now,
                    SigningAuthority = "Test",
                    POAResidenceCountry = "Test",
                    POAAddress = "Test",
                    POACountry = "Test",
                    POAState = "Test",
                    POACity = "Test",
                    AuditedFinancials = "Test",
                    Assets = 0,
                    Revenue = 0,
                    Profits = 0,
                    BusinessTurnOver = 0,
                    CompanyCapital = 0,
                    CompanyAddress = "Test",
                    ExpectedCreditTurnOver = 0,
                    ExpectedMaxCreditTurnOver = 0,
                    ExpectedDebitTurnOver = 0,
                    ExpectedMaxDebitTurnOver = 0,
                    CompanyCountry = "Test",
                    ExpectedCashVolume = 0,
                    ExpectedCashValue = 0,
                    ChequesVolume = 0,
                    ChequesValue = 0,
                    SalaryPaymentVolume = 0,
                    SalaryPaymentValue = 0,
                    IntraBankRemittancesVolume = 0,
                    IntraBankRemittancesValue = 0,
                    InternationalRemittancesVolume = 0,
                    InternationalRemittancesValue = 0,
                    DomesticRemittanceValues = 0,
                    DomesticRemittanceVolume = 0,
                    ExpectedCashDebitValue = 0,
                    ExpectedCashDebitVolume = 0,
                    ChequeDebitVolume = 0,
                    ChequeDebitValue = 0,
                    SalaryDVolume = 0,
                    SalaryDValue = 0,
                    IntraDVolume = 0,
                    IntraDValue = 0,
                    InternationalDVolume = 0,
                    InternationalDValue = 0,
                    DomesticDRemittanceValues = 0,
                    DomesticDRemittanceVolume = 0,
                    CreditTradeFinanceVolume = 0,
                    CreditTradeFinanceValue = 0,
                    TradeFinanceVolumeD = 0,
                    TradeFinanceValueD = 0,
                    NoOfStaff = 0,
                    CompanyActivites = "Test",
                    BusinessNatureRelation = "Test",
                    RelationalCompanyStructure = "Test",
                    CompanyGeoGraphicCourage = "Test",
                    UBOBackground = "Test",
                    ProductAndServicesOffered = "Test",
                    IsDisclosureActivity = 1,
                    DisclosureDetails = "Test",
                    IsSanctionCountry = 3,
                    SanctionRemarks = "Test",
                    IsOfficeInSactionCountry = 20,
                    SanctionCountries = "Test",
                    BusinessNatureSanctionCountry = "Test",
                    IsProductOrigninatedCountry = 5,
                    OriginatedProductCountry = "Test",
                    OriginatedCountryRemarks = "Test",
                    IsExportGoods = 50,
                    ExportGoodsCountry = "Test",
                    ExportGoodsRemarks = "Test",
                    isRecievedTransFromSanctionCountry = 10,
                    TransactionSanctionCountry = "Test",
                    TransactionSanctionRemarks = "Test",
                    CorporateStructureChart = "Test",
                    VisitConducted = "Test",
                    SanctionAssestment = "Test",
                    PepAssessment = "Test",
                    AdverseMedia = "Test",
                    AccountOpeningPurpose = "Test",
                    IsVip = 0,
                    POAName = "Test",
                    POANationality = "Test",
                    POADateofBirth = "",
                    POAPlaceOfBirth = "Test",
                    POAGender = "M",
                    selectionOfPayments = "Test",
                    DepositValue = 0,
                    PaymentValue = 0,
                    LoansValue = 0,
                    PlaceofIncorporation = "Test",
                    Designation1Corp = "Test",
                    Department1Corp = "Test",
                    IDExpiryDateCorp = "Test",
                    Designation2Corp = "Test",
                    Department2Corp = "Test",
                    IDExpiryDate2Corp = "Test",
                    DealingwithCISTextarea = "Test",
                    IsOfficeInSactionCountrylist = "4",
                    IsProductOrigninatedCountrylist = "12",
                    ExportGoodsCountrylist = "Test",
                    TransactionSanctionCountrylist = "Test",
                    Status = ""

                },

                SicCodeCorp = new List<SicCodeCorpModelV2>()
                {
                    new SicCodeCorpModelV2()
                    {
                        SicCodeCorp="9900",
                        Type="P"
                    }


                },
                ShareHolders = new List<ShareHolderDTOV2>()
                {

                    new ShareHolderDTOV2()
                    {
                        ShareHolderID = null,
                        ShareHolderName = "Michel",
                        ShareHolderAddress = "078 Avenue",
                        ShareholderType = "Com",
                        ShareholdersIDtype = "Pas",
                        ShareholdersIDNo = "987645312",
                        ShareholdersIDExpirydate = DateTime.Now,
                        CountryofIncorporation = null,
                        BoardMember = "N",
                        AuthorisedSignatory = "N",
                        BeneficialOwner = "N",
                        ResidentNonResident = "N",
                        CountryofResidence = "Ago",
                        OwnershipPercentage = Convert.ToDecimal(100.00),
                        Blacklist = "N",
                        PEP = "N",
                        FATCARelevant = "N",
                        CRSRelevant = "N",
                        USCitizen= "N",
                        AreyouaUSTaxResident = "N",
                        WereyoubornintheUS = "N",
                        USAEntity = null,
                        FFI = "",
                        FFICategory = "",
                        GIINNo = "",
                        GIINNA = "",
                        SponsorName = "",
                        SponsorGIIN = "",
                        NFFE = "",
                        StockExchange = "",
                        TradingSymbol = "",
                        USAORUAE = "N",
                        TINAvailable = "",
                        TinNo1 = "",
                        TaxCountry1 = "",
                        TinNo2 = "",
                        TaxCountry2 = "",
                        TinNo3 = "",
                        TaxCountry3 = "",
                        TinNo4 = "",
                        TaxCountry4 = "",
                        TinNo5 = "",
                        TaxCountry5 = "",
                        FATCANoReason = "",
                        ReasonB = "",
                        SHCity = "00003",
                        SHState = "003",
                        SHCountry = "Ago",
                        SHIDIssuanceCountry = "Ago",
                        IDGender = "M",
                        DateOfBirth = DateTime.Now,
                        IDNationality = "Ago",
                        JurAuthorityId = "001",
                        JurEmiratesId = "001",
                        JurTypeId = "001",
                        POBox = "1234",
                        JurOther = null,
                        ParentShareHolderId = null,
                        EntityType = "Test",
                        ResidenceShareHolderCountryID = "Are",
                        PlaceofBirth = "Khi"

                    }


                },

                Authignatories = new List<AuthorisedSignatoriesDTOModelV2>()
                {
                    new AuthorisedSignatoriesDTOModelV2()
                    {
                        AuthorisedSignatoriesID = null,
                        AuthorisedsignatoriesName = "ALI FARAZ",
                        PhoneNumber = "132-3355-544466",
                        EmailID = "User@Company1.com",
                        CorrespondenceAddress = "ABC",
                        AuthorisedsignatoryIDtype1 = "PAS",
                        AuthorisedsignatoryIDNumber1 = "DY9895912",
                        AuthorisedsignatoryIDExpirydate1 = DateTime.Now,
                        AuthorisedsignatoryIDIssueCountry1 = "PAK",
                        AuthorisedsignatoryIDtype2 = "EID",
                        AuthorisedsignatoryIDNumber2 = "784197747972764",
                        AuthorisedsignatoryIDExpirydate2 = DateTime.Now,
                        AuthorisedsignatoryIDIssueCountry2 = "ARE",
                        AuthorisedsignatoryVisaIssuedby = "UAE",
                        AuthorisedsignatoryVisaNumber = "115751051",
                        AuthorisedsignatoryVisaExpiry = DateTime.Now,
                        Remarks = null,
                        Blacklist= null,
                        PEP= "N",
                        FATCARelevant= "N",
                        CRSRelevant= "N",
                        AreyouaUSCitizen= "N",
                        AreyouaUSTaxResident= "N",
                        WereyoubornintheUS= "N",
                        USAORUAE= "N",
                        TINAvailable= "N",
                        TinNo1= null,
                        TaxCountry1= null,
                        TinNo2= null,
                        TaxCountry2= null,
                        TinNo3= null,
                        TaxCountry3= null,
                        TinNo4= null,
                        TaxCountry4= null,
                        TinNo5= null,
                        TaxCountry5= null,
                        FATCANoReason= "Reason A",
                        ReasonB= null,
                        IDGender= "M",
                        DateOfBirth= DateTime.Now,
                        IDNationality  = "PAK",
                        ASCity  = "00001",
                        ASState  = "001",
                        ASCountry  = "ARE",
                        ResidentNonResident  = null,
                        GroupID  = null,
                        Designation  = null
                    }
                },
                BeneficialOwners = new List<BenificialOwnerDTOV2>()
                {
                    new BenificialOwnerDTOV2()
                    {
                        BeneficialOwnersID= null,
                        BeneficialName= "ALI FARAZ",
                        BeneficialAddress= "ABC",
                        BeneficialIDNo= "DY9895912",
                        BeneficialIDExpirydate= DateTime.Now,
                        ResidentNonResident= "N",
                        CountryOfResidence= "ARE",
                        BeneficialIDtype= "EID",
                        Blacklist= null,
                        PEP= "N",
                        FATCARelevant= "N",
                        CRSRelevant= "N",
                        AreyouaUSCitizen= "N",
                        AreyouaUSTaxResident= "N",
                        WereyoubornintheUS= "N",
                        USAORUAE= "N",
                        TINAvailable= "N",
                        TinNo1= null,
                        TaxCountry1= null,
                        TinNo2= null,
                        TaxCountry2= null,
                        TinNo3= null,
                        TaxCountry3= null,
                        TinNo4= null,
                        TaxCountry4= null,
                        TinNo5= null,
                        TaxCountry5= null,
                        FATCANoReason= "Reason A",
                        ReasonB= null,
                        IDGender= "M",
                        DateOfBirth= DateTime.Now,
                        IDNationality= "ARE",
                        BOCity= null,
                        BOState= "001",
                        BOCountry= "ARE",
                        PlaceOfBirth= null,
                        MobileNo= null,
                        Email= null,
                        IDIssuanceCountry= null

                   }
               },
                BoardDirectors = new List<BoardDirectorsDTOV2>()
                {
                    new BoardDirectorsDTOV2()
                    {
                        BoardDirector= null,
                        BoardDirectorName= "ALI FARAZ",
                        Designation= "Director",
                        BoardDirectorAddress= "dubai",
                        BoardDirectorIDExpirydate= DateTime.Now,
                        ResidentNonResident= "N",
                        CountryofResidence= "ARE",
                        BoardDirectorIDtype= "PAS",
                        BoardDirectorIDNo= "DY9895912",
                        Blacklist= null,
                        PEP= "N",
                        FATCARelevant= "N",
                        CRSRelevant= "N",
                        AreyouaUSCitizen= "N",
                        AreyouaUSTaxResident= "N",
                        WereyoubornintheUS= "N",
                        USAORUAE= "N",
                        TINAvailable= "N",
                        TinNo1= null,
                        TaxCountry1= null,
                        TinNo2= null,
                        TaxCountry2= null,
                        TinNo3= null,
                        TaxCountry3= null,
                        TinNo4= null,
                        TaxCountry4= null,
                        TinNo5= null,
                        TaxCountry5= null,
                        FATCANoReason= "Reason A",
                        ReasonB= null,
                        IDGender= "M",
                        DateOfBirth= DateTime.Now,
                        IDNationality= "ARE",
                        BDCity= null,
                        BDState= "001",
                        BDCountry= "ARE",
                        BODPlaceOfBirth= null,
                        BODMobileNo= null,
                        BODEmail= null,

                    }
                },
                ClientBuyer = new List<ClientBuyerDTO>()
                {
                    new ClientBuyerDTO()
                    {
                        ClientBuyerID= "0",
                        Name= "twaet",
                        IsWebsite= 0,
                        WebsiteURL= "test",
                        Profile= "test",
                        Country= "ABW",
                        TransactionVolume= 0,
                        TransactionValue= 0,
                        AdverseInfo= ""


                    }
                },
                ClientSeller = new List<ClientSellerDTO>()
                {
                    new ClientSellerDTO()
                    {
                        SellerID = "0",
                        Name = "testmohsin",
                        IsWebsite = 0,
                        Website = null,
                        Profile = "test",
                        Country = "ABW",
                        TransactionVolume = 0,
                        TransactionValue = 0,
                        AdverseInfo = ""
                    }

                },
                CompanyInFo = new List<CreateCorporateCIFV2CompanyInFoV2>()
                {
                    new CreateCorporateCIFV2CompanyInFoV2()
                    {
                        ClientID="",
                        AccountNo="",
                        BankCountry="",
                        BankName="",
                        SerialID=79
                    }
                }
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}

}
