﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWCorpOnboarding.Swagger.BankSmart.CreateCorporateAccount
{
    //REQUEST
    public class CreateCorporateAccountRequest : IExamplesProvider<DBHandler.GetCorporateAccountDetailEBosModel.GetCorporateAccountDetailEBosModel>
    {
        public DBHandler.GetCorporateAccountDetailEBosModel.GetCorporateAccountDetailEBosModel  GetExamples()
        {
            return new DBHandler.GetCorporateAccountDetailEBosModel.GetCorporateAccountDetailEBosModel()
            {
                AccountId="",
                IBAN=""
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
