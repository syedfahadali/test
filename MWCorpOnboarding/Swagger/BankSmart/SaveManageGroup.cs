﻿using DBHandler.Helper;
using DBHandler.Model;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWCorpOnboarding.Swagger.BankSmart.SaveManageGroup
{
    //REQUEST
    public class SaveManageGroupRequest : IExamplesProvider<DBHandler.Model.SaveManageGroup.SaveManageGroupModel>
    {
        public DBHandler.Model.SaveManageGroup.SaveManageGroupModel GetExamples()
        {
            return new DBHandler.Model.SaveManageGroup.SaveManageGroupModel()
            {
                GroupID = "",
                GroupName="",
                ClientID="",
                Action=""
            };
        }
    }

    //RESPONSE
    //public class BalanceInquiryResponse : IExamplesProvider<ActiveResponseSucces<Balance>>
    //{
    //    public ActiveResponseSucces<Balance> GetExamples()
    //    {
    //        return new ActiveResponseSucces<Balance>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new Balance()
    //            {
    //                AccountId = "0315100003348801"

    //            }

    //        };
    //    }
    //}
}
