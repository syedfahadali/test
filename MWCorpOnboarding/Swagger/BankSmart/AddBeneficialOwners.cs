﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWCorpOnboarding.Swagger.BankSmart.AddBeneficialOwners
{
    //REQUEST
    public class AddBeneficialOwnersRequest : IExamplesProvider<CIFBeneficialOwnersDTO>
    {
        public CIFBeneficialOwnersDTO GetExamples()
        {
            return new CIFBeneficialOwnersDTO()
            {
                ClientID = "000000139",
                BeneficialOwners = new BeneficialOwners()
                {
                    BeneficialOwnersID = 55,
                    BeneficialName = "fahad",
                    BeneficialAddress = "4/c",
                    BeneficialIDNo = "1212",
                    BeneficialIDExpirydate = DateTime.Now,
                    ResidentNonResident = "Y",
                    CountryOfResidence = "UAE",
                    BeneficialIDtype = "DLL",
                    Blacklist = "Y",
                    PEP = "Y",
                    FATCARelevant = "Y",
                    CRSRelevant = "Y",
                    AreyouaUSCitizen = "Y",
                    AreyouaUSTaxResident = "Y",
                    WereyoubornintheUS = "Y",
                    USAORUAE = "Y",
                    TINAvailable = "",
                    TinNo1 = "",
                    TaxCountry1 = null,
                    TinNo2 = "",
                    TaxCountry2 = null,
                    TinNo3 = "",
                    TaxCountry3 = null,
                    TinNo4 = "",
                    TaxCountry4 = null,
                    TinNo5 = "",
                    TaxCountry5 = null,
                    FATCANoReason = "Reason A",
                    ReasonB = "",
                    BOCity = "DBX",
                    BOState = "DAU",
                    BOCountry = "UAE",
                    IDGender = "M",
                    DateOfBirth = DateTime.Now,
                    IDNationality = "UAE"
                }

            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}

}
