﻿using DBHandler.Helper;
using DBHandler.Model;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWCorpOnboarding.Swagger.BankSmart.UpdateCorporateClient
{
    //REQUEST
    public class UpdateCorporateClientRequest : IExamplesProvider<UpdateCorporateClientV2Model>
    {
        public UpdateCorporateClientV2Model GetExamples()
        {
            return new UpdateCorporateClientV2Model()
            {
                Cust = new UpdateCorporateCustomerModelV2()
                {
                    OurBranchID = "03",
                    ClientID = "000051210",
                    GroupCode = "HNIND",
                    RManager = "004",
                    RiskProfile = null,
                    EntityNameCorp = "FUTURE EYE TECHNOLOGY",
                    EntityTypeCorp = "LLC",
                    YearofestablishmentCorp = DateTime.Now,
                    RegisteredAddressCorp = "hfghgfgh",
                    ContactName1Corp = "hfghfhg",
                    ContactID1Corp = null,
                    IDTypeCorp = "TLL",
                    Address1Corp = "hfghgfgh",
                    PhoneNumberCorp = "+971504850841",
                    AlternatePhonenumberCorp = null,
                    EmailIDCorp = "testchildcomp25@gmail.com",
                    ContactName2Corp = "",
                    ContactID2Corp = "",
                    IDType2 = null,
                    Address2Corp = "",
                    PhoneNumber2Corp = null,
                    AlternatePhonenumber2Corp = null,
                    EmailID2Corp = null,
                    BusinessActivity = "0143",
                    SICCodesCorp = "",
                    IndustryCorp = "OTH",
                    PresenceinCISCountryCorp = "N",
                    DealingwithCISCountryCorp = "N",
                    CountryofIncorporationCorp = "ARE",
                    TradeLicenseIssuedateCorp = DateTime.Now,
                    TradeLicenseExpirydateCorp = DateTime.Now,
                    TradeLicenseIssuanceAuthorityCorp = "DUBAIECONOMY",
                    TRNNoCorp = "6565675765",
                    PEPCorp = "N",
                    BlackListCorp = "",
                    KYCReviewDateCorp = null,
                    FATCARelevantCorp = "Y",
                    CRSRelevantCorp = "Y",
                    USAEntityCorp = "SUP",
                    FFICorp = "N",
                    FFICategoryCorp = null,
                    GIINNoCorp = null,
                    GIINNACorp = null,
                    SponsorNameCorp = null,
                    SponsorGIIN = null,
                    NFFECorp = "003",
                    StockExchangeCorp = "hghjghj",
                    TradingSymbolCorp = null,
                    USAORUAECorp = "Y",
                    TINAvailableCorp = "N",
                    TinNo1Corp = null,
                    TaxCountry1Corp = null,
                    TinNo2Corp = null,
                    TaxCountry2Corp = null,
                    TinNo3Corp = null,
                    TaxCountry3Corp = null,
                    TinNo4Corp = null,
                    TaxCountry4Corp = null,
                    TinNo5Corp = null,
                    TaxCountry5Corp = null,
                    FATCANoReasonCorp = null,
                    ReasonBCorp = null,
                    ReasonACorp = "Reason B",
                    CreateBy = "10",
                    CompanyWebsite = null,
                    CompCashIntensive = "N",
                    PubLimCompCorp = "N",
                    RegAddCityCorp = "00006",
                    RegAddCountryCorp = "ARE",
                    RegAddStateCorp = "004",
                    Add1CountryCorp = "ARE",
                    Add1StateCorp = "004",
                    Add1CityCorp = "00006",
                    Add2CountryCorp = null,
                    Add2StateCorp = null,
                    Add2CityCorp = null,
                    VATRegistered = null,
                    JurEmiratesID = "002",
                    JurTypeID = "001",
                    JurAuthorityID = "001",
                    POBox = "6465465465",
                    JurOther = null,
                    GroupID = null,
                },

                SicCodeCorp = new List<UpdateCorporateClientSicCodeCorpModelV2>()
                {
                    new UpdateCorporateClientSicCodeCorpModelV2()
                    {
                        SicCodeCorp ="9511",
                        Type ="P"
                    }
                }

            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
