﻿using DBHandler.Helper;
using DBHandler.Model;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWCorpOnboarding.Swagger.BankSmart.GetBorrowMandate
{
    //REQUEST
    public class GetBorrowMandateRequest : IExamplesProvider<DBHandler.Model.GetBorrowManDate.GetBorrowManDateModel>
    {
        public DBHandler.Model.GetBorrowManDate.GetBorrowManDateModel GetExamples()
        {
            return new DBHandler.Model.GetBorrowManDate.GetBorrowManDateModel()
            {
                AccountID="0310100000096604",
                SerialID=7654321

            };
        }
    }

    //RESPONSE
    //public class GetBorrowMandateResponse : IExamplesProvider<ActiveResponseSucces<DBHandler.Model.GetBorrowManDate.GetBorrowManDateResponse>>
    //{
    //    public ActiveResponseSucces<DBHandler.Model.GetBorrowManDate.GetBorrowManDateResponse> GetExamples()
    //    {
    //        return new ActiveResponseSucces<DBHandler.Model.GetBorrowManDate.GetBorrowManDateResponse>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new DBHandler.Model.GetBorrowManDate.GetBorrowManDateResponse()
    //            {


    //            }

    //        };
    //    }
    //}
}
