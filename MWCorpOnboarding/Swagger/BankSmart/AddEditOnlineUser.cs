﻿using DBHandler.Helper;
using DBHandler.Model;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWCorpOnboarding.Swagger.BankSmart.AddEditOnlineUser
{
    //REQUEST
    public class AddEditOnlineUserRequest : IExamplesProvider<AddEditOnlineUserModel>
    {
        public AddEditOnlineUserModel GetExamples()
        {
            return new AddEditOnlineUserModel()
            {
                Name = "Filza",
                FirstName = "Filza",
                LastName = "Atif",
                MiddleName = "Filz",
                Email = "demo@demomail.com",
                MobileNo = "03131313131",
                UserType = 1,
                DOB = DateTime.Now,
                ProfileImageUrl = "",
                //UserImage = "",
                //IsAdmin = "",
                TimeZone = "",
                LanguageCode = "",
                ExpiryDate = DateTime.Now,
                //Gender = "M",
                //UserID = "",
                RolesAssigned = "",
                ActionTypeAssigned = "",
                PassportNo = "",
                NationalIDNo = "",
                Designation = "",
                //CountryID = "",
                Address = "",
                //DailyLimit = "",
                //WPSFileTotalAmount = "",
                //WPSPerDayTotalAmount = "",
                SubRolesAssigned = "",
                ClientId = "",
                SerialNo = 56
                


            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}

}
