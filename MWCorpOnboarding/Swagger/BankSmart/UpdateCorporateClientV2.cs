﻿using DBHandler.Helper;
using DBHandler.Model;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWCorpOnboarding.Swagger.BankSmart.UpdateCorporateClientV2
{
    //REQUEST
    public class UpdateCorporateClientV2Request : IExamplesProvider<UpdateCorporateClientV2Model>
    {
        public UpdateCorporateClientV2Model GetExamples()
        {
            return new UpdateCorporateClientV2Model()
            {
                Cust = new UpdateCorporateCustomerModelV2()
                {
                    ClientID = "000008150",
                    RManager = "001",
                    RiskProfile = "High",
                    EntityNameCorp = "abc ",
                    EntityTypeCorp = "FOC",
                    YearofestablishmentCorp = DateTime.Now,
                    RegisteredAddressCorp = "abc",
                    ContactName1Corp = "Fahad",
                    ContactID1Corp = "abc",
                    IDTypeCorp = "DLL",
                    Address1Corp = "abc",
                    PhoneNumberCorp = "134234",
                    AlternatePhonenumberCorp = null,
                    EmailIDCorp = "fahad_Ali1003@hotmail.com",
                    ContactName2Corp = "",
                    ContactID2Corp = "",
                    IDType2 = "",
                    Address2Corp = "",
                    PhoneNumber2Corp = null,
                    AlternatePhonenumber2Corp = null,
                    EmailID2Corp = "",
                    BusinessActivity = "0111",
                    SICCodesCorp = "0112",
                    IndustryCorp = "MFT",
                    PresenceinCISCountryCorp = "Y",
                    DealingwithCISCountryCorp = "Y",
                    //TradeLicenseNumberCorp = "777701777",
                    CountryofIncorporationCorp = "ARE",
                    TradeLicenseIssuedateCorp = DateTime.Now,
                    TradeLicenseExpirydateCorp = DateTime.Now,
                    TradeLicenseIssuanceAuthorityCorp = "abc",
                    TRNNoCorp = "",
                    PEPCorp = "Y",
                    BlackListCorp = "Y",
                    KYCReviewDateCorp = DateTime.Now,
                    FATCARelevantCorp = "Y",
                    CRSRelevantCorp = "Y",
                    USAEntityCorp = "SUP",
                    FFICorp = "Y",
                    FFICategoryCorp = null,
                    GIINNoCorp = "",
                    GIINNACorp = "",
                    SponsorNameCorp = "",
                    SponsorGIIN = "",
                    NFFECorp = null,
                    StockExchangeCorp = "",
                    TradingSymbolCorp = "",
                    USAORUAECorp = "Y",
                    TINAvailableCorp = "",
                    TinNo1Corp = "1",
                    TaxCountry1Corp = "",
                    TinNo2Corp = "",
                    TaxCountry2Corp = "",
                    TinNo3Corp = "",
                    TaxCountry3Corp = "",
                    TinNo4Corp = "",
                    TaxCountry4Corp = "",
                    TinNo5Corp = "",
                    TaxCountry5Corp = "",
                    FATCANoReasonCorp = "Reason B",
                    ReasonBCorp = "",
                    CompanyWebsite = "",
                    CompCashIntensive = "Y",
                    PubLimCompCorp = "Y",
                    RegAddCountryCorp = "ARE",
                    RegAddStateCorp = "001",
                    RegAddCityCorp = "00001",
                    Add1CountryCorp = "ARE",
                    Add1StateCorp = "001",
                    Add1CityCorp = "00001",
                    Add2CountryCorp = "",
                    Add2StateCorp = "",
                    Add2CityCorp = "",
                    VATRegistered = null,
                    JurEmiratesID = "001",
                    JurTypeID = "001",
                    JurAuthorityID = "001",
                    POBox = "2344",
                    LicenseType = "",
                    CountryID = "",
                    MOAMOU = DateTime.Now,
                    SigningAuthority = "",
                    POAResidenceCountry = "",
                    POAAddress = "",
                    POACountry = "",
                    POAState = "",
                    POACity = "",
                    AuditedFinancials = "",
                    Assets = 0,
                    Revenue = 0,
                    Profits = 0,
                    BusinessTurnOver = 0,
                    CompanyCapital = 0,
                    CompanyCountry = "",
                    ExpectedDebitTurnOver = 0,
                    ExpectedMaxDebitTurnOver = 0,
                    ExpectedCreditTurnOver = 0,
                    ExpectedMaxCreditTurnOver = 0,
                    CompanyAddress = "",
                    ExpectedCashVolume = 0,
                    ExpectedCashValue = 0,
                    ChequesVolume = 0,
                    ChequesValue = 0,
                    SalaryPaymentVolume = 0,
                    SalaryPaymentValue = 0,
                    IntraBankRemittancesVolume = 0,
                    IntraBankRemittancesValue = 0,
                    InternationalRemittancesVolume = 0,
                    InternationalRemittancesValue = 0,
                    DomesticDRemittanceVolume = 0,
                    DomesticDRemittanceValues = 0,
                    ExpectedCashDebitVolume = 0,
                    ExpectedCashDebitValue = 0,
                    ChequeDebitVolume = 0,
                    ChequeDebitValue = 0,
                    SalaryDVolume = 0,
                    SalaryDValue = 0,
                    IntraDVolume = 0,
                    IntraDValue = 0,
                    InternationalDVolume = 0,
                    InternationalDValue = 0,
                    DomesticRemittanceVolume = 0,
                    DomesticRemittanceValues = 0,
                    CreditTradeFinanceVolume = 0,
                    CreditTradeFinanceValue = 0,
                    TradeFinanceVolumeD = 0,
                    TradeFinanceValueD = 0,
                    NoOfStaff = 0,
                    CompanyActivites = "",
                    BusinessNatureRelation = "",
                    RelationalCompanyStructure = "",
                    CompanyGeoGraphicCourage = "",
                    UBOBackground = "",
                    ProductAndServicesOffered = "",
                    IsDisclosureActivity = 0,
                    DisclosureDetails = "",
                    IsSanctionCountry = 0,
                    SanctionRemarks = "",
                    IsOfficeInSactionCountry = 0,
                    SanctionCountries = "",
                    BusinessNatureSanctionCountry = "",
                    IsProductOrigninatedCountry = 0,
                    OriginatedProductCountry = "",
                    OriginatedCountryRemarks = "",
                    IsExportGoods = 0,
                    ExportGoodsCountry = "",
                    ExportGoodsRemarks = "",
                    isRecievedTransFromSanctionCountry = 0,
                    TransactionSanctionCountry = "",
                    TransactionSanctionRemarks = "",
                    CorporateStructureChart = "",
                    VisitConducted = "",
                    SanctionAssestment = "",
                    PepAssessment = "",
                    AdverseMedia = "",
                    AccountOpeningPurpose = "",
                    ReasonACorp = "",
                    POADateofBirth = "",
                    POAGender = "",
                    DepositValue = 0,
                    IsVip = 0,
                    POAName = "",
                    POANationality = "",
                    POAPlaceOfBirth = "",
                    CreateBy = "",
                    DealingwithCISTextarea = "",
                    Department1Corp = "",
                    Department2Corp = "",
                    Designation1Corp = "",
                    Designation2Corp = "",
                    ExportGoodsCountrylist = "",
                    GroupCode = "",
                    GroupID = "",
                    IDExpiryDate2Corp = "",
                    IDExpiryDateCorp = "",
                    IsOfficeInSactionCountrylist = "",
                    IsProductOrigninatedCountrylist = "",
                    JurOther = "",
                    LoansValue = 0,
                    OurBranchID = "",
                    ParentClientID = "",
                    PaymentValue = 0,
                    PlaceofIncorporation = "",
                    selectionOfPayments = "",
                    TransactionSanctionCountrylist = ""



                },
                SicCodeCorp = new List<UpdateCorporateClientSicCodeCorpModelV2>()
                {
                    new UpdateCorporateClientSicCodeCorpModelV2()
                    {
                        SicCodeCorp ="155",
                        Type = "P"
                    }
                },
                ClientSeller = new List<UpdateCorporateClientClientSellerDTOV2>()
                {
                    new UpdateCorporateClientClientSellerDTOV2()
                    {

                        SellerID= "3",
                        Name = "test3mohsin",
                        IsWebsite = 0,
                        Website = "test.com",
                        Profile = "test",
                        Country = "ABW",
                        TransactionVolume = 0,
                        TransactionValue = 0,


                    }
                },
                ClientBuyer = new List<UpdateCorporateClientClientBuyerDTOV2>()
                {
                    new UpdateCorporateClientClientBuyerDTOV2()
                    {
                        ClientBuyerID = "1",
                        Name = "mohsintest1",
                        IsWebsite = 0,
                        WebsiteURL = "test",
                        Profile = "test",
                        Country = "ABW",
                        TransactionVolume = 0,
                        TransactionValue = 0,

                    }
                },
                CompanyInFo = new List<UpdateCorporateClientCompanyInFoV2>()
                {
                    new UpdateCorporateClientCompanyInFoV2()
                    {
                        ClientID=null,
                        AccountNo="101233444987",
                        BankCountry="ARE",
                        BankName="101233444987",
                        SerialID=Convert.ToDecimal(1.0)
                    }
                }


            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
