﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWCorpOnboarding.Swagger.BankSmart.AddShareHolder
{
    //REQUEST
    public class AddShareHolderRequest : IExamplesProvider<CIFShareHolderDetiilDTO>
    {
        public CIFShareHolderDetiilDTO GetExamples()
        {
            return new CIFShareHolderDetiilDTO()
            {
                ClientID = "000000139",
                ShareHolder = new ShareHolder()
                {
                    ShareHolderName = "salman",
                    ShareHolderAddress = "5c",
                    ShareholderType = "COM",
                    ShareholdersIDtype = "DLL",
                    ShareholdersIDNo = "123",
                    ShareholdersIDExpirydate = DateTime.Now,
                    CountryofIncorporation = "ARE",
                    BoardMember = "Y",
                    AuthorisedSignatory = "Y",
                    BeneficialOwner = "Y",
                    ResidentNonResident = "N",
                    CountryofResidence = "ARE",
                    OwnershipPercentage = 123,
                    Blacklist = "Y",
                    PEP = "Y",
                    FATCARelevant = "Y",
                    CRSRelevant = "Y",
                    USCitizen = "Y",
                    AreyouaUSTaxResident = "Y",
                    WereyoubornintheUS = "Y",
                    USAEntity = "",
                    FFI = "",
                    FFICategory = "",
                    GIINNo = "",
                    GIINNA = "",
                    SponsorName = "",
                    SponsorGIIN = "",
                    NFFE = "",
                    StockExchange = "",
                    TradingSymbol = "",
                    USAORUAE = "Y",
                    TINAvailable = "",
                    TinNo1 = "",
                    TaxCountry1 = "",
                    TinNo2 = "",
                    TaxCountry2 = "",
                    TinNo3 = "",
                    TaxCountry3 = "",
                    TinNo4 = "",
                    TaxCountry4 = "",
                    TinNo5 = "",
                    TaxCountry5 = "",
                    FATCANoReason = "Reason A",
                    ReasonB = "",
                    SHCity = "00012",
                    SHState = "005",
                    SHCountry = "ARE",
                    SHIDIssuanceCountry = "ARE",
                    IDGender = "M",
                    DateOfBirth = DateTime.Now,
                    IDNationality = "ARE",
                    JurEmiratesId = "001",
                    JurTypeId = "",
                    JurAuthorityId = "",
                    POBox = "2344",
                    JurOther = "",
                    ParentShareHolderId = "",


                }
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",,
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}

}
