﻿using DBHandler.Helper;
using DBHandler.Model;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWCorpOnboarding.Swagger.BankSmart.GetCorporateCIFDetail
{
    //REQUEST
    public class GetCorporateCIFDetailRequest : IExamplesProvider<RimInquiry>
    {
        public RimInquiry GetExamples()
        {
            return new RimInquiry()
            {
                ClientID = "000000129"
            };
        }
    }

    //RESPONSE
    //public class GetCorporateCIFDetailResponse : IExamplesProvider<ActiveResponseSucces<DBHandler.Model.Dtos.CustomerCorporateView>>
    //{
    //    public ActiveResponseSucces<DBHandler.Model.Dtos.CustomerCorporateView> GetExamples()
    //    {
    //        return new ActiveResponseSucces<DBHandler.Model.Dtos.CustomerCorporateView>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new DBHandler.Model.Dtos.CustomerCorporateView()
    //            {
                    

    //            }

    //        };
    //    }
    //}

}
