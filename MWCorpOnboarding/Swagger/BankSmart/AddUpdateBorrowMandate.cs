﻿using DBHandler.Helper;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWCorpOnboarding.Swagger.BankSmart.AddUpdateBorrowMandate
{
    //REQUEST
    public class AddUpdateBorrowMandateRequest : IExamplesProvider<DBHandler.Model.AddUpdateBorrowManDate.AddUpdateBorrowManDateModel>
    {
        public DBHandler.Model.AddUpdateBorrowManDate.AddUpdateBorrowManDateModel GetExamples()
        {
            return new DBHandler.Model.AddUpdateBorrowManDate.AddUpdateBorrowManDateModel()
            {
                AccountID = "0310100000096604",
                SerialID = 1234567,
                MinLimit = 1,
                MaxLimit = 100000,
                AuthGroupID1 = "A",
                AuthGroupID2 = "",
                AuthGroupID3 = "",
                AuthGroupID4 = ""

            };
        }
    }

    //RESPONSE
    //public class AddUpdateBorrowMandateResponse : IExamplesProvider<ActiveResponseSucces<DBHandler.Model.AddUpdateBorrowManDate.AddUpdateBorrowManDateResponse>>
    //{
    //    public ActiveResponseSucces<DBHandler.Model.AddUpdateBorrowManDate.AddUpdateBorrowManDateResponse> GetExamples()
    //    {
    //        return new ActiveResponseSucces<DBHandler.Model.AddUpdateBorrowManDate.AddUpdateBorrowManDateResponse>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new DBHandler.Model.AddUpdateBorrowManDate.AddUpdateBorrowManDateResponse()
    //            {
    //                SerialID = 123
    //            }
    //        };
    //    }
    }

