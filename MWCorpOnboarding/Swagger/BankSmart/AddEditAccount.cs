﻿using DBHandler.Helper;
using DBHandler.Model;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWCorpOnboarding.Swagger.BankSmart.AddEditAccount
{
    //REQUEST
    public class AddEditAccountRequest : IExamplesProvider<AddEditAccountDTO>
    {
        public AddEditAccountDTO GetExamples()
        {
            return new AddEditAccountDTO()
            {
                Account = new AddEditAccountModel()
                {
                    ClientID = "000222759",
                    AccountId = "0497500000012001",
                    NatureOfAccount = "0311",
                    NostroAccountNumber = "123123123",
                    NostroAddress = "abc123",
                    NostroIBAN = "AE0901293818238123",
                    StatementAddress = "abc",
                    City = "00007",
                    Country = "ARE",
                    Email = "abc@msn.com",
                    MinBalanceRequirement = "10000.0000",
                    MobileNo = "1231231232",
                    Name = "CITI BANK",
                    NostroBankName = "abc bank pak",
                    NostroBranchName = "abc bank pak br",
                    NostroCountry = "PAK",
                    NostroSortCode = "91823",
                    NostroSWIFT = "123123",
                    Phone = "12312312",
                    ProductID = "INRNOST",
                    RelationshipFunction = "CORP",
                    State = "003",
                    StatementFrequency = "N"
                }
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}

}
