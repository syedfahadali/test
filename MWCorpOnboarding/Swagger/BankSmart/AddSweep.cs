﻿using DBHandler.Helper;
using DBHandler.Model;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace MWCorpOnboarding.Swagger.BankSmart.AddSweep
{

    //REQUEST
    public class AddSweepRequest : IExamplesProvider<AddSweepDTO>
    {
        public AddSweepDTO GetExamples()
        {
            return new AddSweepDTO()
            {
                Amount = 0,
                coverAccount = "",
                coverAccount2 = "",
                onAccount = "",
                ClientId = "",
                sweepType = ""

            };
        }
    }

    //RESPONSE
    //public class BalanceInquiryResponse : IExamplesProvider<ActiveResponseSucces<Balance>>
    //{
    //    public ActiveResponseSucces<Balance> GetExamples()
    //    {
    //        return new ActiveResponseSucces<Balance>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new Balance()
    //            {
    //                AccountId = "0315100003348801"

    //            }

    //        };
    //    }
    //}
}
