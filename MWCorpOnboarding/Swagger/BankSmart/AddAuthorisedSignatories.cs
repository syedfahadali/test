﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWCorpOnboarding.Swagger.BankSmart.AddAuthorisedSignatories
{
    //REQUEST
    public class AddAuthorisedSignatoriesRequest : IExamplesProvider<CIFAuthorisedSignatoriesDTO>
    {
        public CIFAuthorisedSignatoriesDTO GetExamples()
        {
            return new CIFAuthorisedSignatoriesDTO()
            {
                ClientID = "000000016",
                AuthorisedSignatories = new AuthorisedSignatories()
                {
                    AuthorisedSignatoriesID = 0,
                    AuthorisedsignatoriesName ="",
                    PhoneNumber ="12345678910",
                    EmailID ="dummy@email.com",
                    CorrespondenceAddress ="",
                    AuthorisedsignatoryIDtype1 ="",
                    AuthorisedsignatoryIDNumber1 ="",
                    AuthorisedsignatoryIDExpirydate1 = DateTime.Now,
                    AuthorisedsignatoryIDIssueCountry1 ="",
                    AuthorisedsignatoryIDtype2 ="",
                    AuthorisedsignatoryIDNumber2 ="",
                    AuthorisedsignatoryIDExpirydate2 =DateTime.Now,
                    AuthorisedsignatoryIDIssueCountry2 ="",
                    AuthorisedsignatoryVisaIssuedby ="",
                    AuthorisedsignatoryVisaNumber ="",
                    AuthorisedsignatoryVisaExpiry =DateTime.Now,
                    Remarks ="",
                    Blacklist ="y",
                    PEP ="y",
                    FATCARelevant = "Y",
                    CRSRelevant ="Y",
                    AreyouaUSCitizen ="",
                    AreyouaUSTaxResident ="",
                    WereyoubornintheUS ="",
                    USAORUAE="Y",
                    TINAvailable = "",
                    TinNo1 = "",
                    TaxCountry1 = null,
                    TinNo2 = "",
                    TaxCountry2 = null,
                    TinNo3 = "",
                    TaxCountry3 = null,
                    TinNo4 = "",
                    TaxCountry4 = null,
                    TinNo5 = "",
                    TaxCountry5 = null,
                    FATCANoReason = "Reason A",
                    ReasonB = "",
                    IDGender ="",
                    DateOfBirth =DateTime.Now,
                    IDNationality ="",
                    ASCity ="",
                    ASState ="",
                    ASCountry ="",
                    GroupID ="",

                }
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}

}
