﻿using DBHandler.Helper;
using DBHandler.Model;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWCorpOnboarding.Swagger.BankSmart.DeleteBorrowMandate
{
    //REQUEST
    public class DeleteBorrowMandateRequest : IExamplesProvider<DBHandler.Model.DeleteBorrowManDate.DeleteBorrowManDateModel>
    {
        public DBHandler.Model.DeleteBorrowManDate.DeleteBorrowManDateModel GetExamples()
        {
            return new DBHandler.Model.DeleteBorrowManDate.DeleteBorrowManDateModel()
            {
                AccountID = "",
                SerialID = 1
            };
        }
    }

    //RESPONSE
    //public class DeleteBorrowMandateResponse : IExamplesProvider<ActiveResponseSucces<DBHandler.Model.DeleteBorrowManDate.DeleteBorrowManDateResponse>>
    //{
    //    public ActiveResponseSucces<DBHandler.Model.DeleteBorrowManDate.DeleteBorrowManDateResponse> GetExamples()
    //    {
    //        return new ActiveResponseSucces<DBHandler.Model.DeleteBorrowManDate.DeleteBorrowManDateResponse>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new DBHandler.Model.DeleteBorrowManDate.DeleteBorrowManDateResponse()
    //            {


    //            }

    //        };
    //    }
    //}
}
