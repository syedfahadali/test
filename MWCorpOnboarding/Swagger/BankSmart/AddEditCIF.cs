﻿using DBHandler.Helper;
using DBHandler.Model;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWCorpOnboarding.Swagger.BankSmart.AddEditCIF
{
    //REQUEST
    public class AddEditCIFRequest : IExamplesProvider<AddEditCIFDTO>
    {
        public AddEditCIFDTO GetExamples()
        {
            return new AddEditCIFDTO()
            {
                Customer = new AddEditNosroCustomer()
                {
                    ClientID = "000222759",
                    Name = " test",
                    CustomerSegment = "NOSTRO",
                    GroupCode = "INDIV",
                    RiskProfile = "High",
                    Phone = "+923057676233",
                    Email = "test@gmail.com",
                    Address = "ABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABCABC",
                    Status = "A"
                }
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}

}
