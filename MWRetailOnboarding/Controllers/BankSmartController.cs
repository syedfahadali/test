﻿using System.Collections.Generic;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CoreHandler;
using DBHandler.Helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using DBHandler.Repositories;
using Microsoft.Extensions.Configuration;
using DBHandler.Enum;
using CommonModel;
using Newtonsoft.Json.Linq;
using Swashbuckle.AspNetCore.Annotations;
using System;
//using RuleEngineHandler;
using DBHandler.CreditProgramSearchByCIFModel;
using DBHandler.CreditProgramUpdateModel;
using DBHandler.PostChargesModel;
using DBHandler.Model.Loan;
using DBHandler.Model.Gurantee;
using System.Globalization;
using DigiBancMiddleware.Helper;

namespace VendorApi.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class BankSmartController : ControllerBase
    {
        private readonly HttpHandler.HttpHandler httpHandler;
        private readonly IUserChannelsRepository _userChannels;
        private readonly ILogRepository _logs;
        private readonly IConfiguration _config;
        private Level level = Level.Info;
        private Exception myEx;
        private ControllerBase _controllerBase;
        private Exception excetionForLog;

        public BankSmartController(IConfiguration config, IUserChannelsRepository userChannels, IHttpClientFactory httpClientFactory, ILogRepository logs, ControllerBase controllerbase = null)
        {
            _controllerBase = controllerbase;
            _config = config;
            _userChannels = userChannels;
            _logs = logs;
            httpHandler = new HttpHandler.HttpHandler(httpClientFactory);
        }


        [HttpGet]
        public async Task<IActionResult> HealthCheck()
        {
            string[] t = new string[] { "value1", "value2" };
            return Ok(t);
        }

        [Route("BalanceInquriy")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Balance>", typeof(ActiveResponseSucces<Balance>))]
        public async Task<IActionResult> BalanceInquriy([FromBody] BalanceInquiryModel Model)
        {
            ActiveResponseSucces<Balance> response = new ActiveResponseSucces<Balance>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:BalanceInquriy"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<Balance>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    string specialChar = _config["GlobalSettings:SpecialChar"];
                    if (HttpContext.Request.Headers["ChannelId"] == _config["GlobalSettings:ECSCannel"])
                    {
                        response.Content.Name = response.Content.Name != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Name, specialChar) : null;
                        response.Content.ProductType = response.Content.ProductType != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ProductType, specialChar) : null;
                        response.Content.ProductDesc = response.Content.ProductDesc != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ProductDesc, specialChar) : null;
                    }

                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetInterestAccruedCurrentYear")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.GetInterestAccruedCurrentYearResponse>", typeof(ActiveResponseSucces<DBHandler.Model.GetInterestAccruedCurrentYearResponse>))]
        public async Task<IActionResult> GetInterestAccruedCurrentYear([FromBody] DBHandler.Model.GetInterestAccruedCurrentYearDTO Model)
        {
            ActiveResponseSucces<DBHandler.Model.GetInterestAccruedCurrentYearResponse> response = new ActiveResponseSucces<DBHandler.Model.GetInterestAccruedCurrentYearResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSRetailOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSRetailOnboardingAPI:GetInterestAccruedCurrentYear"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.GetInterestAccruedCurrentYearResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("CreateAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Content>", typeof(ActiveResponseSucces<Content>))]
        public async Task<IActionResult> CreateAccount([FromBody] CreateAccount Model)
        {
            ActiveResponseSucces<Content> response = new ActiveResponseSucces<Content>();
            if (Model != null)
            {
                
                    Model.Acc.OurBranchID = HttpContext.Request.Headers["BranchId"];;
                Model.Acc.CreateBy = HttpContext.Request.Headers["ChannelId"];
            }

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSRetailOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSRetailOnboardingAPI:CreateAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<Content>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    if (string.IsNullOrEmpty(response.Content.status))
                    {
                        response.Content.status = "Active";
                    }
                    else if (response.Content.status == "I")
                    {
                        response.Content.status = "InActive";
                    }
                    else if (response.Content.status == "T")
                    {
                        response.Content.status = "Dormant";
                    }
                    else if (response.Content.status == "X")
                    {
                        response.Content.status = "Deceased";
                    }
                    else if (response.Content.status == "D")
                    {
                        response.Content.status = "Blocked";
                    }
                    else if (response.Content.status == "C")
                    {
                        response.Content.status = "Closed";
                    }
                    else
                    {
                        response.Content.status = "Undefined";
                    }

                    return Ok(response);
                }
            }
            return CreatedAtAction(null, response);

        }

        

           [Route("CloseAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<Balance>>", typeof(ActiveResponseSucces<List<Balance>>))]
        public async Task<IActionResult> CloseAccount([FromBody] BalanceInquiryModel Model)
        {
            
            
            ActiveResponseSucces<List<Balance>> response = new ActiveResponseSucces<List<Balance>>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:CloseAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<Balance>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetTransactionListRetail")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<TransactionView>>", typeof(ActiveResponseSucces<List<TransactionView>>))]
        public async Task<IActionResult> GetTransactionListRetail([FromBody] GetTransactionModel Model)
        {
            
            
            ActiveResponseSucces<List<TransactionView>> response = new ActiveResponseSucces<List<TransactionView>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSRetailOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSRetailOnboardingAPI:GetTransactionListRetail"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<TransactionView>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        
        [Route("GetRetailClient")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<InsertCustomerRetail>", typeof(ActiveResponseSucces<InsertCustomerRetail>))]
        public async Task<IActionResult> GetRetailClient([FromBody] RetailCustomerInquiryModel Model)
        {
            
            
            ActiveResponseSucces<InsertCustomerRetail> response = new ActiveResponseSucces<InsertCustomerRetail>();

            ActiveResponseSucces<InsertCustomerRetailView> res = new ActiveResponseSucces<InsertCustomerRetailView>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSRetailOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSRetailOnboardingAPI:GetRetailClient"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<InsertCustomerRetail>>(result.httpResult.httpResponse);
                res = JsonConvert.DeserializeObject<ActiveResponseSucces<InsertCustomerRetailView>>(result.httpResult.httpResponse);


                if (result != null && result.httpResult?.severity == "Success")
                {
                    res.Content.idno = response.Content.NIC;
                    res.Content.IDExpirydate = response.Content.NicExpirydate;
                    res.Content.customerEmail = response.Content.Email;
                    return Ok(res);
                }
            }
            return BadRequest(response);
        }


        

        

        [Route("GetExpiredCIF")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<ExpiredCIFResponse>>", typeof(ActiveResponseSucces<List<ExpiredCIFResponse>>))]
        public async Task<IActionResult> GetExpiredCIF([FromBody] ExpiredCIFModel Model)
        {
            
            
            ActiveResponseSucces<List<ExpiredCIFResponse>> response = new ActiveResponseSucces<List<ExpiredCIFResponse>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSRetailOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSRetailOnboardingAPI:GetExpiredCIF"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                var resp = JsonConvert.DeserializeObject<ActiveResponseSucces<List<Customer>>>(result.httpResult.httpResponse);
                if (resp != null && result != null && result.httpResult?.severity == "Success")
                {
                    List<ExpiredCIFResponse> cif = new List<ExpiredCIFResponse>();
                    foreach (Customer cust in resp.Content)
                    {
                        ExpiredCIFResponse r = new ExpiredCIFResponse();
                        r.ClientID = cust.ClientID;
                        r.DOB = cust.DOB;
                        r.Email = cust.Email;
                        r.FirstName = cust.FirstName;
                        r.IDExpirydate = cust.NicExpirydate;
                        r.idno = cust.NIC;
                        r.KYCReviewDate = cust.KYCReviewDate;
                        r.LastName = cust.LastName;
                        r.MiddleName = cust.MiddleName;
                        r.MobileNo = cust.MobileNo;
                        r.Name = cust.Name;
                        r.Nationality = cust.Nationality;
                        r.OnboardingCountry = cust.OnboardingCountry;
                        r.OurBranchID = cust.OurBranchID;
                        r.PassportExpiry = cust.PassportExpiry;
                        r.PassportGender = cust.PassportGender;
                        r.PassportIssueCountry = cust.PassportIssueCountry;
                        r.PassportNumber = cust.PassportNumber;
                        r.Phone1 = cust.Phone1;
                        r.Phone2 = cust.Phone2;
                        cif.Add(r);
                    }
                    response.Content = cif;
                    response.Status = resp.Status;
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("UpdateAccountCIFName")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CreditProgramUpdateResponse>", typeof(ActiveResponseSucces<UpdateAccountCIFNameResponse>))]
        public async Task<IActionResult> UpdateAccountCIFName([FromBody] UpdateAccountCIFNameModel Model)
        {
            
            
            ActiveResponseSucces<UpdateAccountCIFNameResponse> response = new ActiveResponseSucces<UpdateAccountCIFNameResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSRetailOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSRetailOnboardingAPI:UpdateAccountCIFName"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<UpdateAccountCIFNameResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("UpdateRetailAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateRetailAccount([FromBody] UpdateAccountModel Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            if (Model != null)
            {
                Model.Acc.OurBranchID = HttpContext.Request.Headers["BranchId"];;
                Model.Acc.CreateBy = HttpContext.Request.Headers["ChannelId"];
            }

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSRetailOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSRetailOnboardingAPI:UpdateRetailAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("CIFinquiryViaCIF")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CustomerInfo>", typeof(ActiveResponseSucces<CustomerInfo>))]
        public async Task<IActionResult> CIFinquiryViaCIF([FromBody] Riminquiryrim Model)
        {
            ActiveResponseSucces<CustomerInfo> response = new ActiveResponseSucces<CustomerInfo>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSRetailOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSRetailOnboardingAPI:riminquiryrim"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CustomerInfo>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("CIFinquiryByIdno")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CustomerInfo>", typeof(ActiveResponseSucces<CustomerInfo>))]
        public async Task<IActionResult> CIFinquiryByIdno([FromBody] Riminquiry Model)
        {
            ActiveResponseSucces<CustomerInfo> response = new ActiveResponseSucces<CustomerInfo>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSRetailOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSRetailOnboardingAPI:riminquirybyIdno"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);

            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];

            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CustomerInfo>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [ProducesResponseType(typeof(ActiveResponseSucces<CustomerInfo>), 200)]
        [ProducesResponseType(400)]
        [Route("CreateRetailCIF")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CustomerInfo>", typeof(ActiveResponseSucces<CustomerInfo>))]
        public async Task<IActionResult> CreateRetailCIF([FromBody] CreateCIF Model)
        {

            ActiveResponseSucces<CustomerInfo> response = new ActiveResponseSucces<CustomerInfo>();

            if (Model != null)
            {
                // 
                Model.Cust.OurBranchID = HttpContext.Request.Headers["BranchId"];
                Model.Cust.CreateBy = HttpContext.Request.Headers["ChannelId"];
                Model.Cust.NIC = Model.Cust.idno;
                Model.Cust.NicExpirydate = Model.Cust.IDExpirydate;
                Model.Cust.Email = Model.Cust.customerEmail;
            }

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSRetailOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSRetailOnboardingAPI:CreateCif"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Now;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CustomerInfo>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                   return CreatedAtAction(null, response);
                }
            }
            else
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CustomerInfo>>(result.httpResult.httpResponse);
            }
            return BadRequest(response);
        }

        [Route("UpdateRetailClient")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateRetailClient([FromBody] UpdateCIF Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            if (Model != null)
            {
                // 
                Model.Cust.OurBranchID = HttpContext.Request.Headers["BranchId"];;
                Model.Cust.CreateBy = HttpContext.Request.Headers["ChannelId"];
                Model.Cust.Email = Model.Cust.customerEmail;
                Model.Cust.NicExpirydate = Model.Cust.IDExpirydate;
            }

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSRetailOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSRetailOnboardingAPI:UpdateCIF"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        





    }
}
