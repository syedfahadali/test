﻿using DBHandler.Helper;
using DBHandler.Model;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWRetailOnboarding.Swagger.BankSmart.GetInterestAccruedCurrentYear
{
   
    //REQUEST
    public class GetInterestAccruedCurrentYearRequest : IExamplesProvider<GetInterestAccruedCurrentYearDTO>
    {
        public GetInterestAccruedCurrentYearDTO GetExamples()
        {
            return new GetInterestAccruedCurrentYearDTO()
            {
                AccountID = "0210100000033601"
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}

}
