﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWRetailOnboarding.Swagger.BankSmart.GetTransactionListRetail
{
    //REQUEST
    public class GetTransactionListRetailRequest : IExamplesProvider<GetTransactionModel>
    {
        public GetTransactionModel GetExamples()
        {
            return new GetTransactionModel()
            {
                StartDate = DateTime.Now,
                EndDate = DateTime.Now,
                AccountID= "0315100003348801"
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}


}
