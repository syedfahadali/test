﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using System.Net;
using Confluent.Kafka;
using Microsoft.Extensions.Configuration;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

namespace HttpHandler
{
    public class HttpHandler
    {
        private readonly IHttpClientFactory _httpClientFactory;


        public HttpHandler(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public async Task<RequestResponse> PostFormData(string baseAddress, string url, Dictionary<string, string> headers, Dictionary<string, string> body, IConfiguration _config)
        {
            HttpClient client = GetHttpClient(_config);
          

            foreach (var header in headers)
            {
                client.DefaultRequestHeaders.TryAddWithoutValidation(header.Key, header.Value);
            }

            client.BaseAddress = new Uri(baseAddress);
            var result = await client.PostAsync(url, new FormUrlEncodedContent(body)).ConfigureAwait(false);
            if (result.IsSuccessStatusCode)
            {

                return new RequestResponse { severity = "Success", httpResponse = result.Content.ReadAsStringAsync().Result, StatusCode = result.StatusCode };
            }
            else
            {
                return new RequestResponse { severity = "failure", httpResponse = result.Content.ReadAsStringAsync().Result, StatusCode = result.StatusCode };
            }
        }

        public async Task<RequestResponse> PostFormData(string baseAddress, string url, Dictionary<string, string> headers, MultipartFormDataContent body, IConfiguration _config)
        {
            HttpClient httpClient = GetHttpClient(_config);
            
            foreach (var header in headers)
            {
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation(header.Key, header.Value);
            }

            var result = await httpClient.PostAsync(baseAddress + url, body);
            if (result.IsSuccessStatusCode)
            {
                return new RequestResponse { severity = "Success", httpResponse = result.Content.ReadAsStringAsync().Result, StatusCode = result.StatusCode };
            }
            else
            {
                return new RequestResponse { severity = "failure", httpResponse = result.Content.ReadAsStringAsync().Result, StatusCode = result.StatusCode };
            }
        }

        public async Task<RequestResponse> PostJsonData(string baseAddress, string url, Dictionary<string, string> headers, string body, bool isRMS = false, IConfiguration _config=null)
        {
            HttpClient client = GetHttpClient(_config);

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    client.DefaultRequestHeaders.TryAddWithoutValidation(header.Key, header.Value);
                }
            }

            client.BaseAddress = new Uri(baseAddress);

            Encoding encoding = Encoding.UTF8;
            if (isRMS)
            {
                encoding = Encoding.Unicode;
            }

            var result = await client.PostAsync(url, new StringContent(body, encoding, "application/json")).ConfigureAwait(false);
            if (result.IsSuccessStatusCode)
            {
                return new RequestResponse { severity = "Success", httpResponse = result.Content.ReadAsStringAsync().Result, StatusCode = result.StatusCode };
            }
            else
            {
                return new RequestResponse { severity = "failure", httpResponse = result.Content.ReadAsStringAsync().Result, StatusCode = result.StatusCode };
            }
        }

      
        public async Task<RequestResponse> PutKafkaData(string baseAddress, string topic, string body, IConfiguration _config)
        {
            RequestResponse response = new RequestResponse();
            var conf = new ProducerConfig { BootstrapServers = baseAddress,MessageTimeoutMs = 20000 };
            Action<DeliveryReport<Null, string>> handler = (DeliveryReport<Null, string> r) =>
            {
                if (r.Error.IsError)
                {
                    response.httpResponse = r.Error.Reason;
                    response.severity = "failiure";
                    response.StatusCode = HttpStatusCode.BadRequest;
                }
                else
                {
                    response.httpResponse = $"Data Delievered to Topic '{r.Topic}'";
                    response.severity = "Successful";
                    response.StatusCode = HttpStatusCode.OK;
                }
            };

            using (var p = new ProducerBuilder<Null, string>(conf).Build())
            {
                try
                {
                    p.Produce(topic, new Message<Null, string> { Value = body }, null);
                    int result  = p.Flush(TimeSpan.FromSeconds(10));

                    if (result > 0)
                    {
                        return new RequestResponse { severity = "failure", httpResponse = "failure", StatusCode = HttpStatusCode.BadRequest };
                    }
                    else
                    {
                        return new RequestResponse { severity = "Success", httpResponse = "Success", StatusCode = HttpStatusCode.OK };
                    }
                }
                catch (ProduceException<Null, string> e)
                {
                    return new RequestResponse { severity = "failure", httpResponse = e.ToString(), StatusCode = HttpStatusCode.BadRequest };
                }
            }
        }
       
        public async Task<RequestResponse> PostJsonDataDigest(string baseAddress, string url, Dictionary<string, string> headers, string body, Dictionary<string, string> creadentials, IConfiguration _config)
        {
            string uname = "";
            string pass = "";
            foreach (var item in creadentials)
            {
                if (item.Key.Equals("UserName"))
                {
                    uname = item.Value;
                }
                if (item.Key.Equals("Password"))
                {
                    pass = item.Value;
                }
            }
            var credentialCache = new CredentialCache();
            Encoding encoding = Encoding.UTF8;
            credentialCache.Add(new Uri(baseAddress), "Digest", new NetworkCredential(uname, pass));
            var handler = new HttpClientHandler()
            {
                AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate,
                Credentials = credentialCache,
                Proxy = GetWebProxy(_config)
            };

            var httpClient = new HttpClient(handler);
            httpClient.Timeout = new TimeSpan(long.Parse((_config["GlobalSettings:ConnectionTimeoutForWebClient"])));

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    httpClient.DefaultRequestHeaders.TryAddWithoutValidation(header.Key, header.Value);
                }
            }
            httpClient.BaseAddress = new Uri(baseAddress);
            var result = await httpClient.PostAsync(url, new StringContent(body, encoding, "application/json")).ConfigureAwait(false);

            if (result.IsSuccessStatusCode)
            {
                return new RequestResponse { severity = "Success", httpResponse = result.Content.ReadAsStringAsync().Result, StatusCode = result.StatusCode };
            }
            else
            {
                return new RequestResponse { severity = "failure", httpResponse = result.Content.ReadAsStringAsync().Result, StatusCode = result.StatusCode };
            }

        }

        public async Task<RequestResponse> GetJsonDataDigest(string baseAddress, string url, Dictionary<string, string> headers, Dictionary<string, string> credentials, IConfiguration _config)
        {
            string uname = "";
            string pass = "";
            foreach (var item in credentials)
            {
                if (item.Key.Equals("UserName"))
                {
                    uname = item.Value;
                }
                if (item.Key.Equals("Password"))
                {
                    pass = item.Value;
                }
            }

            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };
            HttpClient client1 = new HttpClient(clientHandler);
            var credentialCache = new CredentialCache();
            Encoding encoding = Encoding.UTF8;
            credentialCache.Add(new Uri(baseAddress), "Digest", new NetworkCredential(uname, pass));
            var handler = new HttpClientHandler()
            {
                AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate,
                Credentials = credentialCache,
                Proxy = GetWebProxy(_config)
            };


            var httpClient = new HttpClient(handler);
            httpClient.Timeout = new TimeSpan(long.Parse((_config["GlobalSettings:ConnectionTimeoutForWebClient"])));

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    httpClient.DefaultRequestHeaders.TryAddWithoutValidation(header.Key, header.Value);
                }
            }

            httpClient.BaseAddress = new Uri(baseAddress);
            var result = await httpClient.GetAsync(url).ConfigureAwait(false);

            if (result.IsSuccessStatusCode)
            {
                return new RequestResponse { severity = "Success", httpResponse = result.Content.ReadAsStringAsync().Result, StatusCode = result.StatusCode };
            }
            else
            {
                return new RequestResponse { severity = "failure", httpResponse = result.Content.ReadAsStringAsync().Result, StatusCode = result.StatusCode };
            }
        }

        public async Task<RequestResponse> GetJsonData(string baseAddress, string url, Dictionary<string, string> headers, IConfiguration _config)
        {
            HttpClient client = GetHttpClient(_config);
            HttpClientHandler clientHandler = new HttpClientHandler();

            HttpClientHandler clientHandler1 = new HttpClientHandler();
            clientHandler1.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };
            client = new HttpClient(clientHandler1);

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    client.DefaultRequestHeaders.TryAddWithoutValidation(header.Key, header.Value);
                }
            }

            client.BaseAddress = new Uri(baseAddress);
            var result = await client.GetAsync(url);
            if (result.IsSuccessStatusCode)
            {
                return new RequestResponse { severity = "Success", httpResponse = result.Content.ReadAsStringAsync().Result, StatusCode = result.StatusCode };
            }
            else
            {
                return new RequestResponse { severity = "failure", httpResponse = result.Content.ReadAsStringAsync().Result, StatusCode = result.StatusCode };
            }
        }

        public async Task<RequestResponse> DeleteJsonData(string baseAddress, string url, Dictionary<string, string> headers, string body, IConfiguration _config)
        {
            HttpClient client = GetHttpClient(_config);

            HttpRequestMessage request = new HttpRequestMessage();
            request.Method = HttpMethod.Delete;
            request.Content = new StringContent(body, System.Text.Encoding.UTF8, "application/json");
            request.RequestUri = new Uri(baseAddress + url);
                       
            foreach (var header in headers)
            {
                request.Headers.TryAddWithoutValidation(header.Key, header.Value);
            }

            client.BaseAddress = new Uri(baseAddress);

            HttpResponseMessage response = null;
            response = await client.SendAsync(request);
            if (response.IsSuccessStatusCode)
            {
                return new RequestResponse { severity = "Success", httpResponse = response.Content.ReadAsStringAsync().Result, StatusCode = response.StatusCode };
            }
            else
            {
                return new RequestResponse { severity = "failure", httpResponse = response.Content.ReadAsStringAsync().Result, StatusCode = response.StatusCode };
            }
        }

        public async Task<RequestResponse> PutJsonData(string baseAddress, string url, Dictionary<string, string> headers, string body, IConfiguration _config)
        {
            HttpClient client = GetHttpClient(_config);
            HttpClientHandler clientHandler1 = new HttpClientHandler();
            clientHandler1.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };
            client = new HttpClient(clientHandler1);

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    client.DefaultRequestHeaders.TryAddWithoutValidation(header.Key, header.Value);
                }
            }

            client.BaseAddress = new Uri(baseAddress);
            var result = await client.PutAsync(url, new StringContent(body, Encoding.UTF8, "application/json")).ConfigureAwait(false);
            if (result.IsSuccessStatusCode)
            {

                return new RequestResponse { severity = "Success", httpResponse = result.Content.ReadAsStringAsync().Result, StatusCode = result.StatusCode };
            }
            else
            {
                return new RequestResponse { severity = "failure", httpResponse = result.Content.ReadAsStringAsync().Result, StatusCode = result.StatusCode };
            }
        }
        public async Task<RequestResponse> PostJsonData(string baseAddress, string url, Dictionary<string, string> headers, string body, IConfiguration _config)
        {
            HttpClient client = GetHttpClient(_config); HttpClientHandler clientHandler = new HttpClientHandler();

            HttpClientHandler clientHandler1 = new HttpClientHandler();
            clientHandler1.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };
            client = new HttpClient(clientHandler1);

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    client.DefaultRequestHeaders.TryAddWithoutValidation(header.Key, header.Value);
                }
            }

            client.BaseAddress = new Uri(baseAddress);
            var result = await client.PostAsync(url, new StringContent(body, Encoding.UTF8, "application/json")).ConfigureAwait(false);
            if (result.IsSuccessStatusCode)
            {

                return new RequestResponse { severity = "Success", httpResponse = result.Content.ReadAsStringAsync().Result, StatusCode = result.StatusCode };
            }
            else
            {
                return new RequestResponse { severity = "failure", httpResponse = result.Content.ReadAsStringAsync().Result, StatusCode = result.StatusCode };
            }
        }

        public HttpClient GetHttpClient(IConfiguration _config)
        {
            bool ProxyEnable = Convert.ToBoolean(_config["GlobalSettings:ProxyEnable"]);

            HttpClient client = null;
            if (!ProxyEnable)
            {
                client = new HttpClient();
            }
            else
            {
                string ProxyURL = _config["GlobalSettings:ProxyURL"];
                string ProxyUserName = _config["GlobalSettings:ProxyUserName"];
                string ProxyPassword = _config["GlobalSettings:ProxyPassword"];
                string[] ExceptionURL = _config["GlobalSettings:ExceptionURL"].Split(';');
                bool BypassProxyOnLocal = Convert.ToBoolean(_config["GlobalSettings:BypassProxyOnLocal"]);
                bool UseDefaultCredentials = Convert.ToBoolean(_config["GlobalSettings:UseDefaultCredentials"]);

                WebProxy proxy = new WebProxy
                {
                    Address = new Uri(ProxyURL),
                    BypassProxyOnLocal = BypassProxyOnLocal,
                    UseDefaultCredentials = UseDefaultCredentials,
                    BypassList = ExceptionURL,
                    Credentials = new NetworkCredential(ProxyUserName, ProxyPassword)

                };

                HttpClientHandler handler = new HttpClientHandler { Proxy = proxy };
                client = new HttpClient(handler,true);
            }
            client.Timeout =new TimeSpan(long.Parse( (_config["GlobalSettings:ConnectionTimeoutForWebClient"])));
            return client;
        }


        public WebProxy GetWebProxy(IConfiguration _config)
        {
            bool ProxyEnable = Convert.ToBoolean(_config["GlobalSettings:ProxyEnable"]);

            WebProxy WebProxy = null;
            if (!ProxyEnable)
            {
                WebProxy = new WebProxy();
            }
            else
            {
                string ProxyURL = _config["GlobalSettings:ProxyURL"];
                string ProxyUserName = _config["GlobalSettings:ProxyUserName"];
                string ProxyPassword = _config["GlobalSettings:ProxyPassword"];
                string[] ExceptionURL = _config["GlobalSettings:ExceptionURL"].Split(';');
                bool BypassProxyOnLocal = Convert.ToBoolean(_config["GlobalSettings:BypassProxyOnLocal"]);
                bool UseDefaultCredentials = Convert.ToBoolean(_config["GlobalSettings:UseDefaultCredentials"]);

                WebProxy proxy = new WebProxy
                {
                    Address = new Uri(ProxyURL),
                    BypassProxyOnLocal = BypassProxyOnLocal,
                    UseDefaultCredentials = UseDefaultCredentials,
                    BypassList = ExceptionURL,
                    Credentials = new NetworkCredential(ProxyUserName, ProxyPassword)

                };

                HttpClientHandler handler = new HttpClientHandler { Proxy = proxy };
                WebProxy = proxy;
            }
            return WebProxy;
        }
    }
}
