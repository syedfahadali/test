﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CommonModel.GenericCCMGenerate
{
    public class GenericCCMGenerate
    {
        //[Required(ErrorMessage = " is a mandatory field.")]
        //public SignOnRq SignOnRq { get; set; }
        //public RootObject RootObject { get; set; }
    }

    public class RootObject
    {
        public string templateId { get; set; }
        public object templateData { get; set; }
    }
}