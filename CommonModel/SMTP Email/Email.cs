﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonModel.SMTP_Email.Email
{
    public class EmailRequest
    {
        public string FromEmail { get; set; }
        public List<Recipient> Recipient { get; set; }
        public List<CC> CC { get; set; }
        public List<BCC> BCC { get; set; }
        public string EmailSubject { get; set; }
        public string EmailBody { get; set; }
        public List<Attachment> Attachments { get; set; }
    }
    public class Recipient
    {
        public string Name { get; set; }
        public string Email { get; set; }
    }

    public class CC
    {
        public string Name { get; set; }
        public string Email { get; set; }
    }

    public class BCC
    {
        public string Name { get; set; }
        public string Email { get; set; }
    }

    public class Attachment
    {
        public string FileName { get; set; }
        public string File { get; set; }
    }

    public class EmailResponse
    {
        public string Success { get; set; }
    }
    public class EmailErrorResponse
    {
        public string Success { get; set; }
    }

    public class MockResponse
    {
        public string Success { get; set; }
    }
}
