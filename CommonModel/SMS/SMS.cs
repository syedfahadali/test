﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommonModel.SMS.SMS
{
    public class SMSRequest
    {
        public string senderName { get; set; }
        public string messageType { get; set; }
        public int acknowledgement { get; set; }
        public int flashing { get; set; }
        public string messageText { get; set; }
        public string recipients { get; set; }

    }
    public class SentSMSID
    {
        public string SMSId { get; set; }
    }

    public class Data
    {
        public List<SentSMSID> SentSMSIDs { get; set; }
        public string InvalidRecipients { get; set; }
    }

    public class SMSResponse
    {
        public int replyCode { get; set; }
        public string replyMessage { get; set; }
        public string requestId { get; set; }
        public int clientRequestId { get; set; }
        public DateTime requestTime { get; set; }
        public Data data { get; set; }
    }
    public class SMSErrorResponse
    { 

    }

    public class SMSModel
    {
        public string Text { get; set; }
        public string[] Numbers { get; set; }

        public string senderName { get; set; }
        public string messageType { get; set; }
        public int acknowledgement { get; set; }
        public int flashing { get; set; }
    }
}
