﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net.Mail;
using System.Text;

namespace CommonModel
{
    public sealed class EmailAttribute : ValidationAttribute
    {

        public override bool IsValid(object value)
        {
            if (value != null && value.ToString() != "")
            {
                try
                {
                    MailAddress m = new MailAddress(value.ToString());
                    return true;
                }
                catch (FormatException)
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }
    }
}
