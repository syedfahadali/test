﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonModel.DucontEmail
{
    public class DucontEmailModel
    {
        //public SignOnRq SignOnRq { get; set; }
        public RootObject RootObject { get; set; }
    }

    public class TemplateVariable
    {
        public string varName { get; set; }
        public string varValue { get; set; }
    }

    public class RootObject
    {
        public string userId { get; set; }
        public string password { get; set; }
        public string channelId { get; set; }
        public string from { get; set; }
        public string to { get; set; }
        public string cc { get; set; }
        public string bcc { get; set; }
        public string subject { get; set; }
        public string type { get; set; }
        public string body { get; set; }
        public string templateId { get; set; }
        public List<TemplateVariable> templateVariables { get; set; }
        public string attachmentId { get; set; }
        public string attachmentBase64 { get; set; }
        public string attachmentFileName { get; set; }
    }

    public class ResponseRootObject
    {
        public string responseCode { get; set; }
        public string responseMessage { get; set; }
    }
}
