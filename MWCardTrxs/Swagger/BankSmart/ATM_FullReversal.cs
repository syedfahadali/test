﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWCardTrxs.Swagger.BankSmart.ATM_FullReversal
{
    //REQUEST
    public class ATM_FullReversalRequest : IExamplesProvider<FullReversal>
    {
        public FullReversal GetExamples()
        {
            return new FullReversal()
            {
                data = new DBHandler.Model.Host.FullReversalDto()
                {
                    RefNo = "99999905002282710730160720626229",
                    VISATrxID = "",
                    ProcCode = "00",
                    CAVV = "",
                    ResponseCode = "91"
                    
                }

            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
