﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWCardTrxs.Swagger.BankSmart.PreAuthTrx
{
    //REQUEST
    public class PreAuthTrxRequest : IExamplesProvider<PreAuthorizationDto>
    {
        public PreAuthorizationDto GetExamples()
        {
            return new PreAuthorizationDto()
            {
                data = new PreAuthorization()
                {
                    ATMID = "7.2",
                    AccountID = "0210100009839701",
                    Amount = Convert.ToDecimal(15.86),
                    USDAmount = Convert.ToDecimal(15.55),
                    OtherCurrencyAmount = Convert.ToDecimal(15.55),
                    USDRate = Convert.ToDecimal(1.019935),
                    RefNo = "99999926991089420728170813581019",
                    PHXDate = DateTime.Now,
                    MerchantType = "5999",
                    AckInstIDCode = "476135",
                    Currency = "784",
                    NameLocation = "TEST ACQUIRER UK         CITY NAME    QA",
                    SettlmntAmount = Convert.ToDecimal(15.55),
                    ConvRate = Convert.ToDecimal(1.019935),
                    AcqCountryCode = "634",
                    CurrCodeTran = "634",
                    CurrCodeSett = "784",
                    ForwardInstID = "500000",
                    ProcCode = "00",
                    POSEntryMode = "052",
                    POSConditionCode = "00",
                    POSPINCaptCode = "",
                    RetRefNum = "021011008274",
                    CardAccptID = "VTS ENME",
                    VISATrxID = "300210418930359",
                    CAVV = "",
                    ResponseCode = "",
                    MessageType = "0100",
                    ExtendedData = null
                    
                }
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
