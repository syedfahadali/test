﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWCardTrxs.Swagger.BankSmart.CashWithDrawalTrx
{
    //REQUEST
    public class CashWithDrawalTrxRequest : IExamplesProvider<CashWithDrawalDto>
    {
        public CashWithDrawalDto GetExamples()
        {
            return new CashWithDrawalDto()
            {
                data = new DBHandler.Model.Host.CashWithDrawal()
                {
                    CustomerBranchID = "01",
                    WithdrawlBranchID = "26",
                    ATMID = "10027763",
                    AccountID = "0100200000003901",
                    Amount = Convert.ToDecimal(487.5),
                    USDAmount = Convert.ToDecimal(0.0),
                    OtherCurrencyAmount = Convert.ToDecimal(0.0),
                    USDRate = Convert.ToDecimal(1.0),
                    Supervision = "C",
                    RefNo = "XXXXXXFFFFFFXXXX0531145139217580",
                    PHXDate = DateTime.Now,
                    MerchantType = "6011",
                    AckInstIDCode = "00000014397",
                    Currency = "784",
                    NameLocation = "ASFAR HOTEL APARTMENTS DUBAI ARE",
                    ProcCode = "01",
                    SettlmntAmount = Convert.ToDecimal (487.5),
                    ConvRate = Convert.ToDecimal(0.0),
                    AcqCountryCode = "784",
                    CurrCodeTran = "784",
                    CurrCodeSett = "784",
                    ForwardInstID = "500000",
                    MessageType = "0220",
                    OrgTrxRefNo = "XXXXXXFFFFFFXXXX0531145139717580",
                    POSEntryMode = "801",
                    POSConditionCode = "00",
                    POSPINCaptCode = "",
                    RetRefNum = "919512004655",
                    CardAccptID = "VTS ENME       ",
                    VISATrxID = "309199322492678",
                    CAVV = "",
                    ResponseCode = "",
                    ExtendedData = null
                }
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
