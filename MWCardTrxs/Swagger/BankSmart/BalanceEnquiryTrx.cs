﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWCardTrxs.Swagger.BankSmart.BalanceEnquiryTrx
{

    //REQUEST
    public class BalanceEnquiryTrxRequest : IExamplesProvider<BalanceEnquiryDto>
    {
        public BalanceEnquiryDto GetExamples()
        {
            return new BalanceEnquiryDto()
            {
                data = new DBHandler.Model.Host.BalanceEnquiry()
                {
                    ATMID = "TERMID01",
                    AccountID = "0210100000034101",
                    Supervision = "C",
                    RefNo = "0zk1r264xlcxxoj68hv8pxhjx2e7s7ni",
                    PHXDate = DateTime.Now,
                    MerchantType = "6011",
                    WithdrawlBranchID = "OR",
                    AckInstIDCode = "00000014397",
                    ProcCode = "31",
                    SettlmntAmount = Convert.ToDecimal(0.00),
                    ConvRate = 0,
                    AcqCountryCode = "784",
                    CurrCodeTran = "784",
                    CurrCodeSett = "784",
                    ForwardInstID = "999999",
                    MessageType = "0100",
                    OrgTrxRefNo = "",
                    POSEntryMode = "801",
                    POSConditionCode = "00",
                    POSPINCaptCode = "",
                    RetRefNum = "928877403434",
                    CardAccptID = "VTS ENME       ",
                    CardAccptNameLoc = "TEST ACQUIRER            CITY NAME    SG",
                    VISATrxID = "928755424458346",
                    CAVV = "",
                    ExtendedData = "",
                    ResponseCode = ""
                }

            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
