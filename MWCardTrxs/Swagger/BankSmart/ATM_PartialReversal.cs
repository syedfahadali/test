﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWCardTrxs.Swagger.BankSmart.ATM_PartialReversal
{
    //REQUEST
    public class ATM_PartialReversalRequest : IExamplesProvider<PartialReversal>
    {
        public PartialReversal GetExamples()
        {
            return new PartialReversal()
            {
                data = new DBHandler.Model.Host.PartialReversalDto()
                {
                    OrgTrxRefNo = "XXXXXXFFFFFFXXXX0313160444535342",
                    AmtActualTran = Convert.ToDecimal(702.950000),
                    AmtActualStl = Convert.ToDecimal(1000.0000000),
                    ActualTranFee = Convert.ToDecimal(0.0000000),
                    AccountStlFee = Convert.ToDecimal(0.0000000),
                    ForwardInstID = "500000",
                    ProcCode = "01",
                    ResponseCode = "",
                    VISATrxID = "",
                    ConvRate = Convert.ToDecimal(0.0)
                }

            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
