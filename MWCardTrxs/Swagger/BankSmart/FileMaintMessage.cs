﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWCardTrxs.Swagger.BankSmart.FileMaintMessage
{
    //REQUEST
    public class FileMaintMessageRequest : IExamplesProvider<FileManagementMsgDto>
    {
        public FileManagementMsgDto GetExamples()
        {
            return new FileManagementMsgDto()
            {
                data = new DBHandler.Model.Host.FileManagementMsg()
                {
                    RefNo = "999999FFFFFF57490213173137422074",
                    CardNumber = "9999998837970416",
                    DateTime = DateTime.Now,
                    STAN = "422074",
                    StatusType = "SC",
                    PreviousStatus = "NM",
                    CurrentStatus = "LT"
                }
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
