﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CoreHandler;
using DBHandler.Helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using DBHandler.Repositories;
using Microsoft.Extensions.Configuration;
using DBHandler.Enum;
using CommonModel;
using Newtonsoft.Json.Linq;
using Swashbuckle.AspNetCore.Annotations;
using System;
//using RuleEngineHandler;
using DBHandler.CreditProgramSearchByCIFModel;
using DBHandler.CreditProgramUpdateModel;
using DBHandler.PostChargesModel;
using DBHandler.Model.Loan;
using DBHandler.Model.Gurantee;
using System.Globalization;
using DigiBancMiddleware.Helper;

namespace VendorApi.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class BankSmartController : ControllerBase
    {
        private readonly HttpHandler.HttpHandler httpHandler;
        private readonly IUserChannelsRepository _userChannels;
        private readonly ILogRepository _logs;
        private readonly IConfiguration _config;
        private Level level = Level.Info;
        private Exception myEx;
        private ControllerBase _controllerBase;
        private Exception excetionForLog;

        public BankSmartController(IConfiguration config, IUserChannelsRepository userChannels, IHttpClientFactory httpClientFactory, ILogRepository logs, ControllerBase controllerbase = null)
        {
            _controllerBase = controllerbase;
            _config = config;
            _userChannels = userChannels;
            _logs = logs;
            httpHandler = new HttpHandler.HttpHandler(httpClientFactory);
        }


        [Route("BalanceInquriy")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Balance>", typeof(ActiveResponseSucces<Balance>))]
        public async Task<IActionResult> BalanceInquriy([FromBody] BalanceInquiryModel Model)
        {
            
            

            ActiveResponseSucces<Balance> response = new ActiveResponseSucces<Balance>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:BalanceInquriy"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<Balance>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    string specialChar = _config["GlobalSettings:SpecialChar"];
                    if (HttpContext.Request.Headers["ChannelId"] == _config["GlobalSettings:ECSCannel"])
                    {
                        response.Content.Name = response.Content.Name != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Name, specialChar) : null;
                        response.Content.ProductType = response.Content.ProductType != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ProductType, specialChar) : null;
                        response.Content.ProductDesc = response.Content.ProductDesc != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ProductDesc, specialChar) : null;
                    }

                     return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetCIFAccountsBalanceInq")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<Balance>>", typeof(ActiveResponseSucces<List<Balance>>))]
        public async Task<IActionResult> GetCIFAccountsBalanceInq([FromBody] RetailCustomerInquiryModel Model)
        {
            
            
            ActiveResponseSucces<List<Balance>> response = new ActiveResponseSucces<List<Balance>>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:GetCIFAccountsBalanceInq"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<Balance>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }
        
        [Route("GetCIFAccountsBalanceInqEBoss")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DBHandler.Model.BalanceInquiryEBossResult>>", typeof(ActiveResponseSucces<List<DBHandler.Model.BalanceInquiryEBossResult>>))]
        public async Task<IActionResult> GetCIFAccountsBalanceInqEBoss([FromBody] DBHandler.Model.RetailCustomerInquiryEBossModel Model)
        {
            
            
            ActiveResponseSucces<List<DBHandler.Model.BalanceInquiryEBossResult>> response = new ActiveResponseSucces<List<DBHandler.Model.BalanceInquiryEBossResult>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:GetCIFAccountsBalanceInqEBoss"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<DBHandler.Model.BalanceInquiryEBossResult>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }
        
        //todo this need to fix as account model need to have validations

        [Route("CloseAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<Balance>>", typeof(ActiveResponseSucces<List<Balance>>))]
        public async Task<IActionResult> CloseAccount([FromBody] BalanceInquiryModel Model)
        {
            
            
            ActiveResponseSucces<List<Balance>> response = new ActiveResponseSucces<List<Balance>>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:CloseAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<Balance>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }
        
        [Route("CurrencyPairsinquiry")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<CurrencyPairing>>", typeof(ActiveResponseSucces<List<CurrencyPairing>>))]
        public async Task<IActionResult> CurrencyPairsinquiry([FromBody] DebitCardCategoryInquiryView Model)
        {
            
            
            ActiveResponseSucces<List<CurrencyPairing>> response = new ActiveResponseSucces<List<CurrencyPairing>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:CurrencyPairsinquiry"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<CurrencyPairing>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetStatement")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<CustomerStatement>>", typeof(ActiveResponseSucces<List<CustomerStatement>>))]
        public async Task<IActionResult> GetStatement([FromBody] GetStatementView Model)
        {
            
            
            ActiveResponseSucces<List<CustomerStatement>> response = new ActiveResponseSucces<List<CustomerStatement>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:GetStatement"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<CustomerStatement>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("AddRealTimeSweeps")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> AddRealTimeSweeps([FromBody] RealTimeSweepView1 Model)
        {
            
            
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:AddRealTimeSweeps"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("ModifyRealTimeSweeps")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> ModifyRealTimeSweeps([FromBody] RealTimeSweepView1 Model)
        {
            
            
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:ModifyRealTimeSweeps"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("CancelRealTimeSweeps")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> CancelRealTimeSweeps([FromBody] CancelRealTimeSweepView Model)
        {
            
            
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:CancelRealTimeSweeps"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetRealTimeSweep")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<RealTimeSweepResponse1>))]
        public async Task<IActionResult> GetRealTimeSweep([FromBody] CancelRealTimeSweepView Model)
        {
            
            
            ActiveResponseSucces<RealTimeSweepResponse1> response = new ActiveResponseSucces<RealTimeSweepResponse1>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:GetRealTimeSweep"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<RealTimeSweepResponse1>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }


        [Route("AddInternalAccountExcessSweeps")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> AddInternalAccountExcessSweeps([FromBody] InternalSweepView Model)
        {
            
            
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:AddInternalAccountExcessSweeps"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("ModifyInternalAccountExcessSweeps")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> ModifyInternalAccountExcessSweeps([FromBody] InternalSweepView Model)
        {
            
            
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:ModifyInternalAccountExcessSweeps"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("CancelInternalAccountExcessSweeps")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> CancelInternalAccountExcessSweeps([FromBody] CancelRealTimeSweepView Model)
        {
            
            
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:CancelInternalAccountExcessSweeps"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetInternalAccountExcessSweeps")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<RealTimeSweepResponse>))]
        public async Task<IActionResult> GetInternalAccountExcessSweeps([FromBody] CancelRealTimeSweepView Model)
        {
            
            
            ActiveResponseSucces<RealTimeSweepResponse> response = new ActiveResponseSucces<RealTimeSweepResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:GetInternalAccountExcessSweeps"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<RealTimeSweepResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }


        [Route("AddInternalAccountShortageSweeps")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> AddInternalAccountShortageSweeps([FromBody] InternalSweepView Model)
        {
            
            
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:AddInternalAccountShortageSweeps"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("ModifyInternalAccountShortageSweeps")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> ModifyInternalAccountShortageSweeps([FromBody] InternalSweepView Model)
        {
            
            
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:ModifyInternalAccountShortageSweeps"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("CancelInternalAccountShortageSweeps")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> CancelInternalAccountShortageSweeps([FromBody] CancelRealTimeSweepView Model)
        {
            
            
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:CancelInternalAccountShortageSweeps"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetInternalAccountShortageSweeps")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<RealTimeSweepResponse>))]
        public async Task<IActionResult> GetInternalAccountShortageSweeps([FromBody] CancelRealTimeSweepView Model)
        {
            
            
            ActiveResponseSucces<RealTimeSweepResponse> response = new ActiveResponseSucces<RealTimeSweepResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:GetInternalAccountShortageSweeps"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<RealTimeSweepResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

       

        

        

        

        

        

        

        

        


        

        


        


        

        


        

        


        

        [Route("PostCharges")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CreditProgramUpdateResponse>", typeof(ActiveResponseSucces<PostChargesResponse>))]
        public async Task<IActionResult> PostCharges([FromBody] PostChargesModel Model)
        {
            
            
            ActiveResponseSucces<PostChargesResponse> response = new ActiveResponseSucces<PostChargesResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:PostCharges"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<PostChargesResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }


        [Route("TreasuryTransfer")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<TransferRs>", typeof(ActiveResponseSucces<TransferRs>))]
        public async Task<IActionResult> TreasuryTransfer([FromBody] MultiBranchRq Model)
        {
            
            
            ActiveResponseSucces<TransferRs> response = new ActiveResponseSucces<TransferRs>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:TreasuryTransfer"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<TransferRs>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("MultiBranch")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<TransferRs>", typeof(ActiveResponseSucces<TransferRs>))]
        public async Task<IActionResult> MultiBranch([FromBody] MultiBranchRq Model)
        {
            
            
            ActiveResponseSucces<TransferRs> response = new ActiveResponseSucces<TransferRs>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:multibranch"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<TransferRs>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("BalanceInquiry")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Balance>", typeof(ActiveResponseSucces<Balance>))]
        public async Task<IActionResult> BalanceInquiry([FromBody] BalanceInquiryModel Model)
        {

            ActiveResponseSucces<Balance> response = new ActiveResponseSucces<Balance>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:BalanceInquriy"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<Balance>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    string specialChar = _config["GlobalSettings:SpecialChar"];
                    if (HttpContext.Request.Headers["ChannelId"] == _config["GlobalSettings:ECSCannel"])
                    {
                        response.Content.Name = response.Content.Name != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.Name, specialChar) : null;
                        response.Content.ProductType = response.Content.ProductType != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ProductType, specialChar) : null;
                        response.Content.ProductDesc = response.Content.ProductDesc != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ProductDesc, specialChar) : null;
                    }

                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("transfer")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<TransferRs>", typeof(ActiveResponseSucces<TransferRs>))]
        public async Task<IActionResult> transfer([FromBody] TransferRq Model)
        {
            ActiveResponseSucces<TransferRs> response = new ActiveResponseSucces<TransferRs>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:DoTransfer"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.Content = new TransferRs() { ChannelRefId = Model.ChannelRefId };
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<TransferRs>>(result.httpResult.httpResponse);
                if (response.Content == null)
                {
                    response.Content = new TransferRs() { ChannelRefId = Model.ChannelRefId };
                }
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("GetPaymentStatus")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponseSucces<TransferResponse>), 200)]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<TransferStatusResponse> ", typeof(ActiveResponseSucces<TransferStatusResponse>))]
        public async Task<IActionResult> GetPaymentStatus([FromBody] TransferStatusModel Model)
        {
            ActiveResponseSucces<TransferStatusResponse> response = new ActiveResponseSucces<TransferStatusResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:GetTransferStatus"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<TransferStatusResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("GetNarrations")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponseSucces<List<TransactionDescription>>), 200)]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<TransactionDescription>>", typeof(ActiveResponseSucces<List<TransactionDescription>>))]
        public async Task<IActionResult> GetNarrations([FromBody] NarrationModel Model)
        {
            ActiveResponseSucces<List<TransactionDescription>> response = new ActiveResponseSucces<List<TransactionDescription>>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:GetNarations"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<TransactionDescription>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }


        [Route("GetSweepDetail")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.GetSweepDetial.GetSweepDetialResponse>", typeof(ActiveResponseSucces<DBHandler.Model.GetSweepDetial.GetSweepDetialResponse>))]
        public async Task<IActionResult> GetSweepDetail([FromBody] DBHandler.Model.GetSweepDetial.GetSweepDetialModel Model)
        {
            ActiveResponseSucces<DBHandler.Model.GetSweepDetial.GetSweepDetialResponse> response = new ActiveResponseSucces<DBHandler.Model.GetSweepDetial.GetSweepDetialResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:GetSweepDetail"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.GetSweepDetial.GetSweepDetialResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("MultiBranchV2")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<TransferRsV2>", typeof(ActiveResponseSucces<TransferRsV2>))]
        public async Task<IActionResult> MultiBranchV2([FromBody] MultiBranchRqV2 Model)
        {
            ActiveResponseSucces<TransferRsV2> response = new ActiveResponseSucces<TransferRsV2>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:MultiBranchV2"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<TransferRsV2>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("InquireCostCenter")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<InquireCostCenterResponse>>", typeof(ActiveResponseSucces<List<InquireCostCenterResponse>>))]
        public async Task<IActionResult> InquireCostCenter([FromBody] InquireCostCenterDTO Model)
        {
            ActiveResponseSucces<List<InquireCostCenterResponse>> response = new ActiveResponseSucces<List<InquireCostCenterResponse>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTransfersAndSweepsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTransfersAndSweepsAPI:InquireCostCenter"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<InquireCostCenterResponse>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }





    }
}
