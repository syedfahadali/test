﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWTransfersAndSweeps.Swagger.BankSmart.CancelInternalAccountExcessSweeps
{
    
    //REQUEST
    public class CancelInternalAccountExcessSweepsRequest : IExamplesProvider<CancelRealTimeSweepView>
    {
        public CancelRealTimeSweepView GetExamples()
        {
            return new CancelRealTimeSweepView()
            {
                AccountID = "0163500000070902"
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}

}
