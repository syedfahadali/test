﻿using DBHandler.Helper;
using DBHandler.Model;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWTransfersAndSweeps.Swagger.BankSmart.GetCIFAccountsBalanceInqEBoss
{
    //REQUEST
    public class GetCIFAccountsBalanceInqEBossRequest : IExamplesProvider<DBHandler.Model.RetailCustomerInquiryEBossModel>
    {
        public DBHandler.Model.RetailCustomerInquiryEBossModel GetExamples()
        {
            return new DBHandler.Model.RetailCustomerInquiryEBossModel()
            {
                ClientID="000000988"

            };
        }
    }

    //RESPONSE
//    public class GetCIFAccountsBalanceInqEBossResponse : IExamplesProvider<ActiveResponseSucces<DBHandler.Model.Dtos.CustomerCorporateView>>
//    {
//        public ActiveResponseSucces<DBHandler.Model.Dtos.CustomerCorporateView> GetExamples()
//        {
//            return new ActiveResponseSucces<DBHandler.Model.Dtos.CustomerCorporateView>()
//            {
//                LogId = "",
//                Status = new Status()
//                {
//                    Code = "MSG-000000",
//                    Severity = "Success",
//                    StatusMessage = "Success"
//                },
//                Content = new DBHandler.Model.Dtos.CustomerCorporateView()
//                {


//                }

//            };
//        }
//    }
}
