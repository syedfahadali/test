﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWTransfersAndSweeps.Swagger.BankSmart.ModifyInternalAccountExcessSweeps
{
    
    //REQUEST
    public class ModifyInternalAccountExcessSweepsRequest : IExamplesProvider<InternalSweepView>
    {
        public InternalSweepView GetExamples()
        {
            return new InternalSweepView()
            {
                AccountID = "0310100000050101",
                SIAccountID = "0310100000050102",
                Amount = 15
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}

}
