﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWTransfersAndSweeps.Swagger.BankSmart.AddInternalAccountExcessSweeps
{
    
    //REQUEST
    public class AddInternalAccountExcessSweepsRequest : IExamplesProvider<InternalSweepView>
    {
        public InternalSweepView GetExamples()
        {
            return new InternalSweepView()
            {
                AccountID = "0310100000067102",
                SIAccountID = "0310100000067101",
                Amount = 10
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}

}
