﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMSHandler.CardHolderInfo
{
    public class CardHolderInfoRequest
    {
        public string instId { get; set; }
        public string customerNumber { get; set; }
        public string cardId { get; set; }
    }
    public class CardHolderInfoResponse
    {
    }
    public class CardHolderInfoErrorResponse
    {
    }

}
