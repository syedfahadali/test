﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMSHandler.CustomerInfoByIdType
{
    public class CustomerInfoByIdTypeRequest
    {
        public string instId { get; set; }
        public string idType { get; set; }
        public string idNumber { get; set; }
    }

    public class CustomerInfoByIdTypeResponse
    {
        public long customerId { get; set; }
        public string customerNumber { get; set; }
        public string nationalityCode { get; set; }
        public string nationality { get; set; }
        public string firstName { get; set; }
        public string secondName { get; set; }
        public string surname { get; set; }
        public string gender { get; set; }
        public string birthday { get; set; }
        public string placeOfBirth { get; set; }
    }
    public class CustomerInfoByIdTypeErrorResponse
    {
    }
}
