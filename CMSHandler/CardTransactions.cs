﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMSHandler.CardTransactions
{
    public class CardTransactionsRequest
    {
        public string instId { get; set; }
        public string customerNumber { get; set; }
        public string cardId { get; set; }
        public string transactionSearchBody { get; set; }
    }

    public class CardTransactionsResponse
    {
    }
    public class CardTransactionsErrorResponse
    {
    }
}
