﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMSHandler.CardReissue
{
    public class CardReissueRequest
    {
        public string instId { get; set; }
        public string customerNumber { get; set; }
        public string cardId { get; set; }
    }

    public class CardReissueResponse
    {
    }
    public class CardReissueErrorResponse
    {
    }
}
