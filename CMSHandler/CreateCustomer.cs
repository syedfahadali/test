﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMSHandler.CreateCustomer
{
    public class CreateCustomerRequest
    {
        public string instId { get; set; }
        public string operatorId { get; set; }
        public int agentId { get; set; }
        public customer customer { get; set; }
    }

    public class address
    {
        public string country { get; set; }
        public string city { get; set; }
        public string region { get; set; }
        public string street { get; set; }
        public string house { get; set; }
    }

    public class Contact
    {
        public string email { get; set; }
        public string phone { get; set; }
        public string preferredLang { get; set; }
    }

    public class Cardholder
    {
        public string embossedFirstName { get; set; }
        public string embossedSurName { get; set; }
    }

    public class Card
    {
        public string cardUId { get; set; }
        public string cardTypeId { get; set; }
        public Cardholder cardholder { get; set; }
    }

    public class Contract
    {
        public Card card { get; set; }
        public string productNumber { get; set; }
    }

    public class IdentityCard
    {
        public string idExpireDate { get; set; }
        public string idNumber { get; set; }
        public string idType { get; set; }
    }

    public class PersonName
    {
        public string firstName { get; set; }
        public string secondName { get; set; }
        public string surname { get; set; }
    }

    public class Person
    {
        public string birthday { get; set; }
        public string gender { get; set; }
        public IdentityCard IdentityCard { get; set; }
        public PersonName PersonName { get; set; }
        public string placeOfBirth { get; set; }
    }

    public class customer
    {
        public address address { get; set; }
        public Contact contact { get; set; }
        public Contract Contract { get; set; }
        public string customerNumber { get; set; }
        public string nationality { get; set; }
        public Person Person { get; set; }
    }

    public class CreateCustomerResponse
    {
    }
    public class CreateCustomerErrorResponse
    {
    }
}
