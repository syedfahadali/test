﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMSHandler.CloseCard
{
    public class CloseCardRequest
    {
        public string operatorId { get; set; }
        public string instId { get; set; }
        public string agentId { get; set; }
        public string customerNumber { get; set; }
        public string contractNumber { get; set; }
        public string cardId { get; set; }
    }
    public class CloseCardResponse
    {

    }
    public class CloseCardErrorResponse
    {

    }
}
