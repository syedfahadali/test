﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMSHandler.Authentication
{
    public class AuthenticationRequest
    {
        
    }

    public class AuthenticationResponse
    {
        public bool upgraded { get; set; }
        public string access_token { get; set; }
        public int expires_in { get; set; }
        public int refresh_expires_in { get; set; }
        public string refresh_token { get; set; }
        public string token_type { get; set; }
        public object id_token { get; set; }

        //[JsonProperty("not-before-policy")]
        public int NotBeforePolicy { get; set; }
        public object session_state { get; set; }
        public object scope { get; set; }
    }

    public class AuthenticationErrorResponse
    {

    }

}
