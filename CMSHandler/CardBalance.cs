﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMSHandler.CardBalance
{
    public class CardBalanceRequest
    {
        public string instId { get; set; }
        public string customerNumber { get; set; }
        public string cardId { get; set; }
    }
    public class CardBalanceResponse
    {
        public string balance { get; set; }
        public string currencyISOCode { get; set; }
        public string currencyCode { get; set; }
        public string currencyName { get; set; }
    }
    public class CardBalanceErrorResponse
    {
    }
}
