﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMSHandler.DeactivateCard
{
    public class DeactivateCardRequest
    {
        public string instId { get; set; }
        public string customerNumber { get; set; }
        public string cardId { get; set; }
    }
    public class DeactivateCardResponse
    {
    }
    public class DeactivateCardErrorResponse
    {
    }
}
