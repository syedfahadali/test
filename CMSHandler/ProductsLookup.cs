﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMSHandler.ProductsLookup
{
    public class ProductsLookupRequest
    {
        public string instId { get; set; }
    }
    public class ProductsLookupErrorResponse
    {
    }
    public class ProductsLookupResponse
    {
        public string code { get; set; }
        public string cardTypeId { get; set; }
        public string desc { get; set; }
        public bool isDefault { get; set; }
    }
}
