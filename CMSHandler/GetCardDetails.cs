﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMSHandler.GetCardDetails
{
    public class GetCardDetailsRequest
    {
        public string instId { get; set; }
        public string customerNumber { get; set; }
        public string cardId { get; set; }
    }
    public class GetCardDetailsResponse
    {
        public object issDate { get; set; }
        public string expirDate { get; set; }
        public string id { get; set; }
        public string personName { get; set; }
        public string cardType { get; set; }
        public string status { get; set; }
        public string state { get; set; }
        public string cardNumber { get; set; }
        public string cardMask { get; set; }
        public string seqNumber { get; set; }
        public string network { get; set; }
        public string category { get; set; }
        public string agentName { get; set; }
        public string agentNumber { get; set; }
        public string productNumber { get; set; }
        public string productName { get; set; }
        public int counterInvalidPin { get; set; }
        public string stateChangeReason { get; set; }
        public string statusChangeReason { get; set; }
        public string stateChangeDate { get; set; }
        public object statusChangeDate { get; set; }
        public string accountNumber { get; set; }
        public object creditLimit { get; set; }
        public object currency { get; set; }
        public double avalBalance { get; set; }
        public double holdBalance { get; set; }
        public double currentBalance { get; set; }
        public bool @virtual { get; set; }
    }
    public class GetCardDetailsErrorResponse
    {
    }
}
