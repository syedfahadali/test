﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMSHandler.CardTransactionDetails
{
    public class CardTransactionDetailsRequest
    {
        public int instId { get; set; }
        public string customerNumber { get; set; }
        public string cardId { get; set; }
        public string transactionId { get; set; }
    }
    public class CardTransactionDetailsResponse
    {
    }
    public class CardTransactionDetailsErrorResponse
    {
    }
}
