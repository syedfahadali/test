﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMSHandler.LinkexistingCustomerwithCard
{
    public class LinkexistingCustomerwithCardRequest
    {
        public string instId { get; set; }
        public string customerNumber { get; set; }
        public string accountNumber { get; set; }
        public int agentId { get; set; }
        public string operatorId { get; set; }
        public string cardUId { get; set; }
        public string cardTypeId { get; set; }
        public string embossedFirstName { get; set; }
        public string embossedSurName { get; set; }
        public string productNumber { get; set; }
    }
    public class LinkexistingCustomerwithCardResponse
    {
    }
    public class LinkexistingCustomerwithCardErrorResponse
    {
    }
}
