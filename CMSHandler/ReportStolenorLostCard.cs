﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMSHandler.ReportStolenorLostCard
{
    public class ReportStolenorLostCardRequest
    {
        public string instId { get; set; }
        public string customerNumber { get; set; }
        public string cardId { get; set; }
    }
    public class ReportStolenorLostCardResponse
    {
    }
    public class ReportStolenorLostCardErrorResponse
    {
    }
}
