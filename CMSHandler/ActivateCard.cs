﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMSHandler.ActivateCard
{
    public class ActivateCardRequest
    {
        public string instId { get; set; }
        public string customerNumber { get; set; }
        public string cardId { get; set; }
    }
    public class ActivateCardResponse
    {
    }
    public class ActivateCardErrorResponse
    {
    }
}
