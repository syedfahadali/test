﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMSHandler.CustomerDetails
{
    public class CustomerDetailsRequest
    {
        public string instId { get; set; }
        public string customerNumber { get; set; }
    }
    public class Document
    {
        public string idType { get; set; }
        public string idTypeName { get; set; }
        public string idNumber { get; set; }
        public string idExpireDate { get; set; }
    }

    public class Address
    {
        public string country { get; set; }
        public string countryName { get; set; }
        public string region { get; set; }
        public string city { get; set; }
        public string street { get; set; }
        public string house { get; set; }
    }

    public class Contact
    {
        public string preferredLang { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
    }

    public class CustomerDetailsDTO
    {
        public long customerId { get; set; }
        public string customerNumber { get; set; }
        public string nationalityCode { get; set; }
        public string nationality { get; set; }
        public string firstName { get; set; }
        public string secondName { get; set; }
        public string surname { get; set; }
        public string gender { get; set; }
        public string birthday { get; set; }
        public string placeOfBirth { get; set; }
    }

    public class CustomerDetailsResponse
    {
        public string mobileNumber { get; set; }
        public string birthday { get; set; }
        public long customerId { get; set; }
        public List<Document> documents { get; set; }
        public Address address { get; set; }
        public Contact contact { get; set; }
        public CustomerDetailsDTO customerDetailsDTO { get; set; }
    }
    public class CustomerDetailsErrorResponse
    {
    }
}
