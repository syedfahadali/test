﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMSHandler.CustomerCardList
{
    public class CustomerCardListRequest
    {
        public string instId { get; set; }
        public string customerNumber { get; set; }
    }
    public class CustomerCardListResponse
    {
    }
    public class CustomerCardListErrorResponse
    {
    }
}
