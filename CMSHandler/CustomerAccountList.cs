﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMSHandler.CustomerAccountList
{
    public class CustomerAccountListRequest
    {
        public string instId { get; set; }
        public string customerId { get; set; }
    }

    public class CustomerAccountListResponse
    {
    }

    public class CustomerAccountListErrorResponse
    {
    }
}
