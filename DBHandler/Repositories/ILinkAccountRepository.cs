﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using DBHandler.Model;
using DBHandler.Model.Dtos;

namespace DBHandler.Repositories
{
    //LinkAccount
    public interface ILinkAccountRepository : IRepository<LinkAccount>
    {
        //IEnumerable<LibsharaWinner> GetAllWinners();
        //IQueryable<LinkAccount> GetAll(;
        List<PdfMonth> GetSignatureReportPerDay( DateTime startDate, DateTime EndDate);
        //LinkAccount GetByAccountNo(string AccountNo, string UserId);
        //LinkAccount GetByAccountId( int companyId, int accountId);
        //LinkAccount GetByProductAccountId( int companyId, int productId, int accountId, string customerid);
        //LinkAccount GetByProductUnverifiedAccount( int companyId, int productId, string customerid);
        //LinkAccount GetByProductUnverifiedAccount( int companyId, int productId, int accountId, string customerID);
        //LinkAccount GetByProductAccountIdRA( int companyId, int productId, string customerID);
        //LinkAccount GetVerifiedAccount( int companyId, int productId, string customerID);
        //LinkAccount GetAcountDetails( int companyId, int productId, string customerID);
        //LinkAccount GetbyUserID( string customerID);
        //LinkAccount GetByProductAccount( int productId, string Account);
        //LinkAccount GetStatusbyUserID( string customerID);
        //List<LinkAccount> GetRimNobyUserID( string customerID);
        //TODO://Rameez
        //LinkAccount GetAccountbyUserIdProductId( int companyId, string customerId, int productId);
        List<LinkAccount> GetUserWakalaAccount(  string userId);
        List<LinkAccount> GetUserFlexiAccount(  string userId);
        //LinkAccount GetAccountbyUserIdProductCode( int companyId, string customerId, string productCode);
        //LinkAccount GetAcountDetailsbyAccountNo( int companyId, int productId, string customerID, string AccountNo);
        //LinkAccount GetVerifiedAccountexisting( int companyId, int productId, string customerID);
       // List<LinkAccount> GetByProductId( int companyId, string clientId, int productId);
        IQueryable<LinkAccount> GetByProductCode(  int companyId, string clientId, string productCode);


        List<PdfMonth> GetSignatureReport(  DateTime dt);
    }
}
