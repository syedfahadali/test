﻿using DBHandler.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Repositories
{
    public interface IUserChannelsRepository: IRepository<UserChannels>
    {
        string ValidateChannel(string userId,string functionName, string bankid, string branchid);
        Model.Version GetAPIVersion();
        Currencies GetCurrencies(string CurrencyID);
    }
}
