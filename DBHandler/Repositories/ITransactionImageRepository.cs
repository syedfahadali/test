﻿using DBHandler.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Repositories
{
   public interface ITransactionImageRepository: IRepository<TransactionImage>
    {
    }
}
