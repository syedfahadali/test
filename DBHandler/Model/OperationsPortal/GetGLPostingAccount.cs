﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model.OperationsPortal
{
    public class GetGLPostingAccount
    {
        
        [MinLength(10, ErrorMessage = "- Valid Min length should not be less than 10.")]
        [MaxLength(10, ErrorMessage = "- Character length should not be more than 10.")]
        public string AccountID { get; set; }

        
        [StringRangeAttribute(AllowableValues = new[] { "Debit", "Credit", }, ErrorMessage = "- Valid values are Debit or Credit")]
        public string TrxType { get; set; }

        
        public string OperatorID { get; set; }

        
        [MinLength(1, ErrorMessage = "- Valid Min length should not be less than 1.")]
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        public string ModuleType { get; set; }

        
        [MinLength(10, ErrorMessage = "- Valid Min length should not be less than 1.")]
        [MaxLength(10, ErrorMessage = "- Character length should not be more than 1.")]
        public string GLRealTime { get; set; }
        public string IsSystem { get; set; }
    }

    public class GetGLPostingAccountResponse
    {
        public string RetStatus { get; set; }
        public decimal? ClearBalance { get; set; }
        public decimal? ShadowBalance { get; set; }
        public string AccountName { get; set; }
        public string CurrencyID { get; set; }
        public string CurrencyName { get; set; }
        public string GLID { get; set; }
        public decimal? MeanRate { get; set; }
    }
}