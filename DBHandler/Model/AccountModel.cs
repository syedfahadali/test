﻿using CommonModel;
using CoreHandler.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model
{
    public class AccountModel
    {
        
        
        public Account Acc { get; set; }
    }

    public class AccountUpdateModel
    {
       
        public string AccountID { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "" }, ErrorMessage = "- Valid value is either N,D,W,M,Q,H or Y")]
        public string Status { get; set; }
    }
}
