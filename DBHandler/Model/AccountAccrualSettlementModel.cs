﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.AccountAccrualSettlementModel
{
    public class AccountAccrualSettlementModel
    {
        

        [Required(ErrorMessage = " is a mandatory field.")]
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        public string AccountId { get; set; }
    }
    public class AccountAccrualSettlementResponse
    {

        public string AccountId { get; set; }
        public string SavingAccrualSettlementRefNo { get; set; }
        public string ODAccrualSettlementRefNo { get; set; }
        public decimal? SavingAccrual { get; set; }
        public decimal? ODAccrual { get; set; }
        public decimal TotalBalance { get; set; }


    }
}
