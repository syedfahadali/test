﻿using DBHandler.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model.Dtos
{
    public class FatcaScreen
    {
        public bool UsCitizen { get; set; }
        public bool UsTaxResident { get; set; }
        public bool BornInUs { get; set; }
        public bool TaxResidentUsOrUAE { get; set; }
        public BaseClass BaseClass { get; set; }
    }
}
