﻿using DBHandler.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DBHandler.Model.Dtos
{
    public class IdNoDto
    {
        [Display(Name = "Id No.")]
        [Required(ErrorMessage = "Please enter {0}")]
        //[RegularExpression("^[0-9]+$", ErrorMessage = "Please enter a valid {0}")]
     //   [StringLength(9, ErrorMessage = "{0} must be {1} characters long.", MinimumLength = 9)]
        public string IdNo { get; set; }
    }
    public class OpenWakalaIfxDto
    {
        public string LogId { get; set; }

        [Display(Name = "Rim No.")]
        [Required(ErrorMessage = "Please enter {0}")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "Please enter a valid {0}")]
        [StringLength(15, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string RimNo { get; set; }

        [Required(ErrorMessage = "Please enter Title Line 1.")]
        [StringLength(40, ErrorMessage = "Title Line 1 must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string TitleLine1 { get; set; }

        [Required(ErrorMessage = "Please enter Title Line 2.")]
        [StringLength(40, ErrorMessage = "Title Line 2 must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string TitleLine2 { get; set; }

        [Required(ErrorMessage = "Please enter Account Type.")]
        [StringLength(3, ErrorMessage = "Please enter a valid Account Type.", MinimumLength = 1)]
        public string AcctType { get; set; }

        [Required(ErrorMessage = "Please enter Account Key.")]
        [StringLength(3, ErrorMessage = "Please enter a valid Account Key.", MinimumLength = 1)]
        [RegularExpression("^([0-9])+$", ErrorMessage = "Please enter a valid Account Key")]
        public string AcctKey { get; set; }

        [Required(ErrorMessage = "Please enter Face Account Type.")]
        //    [StringLength(3, ErrorMessage = "Please enter a valid Account Type.", MinimumLength = 1)]
        public string FaceAcctType { get; set; }

        [Required(ErrorMessage = "Please enter Face Account ")]
        //     [StringLength(3, ErrorMessage = "Please enter a valid Account Type.", MinimumLength = 1)]
        public string FaceAcct { get; set; }

        [Required(ErrorMessage = "Please enter Transfer Type ")]
        //     [StringLength(3, ErrorMessage = "Please enter a valid Account Type.", MinimumLength = 1)]
        public string TransferType { get; set; }

        [Required(ErrorMessage = "Please enter Transfer Type ")]

        public double FaceAmount { get; set; }

    }

    public class HoldAmountDTO
    {
        public string AccountNo { get; set; }


        public string AccountType { get; set; }
        public string Amount { get; set; }
        public string Currency { get; set; }
        public int ExpireDays { get; set; }
        public string Description { get; set; }
    }

    public class RimNoDto
    {
        [Display(Name = "Rim No.")]
        [Required(ErrorMessage = "Please enter {0}")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "Please enter a valid {0}")]
        [StringLength(15, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string RimNo { get; set; }
    }

    public class RimCreateDto
    {
        public string LogId { get; set; }

        [Required(ErrorMessage = "Please enter Last Name.")]
        [StringLength(40, ErrorMessage = "Last Name must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please enter First Name.")]
        [StringLength(40, ErrorMessage = "First Name must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Please enter Middle Name.")]
        [StringLength(40, ErrorMessage = "Middle Name must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string MiddleName { get; set; }


        [Display(Name = "Id No.")]
        [Required(ErrorMessage = "Please enter {0}")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "Please enter a valid {0}")]
       // [StringLength(9, ErrorMessage = "{0} must be {1} characters long.", MinimumLength = 9)]
        public string IdNo { get; set; }

        public string IdIssuer { get; set; }

        [Required(ErrorMessage = "Please enter Date of Issue.")]
        public string IdIssueDate { get; set; }

        [Required(ErrorMessage = "Please enter Date of Expiry.")]
        public string IdExpiryDate { get; set; }

        [Required(ErrorMessage = "Please enter Date of Birth.")]
        public string BirthDate { get; set; }

        [Required(ErrorMessage = "Please enter Nationality.")]
        public string Nationality { get; set; }

        [Required(ErrorMessage = "Please select Gender.")]
        public string Gender { get; set; }
        public string PassportNo { get; set; }
        public string PassportIssuer { get; set; }
        public string PassportIssueDate { get; set; }
        public string PassportExpireDate { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public Int32? EmploymentType { get; set; }

        [Required(ErrorMessage = "Please enter Country ID.")]
        [StringLength(10, ErrorMessage = "Country ID must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string CountryId { get; set; }

        [Required(ErrorMessage = "Please enter Mobile Number.")]
        [RegularExpression("^[1-9]([0-9])+$", ErrorMessage = "Please enter a valid Mobile Number")]
        [StringLength(10, ErrorMessage = "Home Number must be at least {2} and at max {1} characters long.", MinimumLength = 10)]
        public string MobilePhone { get; set; }

        [Required(ErrorMessage = "Please enter Home Number.")]
        [RegularExpression("^[1-9]([0-9])+$", ErrorMessage = "Please enter a valid Home Number")]
        [StringLength(10, ErrorMessage = "Home Number must be at least {2} and at max {1} characters long.", MinimumLength = 10)]
        public string HomePhone { get; set; }

        [Required(ErrorMessage = "Please enter Email address.")]
        [EmailAddress(ErrorMessage = "Please enter a valid Email address.")]
        public string EmailId { get; set; }

        [Required(ErrorMessage = "Please enter Account Key.")]
        [StringLength(3, ErrorMessage = "Please enter a valid Account Key.", MinimumLength = 1)]
        [RegularExpression("^([0-9])+$", ErrorMessage = "Please enter a valid Account Key")]
        public string AcctKey { get; set; }

        [Display(Name = "Org. Info")]
        [Required(ErrorMessage = "Please enter {0}")]
        [StringLength(40, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 1)]
        public string OrgInfo { get; set; }

        [Display(Name = "Education Level")]
        [Required(ErrorMessage = "Please enter {0}")]
        public int EducationLevel { get; set; }

        [Display(Name = "Nationality")]
        [Required(ErrorMessage = "Please enter {0}")]
        public int NationalityId { get; set; }

        [Display(Name = "Nationality Desc")]
        [Required(ErrorMessage = "Please enter {0}")]
        public string NationalityDesc { get; set; }

    
    }

    public class AccountCreateDto
    {
        public string LogId { get; set; }

        [Display(Name = "Rim No.")]
        [Required(ErrorMessage = "Please enter {0}")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "Please enter a valid {0}")]
        [StringLength(15, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string RimNo { get; set; }

        [Required(ErrorMessage = "Please enter Title Line 1.")]
        [StringLength(40, ErrorMessage = "Title Line 1 must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string TitleLine1 { get; set; }

        [Required(ErrorMessage = "Please enter Title Line 2.")]
        [StringLength(40, ErrorMessage = "Title Line 2 must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string TitleLine2 { get; set; }

        [Required(ErrorMessage = "Please enter Account Type.")]
        [StringLength(3, ErrorMessage = "Please enter a valid Account Type.", MinimumLength = 1)]
        public string AcctType { get; set; }

        [Required(ErrorMessage = "Please enter Account Key.")]
        [StringLength(3, ErrorMessage = "Please enter a valid Account Key.", MinimumLength = 1)]
        [RegularExpression("^([0-9])+$", ErrorMessage = "Please enter a valid Account Key")]
        public string AcctKey { get; set; }
    }

    public class InitialDepositDto
    {
        public string LogId { get; set; }

        [Display(Name = "Account Id")]
        [Required(ErrorMessage = "Please enter {0}")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "Please enter a valid {0}")]
        [StringLength(12, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 12)]
        public string AccountId { get; set; }

        [Required(ErrorMessage = "Please enter Account Type.")]
        [StringLength(3, ErrorMessage = "Please enter a valid Account Type.", MinimumLength = 3)]
        [RegularExpression("^([0-9])+$", ErrorMessage = "Please enter a valid Account Type")]
        public string AccountType { get; set; }

        public string CurrencyCode { get; set; }
    }

    public class MiniStatementDto
    {
        [Display(Name = "Account Id")]
        [Required(ErrorMessage = "Please enter {0}")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "Please enter a valid {0}")]
        [StringLength(12, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 12)]
        public string AccountId { get; set; }

        [Required(ErrorMessage = "Please enter Account Type.")]
        [StringLength(3, ErrorMessage = "Please enter a valid Account Type.", MinimumLength = 3)]
        [RegularExpression("^([0-9])+$", ErrorMessage = "Please enter a valid Account Type")]
        public string AccountType { get; set; }
    }

    public class StatementDto
    {
        [Display(Name = "Account Id")]
        [Required(ErrorMessage = "Please enter {0}")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "Please enter a valid {0}")]
        [StringLength(12, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 12)]
        public string AccountId { get; set; }

        [Required(ErrorMessage = "Please enter Account Type.")]
        [StringLength(3, ErrorMessage = "Please enter a valid Account Type.", MinimumLength = 3)]
        [RegularExpression("^([0-9])+$", ErrorMessage = "Please enter a valid Account Type")]
        public string AccountType { get; set; }
    }

    public class AccountDto
    {
        [Display(Name = "Account Id")]
        [Required(ErrorMessage = "Please enter {0}")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "Please enter a valid {0}")]
        [StringLength(12, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 12)]
        public string AccountId { get; set; }
    }

    public class FinancialEntryDto
    {
        [Display(Name = "Account Id")]
        [Required(ErrorMessage = "Please enter {0}")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "Please enter a valid {0}")]
        [StringLength(12, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 12)]
        public string AccountId { get; set; }

        [Required(ErrorMessage = "Please enter Account Type.")]
        [StringLength(3, ErrorMessage = "Please enter a valid Account Type.", MinimumLength = 3)]
        [RegularExpression("^([0-9])+$", ErrorMessage = "Please enter a valid Account Type")]
        public string AccountType { get; set; }

        [Display(Name = "Entry Type")]
        [Required(ErrorMessage = "Please enter {0}")]
        [StringLength(2, ErrorMessage = "Please enter a valid {0}", MinimumLength = 2)]
        public string EntryType { get; set; }

        [Display(Name = "Amount")]
        [Required(ErrorMessage = "Please enter {0}")]
        [Range(typeof(decimal), "1", "999999999999", ErrorMessage = "{0} must be between {2} and at max {1}")]
        public decimal Amount { get; set; }

        [Display(Name = "Description")]
        [Required(ErrorMessage = "Please enter {0}")]
        [StringLength(40, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string Description { get; set; }
    }

    public class SpecificAccountInquiryDto
    {
        [Display(Name = "Rim No.")]
        [Required(ErrorMessage = "Please enter {0}")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "Please enter a valid {0}")]
        [StringLength(15, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string RimNo { get; set; }

        [Display(Name = "Account Id")]
        [Required(ErrorMessage = "Please enter {0}")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "Please enter a valid {0}")]
        [StringLength(12, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 12)]
        public string AccountId { get; set; }

        [Required(ErrorMessage = "Please enter Account Type.")]
        [StringLength(3, ErrorMessage = "Please enter a valid Account Type.", MinimumLength = 3)]
        [RegularExpression("^([0-9])+$", ErrorMessage = "Please enter a valid Account Type")]
        public string AccountType { get; set; }
    }

    public class SendEmailDto
    {
        [Display(Name = "Email Id")]
        [Required(ErrorMessage = "Please enter {0}")]
        [StringLength(100, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 5)]
        [EmailAddress(ErrorMessage = "Please enter a valid {0}")]
        public List<string> Email { get; set; }

        [Display(Name = "Name")]
        [Required(ErrorMessage = "Please enter {0}")]
        [StringLength(40, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public List<string> Name { get; set; }

        [Display(Name = "Subject")]
        [Required(ErrorMessage = "Please enter {0}")]
        [StringLength(50, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string Subject { get; set; }

        [Display(Name = "Message")]
        [Required(ErrorMessage = "Please enter {0}")]
        public string Message { get; set; }
    }

 
}
