﻿using DBHandler.Helper;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DBHandler.Model.Dtos
{
    public class RegisterDto
    {
        //public int Id { get; set; }
        //[Required(ErrorMessage = "Please enter Device Id.")]
        
        //public string UserId { get; set; }
        //[Required(ErrorMessage = "Please enter Instance Id.")]

        public string InstanceId { get; set; }
        public decimal ScanPercentage { get; set; }

        public int CompanyId { get; set; }
        public int? OrgId { get; set; }
        public string Type { get; set; }

        [Required(ErrorMessage = "Please enter Full Name.")]
        [StringLength(100, ErrorMessage = "Full Name must be at least {2} and at max {1} characters long.", MinimumLength = 1)]
        public string FullName { get; set; }

        [Required(ErrorMessage = "Please enter First Name.")]
        [StringLength(100, ErrorMessage = "First Name must be at least {2} and at max {1} characters long.", MinimumLength = 1)]
        public string FirstName { get; set; }

        [StringLength(100, ErrorMessage = "Middle Name must not be greater than 100 characters.")]
        public string MiddleName { get; set; }

        [StringLength(100, ErrorMessage = "Family Name must not be greater than 100 characters.")]
        public string FamilyName { get; set; }

        [Required(ErrorMessage = "Please enter Email address.")]
        [EmailAddress(ErrorMessage = "Please enter a valid Email address.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter Country ID.")]
        [StringLength(10, ErrorMessage = "Country ID must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string CountryID { get; set; }

        public int? CityID { get; set; }

        [Required(ErrorMessage = "Please enter Mobile Number.")]
        //[RegularExpression("^[0-9]+$", ErrorMessage = "Please enter a valid Mobile Number")]
        //[StringLength(15, ErrorMessage = "Mobile Number must be at least {2} and at max {1} characters long.", MinimumLength = 11)]
        public string MobileNumber { get; set; }

        public bool ResidentStatus { get; set; }
        public string OTPCode { get; set; }
        public bool OTPStatus { get; set; }
        public bool EmailOTPStatus { get; set; }

        [Required(ErrorMessage = "Please enter Date of Birth.")]

        public DateTime DOB { get; set; }

        [Required(ErrorMessage = "Please select Gender.")]
        public string Gender { get; set; }

        [Required(ErrorMessage = "Please enter Nationality.")]
        public string Nationality { get; set; }

        [Required(ErrorMessage = "Please enter Date of Expiry.")]
        public DateTime DOE { get; set; }

        [Required(ErrorMessage = "Please enter Document Code.")]
        [StringLength(2, ErrorMessage = "Document Code must be less than 3 characters long.", MinimumLength = 1)]
        public string DocumentCode { get; set; }

        [Required(ErrorMessage = "Please enter Document Number.")]
      //  [StringLength(10, ErrorMessage = "Document Number must be 9 characters long.", MinimumLength = 9)]
        public string DocumentNumber { get; set; }

        [Compare("CountryID", ErrorMessage = "Issuer and Country Id does not match.")]
        public string Issuer { get; set; }
        public string OptionalData1 { get; set; }
        public string OptionalData2 { get; set; }
        public string MrtDraw { get; set; }

        [Required(ErrorMessage = "Please enter Front Card Image.")]
        public string FrontCardImage { get; set; }

        [Required(ErrorMessage = "Please enter Back Card Image.")]
        public string BackCardImage { get; set; }

       // [Required(ErrorMessage = "Please enter Person's Face Image.")]
        public string PersonFaceImage { get; set; }

        [Required(ErrorMessage = "Please enter Password.")]
        [DataType(DataType.Password)]
       // [RegularExpression("^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{8,}$", ErrorMessage = "Please enter a valid Password")]
        [StringLength(20, ErrorMessage = "{0} must be at max {1} characters long.")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
      //  [RegularExpression("^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{8,}$", ErrorMessage = "Please enter a valid Confirm Password")]
        [StringLength(20, ErrorMessage = "{0} must be at max {1} characters long.")]
        public string ConfirmPassword { get; set; }

        public BaseClass BaseClass { get; set; }

        [Required(ErrorMessage = "Please Provide UserName")]
        public string UserName { get; set; }

        [Required(ErrorMessage ="Please Provide SyncContactAnswer")]
        public bool SyncContacts { get; set; }

        [Required(ErrorMessage = "Please Provide AllowMeToDiscoverAnswer")]
        public bool DiscoverMe { get; set; }


        public int FacialScore { get; set; }
        public bool FacialMatch { get; set; }

    }
}
