﻿using CommonModel;
using DBHandler.Model.StatementSmart;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model.Dtos
{
    public class CustomerStatementDto
    {
        
        public CustStatementDateWise data { get; set; }
    }
    public class TrxListLastNTrxDto
    {
        
        public TrxListLastNTrx data { get; set; }
    }
    public class CBStatementReqDto
    {
        
        public CBStatementRequest data { get; set; }
    }
    public class MonthlyStatementDto
    {
        
        public MonthlyStatement data { get; set; }
    }
    public class VATAdviceDto
    {
        
        public VATAdvice data { get; set; }
    }
    public class MonthlyVATAdviceDto
    {
        
        public MonthlyVATAdvice data { get; set; }
    }
    public class TrxAdviceDto
    {
        
        public TrxAdvice data { get; set; }
    }
    public class DebitAdviceDto
    {
        
        public DebitAdvice data { get; set; }
    }
    public class CreditAdviceDto
    {
        
        public CreditAdvice data { get; set; }
    }
    public class GetStatementDetailsDto
    {
        
        public GetStatementDetails data { get; set; }
    }
    public class MonthlyStatementStatusDto
    {
        
        public MonthlyStatementStatus data { get; set; }
    }
}
