﻿//using IdentityServer4.Models;
using DBHandler.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DBHandler.Model.Dtos
{
    public class AdditionalDocDto
    {
        [Required(ErrorMessage = "Please enter Product Id.")]
        [StringLength(8, ErrorMessage = "Please enter a valid Product Id.", MinimumLength = 5)]
        public string ProductId { get; set; }
        
        public string DrivingLicense { get; set; }
        public string PassportSignature { get; set; }
        public string BahrainResidentVisa { get; set; }
        public string BirthCerificate { get; set; }
        public string UtilityBill { get; set; }
        public string SalaryCertificate { get; set; }
        public string BankStatement { get; set; }
        public string LegalDocument { get; set; }
        public string GuardianVisa { get; set; }
        public string DeviceID { get; set; }
        public BaseClass BaseClass { get; set; }

    }
}
