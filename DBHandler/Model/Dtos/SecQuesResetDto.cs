﻿using DBHandler.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DBHandler.Model.Dtos
{
    public class SecQuesResetDto
    {
        [Display(Name = "ID No.")]
        [Required(ErrorMessage = "Please enter ID No.")]
        [RegularExpression("^([0-9])+$", ErrorMessage = "Please enter a valid ID No.")]
        public string IDNo { get; set; }

        [Display(Name = "Answer # 1")]
        [StringLength(100, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 1)]
        public string Answer1 { get; set; }

        [Display(Name = "Answer # 2")]
        [StringLength(100, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 1)]
        public string Answer2 { get; set; }

        [Required(ErrorMessage = "Please enter Password.")]
        [DataType(DataType.Password)]
        //  [RegularExpression("^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{8,}$", ErrorMessage = "Please enter a valid Password")]
        [StringLength(20, ErrorMessage = "{0} must be at max {1} characters long.")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        //  [RegularExpression("^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{8,}$", ErrorMessage = "Please enter a valid Confirm Password")]
        [StringLength(20, ErrorMessage = "{0} must be at max {1} characters long.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Please enter Device ID.")]

        public string DeviceID { get; set; }

        public BaseClass BaseClass { get; set; }

    }
}
