﻿using DBHandler.Helper;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model.Dtos
{
    public class FileDto
    {
        public BaseClass BaseClass { get; set; }
        public IFormFile File { get; set; }
    }

    public class ConfigVersion
    {
        public string FileType { get; set; }
        public string Version { get; set; }
        public BaseClass BaseClass { get; set; }
    }

    public class Download
    {
        public byte[] Bytes { get; set; }
        public BaseClass BaseClass { get; set; }

    }
}
