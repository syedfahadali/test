﻿using DBHandler.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model.Dtos
{
    public class Verifyotpdto
    {
        public string Otp { get; set; }
        public BaseClass BaseClass { get; set; }
    }
}
