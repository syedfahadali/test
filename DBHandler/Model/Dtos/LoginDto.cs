﻿using DBHandler.Helper;
using System.ComponentModel.DataAnnotations;

namespace DBHandler.Model.Dtos
{
    public class LoginDto
    {
        [Required(ErrorMessage = "Please enter Email address")]
       // [StringLength(200, ErrorMessage = "Email address must not be greator than 200 characters")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Please enter Password.")]
        [DataType(DataType.Password)]
       // [RegularExpression("^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{8,}$", ErrorMessage = "Please enter a valid Password")]
        [StringLength(20, ErrorMessage = "{0} must be at max {1} characters long.")]
        public string Password { get; set; }

        //[Display(Name = "Remember me?")]
        //public bool RememberMe { get; set; }
        
        //[Required(ErrorMessage = "Please enter DeviceID")]
        //public string DeviceID { get; set; }

        public string FireBaseToken { get; set; }
        public string Platform { get; set; }

        public BaseClass BaseClass { get; set; }
    }
    public class AndroidLogin
    {
        [Required(ErrorMessage = "Please enter Device ID")]
        public string DeviceID { get; set; }

        [Required(ErrorMessage = "Please enter Cipher")]
        public string Cipher { get; set; }

        [Required(ErrorMessage = "Please enter Password")]
        public string Password { get; set; }
        public string FireBaseToken { get; set; }
        public BaseClass BaseClass { get; set; }

    }

    public class IPhoneLogin
    {
        [Required(ErrorMessage = "Please enter Device ID")]
        public string DeviceID { get; set; }

        [Required(ErrorMessage = "Please enter Cipher")]
        public string Cipher { get; set; }

        public BaseClass BaseClass { get; set; }


    }

    public class KycLoginDto
    {
        [Required(ErrorMessage = "Please enter Email address")]
        [EmailAddress(ErrorMessage = "Please enter a valid Email address")]
        [StringLength(200, ErrorMessage = "Email address must not be greator than 200 characters")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter Password.")]
        [DataType(DataType.Password)]
        [RegularExpression("^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{8,}$", ErrorMessage = "Please enter a valid Password")]
        [StringLength(20, ErrorMessage = "{0} must be at max {1} characters long.")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }

        
    }

}
