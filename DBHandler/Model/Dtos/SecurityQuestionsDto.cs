﻿using DBHandler.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DBHandler.Model.Dtos
{
    public class SecurityQuestionsDto
    {
        [Display(Name = "Answer # 1")]
        [StringLength(100, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string Answer1 { get; set; }

        [Display(Name = "Answer # 2")]
        [StringLength(100, ErrorMessage = "{0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string Answer2 { get; set; }

        [Required(ErrorMessage = "Please enter DeviceId.")]
        public string DeviceID { get; set; }

        public BaseClass BaseClass { get; set; }


    }
}
