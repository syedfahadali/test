﻿using DBHandler.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using CommonModel;
using System.Diagnostics.CodeAnalysis;

namespace DBHandler.Model.Dtos
{
    public class TransferRs
    {
        public string ReferenceNo { get; set; }
        public string ChannelRefId { get; set; }
        public List<ReferenceData> RefData { get; set; }
    }

    public class ReferenceDataV2
    {
        public int SerialNo { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public string EmiratesIDNo { get; set; }
        public string CountryID { get; set; }
        public string PassportNo { get; set; }
        public string TradeLicenseNo { get; set; }
        public string EconoActCode { get; set; }
    }
    public class TransferRsV2
    {
        public string ReferenceNo { get; set; }
        public string ChannelRefId { get; set; }
        public List<ReferenceData> RefData { get; set; }
    }

    public class InquireCostCenterResponse
    {
        public string OurBranchId { get; set; }
        public string CostCenterId { get; set; }
        public string CostCenterName { get; set; }
        public string CostCenterType { get; set; }
        public string Owner { get; set; }

    }

    public class UpdateSOCIDResponse
    {
        public string accountId { get; set; }
        public string socId { get; set; }
    }


    public class ReferenceData
    {
        public int SerialNo { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public string EmiratesIDNo { get; set; }
        public string CountryID { get; set; }
        public string PassportNo { get; set; }
        public string TradeLicenseNo { get; set; }
        public string EconoActCode { get; set; }
    }
    public class TransferRq
    {
        
        [Required(ErrorMessage = "Financial entries are required")]
        public List<TransferEntry> TransferEntries { get; set; }
        [Required(ErrorMessage = "Channel Reference ID is require")]
        public string ChannelRefId { get; set; }
    }
    public class TransferEntry
    {
        [Required(ErrorMessage = "Account ID is required")]
        public string AccountId { get; set; }
        [Required(ErrorMessage = "Account Type is required")]
        public string AccountType { get; set; }
        [Required(ErrorMessage = "Entry Type is required")]
        public string EntryType { get; set; }
        [Required(ErrorMessage = "Currency ID is required")]
        public string CurrencyID { get; set; }
        [Required(ErrorMessage = "Amount is required")]
        public decimal Amount { get; set; }
        [Required(ErrorMessage = "Exchange Rate is required")]
        public decimal ExchangeRate { get; set; }
        [Required(ErrorMessage = "Local Equivalent is required")]
        public decimal LocalEq { get; set; }
        [Required(ErrorMessage = "Narration Id is required")]
        public string NarrationId { get; set; }
        [Required(ErrorMessage = "Memo is required")]
        public string Memo { get; set; }
        public string ChequeId { get; set; }
        public DateTime? ChequeDate { get; set; }
        public DateTime? ValueDate { get; set; }
    }

    public class CollateralInquiry
    {
        
        

        [Required(ErrorMessage = "Collateral ID is required")]
        public string CollateralID { get; set; }
    }
    public class InquireCollateralOut
    {
        public string ClientID { get; set; }
        public decimal? SerialNo { get; set; }
        public string Description { get; set; }
        public string CollateralTypeID { get; set; }
        public string CollateralSubTypeID { get; set; }
        public string CollateralStatusID { get; set; }
        public decimal? CollateralValue { get; set; }
        public string CurrencyID { get; set; }
        public decimal? ExRate { get; set; }
        public decimal? LocalEq { get; set; }
        public decimal? Margin { get; set; }
        public decimal? AfterDeductingMargin { get; set; }
        public DateTime? ReviewDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string ReferenceNo { get; set; }
        public string CollateralSubType2ID { get; set; }
        public string DocTypeID { get; set; }
        public string VirtualAccountID { get; set; }
        public string VirtualAccountIBAN { get; set; }
        public string VirtualAccountSwift { get; set; }
        public string TitleDeedRefNo { get; set; }
        public string Area { get; set; }
        public string SecurityProvider { get; set; }
        public string MerchantName { get; set; }
        public string TradeLicenseNo { get; set; }
        public string POBox { get; set; }
        public string Emirates { get; set; }
        public string NameOfAS { get; set; }
        public string MerchantID { get; set; }
        public string ServiceProvider { get; set; }
        public string InsuranceCompanyName { get; set; }
        public string PropertyName { get; set; }
        public string PropertyLocation { get; set; }
        public string PropertyEmirates { get; set; }
        public string SchoolName { get; set; }
        public string SchoolLocation { get; set; }
        public string SchoolEmirates { get; set; }
        public string CounterParty { get; set; }
        public string ContractDescription { get; set; }
        public DateTime? ContractDate { get; set; }
        public string ContractRefNo { get; set; }
        public string ALPPropertyName { get; set; }
        public string ALPPropertyLocation { get; set; }
        public string ALPPropertyEmirates { get; set; }
        public string ALPDeveloperName { get; set; }
        public string AIPInsuranceCompanyName { get; set; }
        public string AIPInsurancePolicyNumber { get; set; }
        public string AIPExistBankApprovedList { get; set; }
        public string AOSInventoryName { get; set; }
        public string AOSLocation { get; set; }
        public string AOSEmirates { get; set; }
        public string AOSGoodsDescription { get; set; }
        public string AOSStakeholderName { get; set; }
        public string FDReceiptNo { get; set; }
        public string CFARefNo { get; set; }
        public string FOLRefNo { get; set; }
        public string AddendumCFARefNo { get; set; }
        public string AddendumFOLRefNo { get; set; }
        public string PGCFARefNo { get; set; }
        public string PGFOLRefNo { get; set; }
        public string PGAddendumCFARefNo { get; set; }
        public string PGAddendumFOLRefNo { get; set; }
        public string PGPassportNo { get; set; }
        public DateTime? PGPassportExpiry { get; set; }
        public string PGEmiratesID { get; set; }
        public DateTime? PGEmiratesIDExpiry { get; set; }
        public string PGVisaNo { get; set; }
        public DateTime? PGVisaExpiry { get; set; }
        public string FDMMortgageName { get; set; }
        public string FDMPropertyOwnerName { get; set; }
        public string FDMEvaluatorName { get; set; }
        public DateTime? FDMEvaluatorIssueDate { get; set; }
        public DateTime? FDMEvaluatorExpiryDate { get; set; }
        public string FDMExistBankList { get; set; }
        public string SCQChequeNo { get; set; }
        public string SCQPropertyDetails { get; set; }
        public string MOMMortgageName { get; set; }
        public string MOMMachineRefNo { get; set; }
        public string MOMMachineDetails { get; set; }
        public string PromiseeName { get; set; }
        public string SAGSubordinatorName { get; set; }
        public string SAGCurrentAccount { get; set; }
        public string SAGLoanAccount { get; set; }
    }

    public class SearchCollateral
    {
        

        [Required(ErrorMessage = "Client ID is required")]
        public string ClientID { get; set; }
    }

    public class CreateCollateralDto
    {
        
        [Required(ErrorMessage = "Client ID is required")]
        public string ClientID { get; set; }
        [Required(ErrorMessage = "Description is required")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Collateral Type is required")]
        public string CollateralTypeID { get; set; }
        public string CollateralSubTypeID { get; set; }
        [Required(ErrorMessage = "Collateral Status is required")]
        public string CollateralStatusID { get; set; }
        //[Required(ErrorMessage = "Collateral Value is required")]
        //[Range(1, 999999999999.99, ErrorMessage = "Collateral Value must be between 1 to 999,999,999,999.99")]
        public decimal? CollateralValue { get; set; }
        [Required(ErrorMessage = "Currency is required")]
        public string CurrencyID { get; set; }
        //[Required(ErrorMessage = "Exchange Rate is required")]
        //[Range(0.01, 999.99, ErrorMessage = "Exchange Rate must be between 0.01 to 999.99")]
        public decimal? ExRate { get; set; }
        //[Required(ErrorMessage = "Local Equivalent is required")]
        //[Range(1, 999999999999.99, ErrorMessage = "Local Equivalent must be between 1 to 999,999,999,999.99")]
        public decimal? LocalEq { get; set; }
        [Required(ErrorMessage = "Margin is required")]
        [Range(0, 100, ErrorMessage = "Margin must be between 1 to 0")]
        public decimal Margin { get; set; }
        //[Required(ErrorMessage = "Review Date is required")]
        public DateTime? ReviewDate { get; set; }
        //[Required(ErrorMessage = "Expiry Date is required")]
        public DateTime? ExpiryDate { get; set; }
        public string CollateralSubType2ID { get; set; }
        public string DocTypeID { get; set; }
        public string VirtualAccountID { get; set; }
        public string VirtualAccountIBAN { get; set; }
        public string VirtualAccountSwift { get; set; }
        public string TitleDeedRefNo { get; set; }
        public string Area { get; set; }
        public string SecurityProvider { get; set; }
        public string MerchantName { get; set; }
        public string TradeLicenseNo { get; set; }
        public string POBox { get; set; }
        public string Emirates { get; set; }
        public string NameOfAS { get; set; }
        public string MerchantID { get; set; }
        public string ServiceProvider { get; set; }
        public string InsuranceCompanyName { get; set; }
        public string PropertyName { get; set; }
        public string PropertyLocation { get; set; }
        public string PropertyEmirates { get; set; }
        public string SchoolName { get; set; }
        public string SchoolLocation { get; set; }
        public string SchoolEmirates { get; set; }
        public string CounterParty { get; set; }
        public string ContractDescription { get; set; }
        public DateTime? ContractDate { get; set; }
        public string ContractRefNo { get; set; }
        public string ALPPropertyName { get; set; }
        public string ALPPropertyLocation { get; set; }
        public string ALPPropertyEmirates { get; set; }
        public string ALPDeveloperName { get; set; }
        public string AIPInsuranceCompanyName { get; set; }
        public string AIPInsurancePolicyNumber { get; set; }
        public string AIPExistBankApprovedList { get; set; }
        public string AOSInventoryName { get; set; }
        public string AOSLocation { get; set; }
        public string AOSEmirates { get; set; }
        public string AOSGoodsDescription { get; set; }
        public string AOSStakeholderName { get; set; }
        public string FDReceiptNo { get; set; }
        public string CFARefNo { get; set; }
        public string FOLRefNo { get; set; }
        public string AddendumCFARefNo { get; set; }
        public string AddendumFOLRefNo { get; set; }
        public string PGCFARefNo { get; set; }
        public string PGFOLRefNo { get; set; }
        public string PGAddendumCFARefNo { get; set; }
        public string PGAddendumFOLRefNo { get; set; }
        public string PGPassportNo { get; set; }
        public DateTime? PGPassportExpiry { get; set; }
        public string PGEmiratesID { get; set; }
        public DateTime? PGEmiratesIDExpiry { get; set; }
        public string PGVisaNo { get; set; }
        public DateTime? PGVisaExpiry { get; set; }
        public string FDMMortgageName { get; set; }
        public string FDMPropertyOwnerName { get; set; }
        public string FDMEvaluatorName { get; set; }
        public DateTime? FDMEvaluatorIssueDate { get; set; }
        public DateTime? FDMEvaluatorExpiryDate { get; set; }
        public string FDMExistBankList { get; set; }
        public string SCQChequeNo { get; set; }
        public string SCQPropertyDetails { get; set; }
        public string MOMMortgageName { get; set; }
        public string MOMMachineRefNo { get; set; }
        public string MOMMachineDetails { get; set; }
        public string PromiseeName { get; set; }
        public string SAGSubordinatorName { get; set; }
        public string SAGCurrentAccount { get; set; }
        public string SAGLoanAccount { get; set; }
        //public string CStatus { get; set; }

    }

    public class CreateCollateralOut
    {
        public decimal SerialNo { get; set; }
        public string CollateralID { get; set; }
        public decimal AfterDeductingMargin { get; set; }
    }

    public class UpdateCollateralDto
    {
        
        [Required(ErrorMessage = "Collateral ID is required")]
        public string CollateralID { get; set; }
        [Required(ErrorMessage = "Client ID is required")]
        public string ClientID { get; set; }
        public decimal? SerialNo { get; set; }
        [Required(ErrorMessage = "Description is required")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Collateral Type is required")]
        public string CollateralTypeID { get; set; }
        public string CollateralSubTypeID { get; set; }
        [Required(ErrorMessage = "Collateral Status is required")]
        public string CollateralStatusID { get; set; }
        //[Required(ErrorMessage = "Collateral Value is required")]
        //[Range(1, 999999999999.99, ErrorMessage = "Collateral Value must be between 1 to 999,999,999,999.99")]
        public decimal? CollateralValue { get; set; }
        [Required(ErrorMessage = "Currency is required")]
        public string CurrencyID { get; set; }
        //[Required(ErrorMessage = "Exchange Rate is required")]
        //[Range(0.01, 999.99, ErrorMessage = "Exchange Rate must be between 0.01 to 999.99")]
        public decimal? ExRate { get; set; }
        //[Required(ErrorMessage = "Local Equivalent is required")]
        //[Range(1, 999999999999.99, ErrorMessage = "Local Equivalent must be between 1 to 999,999,999,999.99")]
        public decimal? LocalEq { get; set; }
        [Required(ErrorMessage = "Margin is required")]
        [Range(0, 100, ErrorMessage = "Margin must be between 1 to 0")]
        public decimal Margin { get; set; }
        //[Required(ErrorMessage = "Review Date is required")]
        public DateTime? ReviewDate { get; set; }
        //[Required(ErrorMessage = "Expiry Date is required")]
        public DateTime? ExpiryDate { get; set; }
        public string CollateralSubType2ID { get; set; }
        public string DocTypeID { get; set; }
        public string VirtualAccountID { get; set; }
        public string VirtualAccountIBAN { get; set; }
        public string VirtualAccountSwift { get; set; }
        public string TitleDeedRefNo { get; set; }
        public string Area { get; set; }
        public string SecurityProvider { get; set; }
        public string MerchantName { get; set; }
        public string TradeLicenseNo { get; set; }
        public string POBox { get; set; }
        public string Emirates { get; set; }
        public string NameOfAS { get; set; }
        public string MerchantID { get; set; }
        public string ServiceProvider { get; set; }
        public string InsuranceCompanyName { get; set; }
        public string PropertyName { get; set; }
        public string PropertyLocation { get; set; }
        public string PropertyEmirates { get; set; }
        public string SchoolName { get; set; }
        public string SchoolLocation { get; set; }
        public string SchoolEmirates { get; set; }
        public string CounterParty { get; set; }
        public string ContractDescription { get; set; }
        public DateTime? ContractDate { get; set; }
        public string ContractRefNo { get; set; }
        public string ALPPropertyName { get; set; }
        public string ALPPropertyLocation { get; set; }
        public string ALPPropertyEmirates { get; set; }
        public string ALPDeveloperName { get; set; }
        public string AIPInsuranceCompanyName { get; set; }
        public string AIPInsurancePolicyNumber { get; set; }
        public string AIPExistBankApprovedList { get; set; }
        public string AOSInventoryName { get; set; }
        public string AOSLocation { get; set; }
        public string AOSEmirates { get; set; }
        public string AOSGoodsDescription { get; set; }
        public string AOSStakeholderName { get; set; }
        public string FDReceiptNo { get; set; }
        public string CFARefNo { get; set; }
        public string FOLRefNo { get; set; }
        public string AddendumCFARefNo { get; set; }
        public string AddendumFOLRefNo { get; set; }
        public string PGCFARefNo { get; set; }
        public string PGFOLRefNo { get; set; }
        public string PGAddendumCFARefNo { get; set; }
        public string PGAddendumFOLRefNo { get; set; }
        public string PGPassportNo { get; set; }
        public DateTime? PGPassportExpiry { get; set; }
        public string PGEmiratesID { get; set; }
        public DateTime? PGEmiratesIDExpiry { get; set; }
        public string PGVisaNo { get; set; }
        public DateTime? PGVisaExpiry { get; set; }
        public string FDMMortgageName { get; set; }
        public string FDMPropertyOwnerName { get; set; }
        public string FDMEvaluatorName { get; set; }
        public DateTime? FDMEvaluatorIssueDate { get; set; }
        public DateTime? FDMEvaluatorExpiryDate { get; set; }
        public string FDMExistBankList { get; set; }
        public string SCQChequeNo { get; set; }
        public string SCQPropertyDetails { get; set; }
        public string MOMMortgageName { get; set; }
        public string MOMMachineRefNo { get; set; }
        public string MOMMachineDetails { get; set; }
        public string PromiseeName { get; set; }
        public string SAGSubordinatorName { get; set; }
        public string SAGCurrentAccount { get; set; }
        public string SAGLoanAccount { get; set; }
        //public string CStatus { get; set; }
    }

    public class UpdateCollateralOut
    {
        public string ReferenceNo { get; set; }
        public decimal? AfterDeductingMargin { get; set; }
    }

    public class CIFExist
    {
        

        [Required(ErrorMessage = "Client ID is required")]
        [MaxLength(9, ErrorMessage = "Invalid Client ID")]
        [MinLength(9, ErrorMessage = "Invalid Client ID")]
        public string ClientID { get; set; }
    }
    public class SearchCIF
    {
        

        public string ClientID { get; set; }
        public string IDNo { get; set; }
        public string Name { get; set; }
        public string PhoneNo { get; set; }
        public string MobileNo { get; set; }
        public string EmailID { get; set; }
        public string CIBCode { get; set; }
        public string RelationshipDeptID { get; set; }
        public string GroupID { get; set; }
    }

    public class CIFExposure
    {
        public string ReferenceNo { get; set; }
        public string FacilityID { get; set; }
        public string FacilityDesc { get; set; }
        public string ProductID { get; set; }
        public string ProductDesc { get; set; }
        public string LimitSecType { get; set; }
        public string LimitType { get; set; }
        public decimal? Limit { get; set; }
        public decimal? Utilized { get; set; }
        public decimal? Outstanding { get; set; }
        public DateTime? ReviewDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
    }
    public class CIFAccounts
    {
        public string ClientID { get; set; }
        public string AccountID { get; set; }
        public string Name { get; set; }
        public string IBAN { get; set; }
        public string ProductID { get; set; }
        public string CurrencyID { get; set; }
        public string Status { get; set; }
        public int DebitFrozen { get; set; }
        public int CreditFrozen { get; set; }
    }
    public class CIFHierarchy
    {
        public string ClientID { get; set; }
        public string ParentClientID { get; set; }
    }

    public class FacilityAccInquiry
    {
        

        [Required(ErrorMessage = "Account Limit ID is required")]
        public string AccountLimitId { get; set; }
    }
    public class FacilityAccInquiryOut
    {
        public string ClientID { get; set; }
        public string FacilityID { get; set; }
        public decimal SerialNo { get; set; }
        public string AccountID { get; set; }
        public string ProductID { get; set; }
        public decimal Limit { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public decimal? Limit1 { get; set; }
        public string RateType1 { get; set; }
        public string RateID1 { get; set; }
        public decimal? Margin1 { get; set; }
        public decimal? MinRate1 { get; set; }
        public decimal? Rate1 { get; set; }
        public decimal? Limit2 { get; set; }
        public string RateType2 { get; set; }
        public string RateID2 { get; set; }
        public decimal? Margin2 { get; set; }
        public decimal? MinRate2 { get; set; }
        public decimal? Rate2 { get; set; }
        public decimal? Limit3 { get; set; }
        public string RateType3 { get; set; }
        public string RateID3 { get; set; }
        public decimal? Margin3 { get; set; }
        public decimal? MinRate3 { get; set; }
        public decimal? Rate3 { get; set; }
        public string ExcessRateType { get; set; }
        public string ExcessRateID { get; set; }
        public decimal? ExcessMargin { get; set; }
        public decimal? ExcessMinRate { get; set; }
        public decimal? ExcessRate { get; set; }
    }
    public class SearchAccountLimitDto
    {
        
        [Required(ErrorMessage = "Facility ID is required")]
        public string FacilityID { get; set; }
    }
    public class SearchAccountLimitOut
    {
        public List<AccountLimits> AccountLimits { get; set; }
    }
    public class AccountLimits
    {
        public string AccountLimitId { get; set; }
        public string Status { get; set; }
    }
    public class CreateAccountLimitOut
    {
        public decimal SerialNo { get; set; }
        public string AccountLimitID { get; set; }
    }
    public class CreateAccountLimitDto
    {
        
        [Required(ErrorMessage = "Facility ID is required")]
        public string FacilityID { get; set; }
        [Required(ErrorMessage = "Account ID is required")]
        public string AccountID { get; set; }
        [Required(ErrorMessage = "Limit is required")]
        [Range(0, 999999999999.99, ErrorMessage = "Limit must be between 0 to 999,999,999,999.99")]
        public decimal Limit { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public decimal? Limit1 { get; set; }
        public string RateType1 { get; set; }
        public string RateID1 { get; set; }
        public decimal? Margin1 { get; set; }
        public decimal? MinRate1 { get; set; }
        public decimal? Rate1 { get; set; }
        public decimal? Limit2 { get; set; }
        public string RateType2 { get; set; }
        public string RateID2 { get; set; }
        public decimal? Margin2 { get; set; }
        public decimal? MinRate2 { get; set; }
        public decimal? Rate2 { get; set; }
        public decimal? Limit3 { get; set; }
        public string RateType3 { get; set; }
        public string RateID3 { get; set; }
        public decimal? Margin3 { get; set; }
        public decimal? MinRate3 { get; set; }
        public decimal? Rate3 { get; set; }
        public string ExcessRateType { get; set; }
        public string ExcessRateID { get; set; }
        public decimal? ExcessMargin { get; set; }
        public decimal? ExcessMinRate { get; set; }
        public decimal? ExcessRate { get; set; }
    }

    public class ModifyAccountLimitDto
    {
        
        [Required(ErrorMessage = "Facility ID is required")]
        public string FacilityID { get; set; }
        [Required(ErrorMessage = "Account Limit ID is required")]
        public string AccountLimitID { get; set; }
        [Required(ErrorMessage = "Account ID is required")]
        public string AccountID { get; set; }
        [Required(ErrorMessage = "Limit is required")]
        [Range(0, 999999999999.99, ErrorMessage = "Limit must be between 0 to 999,999,999,999.99")]
        public decimal Limit { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public decimal? Limit1 { get; set; }
        public string RateType1 { get; set; }
        public string RateID1 { get; set; }
        public decimal? Margin1 { get; set; }
        public decimal? MinRate1 { get; set; }
        public decimal? Rate1 { get; set; }
        public decimal? Limit2 { get; set; }
        public string RateType2 { get; set; }
        public string RateID2 { get; set; }
        public decimal? Margin2 { get; set; }
        public decimal? MinRate2 { get; set; }
        public decimal? Rate2 { get; set; }
        public decimal? Limit3 { get; set; }
        public string RateType3 { get; set; }
        public string RateID3 { get; set; }
        public decimal? Margin3 { get; set; }
        public decimal? MinRate3 { get; set; }
        public decimal? Rate3 { get; set; }
        public string ExcessRateType { get; set; }
        public string ExcessRateID { get; set; }
        public decimal? ExcessMargin { get; set; }
        public decimal? ExcessMinRate { get; set; }
        public decimal? ExcessRate { get; set; }
    }
    public class CancelAccountLimitOut
    {
        public string AccountLimitCancelRefNo { get; set; }
    }
    public class CancelAccountLimitDto
    {
        
        [Required(ErrorMessage = "Account Limit ID is required")]
        public string AccountLimitID { get; set; }
    }

    public class FacilityInquiry
    {
        

        [Required(ErrorMessage = "Facility ID is required")]
        public string FacilityID { get; set; }
    }
    public class FacInquireIssGuaranteeAmedmentRefNumModelilityProductInquiry
    {
        

        [Required(ErrorMessage = "Facility ID is required")]
        public string FacilityID { get; set; }
        [Required(ErrorMessage = "Product ID is required")]
        public string ProductID { get; set; }
    }

    public class FacilityBlockOut
    {
        public string ReferenceNo { get; set; }
    }
    public class FacilityUnBlockOut
    {
        public string ReferenceNo { get; set; }
    }
    public class FacilityProductBlockOut
    {
        public string ReferenceNo { get; set; }
    }
    public class FacilityProductUnBlockOut
    {
        public string ReferenceNo { get; set; }
    }
    public class FacilityProductBlockInquiry
    {
        public string FacilityID { get; set; }
        public string ProductID { get; set; }
        public string FacilityProductStatus { get; set; }
    }
    public class FacilityBlockInquiry
    {
        public string FacilityID { get; set; }
        public string FacilityStatus { get; set; }
    }

    public class FacilityOut
    {
        public string ClientID { get; set; }
        public decimal SerialNo { get; set; }
        public string FacilityRefNo { get; set; }
        public string Description { get; set; }
        public string LimitSecuredType { get; set; }
        public string LimitType { get; set; }
        public decimal? AskingLimit { get; set; }
        public decimal? DrawingPower { get; set; }
        public DateTime? ReviewDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string CreditProgramID { get; set; }
        public List<Products1> Products { get; set; }
        public List<Collaterals1> CollateralID { get; set; }
    }
    public class Products1
    {
        public string ProductID { get; set; }
        public string LimitType { get; set; }
        public decimal? Limit { get; set; }
    }

    public class Collaterals1
    {
        public string CollateralID { get; set; }
    }

    public class CreateFacilityOut
    {
        public decimal SerialNo { get; set; }
        public string FacilityID { get; set; }
        public decimal? DrawingPower { get; set; }
    }
    public class CreateFacilityDto
    {
        
        [Required(ErrorMessage = "Client ID is required")]
        public string ClientID { get; set; }
        [Required(ErrorMessage = "Facility Ref. No. is required")]
        public string FacilityRefNo { get; set; }
        [Required(ErrorMessage = "Description is required")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Limit Secured Type is required")]
        [StringRangeAttribute(AllowableValues = new[] { "Clean", "Secured", "SCF File" }, ErrorMessage = "Valid values for this filed are Clean, Secured, SCF File")]
        public string LimitSecuredType { get; set; }
        [Required(ErrorMessage = "Limit Type is required")]
        [StringRangeAttribute(AllowableValues = new[] { "Fixed", "Revolving" }, ErrorMessage = "Valid values for this filed are Fixed, Revolving")]
        public string LimitType { get; set; }
        [Required(ErrorMessage = "Asking Limit is required")]
        public decimal? AskingLimit { get; set; }
        [Required(ErrorMessage = "Review Date is required")]
        public DateTime? ReviewDate { get; set; }
        [Required(ErrorMessage = "Expiry Date is required")]
        public DateTime? ExpiryDate { get; set; }
        public string CreditProgramID { get; set; }
        [Required(ErrorMessage = "Product ID is required")]
        public List<Products1> Products { get; set; }
        public List<Collaterals1> Collaterals { get; set; }
    }
    public class ModifyFacilityDto
    {
        
        [Required(ErrorMessage = "Facility ID is required")]
        public string FacilityID { get; set; }
        [Required(ErrorMessage = "Client ID is required")]
        public string ClientID { get; set; }
        [Required(ErrorMessage = "Facility Ref. No. is required")]
        public string FacilityRefNo { get; set; }
        [Required(ErrorMessage = "Description is required")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Limit Secured Type is required")]
        [StringRangeAttribute(AllowableValues = new[] { "Clean", "Secured", "SCF File" }, ErrorMessage = "Valid values for this filed are Clean, Secured, SCF File")]
        public string LimitSecuredType { get; set; }
        [Required(ErrorMessage = "Limit Type is required")]
        [StringRangeAttribute(AllowableValues = new[] { "Fixed", "Revolving" }, ErrorMessage = "Valid values for this filed are Fixed, Revolving")]
        public string LimitType { get; set; }
        [Required(ErrorMessage = "Asking Limit is required")]
        public decimal? AskingLimit { get; set; }
        [Required(ErrorMessage = "Review Date is required")]
        public DateTime? ReviewDate { get; set; }
        [Required(ErrorMessage = "Expiry Date is required")]
        public DateTime? ExpiryDate { get; set; }
        public string CreditProgramID { get; set; }
        [Required(ErrorMessage = "Product ID is required")]
        public List<Products1> Products { get; set; }
        public List<Collaterals1> Collaterals { get; set; }
    }

    public class ModifyFacilityOut
    {
        public string ReferenceNo { get; set; }
        public decimal? DrawingPower { get; set; }
    }
    public class SearchFacilityOut
    {
        public List<Facilities> Facilities { get; set; }
    }
    public class SearchFacilityDto
    {
        
        [Required(ErrorMessage = "Client ID is required")]
        public string ClientID { get; set; }
    }
    public class Facilities
    {
        public string FacilityId { get; set; }
        public string Status { get; set; }
    }
    public class CancelFacilityOut
    {
        public string FacilityCancelRefNo { get; set; }
    }
    public class CancelFacilityDto
    {
        
        [Required(ErrorMessage = "Facility ID is required")]
        public string FacilityID { get; set; }
    }
    public class LoanSimulatorDto
    {
        

        [Required(ErrorMessage = "Loan Amount is required")]
        [Range(1, 999999999999.99, ErrorMessage = "Loan Amount must be between 1 to 999,999,999,999.99")]
        public decimal LoanAmount { get; set; }
        //[Required(ErrorMessage = "Tenure Type is required")]
        //public string TenureType { get; set; }
        //[Required(ErrorMessage = "Tenure is required")]
        //[Range(1, 30, ErrorMessage = "Tenure must be between 1 to 30")]
        //public decimal Tenure { get; set; }
        [Required(ErrorMessage = "Interest Rate is required")]
        [Range(1, 100, ErrorMessage = "Interest Rate must be between 1 to 100")]
        public decimal InterestRate { get; set; }
        [Required(ErrorMessage = "Method is required")]
        public string Method { get; set; }
        [Required(ErrorMessage = "Re-Payment Mode is required")]
        public string RePaymentMode { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime MaturityDate { get; set; }
        [AllowNull]
        public List<ManualSchedule> ManualSchedule { get; set; }
    }
    public class ManualSchedule
    {
        public int Percentage { get; set; }
        public DateTime DueDate { get; set; }
    }

    public class LoanSimulatorOut
    {
        public string ReferenceNo { get; set; }
        public decimal LoanAmount { get; set; }
        public decimal InterestAmount { get; set; }
        public string PayOffDate { get; set; }
        public List<Schedule> Schedule { get; set; }
    }

    public class Schedule
    {
        public int InstNo { get; set; }
        public string PaymentDate { get; set; }
        public decimal InstAmount { get; set; }
        public int NoOfDays { get; set; }
        public decimal PrincipalAmount { get; set; }
        public decimal InterestAmount { get; set; }
        public decimal OutstandingAmount { get; set; }
        public string Type { get; set; }
    }

    public class LoanAccounts
    {
        public string AccountType { get; set; }
        public string AccountID { get; set; }
        public string AccountTitle { get; set; }
        public string ProductID { get; set; }
        public string ProductDescription { get; set; }
        public string CurrencyID { get; set; }
        public string CurrencyDescription { get; set; }
    }

    public class LoanAccountOut
    {
        public string ClientID { get; set; }
        public List<LoanAccounts> LoanAccounts { get; set; }
    }
    public class CreateLoanAccountDto
    {
        
        [Required(ErrorMessage = "Client ID is required")]
        public string ClientID { get; set; }
        [Required(ErrorMessage = "Product ID is required")]
        public string ProductID { get; set; }
        public string CreateNew { get; set; }
    }

    public class LoanAccount
    {
        public string Status { get; set; }
        public string MessageCode { get; set; }
        public string ClientID { get; set; }
        public string AccountType { get; set; }
        public string AccountID { get; set; }
        public string AccountTitle { get; set; }
        public string ProductID { get; set; }
        public string ProductDescription { get; set; }
        public string CurrencyID { get; set; }
        public string CurrencyDescription { get; set; }
    }
    public class LoanDisbReversalDto
    {
        
        [Required(ErrorMessage = "Reference No. is required")]
        public string ReferenceNo { get; set; }
    }

    public class DisbursementDto
    {
        
        [Required(ErrorMessage = "Loan Account ID is required")]
        public string AccountID { get; set; }
        public string LoanCategoryID { get; set; }
        public string TenureType { get; set; }
        [AllowNull]
        [Range(1, 30, ErrorMessage = "Tenure must be between 1 to 30")]
        public decimal? Tenure { get; set; }
        public DateTime? MaturityDate { get; set; }
        [Required(ErrorMessage = "Method is required")]
        public string Method { get; set; }
        [Required(ErrorMessage = "Re-Payment Mode is required")]
        public string RePaymentMode { get; set; }
        [Required(ErrorMessage = "Grace Period is required")]
        [Range(0, 15, ErrorMessage = "Grace Period must be between 0 and 15")]
        public int GracePeriod { get; set; }
        [Required(ErrorMessage = "Rate Type is required")]
        public string RateType { get; set; }
        [Range(1, 100, ErrorMessage = "Interest Rate must be between 1 to 100")]
        public decimal? InterestRate { get; set; }
        public string RateID { get; set; }
        [Range(0, 100, ErrorMessage = "Margin must be between 0 to 100")]
        public decimal? Margin { get; set; }
        [Required(ErrorMessage = "Loan Amount is required")]
        [Range(1, 999999999999.99, ErrorMessage = "Loan Amount must be between 1 to 999,999,999,999.99")]
        public decimal LoanAmount { get; set; }
        [Required(ErrorMessage = "Early Settlement Fee Type is required")]
        public string esFeeType { get; set; }
        [Range(1, 999999999999.99, ErrorMessage = "Early Settlement Fee must be between 1 to 999,999,999,999.99")]
        public decimal MinEsFee { get; set; }
        [Range(1, 100, ErrorMessage = "Early Settlement Fee must be between 1 to 100")]
        public decimal EsFeePercentage { get; set; }

        [Required(ErrorMessage = "Remarks is required")]
        public string Remarks { get; set; }
        [Required(ErrorMessage = "Disbursement Mode is required")]
        public string DisbursementMode { get; set; }
        [Required(ErrorMessage = "Disbursement Account ID is required")]
        public string DisbAccountID { get; set; }
        [Required(ErrorMessage = "Fees Account ID is required")]
        public string FeesAccountID { get; set; }
        [Required(ErrorMessage = "Transaction Account ID is required")]
        public string TrxAccountID { get; set; }
        [Required(ErrorMessage = "Receivable Account ID is required")]
        public string RecAccountID { get; set; }
        [Required(ErrorMessage = "Interest Account ID is required")]
        public string interestAccountID { get; set; }
        public string CapAccountID { get; set; }
        public List<DisbCharges> Charges { get; set; }
    }

    public class DisbCharges
    {
        public string ChargeID { get; set; }
        public string ChargeType { get; set; }
        [Range(0, 999999999999.99, ErrorMessage = "Charges must be between 0 to 999,999,999,999.99")]
        public decimal? Charges { get; set; }
    }

    public class DisbursementOut
    {
        public string ReferenceNo { get; set; }
        public decimal LoanAmount { get; set; }
        public decimal InterestAmount { get; set; }
        public string PayOffDate { get; set; }
        public List<Schedule> Schedule { get; set; }
    }

    public class DisbursementReversalOut
    {
        public string ReferenceNo { get; set; }
    }

    public class CreditProgramInquiry
    {
        

        [Required(ErrorMessage = "Credit Program ID is required")]
        public string CreditProgramID { get; set; }
    }

    public class CreditProgramOut
    {
        public string CreditProgramName { get; set; }
        public string ClientID { get; set; }
        public decimal? CreditProgramLimit { get; set; }
        public DateTime? CreditProgramExpiryDate { get; set; }
        public string CreditProgramId { get; set; }
    }
    public class CreateCreditProgramDto
    {
        
        [Required(ErrorMessage = "Credit Program Name is required")]
        public string CreditProgramName { get; set; }
        [Required(ErrorMessage = "Client ID is required")]
        public string ClientID { get; set; }
        [Required(ErrorMessage = "Credit Program Limit is required")]
        [Range(1, 999999999999.99, ErrorMessage = "Credit Program Limit must be between 1 to 999,999,999,999.99")]
        public decimal CreditProgramLimit { get; set; }
        [Required(ErrorMessage = "Credit Program Expiry Date is required")]
        public DateTime? CreditProgramExpiryDate { get; set; }
    }
    public class CreateCreditProgramOut
    {
        public string CreditProgramID { get; set; }
    }
    public class CalculatePenaltyAmountModel
    {
        
        
        
        public string ReceiptID { get; set; }
        
        public decimal PreMatureRate { get; set; }
        public string ClientId { get; set; }
    }
    public class CalculatePenaltyAmountResponse
    {
        public string ReceiptID { get; set; }
        public decimal Amount { get; set; }
        public decimal TotalInterest { get; set; }
        public decimal Paid { get; set; }
        public decimal Net { get; set; }
    }

    public class DynamicParametersDTO
    {
        
        
        [Required(ErrorMessage = "Form Name is required")]
        public string FormName { get; set; }
        [Required(ErrorMessage = "Where Clause is required")]
        public List<WhereClause> WhereClause { get; set; }
    }

    public class SaveDynamicParametersDTO
    {
        
        
        [Required(ErrorMessage = "Form Name is required")]
        public string FormName { get; set; }
        [Required(ErrorMessage = "Where Clause is required")]
        public List<Columns> WhereClause { get; set; }
        public List<Columns> UpdateOrInsertColumns { get; set; }
    }

    public class GetCashback
    {
        public int SNo { get; set; }
        public DateTime ValueDate { get; set; }
        public decimal Cashback { get; set; }
        public decimal TCashback { get; set; }
    }


    public class GetZXDetailsOut
    {
        public decimal CashbackAmount { get; set; }
        public decimal Limit { get; set; }
        public decimal Utilized { get; set; }
        public decimal UnUtilized { get; set; }
        public decimal InterestAccrued { get; set; }
        public List<GetCashback> CashBack { get; set; }
        public DateTime? lastPaymentDate { get; set; }
        public double? lastPaymentAmount { get; set; }
        public decimal CashBacksPercentage { get; set; }
    }

    public class GetZXDetailsDTO
    {
        
        
        [Required(ErrorMessage = "Account ID is required")]
        public string ZAccountID { get; set; }
        public string ZXAccountID { get; set; }
    }

    public class WhereClause
    {
        [Required(ErrorMessage = "Key is required")]
        public string Key { get; set; }
        [Required(ErrorMessage = "Value is required")]
        public string Value { get; set; }
    }

    public class Columns
    {
        [Required(ErrorMessage = "Key is required")]
        public string Key { get; set; }
        public string Value { get; set; }
    }

    public class ReturnDTO
    {
        public DateTime RequestDateTime { get; set; }
        public string LogId { get; set; }
        public Status Status { get; set; }
        public dynamic Content { get; set; }
    }
}
