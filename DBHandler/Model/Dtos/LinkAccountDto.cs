﻿namespace DBHandler.Model.Dtos
{
    public class LinkAccountDto
    {
        public string Guid { get; set; }
        public string AccountId { get; set; }
        public string CustomerId { get; set; }
    }
}
