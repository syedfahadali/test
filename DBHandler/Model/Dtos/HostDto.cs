﻿using CommonModel;
using DBHandler.Model.Host;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model.Dtos
{
    public class CashWithDrawalDto
    {
        
        public CashWithDrawal data { get; set; }
    }

    public class PreAuthorizationDto
    {
        
        public PreAuthorization data { get; set; }
    }
    public class PurchaseRequestDto
    {
        
        public PurchaseRequest data { get; set; }
    }
    public class FullReversal
    {
        
        public FullReversalDto data { get; set; }
    }
    public class PartialReversal
    {        
        
        public PartialReversalDto data { get; set; }
    }
    public class BalanceEnquiryDto
    {
        
        public BalanceEnquiry data { get; set; }
    }
    public class AccountVerificationDto
    {
        
        public AccountVerification data { get; set; }
    }
    public class CreditTransactionDto
    {
        
        public CreditTransaction data { get; set; }
    }

    public class FileManagementMsgDto
    {
        
        public FileManagementMsg data { get; set; }
    }

    public class GenericHostDto
    {
        
        // public string ourBranchId { get; set; }
    }
}
