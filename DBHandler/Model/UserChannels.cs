﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBHandler.Model
{

    [Table("t_UserChannels")]
    public class UserChannels
    {
        [Key]
        public Int32 Id { get; set; }
        public string UserId { get; set; }
        public string IPAddress { get; set; }
        public string ChannelId { get; set; }
    }
}
