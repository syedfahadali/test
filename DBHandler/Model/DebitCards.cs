﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model
{
    public class DebitCards
    {
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        
        public string AccountID { get; set; }
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        [MinLength(16, ErrorMessage = "- Valid Min length should be less than 16.")]
        
        public string DebitCardToken { get; set; }
        
        public string CardName { get; set; }
        
        public string DebitCardCategory { get; set; }
        
        public string CardType { get; set; }
        
        public DateTime? ExpiryDate { get; set; }
        
        public string Status { get; set; }


        public DateTime? CreateDateTime { get; set; }
    }
}
