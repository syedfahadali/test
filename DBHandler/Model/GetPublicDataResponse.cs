﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model
{
    public class GetPublicDataResponse
    {
        public string IDNo { get; set; }
        public DateTime? DOB { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string Nationality { get; set; }



    }
}
