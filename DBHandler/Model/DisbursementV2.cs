﻿using CommonModel;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace DBHandler.Model.DisbursementV2
{
    public class DisbursementV2Model
    {
        
        [Required(ErrorMessage = "Loan Account ID is required")]
        public string AccountID { get; set; }
        [Required(ErrorMessage = "Facility ID is required")]
        public string FacilityID { get; set; }
        public string LoanCategoryID { get; set; }
        public string TenureType { get; set; }
        [AllowNull]
        //[Range(1, 30, ErrorMessage = "Tenure must be between 1 to 30")]
        public decimal? Tenure { get; set; }
        public DateTime? MaturityDate { get; set; }
        [Required(ErrorMessage = "Method is required")]
        public string Method { get; set; }
        [Required(ErrorMessage = "Re-Payment Mode is required")]
        public string RePaymentMode { get; set; }
        [Required(ErrorMessage = "Grace Period is required")]
        [Range(0, 15, ErrorMessage = "Grace Period must be between 0 and 15")]
        public int GracePeriod { get; set; }
        [Range(0, 100, ErrorMessage = "Penal Rate must be between 0 to 100")]
        public decimal? PenalRate { get; set; }
        [Required(ErrorMessage = "Rate Type is required")]
        public string RateType { get; set; }
        [Range(1, 100, ErrorMessage = "Interest Rate must be between 1 to 100")]
        public decimal? InterestRate { get; set; }
        public string RateID { get; set; }
        [Range(0, 100, ErrorMessage = "Margin must be between 0 to 100")]
        public decimal? Margin { get; set; }
        [Required(ErrorMessage = "Loan Amount is required")]
        [Range(1, 999999999999.99, ErrorMessage = "Loan Amount must be between 1 to 999,999,999,999.99")]
        public decimal LoanAmount { get; set; }
        [Required(ErrorMessage = "Early Settlement Fee Type is required")]
        public string esFeeType { get; set; }
        [Range(0, 999999999999.99, ErrorMessage = "Early Settlement Fee must be between 0 to 999,999,999,999.99")]
        public decimal MinEsFee { get; set; }
        [Range(0, 100, ErrorMessage = "Early Settlement Fee must be between 0 to 100")]
        public decimal EsFeePercentage { get; set; }

        [Required(ErrorMessage = "Remarks is required")]
        public string Remarks { get; set; }
        [Required(ErrorMessage = "Disbursement Mode is required")]
        public string DisbursementMode { get; set; }
        [Required(ErrorMessage = "Disbursement Account ID is required")]
        public string DisbAccountID { get; set; }
        [Required(ErrorMessage = "Fees Account ID is required")]
        public string FeesAccountID { get; set; }
        [Required(ErrorMessage = "Transaction Account ID is required")]
        public string TrxAccountID { get; set; }
        [Required(ErrorMessage = "Receivable Account ID is required")]
        public string RecAccountID { get; set; }
        [Required(ErrorMessage = "Interest Account ID is required")]
        public string interestAccountID { get; set; }
        public string CapAccountID { get; set; }
        public int MoretoreumPeriod { get; set; }
        public List<DisbChargesD> Charges { get; set; }
        [AllowNull]
        public List<ManualSchedule> ManualSchedule { get; set; }

    }


    public class ManualSchedule
    {
        public int Percentage { get; set; }
        public DateTime DueDate { get; set; }
    }


    public class DisbChargesD
    {
        public string ChargeID { get; set; }
        public string ChargeType { get; set; }
        [Range(0, 999999999999.99, ErrorMessage = "Charges must be between 0 to 999,999,999,999.99")]
        public decimal? Charges { get; set; }
    }
    public class DisbursementV2Response
    {
        public string Status { get; set; }
        public string MessageCode { get; set; }
        public string ReferenceNo { get; set; }
        public decimal LoanAmount { get; set; }
        public decimal InterestAmount { get; set; }
        public string PayOffDate { get; set; }
        public List<Schedule> Schedule { get; set; }
    }

    public class Schedule
    {
        public int InstNo { get; set; }
        public string PaymentDate { get; set; }
        public decimal InstAmount { get; set; }
        public int NoOfDays { get; set; }
        public decimal PrincipalAmount { get; set; }
        public decimal InterestAmount { get; set; }
        public decimal OutstandingAmount { get; set; }
        public string Type { get; set; }
    }
}
