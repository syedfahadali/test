﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model
{
    public class GetPreAccountClosureResponse
    {
        public string AccountID { get; set; }
        public string AccountTitle { get; set; }
        public string ClientID { get; set; }
        public string IBAN { get; set; }
        public string ProductID { get; set; }
        public string CurrencyID { get; set; }
        public string Status { get; set; }
        public decimal TotalBalance { get; set; }
        public decimal? SavingsAccrual { get; set; }
        public DateTime? SavingsAccrualUpto { get; set; }
        public decimal? ODAccrual { get; set; }
        public DateTime? ODAccrualUpto { get; set; }
    }
}
