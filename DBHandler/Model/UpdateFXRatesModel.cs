﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model
{
    public class UpdateFXRatesModel
    {
      
        
        
        public List<Rate> Rate { get; set; }
    }
    public class Rate
    {
        
        public string CurrencyID { get; set; }
        
        public decimal BuyingRate { get; set; }
        
        public decimal SellingRate { get; set; }
        
        public decimal MeanRate { get; set; }
    }
}
