﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model
{
    public class DDMandate
    {
        
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        public string AccountID { get; set; }
        
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string Purpose { get; set; }
        
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string ReferenceNo { get; set; }
    }

    public class DDMandateView
    {
        public string AccountID { get; set; }
        public decimal? SerialNo { get; set; }
        public string Purpose { get; set; }
        public string Status { get; set; }
        public string ReferenceNo { get; set; }
    }
}
