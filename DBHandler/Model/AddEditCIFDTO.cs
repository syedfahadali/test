﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model
{
    public class AddEditCIFDTO
    {
        
        

        public AddEditNosroCustomer Customer { get; set; }
    }

    public class AddEditNosroCustomer
    {
        public string ClientID { get; set; }
        
        public string Name { get; set; }
        
       // [StringRangeAttribute(AllowableValues = new[] { "NOSTRO" }, ErrorMessage = "- Valid value is NOSTRO.")]
        public string CustomerSegment { get; set; }
        
        public string GroupCode { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "Low", "Medium", "High" }, ErrorMessage = "- Valid value is either Low,Medium or High.")]
        public string RiskProfile { get; set; }
        
        public string Phone { get; set; }
        
        public string Email { get; set; }
        
        public string Address { get; set; }
        
        [StringRangeAttribute(AllowableValues = new[] { "A", "P", "D" }, ErrorMessage = "- Valid value is either A,P or D.")]
        public string Status { get; set; }


    }

}
