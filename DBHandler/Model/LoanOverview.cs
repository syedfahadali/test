﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model.LoanOverview
{

    public class LoanOverviewModel
    {
        
        [Required(ErrorMessage = "Client Id is required")]
        public string ClientID { get; set; }
        [Required(ErrorMessage = "Facility Id is required")]
        public string FacilityID { get; set; }
    }

    public class Status
    {
        public string code { get; set; }
        public string severity { get; set; }
        public string statusMessage { get; set; }
    }

    public class LoanOverviewResponse
    {
        public string FacilityID { get; set; }
        public string ClientID { get; set; }
        public string CreditProgramID { get; set; }
        public string ProgramName { get; set; }
        public DateTime ReviewDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string SecuredType { get; set; }
        public string LimitType { get; set; }
        public decimal NoOfLoanProducts { get; set; }
        public decimal ApprovedLimit { get; set; }
        public decimal UtilizedLimit { get; set; }
        public decimal AvailableLimit { get; set; }
        public string Status { get; set; }
        public List<ProductLimit> ProductLimits { get; set; }
    }
    public class ProductLimit
    {
        public string ProductID { get; set; }
        public string ProductName { get; set; }
        public string ProductType { get; set; }
        public string CurrencyID { get; set; }
        public string LimitType { get; set; }
        public string LimitStatus { get; set; }
        public decimal ApprovedLimit { get; set; }
        public decimal UtilizedLimit { get; set; }
        public decimal AvailableLimit { get; set; }
    }
 

}
