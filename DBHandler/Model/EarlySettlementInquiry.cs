﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
namespace DBHandler.Model.EarlySettlementInquiry
{
    public class EarlySettlementInquiryModel
    {
        
        [Required(ErrorMessage = "ClientId is required")]
        public string ClientId { get; set; }
        [Required(ErrorMessage = "FacId is required")]
        public string FacId { get; set; }
        [Required(ErrorMessage = "ProdId is required")]
        public string ProdId { get; set; }
        [Required(ErrorMessage = "DealRefNo is required")]
        public string DealRefNo { get; set; }
        [Required(ErrorMessage = "SettlementDate is required")]
        public DateTime SettlementDate { get; set; }
        [Required(ErrorMessage = "principalAmt is required")]
        public decimal principalAmt { get; set; }
    }

    public class EarlySettlementInquiryResponse
    {
        public decimal Principal { get; set; }
        public decimal Interest { get; set; }
        public decimal Penal { get; set; }
        public decimal EarlySettleFee { get; set; }
        public decimal TotalAmt { get; set; }
    }
}
