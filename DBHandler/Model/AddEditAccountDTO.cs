﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model
{
    public class AddEditAccountDTO
    {
      

        
        public AddEditAccountModel Account { get; set; }
    }

    public class AddEditAccountModel
    {
        
        public string ClientID { get; set; }
        
        public string Name { get; set; }
        
        public string ProductID { get; set; }

        
        public string StatementAddress { get; set; }
        
        public string Country { get; set; }
        
        public string State { get; set; }
        
        public string City { get; set; }
        
        [StringRangeAttribute(AllowableValues = new[] { "N", "D", "W", "M", "Q", "H", "Y" }, ErrorMessage = "- Valid value is either N,D,W,M,Q,H or Y.")]
        public string StatementFrequency { get; set; }
        
        public string NatureOfAccount { get; set; }
        
        public string RelationshipFunction { get; set; }
        
        public string NostroAccountNumber { get; set; }
        
        public string NostroIBAN { get; set; }
        
        public string NostroSWIFT { get; set; }
        
        public string NostroAddress { get; set; }
        
        public string NostroCountry { get; set; }
        
        public string MinBalanceRequirement { get; set; }
        
        public string NostroBankName { get; set; }
        
        public string NostroBranchName { get; set; }
        
        public string NostroSortCode { get; set; }
        public string AccountId { get; set; }
        
        public string Phone { get; set; }
        
        public string Email { get; set; }
        
        public string MobileNo { get; set; }
    }
}
