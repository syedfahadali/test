﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model
{
    public class EditSweepDTO
    {
        
        public string ClientId { get; set; }
        public string sweepId { get; set; }
        public string onAccount { get; set; }
        public string coverAccount { get; set; }
        public string coverAccount2 { get; set; }
        public decimal? Amount { get; set; }

    }
}
