﻿using CommonModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DBHandler.Model
{
    public class FinancialEntry
    {
        [Required(ErrorMessage = "AccountId  is required")]
        public string AcctId { get; set; }
        [Required(ErrorMessage = "Account Type  is required")]
        public string AcctType { get; set; }
        [Required(ErrorMessage = "Entry Type  is required")]
        public string EntryType { get; set; }
        public DateTime? EffDt { get; set; }
        [Required(ErrorMessage = "Currency Code  is required")]
        public string CurCode { get; set; }
        [Required(ErrorMessage = "Amount  is required")]
        public decimal Amt { get; set; }

        public decimal ForeignAmount { get; set; }

        [Required(ErrorMessage = "Exchange Rate  is required")]
        public decimal Rate { get; set; }
        [Required(ErrorMessage = "Memo  is required")]
        public string Memo { get; set; }
        [Required(ErrorMessage = "Description Id  is required")]
        public string DescId { get; set; }

        public string ChequeId { get; set; }
        public DateTime? ChequeDate { get; set; }
        public string VAccountID { get; set; }
    }

    public class AccountInquiryRequest
    {
        
        

        public string AccountId { get; set; }
        public string MobileNo { get; set; }
        public string IBAN { get; set; }
        public string ClientId { get; set; }

    }
    public class AccountMandateViewModel
    {
        
        

        
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        public string AccountID { get; set; }
    }

    public class AccountMandateModel
    {
        
        

        
        public AccountMandate AccountMandate { get; set; }
    }

    public class MarkAccountStatusModel
    {
        
        

        
        public InsertAccountStatus AccountStatus { get; set; }
    }

    public class InsertAccountStatus
    {
        
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        public string AccountID { get; set; }

        
        public string Status { get; set; }

        
        public string Comments { get; set; }
        
        public DateTime? WorkingDate { get; set; }
    }

    public class UnMarkAccountStatusModel
    {
        
        

        
        public UpdateAccountStatus AccountStatus { get; set; }
    }

    public class UpdateAccountStatus
    {
        
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        public string AccountID { get; set; }
        
        public decimal? Serial { get; set; }
        
        public string Comments { get; set; }
    }

    public class AccountStatusMarkingModel
    {
        
        public Accountstatusmarking AccountStatus { get; set; }
    }

    public class GetAccountStatusMarkingModel
    {
        
        [Required(ErrorMessage = "AccountId  is required")]
        public string AccountId { get; set; }
    }
    public class Accountstatusmarking
    {
        public string AccountID { get; set; }
        public decimal Serial { get; set; }
        public DateTime WorkingDate { get; set; }
        public string Status { get; set; }
        public string Comments { get; set; }
    }

    public class CurrenciesInquiryRequest
    {
       

        [Required(ErrorMessage = "Currency ID required")]
        public string Currency { get; set; }
        public bool IsLive { get; set; }
    }

    public class TransferStatusModel
    {
        



        [Required(ErrorMessage = "Referance number is required")]
        public string ChannelRefID { get; set; }
    }

    public class NarrationModel
    {
        
    }

    public class TransferStatusResponse
    {
        public string Status { get; set; }
    }

    public class TransferModel
    {
       
        

        
        public List<FinancialEntry> FinancialEntries { get; set; }
        
        public string ChannelRefId { get; set; }
    }

    public class BalanceInquiryModel
    {          
                
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        public string AccountId { get; set; }
        public string ClientId { get; set; }
    }


    public class BalanceIBANInquiryModel
    {
        
        
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        public string AccountId { get; set; }

        [MaxLength(29, ErrorMessage = "- Character length should not be more than 29.")]
        public string IBAN { get; set; }
        public string ClientId { get; set; }
    }

    public class FreezeAccountModel
    {
        
        

        
        public InsertAccountFreeze Freeze { get; set; }
    }

    public class UpdateAccountFreezeModel
    {
        
        

        
        public UpdateAccountFreeze Freeze { get; set; }
    }

    public class GetTransactionModel
    {
        
        
        
        public DateTime StartDate { get; set; }
        
        public DateTime EndDate { get; set; }
        
        public string AccountID { get; set; }
        public string ClientId { get; set; }
    }


    public class TransactionView
    {
        public DateTime WorkingDate { get; set; }
        public DateTime ValueDate { get; set; }
        public string trxType { get; set; }
        public string ProductID { get; set; }
        public string CurrencyID { get; set; }
        public string ChequeID { get; set; }
        public DateTime? ChequeDate { get; set; }
        public decimal Amount { get; set; }
        public decimal ForeignAmount { get; set; }
        public decimal? ExchangeRate { get; set; }
        public string DescriptioID { get; set; }
        public string Description { get; set; }
    }

    public class LinkDebitCardStatusUpdateView
    {
        
        
        
        public string AccountID { get; set; }

        
        public string status { get; set; }

        
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        [MinLength(16, ErrorMessage = "- Valid Min length should be less than 16.")]
        public string DebitCardToken { get; set; }
    }

    public class LinkDebitCardStatusInsertView
    {
        
        
        
        public DebitCards DebitCards { get; set; }
    }

    public class DebitCardInquiryView
    {
        
        
        
        public string AccountID { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "", "Active", "Blocked", "Expired", }, ErrorMessage = "- Valid value is either N,D,W,M,Q,H or Y")]
        public string status { get; set; }

    }

    public class DebitCardCategoryInquiryView
    {
        
        
    }

    public class GetStatementView
    {
        
        
        
        [StringRangeAttribute(AllowableValues = new[] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" }, ErrorMessage = "- Valid value is either N,D,W,M,Q,H or Y")]
        public string Month { get; set; }
        
        public int Year { get; set; }
        
        public string AccountID { get; set; }
    }

    public class RealTimeSweepView
    {
        
        
        
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        [MinLength(16, ErrorMessage = "- Valid Min length should be less than 16.")]
        public string AccountID { get; set; }
        
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        [MinLength(16, ErrorMessage = "- Valid Min length should be less than 16.")]
        public string SIAccountID { get; set; }
    }

    public class InternalSweepView
    {
        
        

        
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        [MinLength(16, ErrorMessage = "- Valid Min length should be less than 16.")]
        public string AccountID { get; set; }

        
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        [MinLength(16, ErrorMessage = "- Valid Min length should be less than 16.")]
        public string SIAccountID { get; set; }

        
        [Range(0d, (double)decimal.MaxValue, ErrorMessage = "Amount must be greater than zero.")]
        public decimal Amount { get; set; }
        public string Comments { get; set; }
    }

    public class ModifyInternalAccountShortageSweepsView
    {
        
        

        
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        [MinLength(16, ErrorMessage = "- Valid Min length should be less than 16.")]
        public string AccountID { get; set; }

        
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        [MinLength(16, ErrorMessage = "- Valid Min length should be less than 16.")]
        public string SIAccountID { get; set; }

        
        [Range(0d, (double)decimal.MaxValue, ErrorMessage = "Amount must be greater than zero.")]
        public decimal Amount { get; set; }
        public string Comments { get; set; }
    }

    public class AddInternalAccountShortageSweepsView
    {
        

        
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        [MinLength(16, ErrorMessage = "- Valid Min length should be less than 16.")]
        public string AccountID { get; set; }

        
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        [MinLength(16, ErrorMessage = "- Valid Min length should be less than 16.")]
        public string SIAccountID { get; set; }

        
        [Range(0d, (double)decimal.MaxValue, ErrorMessage = "Amount must be greater than zero.")]
        public decimal Amount { get; set; }
        public string Comments { get; set; }
    }

    public class ModifyInternalAccountExcessSweepsView
    {
        
        

        
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        [MinLength(16, ErrorMessage = "- Valid Min length should be less than 16.")]
        public string AccountID { get; set; }

        
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        [MinLength(16, ErrorMessage = "- Valid Min length should be less than 16.")]
        public string SIAccountID { get; set; }

        
        [Range(0d, (double)decimal.MaxValue, ErrorMessage = "Amount must be greater than zero.")]
        public decimal Amount { get; set; }
        public string Comments { get; set; }
    }


    public class DDMandateCancelView
    {
        
        
        
        [MaxLength(100, ErrorMessage = "- Valid Min length should be less than 100.")]
        public string ReferenceNo { get; set; }
        
        [MinLength(16, ErrorMessage = "- Valid Min length should be less than 16.")]
        public string AccountID { get; set; }
    }
    public class CharityModel
    {
        
        
        [Required(ErrorMessage = "Account ID is required")]
        public string AccountID { get; set; }
        [Required(ErrorMessage = "Is Charity is required")]
        public int? IsCharity { get; set; }
        [Required(ErrorMessage = "Charity Amount is required")]
        public decimal? CharityAmount { get; set; }
    }

    public class CustomerPostingAccountModel
    {
        
        
        [Required(ErrorMessage = "Account ID is required")]
        public string AccountID { get; set; }
        [Required(ErrorMessage = "TrxType is required")]
        public string TrxType { get; set; }
        public string Operator { get; set; }
        public string Module { get; set; }
    }

    public class GetCustPostingAccountNew
    {
        public string RetStatus { get; set; }
        public decimal? ClearBalance { get; set; }
        public decimal? Effects { get; set; }
        public decimal? ShadowBalance { get; set; }
        public decimal? Limit { get; set; }
        //public string ClientID { get; set; }
        public string AccountName { get; set; }
        public string ProductID { get; set; }
        public string ProductName { get; set; }
        public string CurrencyID { get; set; }
        public string CurrencyName { get; set; }
        public string Reminder { get; set; }
        public decimal? BlockedAmount { get; set; }
        public decimal? TotalBalance { get; set; }
        public decimal? ProductMinBalance { get; set; }
    }

    public class InquiryNOSTROModel
    {
        
        
        
        [StringRangeAttribute(AllowableValues = new[] { "1279" }, ErrorMessage = "Valid value for this field is 1279")]
        public string BankID { get; set; }
    }

    public class IssueFixDepositResponse
    {
        public string ReferenceNo { get; set; }
        public string Recipts { get; set; }
    }

    public class DepositsDetailsModel
    {
        
        [MinLength(16, ErrorMessage = "- Valid Min length should be less than 16.")]
        public string AccountID { get; set; }
        
        public string ReceiptID { get; set; }
        
        
        public string ClientId { get; set; }
    }
    public class InquireOutstandingAmt
    {
        public string ClientID { get; set; }
        public string AccountId { get; set; }
        public string Name { get; set; }
        public string ProductID { get; set; }
        public string CurrencyID { get; set; }
        public decimal DrawableBalance { get; set; }
    }

    public class InquireLienModel
    {
        
        
        
        public string ReceiptID { get; set; }
    }
    public class InquireLien
    {
        public string AccountID { get; set; }
        public string RecieptID { get; set; }
        public decimal SerialNo { get; set; }
        public int IsLien { get; set; }
        public decimal LienAmount { get; set; }
        public string LienAccountID { get; set; }
        public string LienRemarks { get; set; }
    }

    public class LienUnlienMarkingModel
    {
        
        
        
        [MinLength(16, ErrorMessage = "- Valid Min length should be less than 16.")]
        public string AccountID { get; set; }
        
        public string ReceiptID { get; set; }
        
        [MinLength(16, ErrorMessage = "- Valid Min length should be less than 16.")]
        public string LienAccountID { get; set; }
        
        public string IsLien { get; set; }
        public decimal? LienAmount { get; set; }
        public decimal? LienID { get; set; }
        public string Remarks { get; set; }
    }

    public class DepositsListClientWiseModel
    {
        
        [MaxLength(9, ErrorMessage = "- Character length should not be more than 9.")]
        public string ClientID { get; set; }
        public string ProductID { get; set; }
        
        
    }


    public class DepositsListModel
    {
        
        [MaxLength(9, ErrorMessage = "- Character length should not be more than 9.")]
        public string ClientID { get; set; }
        
        
    }

    public class DepositsListAccountwiseModel
    {
        
        [MinLength(16, ErrorMessage = "- Valid Min length should be less than 16.")]
        public string AccountID { get; set; }
        
        public string ProductID { get; set; }
        
        
    }

    public class DepositReceiptwiseModel
    {
        
        public string ReceiptID { get; set; }
        
        
    }

    public class FDAccountsProductwiseModel
    {
        
        public string ProductID { get; set; }
        
        
    }

    public class FDAccountsProductwise
    {
        public string ClientID { get; set; }
        public string ProductID { get; set; }
        public string DepositAccountID { get; set; }
    }

    public class GetFDCollectionAccountsModel
    {
        
        public string ClientID { get; set; }
        
        public string ProductID { get; set; }
        
    }

    public class FDCollectionAccounts
    {
        public string AccountID { get; set; }
        public string AccountName { get; set; }
        public string ProductID { get; set; }
        public string ProductName { get; set; }
        public string CurrencyID { get; set; }
        public string CurrencyName { get; set; }
    }

    public class FDChargesModel
    {
        
        public string RecieptID { get; set; }
        
        public string AccountID { get; set; }
        
    }

    public class FDChargesArray
    {
        [Key]
        public string ChargeId { get; set; }
        public string ChargeSerial { get; set; }
        public decimal Amount { get; set; }
        public string ChargeOnPrincipal { get; set; }
        public string ChargeOnInterest { get; set; }
        public string Type { get; set; }
        public string ModeOfCharges { get; set; }
        public string IsRoundingRequired { get; set; }
        public int RoundingPrecision { get; set; }
        public string CreateBy { get; set; }
        public string CreateTime { get; set; }
        public string CreateTerminal { get; set; }
    }
    public class TDRLostModel
    {
        
        public string ReceiptID { get; set; }
        
        public string AccountID { get; set; }
        
        public string LostRemarks { get; set; }
        
    }
    public class ModifyFDModel
    {
        
        public string ReceiptID { get; set; }
        
        public string AutoProfit { get; set; }
        
        public string PaymentPeriod { get; set; }
        
        public string Treatment { get; set; }
        
        public string AutoRollover { get; set; }
        
        public string OptionsAtMaturity { get; set; }
        
        
    }

    public class OpenFDAccountModel
    {
        
        public string ClientID { get; set; }
        
        public string ProductID { get; set; }
        
        
    }

    public class OpenFixDepositModel
    {
        
        public TDRIssuanceModel TDRIssuance { get; set; }
        
        public int NoOfIssues { get; set; }
        


    }

    public class DepositsListAccountwiseTDRIssuance
    {
        public string RecieptID { get; set; }
        public string ProductID { get; set; }
        public string AccountID { get; set; }
        public string NAccount { get; set; }
        public decimal Amount { get; set; }
        public DateTime IssueDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime MaturityDate { get; set; }
        public decimal Rate { get; set; }
        public string AutoProfit { get; set; }
        public string PaymentPeriod { get; set; }
        public string Treatment { get; set; }
        public decimal? TotalLienAmount { get; set; }
        public decimal? AvailableLienAmount { get; set; }
        public string Status { get; set; }
        public string AutoRollover { get; set; }
        public string ParentReceiptID { get; set; }
    }

    public class DepositReceiptwiseTDRIssuance
    {
        public string RecieptID { get; set; }
        public string ProductID { get; set; }
        public string AccountID { get; set; }
        public string NAccount { get; set; }
        public decimal Amount { get; set; }
        public DateTime IssueDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime MaturityDate { get; set; }
        public decimal Rate { get; set; }
        public string AutoProfit { get; set; }
        public string PaymentPeriod { get; set; }
        public string Treatment { get; set; }
        public decimal? TotalLienAmount { get; set; }
        public decimal? AvailableLienAmount { get; set; }
        public string Status { get; set; }
        public string AutoRollover { get; set; }
        public string OptionsAtMaturity { get; set; }
        public string ParentReceiptID { get; set; }
    }

    public class DepositsListClientWiseTDRIssuance
    {
        public string RecieptID { get; set; }
        public string ProductID { get; set; }
        public string AccountID { get; set; }
        public string NAccount { get; set; }
        public decimal Amount { get; set; }
        public DateTime IssueDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime MaturityDate { get; set; }
        public decimal Rate { get; set; }
        public string AutoProfit { get; set; }
        public string PaymentPeriod { get; set; }
        public string Treatment { get; set; }
        public decimal? TotalLienAmount { get; set; }
        public decimal? AvailableLienAmount { get; set; }
        public string Status { get; set; }
        public string AutoRollover { get; set; }
        public string ParentReceiptID { get; set; }
    }

    public class DepositsListTDRIssuance
    {
        public string RecieptID { get; set; }
        public string ProductID { get; set; }
        public string AccountID { get; set; }
        public string NAccount { get; set; }
        public decimal Amount { get; set; }
        public DateTime IssueDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime MaturityDate { get; set; }
        public decimal Rate { get; set; }
        public string AutoProfit { get; set; }
        public string PaymentPeriod { get; set; }
        public string Treatment { get; set; }
        public decimal? TotalLienAmount { get; set; }
        public decimal? AvailableLienAmount { get; set; }
        public string Status { get; set; }
        public string AutoRollover { get; set; }
    }

    public class TDRIssuanceModel
    {
        public string ClientId { get; set; }
        
        public string ProductID { get; set; }
        
        [MinLength(16, ErrorMessage = "- Valid Min length should be less than 16.")]
        public string AccountID { get; set; }
        
        [MinLength(16, ErrorMessage = "- Valid Min length should be less than 16.")]
        public string NAccount { get; set; }
        
        public decimal Amount { get; set; }
        
        public DateTime IssueDate { get; set; }
        
        public DateTime StartDate { get; set; }
        
        public DateTime MaturityDate { get; set; }
        
        public decimal Rate { get; set; }
        
        public string AutoProfit { get; set; }
        
        [StringRangeAttribute(AllowableValues = new[] { "Monthly", "Quarterly", "Half Yearly", "Yearly", "None" }, ErrorMessage = "Valid value for this field is 1279")]
        public string PaymentPeriod { get; set; }
        
        [StringRangeAttribute(AllowableValues = new[] { "Encashment", "Principal Rollover", "Principal Plus Profit", "Principal Plus Profit Encashment", "Principal" }, ErrorMessage = "Valid value for this field is 1279")]
        public string Treatment { get; set; }
        public string Status { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "Yes", "No", "None" }, ErrorMessage = "Valid value for this field is 1279")]
        public string AutoRollover { get; set; }
        public string ParentReceiptID { get; set; }
    }

    public class DepositRatesModel
    {
        
        public string ProductID { get; set; }
        public string ReciptID { get; set; }
        public DateTime Date { get; set; }
        
        public string ClientId { get; set; }
    }

    public class DepositRatesDTO
    {
        public List<DepositRates> DepositRates { get; set; } = new List<DepositRates>();
        public List<SpecialRates> SpecialRates { get; set; } = new List<SpecialRates>();
        public List<PrematureRates> PrematureRates { get; set; } = new List<PrematureRates>();
    }


    public class DepositRates
    {
        public string ProductID { get; set; }
        public decimal? SerialID { get; set; }
        public string Type { get; set; }
        public decimal? MinAmount { get; set; }
        public decimal? MaxAmount { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public decimal? MonthRate { get; set; }
        public decimal? WeightRate { get; set; }
        public decimal? PremiumRate { get; set; }
        public decimal? PreMatureRate { get; set; }
        public decimal? Accrual { get; set; }
        public decimal? tAccrual { get; set; }
    }

    public class SpecialRates
    {
        public string RecieptID { get; set; }
        public string SerialID { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public decimal? Rate { get; set; }
    }

    public class PrematureRates
    {
        public string RecieptID { get; set; }
        public string SerialID { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public decimal? Rate { get; set; }
    }


    public class ChequeResponseModel
    {
        public string ChqBookTypeID { get; set; }
        public string ChequeStart { get; set; }
        public string ChequeEnd { get; set; }
    }


    public class ChequeModel
    {
        
        
        
        [MinLength(16, ErrorMessage = "- Valid Min length should be less than 16.")]
        public string AccountID { get; set; }
        
        public string ChequeStart { get; set; }
        
        public int NoOfLeaves { get; set; }
        
        public string ChqBookTypeID { get; set; }
        
        [StringRangeAttribute(AllowableValues = new[] { "1", "0" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public int ApplyCharges { get; set; }
    }

    public class ChequeTypeModel
    {
        
        
        
        [MinLength(3, ErrorMessage = "- Valid Min length should be less than 3.")]
        [MaxLength(10, ErrorMessage = "- Character length should not be more than 10.")]
        public string ProductID { get; set; }
    }

    public class RealTimeSweepResponse
    {
        public string AccountID { get; set; }
        public decimal? Amount { get; set; }
        public string Comments { get; set; }
    }

    public class CancelRealTimeSweepView
    {
        
        
        
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        [MinLength(16, ErrorMessage = "- Valid Min length should be less than 16.")]
        public string AccountID { get; set; }
    }

    public class DDMandateInsertView
    {
        
        

        
        public DDMandate DDMandate { get; set; }
    }

    public class CustomerStatement
    {
        [Key]
        public DateTime? WDate { get; set; }
        public DateTime? ValueDate { get; set; }
        public string DescriptionID { get; set; }
        public string Description { get; set; }
        public string ChequeID { get; set; }
        public DateTime? ChequeDate { get; set; }
        public string TrxType { get; set; }
        public decimal? Amount { get; set; }
        public decimal? ForeignAmount { get; set; }
        public decimal? ExchangeRate { get; set; }
        public string ProductID { get; set; }
        public string CurrencyID { get; set; }
    }


    public class AccrualRatesModel
    {
        
        
        
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        [MinLength(16, ErrorMessage = "- Valid Min length should be less than 16.")]
        public string AccountID { get; set; }
    }

    public class CloseFixedDepositModel
    {
        
        
        
        public string ReceiptID { get; set; }
        
        public string AccountID { get; set; }
        //
        //[Range(0, 50, ErrorMessage = "Pre-Mature Rate must be between 0 to 50")]
        public decimal? PreMatureRate { get; set; }
        
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public string Penalty { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "R", "F", "" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public string PenaltyType { get; set; }
        public decimal PenaltyAmount { get; set; }
        public decimal? PenaltyRate { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N", "" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public string ZeroInterest { get; set; }
        public string ClientId { get; set; }
    }

    public class CountryLimitsModel
    {
        
        
        
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        [MinLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string CountryID { get; set; }
        
        public decimal countryLimit { get; set; }
    }

    public class CountryLimits
    {
        public string CountryID { get; set; }
        public decimal CountryLimit { get; set; }
        public string ReferenceID { get; set; }
    }

    public class InquirCountryLimitsModel
    {
        
        

        
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        [MinLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string CountryID { get; set; }
    }

    public class VAResponse
    {
        public string VirtualAccountID { get; set; }
        public string AccountTitle { get; set; }
        public string ContactNo { get; set; }
        public string EmailID { get; set; }
        public string Address { get; set; }
        public string ClientID { get; set; }
        public string CollateralID { get; set; }
        public string Status { get; set; }
        public string VirtualAccIBAN { get; set; }
        public string VirtualAccSwiftCode { get; set; }
    }

    public class GetCharity
    {
        public string AccountID { get; set; }
        public decimal MonthCharity { get; set; }
        public decimal TotalCharity { get; set; }
    }

    public class GetVirtualAccountModel
    {
        
        

        
        [MaxLength(9, ErrorMessage = "- Character length should not be more than 9.")]
        [MinLength(9, ErrorMessage = "- Character length should not be more than 9.")]
        public string ClientID { get; set; }
    }
    public class CancelVirtualAccountModel
    {
        
        

        
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        [MinLength(16, ErrorMessage = "- Character length should not be less than 16.")]
        public string VirtualAccountID { get; set; }
    }

    public class VirtualAccountResponse
    {
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        public string VirtualAccountID { get; set; }
        public string ReferenceID { get; set; }
        public string VirtualAccIBAN { get; set; }
        public string VirtualAccSwiftCode { get; set; }
    }


    public class VirtualAccountModel
    {
        
        
        
        public VirtualAccounts VirtualAccounts { get; set; }
    }

    public class PenaltyMatrix
    {
        public string ProductID { get; set; }
        public decimal SerialNo { get; set; }
        public decimal MinSlabAmount { get; set; }
        public decimal MaxSlabAmount { get; set; }
        public decimal MinDays { get; set; }
        public decimal MaxDays { get; set; }
        public decimal Rate { get; set; }
        public string Description { get; set; }
    }

    public class RelationshipOfficer
    {
        public string OfficerID { get; set; }
        public string FullName { get; set; }
    }

    public class RelationshipOfficerModel
    {
        
        
    }

    public class PenaltyMatrixModel
    {
        
        
        public string ProductID { get; set; }
    }


    public class GetLoanAccountsPerCIFModel
    {
        
        
        [MaxLength(9, ErrorMessage = "- Character length should not be more than 9.")]
        
        public string ClientID { get; set; }
    }

    public class GetLoanDetailsPerAccountModel
    {
        
        
        
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        public string AccountID { get; set; }
        public string ClientId { get; set; }
    }


    public class ExpiredCIFModel
    {
        
        
        
        public string type { get; set; }
        public int kycPeriod { get; set; }
    }

    public class CheckIDForCIFModel
    {
        
        

        
        public string IDNumber { get; set; }

        
        public string Type { get; set; }
    }

    public class UpdateKYCDateModel
    {
        
        

        [MaxLength(9, ErrorMessage = "- Character length should not be more than 9.")]
        
        public string ClientID { get; set; }

        
        [FutureDateAttribute(ErrorMessage = " is not a valid date")]
        public DateTime KYCDate { get; set; }
    }

    public class GetLoanScheduleDetailsModel
    {
        
        
        
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string DealReferenceNo { get; set; }
    }

    public class LoanAdviceModel
    {
        
        
        
        public string ClientID { get; set; }
        
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        public string AccountID { get; set; }
        //
        public string DealId { get; set; }
    }

    public class LoanAdviceModelV2
    {

        

        public string ClientID { get; set; }

        public string AccountID { get; set; }
        //
        public string DealId { get; set; }
    }

    public class LiabilityLetterModel
    {
        
        
        
        public string ClientID { get; set; }
    }

    public class LiabilityLetterModelV2
    {

        

        public string ClientID { get; set; }
    }

    public class NoLiabilityLetterModelV2
    {

        

        public string ClientID { get; set; }
    }

    public class BalanceConfirmationModelV2
    {

        

        public string ClientID { get; set; }
    }



    public class LoanDetails
    {
        public string loanAccount { get; set; }
        public string accountName { get; set; }
        public string approvedLimit { get; set; }
        public string availableForDisbursal { get; set; }
        public string loanType { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string currency { get; set; }
        public string amount { get; set; }
        public string dealReferenceNumber { get; set; }
        public string totalNumberOfInstallments { get; set; }
        public string interestRate { get; set; }
        public string paymentFrequency { get; set; }
        public string fundingAccount { get; set; }

    }
    public class PaymentScheduler
    {
        public string valueDate { get; set; }
        public string amount { get; set; }
        public string interestRate { get; set; }
        public string principalAmountPaid { get; set; }
        public string interestPaid { get; set; }
        public string outstandingAmount { get; set; }
    }
    public class NoLiabilityLetterResponse
    {
        public string letterDate { get; set; }
        public string letterReference { get; set; }
        public List<string> customerNameAddress { get; set; }
        public string cifNumber { get; set; }
        public string requestDate { get; set; }
    }



    public class NoLiabilityLetterResponseV2
    {
        public string letterDate { get; set; }
        public string letterReference { get; set; }
        public string customerName { get; set; }
        public List<string> customerAddress { get; set; }
        public string cifNumber { get; set; }
        public string requestDate { get; set; }
    }
    public class BalanceConfirmationResponse
    {
        public string letterDate { get; set; }
        public string letterReference { get; set; }
        public List<string> customerNameAddress { get; set; }
        public string requestDate { get; set; }
        public List<AccountsModel> accounts { get; set; }
    }

    public class BalanceConfirmationResponseV2
    {
        public string letterDate { get; set; }
        public string letterReference { get; set; }
        public bool toCustomer { get; set; }
        public bool toAuditor { get; set; }
        public string name { get; set; }
        public List<string> nameAndAddress { get; set; }
        public string requestDate { get; set; }
        public List<AccountsModelV2> accounts { get; set; }
    }
    public class AccountsModelV2
    {
        public string product { get; set; }
        public string accountNumber { get; set; }
        public string currency { get; set; }
        public string balance { get; set; }
        public string debitCredit { get; set; }
        public decimal lienBlockAmount { get; set; }
        public decimal receivables { get; set; }
        public decimal totalAmount { get; set; }
    }
    public class AccountsModel
    {
        public string accountNumber { get; set; }
        public string accountType { get; set; }
        public string currency { get; set; }
        public string balance { get; set; }
    }
    public class NoLiabilityLetterModel
    {
        
        public string ClientID { get; set; }
    }

    public class LiabilityLetterResponse
    {
        public string letterDate { get; set; }
        public string letterReference { get; set; }
        public List<string> customerNameAddress { get; set; }
        public string cifNumber { get; set; }
        public string requestDate { get; set; }
        public List<OverDraftFacilitiesModel> overdraftFacilities { get; set; }
        public List<loanFacilitiesModel> loanFacilities { get; set; }
        public List<bankGuaranteeFacilitiesModel> bankGuaranteeFacilities { get; set; }
    }

    public class LiabilityLetterResponseV2
    {
        public string letterDate { get; set; }
        public string letterReference { get; set; }
        public string customerName { get; set; }
        public List<string> customerAddress { get; set; }
        public string cifNumber { get; set; }
        public string requestDate { get; set; }
        public List<OverDraftFacilitiesModelV2> overdraftFacilities { get; set; }
        public List<loanFacilitiesModelV2> loanFacilities { get; set; }
        public List<bankGuaranteeFacilitiesModelV2> bankGuaranteeFacilities { get; set; }
    }


    public class OverDraftFacilitiesModelV2
    {
        public string serialNumber { get; set; }
        public string facilityReference { get; set; }
        public string facilityType { get; set; }
        public string currency { get; set; }
        public string principalOutstanding { get; set; }
        public string accruedInterest { get; set; }
        public string totalOutstanding { get; set; }
    }

    public class loanFacilitiesModelV2
    {
        public string serialNumber { get; set; }
        public string facilityReference { get; set; }
        public string facilityType { get; set; }
        public string currency { get; set; }
        public string principalOutstanding { get; set; }
        public string accruedInterest { get; set; }
        public string earlySettlementCharges { get; set; }
        public string totalOutstanding { get; set; }
    }

    public class bankGuaranteeFacilitiesModelV2
    {
        public string serialNumber { get; set; }
        public string facilityReference { get; set; }
        public string facilityType { get; set; }
        public string currency { get; set; }
        public string principalOutstanding { get; set; }
        public string accruedInterest { get; set; }
        public string earlySettlementCharges { get; set; }
        public string totalOutstanding { get; set; }
    }




    public class bankGuaranteeFacilitiesModel
    {
        public string serialNumber { get; set; }
        public string facilityReference { get; set; }
        public string facilityType { get; set; }
        public string currency { get; set; }
        public string principalOutstanding { get; set; }
        public string accruedInterest { get; set; }
        public string earlySettlementCharges { get; set; }
        public string totalOutstanding { get; set; }
    }
    public class loanFacilitiesModel
    {
        public string serialNumber { get; set; }
        public string facilityReference { get; set; }
        public string facilityType { get; set; }
        public string currency { get; set; }
        public string principalOutstanding { get; set; }
        public string accruedInterest { get; set; }
        public string earlySettlementCharges { get; set; }
        public string totalOutstanding { get; set; }
    }
    public class OverDraftFacilitiesModel
    {
        public string serialNumber { get; set; }
        public string facilityReference { get; set; }
        public string facilityType { get; set; }
        public string currency { get; set; }
        public string principalOutstanding { get; set; }
        public string accruedInterest { get; set; }
        public string totalOutstanding { get; set; }
    }
    public class LoanAdviceResponse
    {
        public string companyName { get; set; }
        public string companyAddress { get; set; }
        public string contactDetails { get; set; }
        public string adviceDate { get; set; }
        public LoanDetails loanDetails { get; set; }
        public List<string> termsAndCondition { get; set; }
        public List<PaymentScheduler> paymentScheduler { get; set; }

    }

    public class LoanAdviceResponseV2
    {
        public string companyName { get; set; }
        public string companyAddress { get; set; }
        public string contactDetails { get; set; }
        public string adviceDate { get; set; }
        public LoanDetailsV2 loanAdvice { get; set; }
        public TermsAndConditionsV2 termsAndCondition { get; set; }
        public List<PaymentSchedulerV2> paymentScheduler { get; set; }

    }

    public class LoanDetailsV2
    {
        public string loanAccount { get; set; }
        public string accountName { get; set; }
        public string approvedLimit { get; set; }
        public string availableForDisbursal { get; set; }
        public string loanType { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string currency { get; set; }
        public string amount { get; set; }
        public string dealReferenceNumber { get; set; }
        public string totalNumberOfInstallments { get; set; }
        public string interestRate { get; set; }
        public string paymentFrequency { get; set; }
        public string repaymentAccount { get; set; }

    }

    public class TermsAndConditionsV2
    {
        public string requestDate { get; set; }
    }

    public class PaymentSchedulerV2
    {
        public string valueDate { get; set; }
        public string amount { get; set; }
        public string interestRate { get; set; }
        public string principalAmountPaid { get; set; }
        public string interestPaid { get; set; }
        public string outstandingAmount { get; set; }
    }


    public class DepositAdviceModel
    {
        
        
        
        public string ClientID { get; set; }
        
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        public string AccountID { get; set; }
        
        public string RecieptID { get; set; }
        public string BeneAccountID { get; set; }
    }



    public class DepositAdviceModelV2
    {

        

        public string ClientID { get; set; }

        public string AccountID { get; set; }

        public string RecieptID { get; set; }
        public string BeneAccountID { get; set; }
    }


    public class DepositDetails
    {
        public string depositAccount { get; set; }
        public string accountName { get; set; }
        public string depositProductType { get; set; }
        public string startDate { get; set; }
        public string maturityDate { get; set; }
        public string tenor { get; set; }
        public string currency { get; set; }
        public string originalAmount { get; set; }
        public string currentAmount { get; set; }
        public string dealReferenceNumber { get; set; }
        public string numberOfPayouts { get; set; }
        public string interestRate { get; set; }
        public string paymentFrequency { get; set; }
        public string maturityInstruction { get; set; }
        public string fundingAccount { get; set; }
        public string paymentAccount { get; set; }

    }
    public class DepositSchedule
    {
        public string valueDate { get; set; }
        public string amount { get; set; }
        public string interestRate { get; set; }
        public string paymentType { get; set; }

    }
    public class DepositEarlySettlementConditions
    {
        [JsonProperty("0M3M")]
        public string R0M3M { get; set; }
        [JsonProperty("3M6M")]
        public string R3M6M { get; set; }
        [JsonProperty("6M1Y")]
        public string R6M1Y { get; set; }
        [JsonProperty("1Y1Y3M")]
        public string R1Y1Y3M { get; set; }
        [JsonProperty("1Y3M1Y6M")]
        public string R1Y3M1Y6M { get; set; }
        [JsonProperty("1Y6M2Y")]
        public string R1Y6M2Y { get; set; }
        [JsonProperty("2Y3Y")]
        public string R2Y3Y { get; set; }
        [JsonProperty("3Y4Y")]
        public string R3Y4Y { get; set; }
        [JsonProperty("4Y5Y")]
        public string R4Y5Y { get; set; }

    }

    public class DepositAdviceResponse
    {
        public string companyName { get; set; }
        public string companyAddress { get; set; }
        public string contactDetails { get; set; }
        public string adviceDate { get; set; }

        public DepositDetails depositDetails { get; set; }
        public DepositEarlySettlementConditions depositEarlySettlementConditions { get; set; }
        public List<DepositSchedule> paymentScheduler { get; set; } = new List<DepositSchedule>();

    }

    public class DepositAdviceResponseV2
    {
        public string date { get; set; }
        public string companyName { get; set; }
        public string companyAddress { get; set; }
        public string contactDetails { get; set; }
        public string depositAccount { get; set; }
        public string accountName { get; set; }
        public string productType { get; set; }
        public string depositAmount { get; set; }
        public string maturityDate { get; set; }
        public string tenor { get; set; }
        public string currency { get; set; }
        public string currentAmount { get; set; }
        public string originalAmount { get; set; }

        public string referenceNumber { get; set; }
        public string startDate { get; set; }
        public string interestRate { get; set; }
        public string maturityAmount { get; set; }
        public string maturityInstruction { get; set; }
        public string sourceAccount { get; set; }
        public string repaymentAccount { get; set; }

        public List<DepositScheduleV2> paymentSchedule { get; set; }

    }

    public class DepositScheduleV2
    {
        public string valueDate { get; set; }
        public string amount { get; set; }
        public string interestRate { get; set; }
        public string paymentType { get; set; }

    }

    public class GetBasicAccountInquiryModel
    {
        
        
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        public string AccountID { get; set; }

        [MinLength(23, ErrorMessage = "- Character length should not be more than 23.")]
        public string IBAN { get; set; }
    }

    public class GetTotalCharityDto
    {
        
        
        [Required(ErrorMessage = "Account ID is required")]
        public string AccountID { get; set; }
    }

    public class MultiBranchRq
    {
        [Required(ErrorMessage = "Sign on header is required")]
        
        
        public List<MultiBranchEntry> TransferEntries { get; set; }
        [Required(ErrorMessage = "Channel Reference ID is require")]
        public string ChannelRefId { get; set; }
    }

    public class MultiBranchRqV2
    {

        

        public List<MultiBranchRqEntryV2> TransferEntries { get; set; }
        public string ChannelRefId { get; set; }
        public string VendorID { get; set; }
    }

    public class InquireCostCenterDTO
    {


        
    }

    public class MultiBranchRqEntryV2
    {

        public string BranchId { get; set; }

        public string AccountId { get; set; }

        public string AccountType { get; set; }

        public string EntryType { get; set; }

        public string CurrencyID { get; set; }

        public string NarrationId { get; set; }

        public decimal Amount { get; set; }


        public decimal ExchangeRate { get; set; }

        public decimal CoverRate { get; set; }


        public decimal LocalEq { get; set; }

        public string Memo { get; set; }

        public string ValueDate { get; set; }
        public string ChequeId { get; set; }
        public DateTime? ChequeDate { get; set; }
        public string CostCenterId { get; set; }
        public string ProjectId { get; set; }

        public string CostTypeId { get; set; }

        public string CustomerLifecycleID { get; set; }

    }
    public class UpdateSOCIDModel
    {
        [Required(ErrorMessage = "Sign on header is required")]
        
        public string socId { get; set; }
        public string accountId { get; set; }
    }


    public class UpdateCIFStatusDTO
    {

        
        

        
        public string ClientId { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "A", "P", "D" }, ErrorMessage = "valid value for this field is A,P or D")]
        public string Status { get; set; }
    }
    public class MultiBranchEntry
    {
        [Required(ErrorMessage = "Branch ID is required")]
        public string BranchId { get; set; }
        [Required(ErrorMessage = "Account ID is required")]
        public string AccountId { get; set; }
        [Required(ErrorMessage = "Account Type is required")]
        public string AccountType { get; set; }
        [Required(ErrorMessage = "Entry Type is required")]
        public string EntryType { get; set; }
        [Required(ErrorMessage = "Currency ID is required")]
        public string CurrencyID { get; set; }
        [Required(ErrorMessage = "Narration Id is required")]
        public string NarrationId { get; set; }
        [Required(ErrorMessage = "Amount is required")]
        public decimal Amount { get; set; }
        [Required(ErrorMessage = "Exchange Rate is required")]
        public decimal ExchangeRate { get; set; }
        [Required(ErrorMessage = "Cover Rate is required")]
        public decimal CoverRate { get; set; }
        [Required(ErrorMessage = "Local Eq. is required")]
        public decimal LocalEq { get; set; }
        [Required(ErrorMessage = "Memo is required")]
        public string Memo { get; set; }
        public string ValueDate { get; set; }
        public string ChequeId { get; set; }
        public DateTime? ChequeDate { get; set; }
    }


    public class UpdateBasicCIFDetails
    {

        
        

        
        [MaxLength(9, ErrorMessage = "- Character length should not be more than 9.")]
        public string ClientId { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        [StringRangeAttribute(AllowableValues = new[] { "Low", "Medium", "High" }, ErrorMessage = "- Valid value is either Low,Medium or High.")]
        public string RiskProfile { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        [MaxLength(1, ErrorMessage = "Black List length should be less than 1")]
        
        public string BlackList { get; set; }


        [MaxLength(1, ErrorMessage = "PEP length should be less than 1")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        
        public string PEP { get; set; }

        //
        [FutureDateAttribute(ErrorMessage = " is not a valid date")]
        public DateTime? KYCReviewDate { get; set; }

        [Range(1, 72, ErrorMessage = "Period must be between 1 to 72")]
        public decimal? Period { get; set; }
    }

    public class ModifyVirtualAccountModel
    {
        
        
        
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        [MinLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        public string VirtualAccountID { get; set; }
        
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        [MinLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string AccountTitle { get; set; }
        [MaxLength(20, ErrorMessage = "- Character length should not be more than 20.")]
        public string ContactNo { get; set; }
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        [EmailAddress(ErrorMessage = "Enter valid email")]
        public string EmailID { get; set; }
        //[MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        public string Address { get; set; }
        public string CollateralID { get; set; }
    }

    public class VirtualAccounts
    {
        
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        [MinLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        public string AccountID { get; set; }

        //[MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        public string Address { get; set; }

        
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        [MinLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string AccountTitle { get; set; }

        [MaxLength(20, ErrorMessage = "- Character length should not be more than 20.")]
        public string ContactNo { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        [EmailAddress(ErrorMessage = "Enter valid email")]
        public string EmailID { get; set; }
        public string CollateralID { get; set; }
    }

    public class CustomerCardDataModel
    {
        
        
        
        public Publicdata publicdata { get; set; }
    }

    public class FDPartialWithdrawalModel
    {
        
        
        
        public string ReceiptID { get; set; }
        
        public string AccountID { get; set; }
        //
        //[Range(0, 50, ErrorMessage = "Pre-Mature Rate must be between 0 to 50")]
        public decimal? PreMatureRate { get; set; }
        //
        //[Range(0, 50, ErrorMessage = "New Receipt Rate must be between 0 to 50")]
        public decimal? NewReceiptRate { get; set; }
        
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public string Penalty { get; set; }
        [StringRangeAttribute(AllowableValues = new[] { "R", "F", "" }, ErrorMessage = "- Valid value is either 1 or 0.")]
        public string PenaltyType { get; set; }
        public decimal PenaltyAmount { get; set; }
        public decimal? PenaltyRate { get; set; }
        
        public decimal PartialAmount { get; set; }
        public string ClientId { get; set; }
    }

    public class CalculateMaturityDateModel
    {
        
        
        
        public string ProductID { get; set; }
        
        public DateTime ValueDate { get; set; }
        
        public int NoOfDays { get; set; }
        
        public decimal Rate { get; set; }
        
        public decimal Amount { get; set; }
    }

    public class MaturityValues
    {
        public DateTime MaturityDate { get; set; }
        public decimal MaturityAmount { get; set; }
    }

    public class CloseFXResponse
    {
        public string ReturnStatus { get; set; }
        public string ReturnTitle { get; set; }
        public string ReturnMessage { get; set; }
    }

    public class FDPartialWithdrawalResponse
    {
        public string AccountID { get; set; }
        public string ReceiptID { get; set; }
        public string RefNo { get; set; }
        public string NewReceiptID { get; set; }
    }

    public class RealTimeSweepView1
    {
        
        
        
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        [MinLength(16, ErrorMessage = "- Valid Min length should be less than 16.")]
        public string AccountID { get; set; }
        
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        [MinLength(16, ErrorMessage = "- Valid Min length should be less than 16.")]
        public string SIAccountID1 { get; set; }
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        public string SIAccountID2 { get; set; }
        public string Comments { get; set; }
    }

    public class ModifyRealTimeSweepsView
    {
        
        
        
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        [MinLength(16, ErrorMessage = "- Valid Min length should be less than 16.")]
        public string AccountID { get; set; }
        
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        [MinLength(16, ErrorMessage = "- Valid Min length should be less than 16.")]
        public string SIAccountID1 { get; set; }
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        public string SIAccountID2 { get; set; }
        public string Comments { get; set; }
    }

    public class RealTimeSweepResponse1
    {
        public string SIAccountID1 { get; set; }
        public string SIAccountID2 { get; set; }
        public string Comments { get; set; }
    }

    public class GetBasicAccountInquiryResponse
    {
        public string OurBranchID { get; set; }
        public string ClientID { get; set; }
        public string ClientName { get; set; }
        public string AccountID { get; set; }
        public string IBAN { get; set; }
        public string AccountTitle { get; set; }
        public string AccountType { get; set; }
        public string CurrencyID { get; set; }
        public string AccountStatus { get; set; }
        public string Segment { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerEmail { get; set; }
        public string GroupCode { get; set; }
        public string CountryCode { get; set; }
        public string EconomicActivityCode { get; set; }
        public int DebitFrozen { get; set; }
        public int CreditFrozen { get; set; }
        public decimal? TotalBalance { get; set; }
        public decimal? AvailableBalance { get; set; }
        public decimal? DrawableBalance { get; set; }
    }

    public class LoanAccountsPerCIFResponse
    {
        public string LoanAccountID { get; set; }
        public string AccountTitle { get; set; }
        public decimal? ApprovedLimits { get; set; }
        public decimal? UsedLimits { get; set; }
        public decimal? Balance { get; set; }
    }

    public class LoanDetailsPerAccountResponse
    {
        public decimal DealId { get; set; }
        public string ReferenceNo { get; set; }
        public decimal? Amount { get; set; }
        public string LoanTypeCode { get; set; }
        public string LoanTypeDesc { get; set; }
        public DateTime? DisbursementDate { get; set; }
        public DateTime? MaturityDate { get; set; }
        public string CurrencyID { get; set; }
        public string PaymentMethod { get; set; }
        public string InterestRateType { get; set; }
        public decimal? InterestRate { get; set; }
        public string BaseRate { get; set; }
        public decimal? Spread { get; set; }
        public decimal OutStandingAmount { get; set; }
        public decimal? TotalInstallments { get; set; }
        public int PaidInstallments { get; set; }
        public decimal? RemainingInstallments { get; set; }
    }

    public class LoanScheduleDetailsResponse
    {
        public string InstallmentStatus { get; set; }
        public decimal? InstallmentAmount { get; set; }
        public DateTime? DueDate { get; set; }
        public decimal? Rate { get; set; }
    }
}