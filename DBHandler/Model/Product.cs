﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DBHandler.Model
{
    [Table("t_Products")]
    public class Product
    {
        [Key]
        public int Id { get; set; }

        public int CompanyId { get; set; }
        public int ProductId { get; set; }
        public string AcctKey { get; set; }
        public string AcctType { get; set; }
        public string ProductCode { get; set; }
        public string ShortCode { get; set; }
        public bool IsLibsharaProduct { get; set; }
        public string Description { get; set; }
        public string CurrencyId { get; set; }
        public int Rounding { get; set; }
        public bool CheckBlackList { get; set; }
        public bool CheckAge { get; set; }
        public int MinimumAge { get; set; }
        public int MaximumAge { get; set; }
        public bool CheckIdExpiry { get; set; }
        public bool CheckResident { get; set; }
        public bool CheckNationality { get; set; }
        public int AccountLimit { get; set; }
      
        public bool DebitCard { get; set; }
        public bool CheckGIDExpiry { get; set; }
        public string DailyLimit { get; set; }
        public bool IsTopUp { get; set; }
        public bool IsTransfer { get; set; }
        public bool ToOwn { get; set; }
        public bool ToLocal { get; set; }
        public bool ToOther { get; set; }
        public bool ToInternational { get; set; }
        public bool ToKfh { get; set; }
        public bool OwnTopUp { get; set; }
        public bool DebitTopUp { get; set; }
        public bool IsWakala { get; set; }
        public bool IsFlexi { get; set; }
        public bool IsWakalaFriendly { get; set; }
        public bool Closure { get; set; }
        public double Rate1 { get; set; }
        public double Rate2 { get; set; }
        public bool IsPartial { get; set; }
        public bool IsTransaction { get; set; }
        public double? MinimumDeposit { get; set; }
    }

    public class NostroAccounts
    {
        public string SerialID { get; set; }
        public string BankID { get; set; }
        public string BankName { get; set; }
        public string CountryID { get; set; }
        public string CountryName { get; set; }
        public string SWIFTCode { get; set; }
        public string SORTCode { get; set; }
        public string BranchID { get; set; }
        public string BranchName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string IBAN { get; set; }
        public string AccountNo { get; set; }
        public string AccountTitle { get; set; }
        public string CurrencyID { get; set; }
        public decimal AvailableBalance { get; set; }
        public decimal MinimumBalance { get; set; }
        public decimal CreditLine { get; set; }
        public string AccountType { get; set; }
    }

    public class ProductView
    {
        public string ProductID { get; set; }
        public string Description { get; set; }
        public string IsTermDeposit { get; set; }
        public string PeriodsInFigure { get; set; }
        public string Period { get; set; }
        public string IsAutoProfit { get; set; }
        public string ProfitPeriod { get; set; }
        public string MaturityProfit { get; set; }
        public string IsAutoRollover { get; set; }
        public string OptionsAtMaturity { get; set; }
        public string MinimumAmount { get; set; }
        public string IsTiersAllow { get; set; }
        public string IsMultipleReceipts { get; set; }
        public string CurrencyID { get; set; }
        public string CurrencyDesc { get; set; }
    }
}
