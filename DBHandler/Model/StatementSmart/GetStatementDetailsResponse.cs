﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model.StatementSmart
{
    public class GetStatementDetailsResponse
    {
        public string retStatus { get; set; }
        public string retCode { get; set; }
        public string PDFBase64String { get; set; }
    }
    public class GetStatementDetailsResponseOut
    {
        public string PDFBase64String { get; set; }
    }
}
