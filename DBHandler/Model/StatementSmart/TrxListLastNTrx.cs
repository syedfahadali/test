﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model.StatementSmart
{
    public class TrxListLastNTrx
    {
        
        [MinLength(16, ErrorMessage = "- Valid Min length should not be less than 16.")]
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        public string AccountID { get; set; }
        
        public int NoOfLastTrx { get; set; }
    }
}
