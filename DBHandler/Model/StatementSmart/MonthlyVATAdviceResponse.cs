﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model.StatementSmart
{
    public class MonthlyVATAdviceResponse
    {
        public string retStatus { get; set; }
        public string retCode { get; set; }
        public string PDFFileName { get; set; }
        public string PDFBase64String { get; set; }
    }
    public class MonthlyVATAdviceResponseOut
    {
        public string PDFFileName { get; set; }
        public string PDFBase64String { get; set; }
    }
    public class VATAdviceResults
    {
        public string ChannelRefID { get; set; }
        public decimal ScrollNo { get; set; }
        public decimal SerialNo { get; set; }
        public string TrxType { get; set; }
        public string AccountType { get; set; }
        public string TrxDescription { get; set; }
        public string IsLocalCurrency { get; set; }
        public DateTime VoucherDate { get; set; }
        public decimal VoucherAmount { get; set; }
        public decimal LocalEqv { get; set; }
    }
}
