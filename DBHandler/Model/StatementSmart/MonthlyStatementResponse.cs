﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model.StatementSmart
{
    public class MonthlyStatementResponse
    {
        public string retStatus { get; set; }
        public string retCode { get; set; }
        public string PDFFileName { get; set; }
        public string PDFBase64String { get; set; }
    }
    public class MonthlyStatementResponseOut
    {
        public string PDFFileName { get; set; }
        public string PDFBase64String { get; set; }
    }
}