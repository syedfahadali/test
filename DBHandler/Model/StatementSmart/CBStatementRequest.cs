﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model.StatementSmart
{
    public class CBStatementRequest
    {
        
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string RecordType { get; set; }

        
        [MaxLength(5, ErrorMessage = "- Character length should not be more than 5.")]
        public string MessageTypeID { get; set; }

        
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        public string SequenceNumber { get; set; }

        
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        public string CBUAEReference { get; set; }

        
        public DateTime? CustConsObtainedOn { get; set; }

        
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string EntityId { get; set; }

        
        [MinLength(23, ErrorMessage = "- Valid Min length should not be less than 23.")]
        [MaxLength(23, ErrorMessage = "- Character length should not be more than 23.")]
        public string IBAN { get; set; }

        
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string CustName { get; set; }

        
        [MaxLength(2, ErrorMessage = "- Character length should not be more than 2.")]
        public string IDType { get; set; }

        
        [MaxLength(50, ErrorMessage = "- Character length should not be more than 50.")]
        public string IDNumber { get; set; }

        
        public DateTime? StatementStartDate { get; set; }

        
        public DateTime? StatementEndDate { get; set; }

        
        [MinLength(6, ErrorMessage = "- Valid Min length should not be less than 6.")]
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        public string FutureUse { get; set; }
    }
}
