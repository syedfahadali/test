﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model.StatementSmart
{
    public class AccountDetails
    {
        public string AccountID { get; set; }
        public string AccountName { get; set; }
        public string ClientID { get; set; }
        public string ProductName { get; set; }
        public string CurrencyID { get; set; }
        public string CurrencyName { get; set; }
        public string Address { get; set; }
    }
}