﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model
{
    public class DeleteOnlineUserModel
    {
       
        
        
        [Range(0, int.MaxValue, ErrorMessage = "Only positive number allowed")]
        public int SerialNo { get; set; }
        
        public string ClientId { get; set; }
    }


    public class DeleteOnlineUserResponse
    {
        public int ID { get; set; }
    }
}
