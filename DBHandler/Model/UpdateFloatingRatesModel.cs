﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model
{
    public class UpdateFloatingRatesModel
    {  
        
        
        public List<FloatingRates> Rate { get; set; }
    }
    public class FloatingRates
    {
        
        public string RateID { get; set; }
        
        public decimal MarketRate { get; set; }

    }
}
