﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model
{
    public class AccountFreeze
    {
        public string AccountID { get; set; }
        public string Serial { get; set; }
        public string IsFreeze { get; set; }
        public string Date { get; set; }
        public string ExpiredOn { get; set; }
        public string Amount { get; set; }
        public string Comments { get; set; }
        public string UnFreezedDate { get; set; }
        public string ReleaseComments { get; set; }
    }

    public class InsertAccountFreeze
    {
        
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        public string AccountID { get; set; }

        
        public DateTime? Date { get; set; }

        
        [FutureDateAttribute(ErrorMessage = " is not a valid date")]
        public DateTime? ExpiredOn { get; set; }

        
        [DataType(DataType.Currency)]
        public decimal? Amount { get; set; }

        
        public string Comments { get; set; }
    }


    public class UpdateAccountFreeze
    {
        
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        public string AccountID { get; set; }

        
        public decimal? Serial { get; set; }
    }
}
