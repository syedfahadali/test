﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model
{
    public class AccountMandate
    {
        
        public string AccountID { get; set; }
        public decimal? SerialID { get; set; }
        
        public decimal MinLimit { get; set; }

        
        public decimal MaxLimit { get; set; }

        public string AuthGroupID1 { get; set; }
        public string AuthGroupID2 { get; set; }
        public string AuthGroupID3 { get; set; }
        public string AuthGroupID4 { get; set; }

        public string Comments { get; set; }
        public string MandateTypeID { get; set; }
    }
}