﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model
{
   public class GetCustomerIDExpiryDto
    {
        

        
        public DateTime ExpiryDate { get; set; }

        
        public string ExpiryColumnName { get; set; }
        public string Type { get; set; }
        
        [StringRangeAttribute(AllowableValues = new[] { "AS", "SH", "BO", "BD", "Customer", }, ErrorMessage = "- Valid value is either AS,SH,BO,BD or customer")]
        public string Module { get; set; }
    }
}
