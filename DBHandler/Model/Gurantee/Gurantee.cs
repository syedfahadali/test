﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model.Gurantee
{
    public class IssueRequest
    {
        
        [Required(ErrorMessage = "Client ID is required")]
        public string ClientID { get; set; }
        [Required(ErrorMessage = "Facility ID is required")]
        public string facID { get; set; }
        [Required(ErrorMessage = "Product ID is required")]
        public string prodID { get; set; }
        [Required(ErrorMessage = "Guarantee Type is required")]
        public string guarType { get; set; }
        [Required(ErrorMessage = "Guarantee Amount is required")]
        public decimal guarAmt { get; set; }
        [Required(ErrorMessage = "Currency ID is required")]
        public string currencyID { get; set; }
        [Required(ErrorMessage = "Open/Close Ended is required")]
        public string openCloseEnded { get; set; }
        [Required(ErrorMessage = "App Tenor is required")]
        public decimal apprTenor { get; set; }
        [Required(ErrorMessage = "App Tenor Code is required")]
        public string apprTenorCode { get; set; }
        [Required(ErrorMessage = "Issuance Date is required")]
        public DateTime issuanceDate { get; set; }
        [Required(ErrorMessage = "Expiry Date is required")]
        public DateTime expiryDate { get; set; }
        [Required(ErrorMessage = "Purpose of Guarantee is required")]
        public string purposeOfGuar { get; set; }
        [Required(ErrorMessage = "Margin is required")]
        public decimal margin { get; set; }
        [Required(ErrorMessage = "Current Account ID is required")]
        public string currAccID { get; set; }
        [Required(ErrorMessage = "Beneficiary Name is required")]
        public string beneName { get; set; }
        [Required(ErrorMessage = "Beneficiary Trade License No is required")]
        public string beneTradeLicenseNo { get; set; }
        [Required(ErrorMessage = "Beneficiary Residence Address is required")]
        public string beneResidingAddr { get; set; }
       // [Required(ErrorMessage = "Beneficiary Residence is required")]
        public string beneResidingCity { get; set; }
        [Required(ErrorMessage = "Beneficiary State is required")]
        public string beneResidingState { get; set; }
        [Required(ErrorMessage = "Beneficiary Country is required")]
        public string beneResidingCountry { get; set; }
        [Required(ErrorMessage = "Commission is required")]
        public string commissionFee { get; set; }
        [Required(ErrorMessage = "Fee Recovery Account is required")]
        public string feeRecoveryAccount { get; set; }
    }
    public class IssueResponse
    {
        public string Status { get; set; }
        public string MessageCode { get; set; }
        public string guarRefNo { get; set; }
        public string guarStatus { get; set; }
    }
    public class AmendRequest
    {
        
        [Required(ErrorMessage = "Guarante Ref No. is required")]
        public string guarRefNo { get; set; }
        [Required(ErrorMessage = "Propose Guarantee Amount is required")]
        public decimal proposeGuarAmt { get; set; }
        [Required(ErrorMessage = "Proposed Expiry Date is required")]
        public DateTime proposedExpiryDate { get; set; }
        [Required(ErrorMessage = "Commission Fee is required")]
        public decimal commissionFee { get; set; }
    }
    public class AmendResponse
    {
        public string Status { get; set; }
        public string MessageCode { get; set; }
        public string guarRefNo { get; set; }
        public string guarStatus { get; set; }
    }
    public class CancelRequest
    {

        
        [Required(ErrorMessage = "Cancel Reason is required")]
        public string cancelReason { get; set; }
        [Required(ErrorMessage = "Cancel Date is required")]
        public DateTime canceldate { get; set; }
    }
    public class CancelResponse
    {
        public string Status { get; set; }
        public string MessageCode { get; set; }
        public string guarRefNo { get; set; }
        public string guarStatus { get; set; }
    }
    public class InvokeRequest
    {
        
        [Required(ErrorMessage = "Guarantee Ref No. is required")]
        public string guarRefNo { get; set; }
        [Required(ErrorMessage = "Beneficiary Bank is required")]
        public DateTime beneBank { get; set; }
        [Required(ErrorMessage = "Beneficiary IBAN is required")]
        public DateTime beneIBAN { get; set; }
        [Required(ErrorMessage = "Beneficiary Swift is required")]
        public DateTime beneSWIFT { get; set; }
        [Required(ErrorMessage = "Beneficiary Clain Amount is required")]
        public DateTime beneClaimAmt { get; set; }
        [Required(ErrorMessage = "Currency ID is required")]
        public DateTime currencyID { get; set; }
    }
    public class InvokeResponse
    {
        public string Status { get; set; }
        public string MessageCode { get; set; }
        public string guarRefNo { get; set; }
        public string guarInvokeRefNo { get; set; }
        public string guarStatus { get; set; }
    }
    public class CloseRequest
    {
        
        [Required(ErrorMessage = "Guarantee Ref No. is required")]
        public string guarRefNo { get; set; }
        [Required(ErrorMessage = "Beneficiary Bank is required")]
        public DateTime closureDate { get; set; }
    }
    public class CloseResponse
    {
        public string Status { get; set; }
        public string MessageCode { get; set; }
        public string guarStatus { get; set; }
    }
    public class InvokeRevRequest
    {
        
        [Required(ErrorMessage = "Guarantee Ref No. is required")]
        public string guarRefNo { get; set; }
        [Required(ErrorMessage = "Guarantee Invoke Ref No. is required")]
        public DateTime guarInvokeRefNo { get; set; }
    }
    public class InvokeRevResponse
    {
        public string Status { get; set; }
        public string MessageCode { get; set; }
        public string guarRefNo { get; set; }
        public string guarInvokeRevRefNo { get; set; }
        public string guarStatus { get; set; }
    }
}