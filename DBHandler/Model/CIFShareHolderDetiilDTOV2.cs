﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model
{
    public class CIFShareHolderDetiilDTOV2
    {
       
        

        
        public string ClientID { get; set; }

        
        public AddUpdateShareHolderV2 ShareHolder { get; set; }
    }
    public class AddUpdateShareHolderV2
    {
        public decimal? ShareHolderID { get; set; }

        
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string ShareHolderName { get; set; }

        
        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        public string ShareHolderAddress { get; set; }

        
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        [StringRangeAttribute(AllowableValues = new[] { "COM", "IND" }, ErrorMessage = "- Valid value is either COM or IND.")]
        public string ShareholderType { get; set; }

        
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string ShareholdersIDtype { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        
        public string ShareholdersIDNo { get; set; }

        
        //  [FutureDateAttribute(ErrorMessage = " is not a valid date")]
        public DateTime? ShareholdersIDExpirydate { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string CountryofIncorporation { get; set; }
        
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string BoardMember { get; set; }
        
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string AuthorisedSignatory { get; set; }
        
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string BeneficialOwner { get; set; }

        //
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string ResidentNonResident { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string CountryofResidence { get; set; }

        
        public decimal? OwnershipPercentage { get; set; }
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string Blacklist { get; set; }
        
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string PEP { get; set; }
        
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string FATCARelevant { get; set; }
        
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string CRSRelevant { get; set; }
        
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string USCitizen { get; set; }
        
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string AreyouaUSTaxResident { get; set; }

        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string WereyoubornintheUS { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        [StringRangeAttribute(AllowableValues = new[] { "SUP", "XUP", "UPX" }, ErrorMessage = "- Valid value is either SUP,XUP or UPX.")]
        public string USAEntity { get; set; }
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string FFI { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        [StringRangeAttribute(AllowableValues = new[] { "PFI", "CFI", "PFFI" }, ErrorMessage = "- Valid value is either PFI,CFI or PFFI.")]
        public string FFICategory { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string GIINNo { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        [StringRangeAttribute(AllowableValues = new[] { "001", "002", "003", "004", "005" }, ErrorMessage = "- Valid value is either 001,002, 003, 004 or 005")]
        public string GIINNA { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string SponsorName { get; set; }

        [MaxLength(50, ErrorMessage = "- Character length should not be more than 50.")]
        public string SponsorGIIN { get; set; }

        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        [StringRangeAttribute(AllowableValues = new[] { "001", "002", "003", "004", "005", "006" }, ErrorMessage = "- Valid value is either 001,002, 003, 004,005 or 006 ")]
        public string NFFE { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string StockExchange { get; set; }

        [MaxLength(50, ErrorMessage = "- Character length should not be more than 50.")]
        public string TradingSymbol { get; set; }

        
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string USAORUAE { get; set; }

        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string TINAvailable { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo1 { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry1 { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo2 { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry2 { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo3 { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry3 { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo4 { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry4 { get; set; }
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo5 { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry5 { get; set; }

        [MaxLength(10, ErrorMessage = "- Character length should not be more than 10.")]
        [StringRangeAttribute(AllowableValues = new[] { "Reason A", "Reason B", "Reason C" }, ErrorMessage = "- Valid value is either Reason A,Reason B or Reason C.")]
        public string FATCANoReason { get; set; }

        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string ReasonB { get; set; }

        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        //
        public string SHIDIssuanceCountry { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "M", "F", "T" }, ErrorMessage = "valid value for this field is M,F or T")]
        [MaxLength(1, ErrorMessage = "Gender length should be less than 1")]
        //
        public string IDGender { get; set; }
        //
        [PastDateAttribute(ErrorMessage = " is not a valid date")]
        public DateTime? DateOfBirth { get; set; }
        // 
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string IDNationality { get; set; }

        // [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        //
        public string SHCity { get; set; }
        
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        public string SHState { get; set; }
        
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string SHCountry { get; set; }
        public string JurEmiratesId { get; set; }
        public string JurTypeId { get; set; }
        public string JurAuthorityId { get; set; }
        public string POBox { get; set; }
        public string JurOther { get; set; }
        public string ParentShareHolderId { get; set; }
        public string EntityType { get; set; }

        //New fields

        public string ResidenceShareHolderCountryID { get; set; }
        public string PlaceofBirth { get; set; }
        public string GroupCodeType { get; set; }
    }
}
