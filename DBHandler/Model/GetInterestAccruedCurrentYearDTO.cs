﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model
{
    public class GetInterestAccruedCurrentYearDTO
    {
       
        

        
        [MaxLength(16, ErrorMessage = "- AccountID length should not be more than 16.")]
        [MinLength(16, ErrorMessage = "- AccountID length should not be less than 16.")]
        public string AccountID { get; set; }
    }
}
