﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model
{
    public class ValidateChannelModel
    {
        public string UserId { get; set; }
        public string ChannelId { get; set; }
        public string functionName { get; set; }
    }
}
