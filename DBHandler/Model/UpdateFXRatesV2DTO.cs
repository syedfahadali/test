﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model
{
    public class UpdateFXRatesV2DTO
    {

        

        public List<RateV2> Rate { get; set; }
    }
    public class RateV2
    {

        public string CurrencyID { get; set; }
        public decimal BuyingRate { get; set; }
        public decimal SellingRate { get; set; }
        public decimal MeanRate { get; set; }
        public decimal PreferredBuy { get; set; }
        public decimal PreferredSell { get; set; }
        public decimal NotesBuy { get; set; }
        public decimal NotesSell { get; set; }
    }

}
