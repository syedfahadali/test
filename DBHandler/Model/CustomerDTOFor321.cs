﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model
{
    public class CustomerDTOFor321
    {
        public string ClientId { get; set; }
        public string Name { get; set; }
        public bool IsVip { get; set; }
        public string Country { get; set; }
        public string EmiritesId { get; set; }
    }
}
