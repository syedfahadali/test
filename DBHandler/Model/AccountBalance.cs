﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model
{
    public class AccountBalance
    {
        public string AccountID { get; set; }
        public string ProductID { get; set; }
        public string Name { get; set; }
        public Nullable<decimal> ClearBalance { get; set; }
        public Nullable<decimal> Effects { get; set; }
        public Nullable<decimal> Limit { get; set; }
        public Nullable<decimal> ShadowBalance { get; set; }
        public Nullable<decimal> LocalClearBalance { get; set; }
        public Nullable<decimal> LocalEffects { get; set; }
        public string AccessCode { get; set; }
        public bool IsFreezed { get; set; }
        public Nullable<decimal> FreezeAmount { get; set; }
        public string Reminder { get; set; }
        public decimal ClearedEffects { get; set; }
        public Nullable<System.DateTime> LastCrTrxDate { get; set; }
        public Nullable<System.DateTime> LastDrTrxDate { get; set; }
        public Nullable<System.DateTime> DormantCrDate { get; set; }
        public Nullable<System.DateTime> DormantDrDate { get; set; }
        public Nullable<decimal> MinBalanceCr { get; set; }
        public Nullable<System.DateTime> MinBalanceCrDate { get; set; }
        public Nullable<decimal> MinBalanceDr { get; set; }
        public Nullable<System.DateTime> MinBalanceDrDate { get; set; }
        public Nullable<decimal> MaxBalanceCr { get; set; }
        public Nullable<System.DateTime> MaxBalanceCrDate { get; set; }
        public Nullable<decimal> MaxBalanceDr { get; set; }
        public Nullable<System.DateTime> MaxBalanceDrDate { get; set; }
        public Nullable<decimal> MinBalanceCrMonthly { get; set; }
        public Nullable<System.DateTime> MinBalanceCrMonthlyDate { get; set; }
        public Nullable<decimal> MinBalanceDrMonthly { get; set; }
        public Nullable<System.DateTime> MinBalanceDrMonthlyDate { get; set; }
        public Nullable<decimal> MaxBalanceCrMonthly { get; set; }
        public Nullable<System.DateTime> MaxBalanceCrMonthlyDate { get; set; }
        public Nullable<decimal> MaxBalanceDrMonthly { get; set; }
        public Nullable<System.DateTime> MaxBalanceDrMonthlyDate { get; set; }
        public Nullable<decimal> MinBalanceCrYearly { get; set; }
        public Nullable<System.DateTime> MinBalanceCrYearlyDate { get; set; }
        public Nullable<decimal> MinBalanceDrYearly { get; set; }
        public Nullable<System.DateTime> MinBalanceDrYearlyDate { get; set; }
        public Nullable<decimal> MaxBalanceCrYearly { get; set; }
        public Nullable<System.DateTime> MaxBalanceCrYearlyDate { get; set; }
        public Nullable<decimal> MaxBalanceDrYearly { get; set; }
        public Nullable<System.DateTime> MaxBalanceDrYearlyDate { get; set; }
        public Nullable<decimal> MinBalanceCrDaily { get; set; }
        public Nullable<System.DateTime> MinBalanceCrDailyDate { get; set; }
        public Nullable<decimal> MinBalanceDrDaily { get; set; }
        public Nullable<System.DateTime> MinBalanceDrDailyDate { get; set; }
        public Nullable<decimal> MaxBalanceCrDaily { get; set; }
        public Nullable<System.DateTime> MaxBalanceCrDailyDate { get; set; }
        public Nullable<decimal> MaxBalanceDrDaily { get; set; }
        public Nullable<System.DateTime> MaxBalanceDrDailyDate { get; set; }
        public Nullable<decimal> MinTrxCr { get; set; }
        public Nullable<System.DateTime> MinTrxCrDate { get; set; }
        public Nullable<decimal> MinTrxDr { get; set; }
        public Nullable<System.DateTime> MinTrxDrDate { get; set; }
        public Nullable<decimal> MaxTrxCr { get; set; }
        public Nullable<System.DateTime> MaxTrxCrDate { get; set; }
        public Nullable<decimal> MaxTrxDr { get; set; }
        public Nullable<System.DateTime> MaxTrxDrDate { get; set; }
        public Nullable<decimal> MinTrxCrMonthly { get; set; }
        public Nullable<System.DateTime> MinTrxCrMonthlyDate { get; set; }
        public Nullable<decimal> MinTrxDrMonthly { get; set; }
        public Nullable<System.DateTime> MinTrxDrMonthlyDate { get; set; }
        public Nullable<decimal> MaxTrxCrMonthly { get; set; }
        public Nullable<System.DateTime> MaxTrxCrMonthlyDate { get; set; }
        public Nullable<decimal> MaxTrxDrMonthly { get; set; }
        public Nullable<System.DateTime> MaxTrxDrMonthlyDate { get; set; }
        public Nullable<decimal> MinTrxCrYearly { get; set; }
        public Nullable<System.DateTime> MinTrxCrYearlyDate { get; set; }
        public Nullable<decimal> MinTrxDrYearly { get; set; }
        public Nullable<System.DateTime> MinTrxDrYearlyDate { get; set; }
        public Nullable<System.DateTime> MaxTrxCrYearlyDate { get; set; }
        public Nullable<decimal> MaxTrxDrYearly { get; set; }
        public Nullable<System.DateTime> MaxTrxDrYearlyDate { get; set; }
        public Nullable<decimal> MinTrxCrDaily { get; set; }
        public Nullable<System.DateTime> MinTrxCrDailyDate { get; set; }
        public Nullable<decimal> MinTrxDrDaily { get; set; }
        public Nullable<System.DateTime> MinTrxDrDailyDate { get; set; }
        public Nullable<decimal> MaxTrxCrDaily { get; set; }
        public Nullable<System.DateTime> MaxTrxCrDailyDate { get; set; }
        public Nullable<decimal> MaxTrxDrDaily { get; set; }
        public Nullable<System.DateTime> MaxTrxDrDailyDate { get; set; }
        public decimal CashAmt { get; set; }

    }
}
