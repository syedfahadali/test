﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model
{
    public class CustomerDigibancRuleDetails
    {
        
        public int WorkingTenureWithCurrentEmployer { get; set; }
        public string LastMonthSalary { get; set; }
        public string LastMonthSalaryCount { get; set; }
        public string cif { get; set; }
        public DateTime? dob { get; set; }

    }
}
