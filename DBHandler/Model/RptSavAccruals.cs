﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model
{
    public class RptSavAccruals
    {
        public DateTime AccrualDate { get; set; }
        public string AccountID { get; set; }
        //public decimal SerialNo { get; set; }
        //public string AccountName { get; set; }
        //public string ProductID { get; set; }
        //public string ProductDesc { get; set; }
        //public int Basis { get; set; }
        public string CurrencyID { get; set; }
        public DateTime ValueDate { get; set; }
        //public DateTime RateChgDate { get; set; }
        public int Days { get; set; }
        public decimal Balance { get; set; }
        public decimal Rate { get; set; }
        public decimal Amount { get; set; }
    }
}
