﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model.LoanSchedule
{
    public class LoanScheduleModel
    {

        
        [Required(ErrorMessage = "Deal ID is required")]
        public string DealID { get; set; }
    }
    public class LoanScheduleResponse
    {
        public string DealID { get; set; }
        public string Status { get; set; }
        public string ClientID { get; set; }
        public string ProductID { get; set; }
        public string Description { get; set; }
        public List<Scheduler> Scheduler { get; set; }

    }
    public class Scheduler
    {
        public DateTime RepaymentDate { get; set; }
        public decimal PrincipalAmount { get; set; }
        public decimal ToDateProfit { get; set; }
        public decimal InstAmount { get; set; }
    }
}
