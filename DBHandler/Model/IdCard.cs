﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DBHandler.Model
{
    [Table("t_IdDetails")]
    public class IdCard
    {
        [Key]
        public string Id { get; set; }
        public string UserId { get; set; }
        public string FullName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string FamilyName { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public string Nationality { get; set; }
        public DateTime DOI { get; set; }
        public DateTime DOE { get; set; }
        public string DocumentCode { get; set; }
        public string DocumentNumber { get; set; }
        public string Issuer { get; set; }
        public string OptionalData1 { get; set; }
        public string OptionalData2 { get; set; }
        public string MrtDraw { get; set; }
        public string FrontCardImage { get; set; }
        public string BackCardImage { get; set; }
        public string PersonFaceImage { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateTime { get; set; }
        public string CreateTerminal { get; set; }
    }
}
