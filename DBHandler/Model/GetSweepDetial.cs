﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBHandler.Model.GetSweepDetial
{
    public class GetSweepDetialModel
    {
        

        public string AccountID { get; set; }
        public string ClientId { get; set; }
    }

    public class GetSweepDetialResponse
    {

        public List<RealTimeSweep> RealTimeSweep { get; set; }
        public List<ExcessSweep> ExcessSweep { get; set; }
        public List<ShortageSweep> ShortageSweep { get; set; }

    }
    public class RealTimeSweep
    {
        public string AccountID { get; set; }
        public string SIAccountID1 { get; set; }
        public string SIAccountID2 { get; set; }
    }
    public class ExcessSweep
    {
        public string AccountID { get; set; }
        public string SIAccountID { get; set; }
        public string ExcessAmount { get; set; }
    }
    public class ShortageSweep
    {
        public string AccountID { get; set; }
        public string SIAccountID { get; set; }
        public string ShortageAmount { get; set; }
    }
}
