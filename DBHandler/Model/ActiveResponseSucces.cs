﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoreHandler.Model
{
    public class ActiveResponseSucces<T>
    {

        public string LogId { get; set; }
        public Status Status { get; set; }
        public T Content { get; set; }

    }

    public class responseSucce<T>
    {
        public ActiveResponseSucces<T> response { get; set; }
    }

    public class Status
    {
        public string Code { get; set; }
        public string Severity { get; set; }
        public string StatusMessage { get; set; }
    }
}
