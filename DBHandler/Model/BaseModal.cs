﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model
{
    public class BaseModel
    {
        public string CreateBy { get; set; }
        public System.DateTime CreateTime { get; set; }
        public string CreateTerminal { get; set; }
        public string UpdateBy { get; set; }
        public Nullable<System.DateTime> UpdateTime { get; set; }
        public string UpdateTerminal { get; set; }
        public string AuthStatus { get; set; }
        public string SuperviseBy { get; set; }
        public Nullable<System.DateTime> SuperviseTime { get; set; }
        public string SuperviseTerminal { get; set; }
        public string VerifyBy { get; set; }
        public Nullable<System.DateTime> VerifyTime { get; set; }
        public string VerifyTerminal { get; set; }
        public string VerifyStatus { get; set; }
        public string frmName { get; set; }
        public string OurBranchID { get; set; }
    }
}
