﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBHandler.Model
{
    public class BoardDirectorsView 
    {
        public decimal? BoardDirector { get; set; }
        
        public string BoardDirectorName { get; set; }
        [MaxLength(50, ErrorMessage = "- Character length should not be more than 50.")]
        public string Designation { get; set; }

        
        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        public string BoardDirectorAddress { get; set; }

        
        //[FutureDateAttribute(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> BoardDirectorIDExpirydate { get; set; }

        
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string ResidentNonResident { get; set; }
        
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string CountryofResidence { get; set; }

        
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string BoardDirectorIDtype { get; set; }
        
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string BoardDirectorIDNo { get; set; }

        
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string Blacklist { get; set; }
        
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string PEP { get; set; }

        
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string FATCARelevant { get; set; }
        
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string CRSRelevant { get; set; }
        
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string AreyouaUSCitizen { get; set; }
        
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string AreyouaUSTaxResident { get; set; }
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string WereyoubornintheUS { get; set; }
        
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string USAORUAE { get; set; }
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string TINAvailable { get; set; }
        public string TinNo1 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry1 { get; set; }
        public string TinNo2 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry2 { get; set; }
        public string TinNo3 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry3 { get; set; }
        public string TinNo4 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry4 { get; set; }
        public string TinNo5 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry5 { get; set; }

        [MaxLength(10, ErrorMessage = "- Character length should not be more than 10.")]
        [StringRangeAttribute(AllowableValues = new[] { "Reason A", "Reason B", "Reason C" }, ErrorMessage = "- Valid value is either Reason A,Reason B orReason C.")]
        public string FATCANoReason { get; set; }
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string ReasonB { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "M", "F", "T" }, ErrorMessage = "valid value for this filed is M,F or T")]
        [MaxLength(1, ErrorMessage = "Gender length should be less than 1")]
        
        public string IDGender { get; set; }
        
        [PastDateAttribute(ErrorMessage = " is not a valid date")]
        public DateTime? DateOfBirth { get; set; }
        
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string IDNationality { get; set; }

        //[MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string BDCity { get; set; }
        
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        public string BDState { get; set; }
        
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string BDCountry { get; set; }
    }

    public class BoardDirectors 
        {
        public decimal? BoardDirector { get; set; }
        
        public string BoardDirectorName { get; set; }
        [MaxLength(50, ErrorMessage = "- Character length should not be more than 50.")]
        public string Designation { get; set; }

        
        [MaxLength(140, ErrorMessage = "- Character length should not be more than 140.")]
        public string BoardDirectorAddress { get; set; }

        
        //[FutureDateAttribute(ErrorMessage = " is not a valid date")]
        public Nullable<System.DateTime> BoardDirectorIDExpirydate { get; set; }

        
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string ResidentNonResident { get; set; }
        
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string CountryofResidence { get; set; }

        
        [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string BoardDirectorIDtype { get; set; }
        
        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string BoardDirectorIDNo { get; set; }

        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string Blacklist { get; set; }
        
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string PEP { get; set; }

        
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string FATCARelevant { get; set; }
        
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string CRSRelevant { get; set; }
        
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string AreyouaUSCitizen { get; set; }
        
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string AreyouaUSTaxResident { get; set; }
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string WereyoubornintheUS { get; set; }
        
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string USAORUAE { get; set; }
        [MaxLength(1, ErrorMessage = "- Character length should not be more than 1.")]
        [StringRangeAttribute(AllowableValues = new[] { "Y", "N" }, ErrorMessage = "- Valid value is either Y or N.")]
        public string TINAvailable { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo1 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry1 { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo2 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry2 { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo3 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry3 { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo4 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry4 { get; set; }

        [MaxLength(15, ErrorMessage = "- Character length should not be more than 15.")]
        public string TinNo5 { get; set; }
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string TaxCountry5 { get; set; }

        [MaxLength(10, ErrorMessage = "- Character length should not be more than 10.")]
        [StringRangeAttribute(AllowableValues = new[] { "Reason A", "Reason B", "Reason C" }, ErrorMessage = "- Valid value is either Reason A,Reason B orReason C.")]
        public string FATCANoReason { get; set; }
        [MaxLength(100, ErrorMessage = "- Character length should not be more than 100.")]
        public string ReasonB { get; set; }

        [StringRangeAttribute(AllowableValues = new[] { "M", "F", "T" }, ErrorMessage = "valid value for this filed is M,F or T")]
        [MaxLength(1, ErrorMessage = "Gender length should be less than 1")]
        
        public string IDGender { get; set; }
        
        [PastDateAttribute(ErrorMessage = " is not a valid date")]
        public DateTime? DateOfBirth { get; set; }
        
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string IDNationality { get; set; }

       // [MaxLength(30, ErrorMessage = "- Character length should not be more than 30.")]
        public string BDCity { get; set; }
        
        [MaxLength(6, ErrorMessage = "- Character length should not be more than 6.")]
        public string BDState { get; set; }
        
        [MaxLength(3, ErrorMessage = "- Character length should not be more than 3.")]
        public string BDCountry { get; set; }
    }


   
}
