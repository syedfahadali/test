﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model.Host
{
    public class FullReversalResponse
    {
        public string RespCode { get; set; }
        public string ClearBalance { get; set; }
        public string AvailableBalance { get; set; }
        public string AccountType { get; set; }
        public string CurrencyCode { get; set; }
        public string AuthIDResp { get; set; }
    }
}
