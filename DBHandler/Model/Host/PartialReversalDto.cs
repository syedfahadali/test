﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model.Host
{
    public class PartialReversalDto
    {
        public string OrgTrxRefNo { get; set; }
        public decimal AmtActualTran { get; set; }
        public decimal AmtActualStl { get; set; }
        public decimal ActualTranFee { get; set; }
        public decimal AccountStlFee { get; set; }
        public string ForwardInstID { get; set; }
        public string ProcCode { get; set; }
        public string ResponseCode { get; set; }
        public string VISATrxID { get; set; }
        public decimal ConvRate { get; set; }

    }
}
