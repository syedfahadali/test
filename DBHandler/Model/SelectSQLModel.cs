﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBHandler.Model
{
    public class SelectSQLModel
    {
        
        public string TableToSelect { get; set; }
        public string TableToSelectAlias { get; set; }
        public List<string> ColumnToSelect { get; set; }
        public bool IsAllColumn { get; set; }
        public List<WhereSQLModel> WhereClause { get; set; }
    }
    public class WhereSQLModel
    {
        //in case columnName is join cluse then, it should be written as customer.DOB
        //means it should be called as alias and then column name like Customer.dob
        public string ColumnName { get; set; }
        public string ColumnNameAlias { get; set; }
        public string Condition { get; set; }
        public string ColumnValue { get; set; }
        public bool IsFormulaColumn { get; set; }
        public string SubQuery { get; set; }
        public string JoinClause { get; set; }
        public string AdditionalFixedWhereClause { get; set; }
        public ConditionType ConditionType { get; set; }
    }

    public enum ConditionType
    {
        GreaterThan = 1,
        LessThan = 2,
        InList = 3,
        GreaterThanEqualTo = 4,
        LessThanEqualTo = 5,
        EqualsTo = 6
    }
    public enum EligibilityCriteriaDataType
    {
        None = 0,
        Age = 1
    }
    public enum InviteStatus
    {
        Invited = 1,
        InviteRejected = 2,
        ProposedByBank = 3,
        RejectedByBank = 4,
        RejectedByCustomer = 5,
        Accepted = 6
    }
}
