﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using CommonModel;

namespace DBHandler.Model.DtoParameter
{
   public class Registration
    {
        
        

        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
    }

    public class JwtTokenConfig
    {
        public string Secret { get; set; }
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public int AccessTokenExpiration { get; set; }
        public int RefreshTokenExpiration { get; set; }
    }

    /// <summary>
    /// Get version model
    /// </summary>
    public class GetVersionModel
    {
        /// <summary>
        /// this is default request.
        /// </summary>
        
        
    }

    public class ChangePassword
    {
        
        
        public string NewPassword { get; set; }
        public string UserName { get; set; }
    }
}
