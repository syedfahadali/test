﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBHandler.Model
{
    [Table("t_TransactionImage")]
   public class TransactionImage
    {
        [Key]
        public string Id { get; set; }
        public string UserId { get; set; }
        public string ImagePath { get; set; }
        public string Note { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
