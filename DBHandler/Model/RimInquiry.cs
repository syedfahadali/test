﻿using DBHandler.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using CommonModel;

namespace DBHandler.Model
{
    public class CIFDetailInquiry
    {
       
        
        
        public string ClientID { get; set; }
    }

    public class SignatureModel
    {
        public string Signature { get; set; }
        public string ClientID { get; set; }
        public string AuthID { get; set; }
    }

    public class CIFBoardDirectorsDTO
    {
      
        

        
        public string ClientID { get; set; }

        
        public BoardDirectors BoardDirectors { get; set; }
    }

    public class CIFBeneficialOwnersDTO
    {    
        

        
        public string ClientID { get; set; }

        
        public BeneficialOwners BeneficialOwners { get; set; }
    }

    public class CIFAuthorisedSignatoriesDTO
    {      
        

        
        public string ClientID { get; set; }

        
        public AuthorisedSignatories AuthorisedSignatories { get; set; }
    }
    public class CIFShareHolderDetiilDTO
    {
        
        

        
        public string ClientID { get; set; }

        
        public ShareHolder ShareHolder { get; set; }
    }

    public class UpdateSignatureModel
    {
       
        
        
        public string ClientID { get; set; }
        
        public string ID { get; set; }
        public string Signature { get; set; }
        public string GroupID { get; set; }
    }


    public class DeleteRequestForClientDetail
    {
        
        
        public string ClientID { get; set; }
        
        public string ID { get; set; }
    }

    public class RimInquiry
    {
        
        
        public string ClientID { get; set; }
    }

   
   




    public class CprInquiry
    {
        
        
        
        public string ClientID { get; set; }
    }

    //public class CustomerModel
    //{
    //    
    //    
    //    public Customer Cust { get; set; }
    //}
}
