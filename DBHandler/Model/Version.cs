﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBHandler.Model
{

    [Table("t_Version")]
    public class Version
    {
        [Key]
        public Int32 Id { get; set; }
        public string VersionNumber { get; set; }
        public DateTime ReleaseDate {get; set; }
        public string ReleaseNotes { get; set; }
    }
}
