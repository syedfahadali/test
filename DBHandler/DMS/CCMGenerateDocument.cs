﻿using CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DMSHandler.CCMGenerateDocumentOutBound
{
    public class Command
    {
        public string CommandName { get; set; }
        public string commandValue { get; set; }
    }

    public class Status
    {
        public string code { get; set; }
        public string severity { get; set; }
        public string statusMessage { get; set; }
    }

    public class Item
    {
        public DateTime requestDateTime { get; set; }
        public string logId { get; set; }
        //public Status status { get; set; }
        public object content { get; set; }
        //public string workFlowDefinition { get; set; }
        //public int responnseType { get; set; }
    }


    public class DataDefinition
    {
        public string ModuleName { get; set; }
        public Object item { get; set; }
        public string itemStr { get; set; }
    }

    public class RootObject
    {
        public List<Command> command { get; set; }
        public List<DataDefinition> dataDefinition { get; set; }
        public string workFlowDefinition { get; set; }
        public int responnseType { get; set; }
    }
}

    namespace DMSHandler.CCMGenerateDocument
{
    public class CCMGenerateDocumentModel
    {
        
        
        
        public RootObject RootObject { get; set; }
    }

    public class TemplateData
    {
        public object content { get; set; }
    }

    public class RootObject
    {
        public string templateId { get; set; }
        public TemplateData templateData { get; set; }
    }

    public class Source
    {
        public string Item { get; set; }
        public string Path { get; set; }
    }

    public class ErrorInfo
    {
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
    }


    public class ResponseObject
    {
        public string log { get; set; }
        public ErrorInfo ErrorInfo { get; set; }
        public List<Source> source { get; set; }
    }

    public class DepositeAdviceGenerateDocumentModel
    {
        
        
        public string ClientID { get; set; }
        
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        public string AccountID { get; set; }
        
        public string RecieptID { get; set; }
    }


    public class CreateLoanAdviceModel
    {
        
        
        public string ClientID { get; set; }
        
        [MaxLength(16, ErrorMessage = "- Character length should not be more than 16.")]
        public string AccountID { get; set; }
        
        public decimal DealId { get; set; }
    }


}
