﻿using DBHandler.Enum;
using DBHandler.Helper;
using DBHandler.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Configuration;
using System.Transactions;
using DBHandler.Model.SPLUNK;
using System.Net;
using System.Net.Sockets;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;

namespace DBHandler.Repositories
{
    public class LogRqChRepository : Repository<LogRqCh>, ILogRqChRepository
    {
        public IConfiguration Configuration { get; }
        private readonly VendorMWContext _ctx;
        public LogRqChRepository(IConfiguration configuration, VendorMWContext ctx) : base(ctx)
        {
            _ctx = ctx;
            Configuration = configuration;
        }

        public async Task<bool> LogData(string logid, string channelid, DateTime? requestdate, string body, string Response, string actionName, string ControllerName, string headers)

        {
            try
            {

                
                LogRqCh req = new LogRqCh();
                SqlConnection conn;
                SqlCommand comm;
                DateTime reqdatetime = requestdate.HasValue ? (DateTime)requestdate : DateTime.Now;
                DBHandler.Common.Decrypt d = new DBHandler.Common.Decrypt(Configuration);
                string connstring = d.DecryptString(Configuration.GetConnectionString("DefaultConnection"));



                req.ChannelID = channelid;
                req.LogID = logid;
                req.RequestDateTime = requestdate;
                req.RequestData = Utility.ModifyRequestBody(actionName, body);
                req.ResponseData = Utility.ModifyResponseBody(actionName, Response);
                req.ID = Guid.NewGuid();


                string query = $@"
INSERT INTO [dbo].[LogRqCh]
           ([LogID]
           ,[RequestDateTime]
           ,[ControllerName]
           ,[FunctionName]
           ,[ChannelID]
           ,[RequestData]
           ,[ResponseData]
           ,[Headers]
           ,[ID])
     VALUES
           (@LogID
           ,@RequestDateTime
           ,@ControllerName
           ,@FunctionName
           ,@ChannelID
           ,@RequestData
           ,@ResponseData
           ,@Headers
           ,@ID)";


                using (conn = new SqlConnection(connstring))
                {
                    conn.Open();
                    comm = new SqlCommand();
                    comm.Connection = conn;
                    //Adding Parameter instances to sqlcommand
                    comm.Parameters.Add("@LogID", SqlDbType.NVarChar).Value = string.IsNullOrEmpty(req.LogID) ? "" : req.LogID;
                    comm.Parameters.Add("@RequestDateTime", SqlDbType.DateTime).Value = reqdatetime.ToString("yyyy-MM-dd HH:mm:ss.fff");
                    comm.Parameters.Add("@ControllerName", SqlDbType.NVarChar, 500).Value = string.IsNullOrEmpty(ControllerName) ? "" : ControllerName;
                    comm.Parameters.Add("@FunctionName", SqlDbType.NVarChar, 500).Value = string.IsNullOrEmpty(actionName) ? "" : actionName;
                    comm.Parameters.Add("@ChannelID", SqlDbType.NVarChar, 100).Value = string.IsNullOrEmpty(req.ChannelID) ? "" : req.ChannelID;
                    comm.Parameters.Add("@RequestData", SqlDbType.NVarChar).Value = string.IsNullOrEmpty(req.RequestData) ? "" : req.RequestData;
                    comm.Parameters.Add("@ResponseData", SqlDbType.NVarChar).Value = string.IsNullOrEmpty(req.ResponseData) ? "" : req.ResponseData;
                    comm.Parameters.Add("@Headers", SqlDbType.NVarChar).Value = string.IsNullOrEmpty(headers) ? "" : headers;
                    comm.Parameters.Add("@ID", SqlDbType.UniqueIdentifier).Value = req.ID;
                    comm.CommandText = query;
                    comm.ExecuteNonQuery();
                    conn.Close();
                }


                string Channel = string.IsNullOrEmpty(channelid) ? "0" : channelid;
                object request = body;
                object res = Response;
                try
                {
                    JToken.Parse(body);
                    request = JsonConvert.DeserializeObject(body);
                }

                catch (Exception e)

                {
                }
                try
                {
                    JToken.Parse(Response);
                    res = JsonConvert.DeserializeObject(Response);
                }

                catch (Exception e)

                {
                }

                object eventObj = new DBHandler.Model.SPLUNK.SplunkConnectorModelForLogRQ.Event()
                {
                    Channel = Convert.ToInt32(Channel),
                    RequestParameters = request,
                    RequestResponse = res,
                    LogId = logid,
                    RequestDateTime = (DateTime)reqdatetime,
                    LocalServerIP = GetLocalIPAddress()
                };


                //Utility.SplunkLogs(eventObj, requestdate, Configuration["SPLUNK:COLLECTOR"].ToString(), Configuration["SPLUNK:BASE_URL"].ToString(), Configuration["SPLUNK:AUTHKEY"].ToString(), "mw:logrq");

            }

            catch (Exception ex)

            {

            }
            
            return true;
        }
        private string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }
        private static long ToUnixEpochDate(DateTime date) => new DateTimeOffset(date).ToUniversalTime().ToUnixTimeSeconds();
    }
}
