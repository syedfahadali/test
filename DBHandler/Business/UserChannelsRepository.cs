﻿using DBHandler.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using MimeKit;

namespace DBHandler.Repositories
{
    public class UserChannelsRepository: Repository<UserChannels>, IUserChannelsRepository
    {
        private readonly VendorMWContext _ctx;

        public UserChannelsRepository(VendorMWContext ctx) : base(ctx)
        {
            _ctx = ctx;
        }

      
        public string ValidateChannel(string userId, string functionName, string bankId, string branchId)
        {
            functionName = functionName.Replace("\\", "@");

            var result = (from u in _ctx.TUserChannels
                          join f in _ctx.TUsersFunctions on u.UserId equals f.UserId
                          where u.UserId == userId && f.functionName.Trim() == functionName.Trim() && f.BankId == bankId && f.BranchId == branchId
                          select new { u.UserId , u.ChannelId }).ToList();
           if(result!=null && result.Count>0)
            {
               return result[0].ChannelId;
            }
            return "";
        }

        public Model.Version GetAPIVersion()
        {
            var version = _ctx.TVersion.Where(x => x.VersionNumber == x.VersionNumber).OrderByDescending(x => x.ReleaseDate).FirstOrDefault();
            return version;
        }

        public Currencies GetCurrencies(string CurrencyID)
        {
            var curr = _ctx.TCurrencies.SingleOrDefault(x => x.CurrencyId == CurrencyID);
            return curr;
        }
    }
}
