﻿using DBHandler.Enum;
using DBHandler.Helper;
using DBHandler.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Data.SqlClient;
using System.Linq;


using System.Transactions;
using Microsoft.AspNetCore.Mvc.Controllers;
using CommonModel;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Net.Http;
using DBHandler.Model.SPLUNK;
using Newtonsoft.Json;
using System.Text.Json;
using Newtonsoft.Json.Linq;
using System.IO;

namespace DBHandler.Repositories
{
    public class LogRepository : Repository<Log>, ILogRepository
    {
        private readonly VendorMWContext _ctx;
        public IConfiguration Configuration { get; }
        



        public LogRepository(IConfiguration configuration, VendorMWContext ctx) : base(ctx)
        {
            _ctx = ctx;
            Configuration = configuration;
           

        }

        private async void WriteLogs(string message, Level Level, string userid, string ip, string function, string Exception, string ControllerName, string status, DateTime Starttime, DateTime Endtime, string RequestParameter, string RequestResponse, string Channel, string logId, DateTime? rDate, System.Net.HttpStatusCode? code, string baseURL = "", string functionName = "",

            DateTime? ApiRequestStartTime = null, DateTime? ApiRequestEndTime = null, string SplunCollector = "", string SplunkBase = "", string SplunkAuth = "", string httpAction = "")
        {


          
            DateTime reqdatetime = rDate.HasValue ? (DateTime)rDate : DateTime.Now;
            DBHandler.Common.Decrypt d = new DBHandler.Common.Decrypt(Configuration);
           
            string connstring = d.DecryptString(Configuration.GetConnectionString("DefaultConnection"));

            Log model = new Log
            {
                Application = "",
                FunctionName = function,
                ControllerName = ControllerName,
                DeviceID = "",
                IP = ip,
                StartTime = Starttime,
                EndTime = Endtime,
                Exception = string.IsNullOrEmpty(Exception) ? "" : Exception,
                Message = message,
                Level = Level.ToString(),
                Status = "",
                UserID = userid,
                RequestParameters = RequestParameter,
                RequestResponse = RequestResponse,
                Channel = Channel,
                LogId = logId,
                RequestDateTime = rDate,
                ErrorCode = code.ToString(),
                APIbaseURL = baseURL,
                APIFunctionName = functionName,
                LocalServerIP = GetLocalIPAddress(),
                ApiRequestEndTime = (DateTime)ApiRequestEndTime,
                ApiRequestStartTime = (DateTime)ApiRequestStartTime,
                httpAction = httpAction
            };
            

            using (SqlConnection conn = new SqlConnection(connstring))
            {






                string query = $@"
INSERT INTO [dbo].[Logs]
           ([Application]
           ,[Level]
           ,[Message]
           ,[Logger]
           ,[Callsite]
           ,[Exception]
           ,[UserId]
           ,[Email]
           ,[DeviceID]
           ,[IP]
           ,[FunctionName]
           ,[ControllerName]
           ,[StartTime]
           ,[EndTime]
           ,[Status]
           ,[RequestParameters]
           ,[RequestResponse]
           ,[Channel]
           ,[LogId]
           ,[RequestDateTime]
           ,[ErrorCode]
           ,[APIbaseURL]
           ,[APIFunctionName]
           ,[LocalServerIP]
            ,[ApiRequestStartTime]
            ,[ApiRequestEndTime]
            ,[HTTPAction])
     VALUES
           (@Application
           ,@Level
           ,@Message
           ,@Logger
           ,@Callsite
           ,@Exception
           ,@UserId
           ,@Email
           ,@DeviceID
           ,@IP
           ,@FunctionName
           ,@ControllerName
           ,@StartTime
           ,@EndTime
           ,@Status
           ,@RequestParameters
           ,@RequestResponse
           ,@Channel
           ,@LogId
           ,@RequestDateTime
           ,@ErrorCode
           ,@APIbaseURL
           ,@APIFunctionName
           ,@LocalServerIP
            ,@ApiRequestStartTime
            ,@ApiRequestEndTime
            ,@HTTPAction)";

                try
                {

                    conn.Open();
                    SqlCommand comm = new SqlCommand();
                    comm.Connection = conn;

                    //Adding Parameter instances to sqlcommand
                    comm.Parameters.Add("@Application", SqlDbType.VarChar, 50).Value = string.IsNullOrEmpty(model.Application) ? "" : model.Application;
                    comm.Parameters.Add("@Level", SqlDbType.VarChar, 50).Value = string.IsNullOrEmpty(model.Level) ? "" : model.Level;
                    comm.Parameters.Add("@Message", SqlDbType.VarChar).Value = string.IsNullOrEmpty(model.Message) ? "" : model.Message;
                    comm.Parameters.Add("@Logger", SqlDbType.VarChar, 50).Value = "";
                    comm.Parameters.Add("@Callsite", SqlDbType.VarChar, 50).Value = "";
                    comm.Parameters.Add("@Exception", SqlDbType.NVarChar).Value = string.IsNullOrEmpty(model.Exception) ? "" : model.Exception;
                    comm.Parameters.Add("@UserId", SqlDbType.VarChar, 50).Value = string.IsNullOrEmpty(model.UserID) ? "" : model.UserID;
                    comm.Parameters.Add("@Email", SqlDbType.VarChar, 50).Value = "";
                    comm.Parameters.Add("@DeviceID", SqlDbType.VarChar, 50).Value = string.IsNullOrEmpty(model.DeviceID) ? "" : model.DeviceID;
                    comm.Parameters.Add("@IP", SqlDbType.VarChar, 50).Value = string.IsNullOrEmpty(model.IP) ? "" : model.IP;
                    comm.Parameters.Add("@FunctionName", SqlDbType.VarChar, 50).Value = string.IsNullOrEmpty(model.FunctionName) ? "" : model.FunctionName;
                    comm.Parameters.Add("@ControllerName", SqlDbType.VarChar, 50).Value = string.IsNullOrEmpty(model.ControllerName) ? "" : model.ControllerName;
                    comm.Parameters.Add("@StartTime", SqlDbType.DateTime).Value = Starttime.ToString("yyyy-MM-dd HH:mm:ss.fff");
                    comm.Parameters.Add("@EndTime", SqlDbType.DateTime).Value = Endtime.ToString("yyyy-MM-dd HH:mm:ss.fff");
                    comm.Parameters.Add("@Status", SqlDbType.VarChar, 50).Value = string.IsNullOrEmpty(model.Status) ? "" : model.Status;
                    comm.Parameters.Add("@RequestParameters", SqlDbType.NVarChar).Value = string.IsNullOrEmpty(model.RequestParameters) ? "" : model.RequestParameters;
                    comm.Parameters.Add("@RequestResponse", SqlDbType.NVarChar).Value = string.IsNullOrEmpty(model.RequestResponse) ? "" : model.RequestResponse;
                    comm.Parameters.Add("@Channel", SqlDbType.VarChar).Value = string.IsNullOrEmpty(model.Channel) ? "" : model.Channel;
                    comm.Parameters.Add("@LogId", SqlDbType.NVarChar, 450).Value = string.IsNullOrEmpty(model.LogId) ? "" : model.LogId;
                    comm.Parameters.Add("@RequestDateTime", SqlDbType.DateTime).Value = reqdatetime.ToString("yyyy-MM-dd HH:mm:ss.fff");
                    comm.Parameters.Add("@ErrorCode", SqlDbType.NVarChar, 100).Value = string.IsNullOrEmpty(model.ErrorCode) ? "" : model.ErrorCode;
                    comm.Parameters.Add("@APIbaseURL", SqlDbType.NVarChar).Value = string.IsNullOrEmpty(model.APIbaseURL) ? "" : model.APIbaseURL;
                    comm.Parameters.Add("@APIFunctionName", SqlDbType.NVarChar).Value = string.IsNullOrEmpty(model.APIFunctionName) ? "" : model.APIFunctionName;
                    comm.Parameters.Add("@LocalServerIP", SqlDbType.NVarChar, 500).Value = string.IsNullOrEmpty(model.LocalServerIP) ? "" : model.LocalServerIP;
                    comm.Parameters.Add("@ApiRequestStartTime", SqlDbType.DateTime).Value = model.ApiRequestStartTime.ToString("yyyy-MM-dd HH:mm:ss.fff");
                    comm.Parameters.Add("@ApiRequestEndTime", SqlDbType.DateTime).Value = model.ApiRequestEndTime.ToString("yyyy-MM-dd HH:mm:ss.fff");
                    comm.Parameters.Add("@HTTPAction", SqlDbType.NVarChar).Value = string.IsNullOrEmpty(model.httpAction) ? "" : model.httpAction;
                    comm.CommandText = query;
                    comm.ExecuteNonQuery();
                    conn.Close();

                }
                catch (Exception e)
                {
                    Utility.SplunkLogs(e, DateTime.Now, SplunkBase, SplunCollector, SplunkAuth, "mw:log");

                }
            }



        }
        public async void Logs(Level Level, string status, HttpContext contaxt, Exception ex,
            DateTime Starttime, DateTime Endtime, string RequestParameter, string RequestResponse, string userid,
            string Channel, string logId, DateTime? RequestDateTime, System.Net.HttpStatusCode? code, string baseURL = "",
            string functionName = "", DateTime? ApiRequestStartTime = null, DateTime? ApiRequestEndTime = null, string clientIp = "", string currentController = "", string currentAction = "", string ErrorLog = "", string SuccessLog = "", string InfoLog = "", string SplunCollector = "", string SplunkBase = "", string SplunkAuth = "", string httpAction = "")
        {
            try
            {
                try
                {
                    RequestParameter = Utility.ModifyRequestBody(currentAction, RequestParameter);
                    RequestResponse = Utility.ModifyResponseBody(currentAction, RequestResponse);
                }
                catch (Exception e)
                {
                    Utility.SplunkLogs(e, DateTime.Now, SplunkBase, SplunCollector, SplunkAuth,"mw:log");
                    

                }

                var id = userid;
                string message = "";
                string except = null;
                if (ex != null)
                {
                    except = ex.Message;
                    if (ex.InnerException != null)
                    {
                        except += "  inner exception :" + ex.InnerException.ToString();
                        except += "  inner message:" + ex.InnerException.Message.ToString();
                    }
                }

                if (Level == Level.Error && Convert.ToBoolean(ErrorLog) == true
                    || Level == Level.Success && Convert.ToBoolean(SuccessLog) == true)
                {
                    WriteLogs(message, Level, id, clientIp, currentAction, except, currentController, status, Starttime, Endtime, RequestParameter, RequestResponse, Channel, logId, RequestDateTime, code, baseURL, functionName, ApiRequestStartTime, ApiRequestEndTime, SplunkBase, SplunCollector, SplunkAuth, httpAction);
                }
                else
                {
                    if (Convert.ToBoolean(InfoLog) == true)
                    {
                        message = "";
                        //$"User with id:{id} Access Function:{currentAction} and Controller:{currentController}";
                        Level = Level.Info;
                        WriteLogs(message, Level, id, clientIp, currentAction, except, currentController, status, Starttime, Endtime, RequestParameter, RequestResponse, Channel, logId, RequestDateTime, code, baseURL, functionName, ApiRequestStartTime, ApiRequestEndTime, SplunkBase, SplunCollector, SplunkAuth, httpAction);
                    }
                }

             
                Channel = string.IsNullOrEmpty(Channel) ? "0" : Channel;

                object req = RequestParameter;
                object res = RequestResponse;
                try
                {
                    JToken.Parse(RequestParameter);
                    req = JsonConvert.DeserializeObject(RequestParameter);
                }
                catch (Exception e)
                {
                    Utility.SplunkLogs(e, DateTime.Now, SplunkBase, SplunCollector, SplunkAuth, "mw:log");
                    
                }
                try
                {
                    JToken.Parse(RequestResponse);
                    res = JsonConvert.DeserializeObject(RequestResponse);
                }
                catch (Exception e)
                {
                    Utility.SplunkLogs(e, DateTime.Now, SplunkBase, SplunCollector, SplunkAuth, "mw:log");
                }

                object eventObj = new Event()
                {
                    Channel = Convert.ToInt32(Channel),
                    APIbaseURL = baseURL,
                    APIFunctionName = functionName,
                    ControllerName = currentController,
                    StartTime = Starttime,
                    EndTime = Endtime,
                    RequestParameters = req,
                    RequestResponse = res,
                    LogId = logId,
                    RequestDateTime = (DateTime)RequestDateTime,
                    ErrorCode = status,
                    FunctionName = currentAction,
                    LocalServerIP = GetLocalIPAddress()
                };
                Utility.SplunkLogs(eventObj, DateTime.Now, SplunkBase, SplunCollector, SplunkAuth, "mw:log");


                //string result = response.Content.ReadAsStringAsync().Result;
                //var response = http.PostAsync(url, data).Result;
                //string result = response.Content.ReadAsStringAsync().Result;
                //int count = 0;
                //if (result == "" || response.StatusCode != HttpStatusCode.OK)
                //{

                //}
                //else
                //{
                //    count++;
                //    StringBuilder sb1 = new StringBuilder();
                //    sb1.Append(count.ToString());
                //    File.AppendAllText("SplunkReqCount.txt", sb1.ToString());
                //}
            }
            catch (Exception e)
            {
                Utility.SplunkLogs(e, DateTime.Now, SplunkBase, SplunCollector, SplunkAuth, "mw:log");
               
            }
        }

        public ErrorMessages GetErrorMessage(string errorCode)
        {
            ErrorMessages message = _ctx.TErrorMessages.FirstOrDefault(x => x.ErrorCode == errorCode);
            return message;
        }

        public bool GetTversion()
        {
            var res = _ctx.TVersion.FirstOrDefault();
            if (res != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool GetLogId(string Id)
        {
            var res = _ctx.TLog.FirstOrDefault(x => x.LogId == Id);
            if (res != null)
            {
                return true;
            }
            return false;
        }

        public List<TVendorApiHeaders> GetHeaders(string VendorId, int Type)
        {

            var list = _ctx.TVendorApiHeaders.Where(x => x.VendorId.Equals(VendorId) && x.Type.Equals(Type)).ToList();
            return list;
        }

        //public VendorAPIAccess GetHeaders(int ChannelId)
        //{
        //    VendorAPIAccess obj = _ctx.TVendorApiHeaders.FirstOrDefault(x=>x.VendorId = ChannelId);
        //    return obj;
        //}

        private static string ConvertAuthorization(string username, string password)
        {
            return Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", username, password)));
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }





    }
}
