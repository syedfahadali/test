﻿using System.Threading.Tasks;
using DBHandler.Helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using VendorApi.Helper;
using Swashbuckle.AspNetCore.Annotations;
using DBHandler.Repositories;
using Microsoft.Extensions.Configuration;
using DBHandler.Enum;
using System.Net.Http;
using System.Net.Mail;
using System.Net;

namespace VendorApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmailController : ControllerBase
    {
        private readonly HttpHandler.HttpHandler httpHandler;
        private readonly IUserChannelsRepository _userChannels;
        private readonly ILogRepository _logs;
        private readonly IConfiguration _config;
        private Level level = Level.Info;

        public EmailController(IConfiguration config, IUserChannelsRepository userChannels, IHttpClientFactory httpClientFactory, ILogRepository logs)
        {
            _config = config;
            _userChannels = userChannels;
            _logs = logs;
            httpHandler = new HttpHandler.HttpHandler(httpClientFactory);
        }

        [Route("SendEmail")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CommonModel.Email.RootObject>", typeof(ActiveResponseSucces<CommonModel.Email.RootObject>))]
        public async Task<IActionResult> SendEmail([FromBody] CommonModel.Email.EmailModel  Model)
        {
            ActiveResponseSucces<CommonModel.Email.RootObject> response = new ActiveResponseSucces<CommonModel.Email.RootObject>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model == null ? null : Model.RootObject,
                                                                        _config["Email:BaseUrl"].ToString(),
                                                                        _config["Email:SendEmail"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                           this,
                                                                        _config,
                                                                        httpHandler, 2);

            response.LogId = Model != null ? Model.SignOnRq.LogId : "";
            response.RequestDateTime = Model != null ? Model.SignOnRq.DateTime : null;

            var client = new SmtpClient(_config["Email:smtp"].ToString(), int.Parse(_config["Email:port"].ToString()))
            {
                Credentials = new NetworkCredential(_config["Email:username"].ToString(), _config["Email:password"].ToString()),
                EnableSsl = true
            };
            MailMessage msg = new MailMessage();
            msg.IsBodyHtml = true;
            msg.From = new MailAddress (Model.RootObject.from);
            msg.To.Add(Model.RootObject.to[0].Email);

            msg.Subject = Model.RootObject.subject;
            msg.Body = Model.RootObject.body;

            client.Send(msg);

            response.Status = new Status { Severity = Severity.Success, Code = "MSG-000000", StatusMessage = Severity.Success };

            response.Content = null;
            return Ok(response);
        }
    }
}