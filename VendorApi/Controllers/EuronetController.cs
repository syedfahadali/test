﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CoreHandler;
using DBHandler.Helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using DBHandler.Repositories;
using DBHandler.Model;
using Microsoft.Extensions.Configuration;
using DBHandler.Model.Dtos;
using DBHandler.Enum;
using HttpHandler;
using VendorApi.Helper;
using CommonModel;
using Newtonsoft.Json.Linq;
using ECSFinHandler;
using Swashbuckle.AspNetCore.Annotations;
using MCCMiDBlocking;
using EuronetHandler;
using AddressUpdate;
using ChannelUnblocking;
using CountryBlocking;
using PhoneUpdate;
using TierUpdateLimitProfileNameUpdate;
using CardIssuanceService;
using CardStatusChangeAndReplacement;
using LimitManagement;
using UpdateCardStatus;
using UpdateOTPService;
using UpdatePINService;

namespace VendorApi.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("api/[controller]")]
    [ApiController]
    public class EuronetController : ControllerBase
    {
        private readonly HttpHandler.HttpHandler httpHandler;
        private readonly IUserChannelsRepository _userChannels;
        private readonly ILogRepository _logs;
        private readonly IConfiguration _config;
        private Level level = Level.Info;

        public EuronetController(IConfiguration config, IUserChannelsRepository userChannels, IHttpClientFactory httpClientFactory, ILogRepository logs)
        {
            _config = config;
            _userChannels = userChannels;
            _logs = logs;
            httpHandler = new HttpHandler.HttpHandler(httpClientFactory);
        }
        private string GenerateSQN()
        {
            //TxnSeqNum Logic(0213145522)		
            //1 to 4 - MMDD(0213)
            //5 to 10 - HHMMSS(145522)
            //11 to 15 - Running Sequence Number(12345)		
            Random generator = new Random();
            String random = generator.Next(0, 999999).ToString("D5");

            string MMDD = DateTime.Now.ToString("MMdd");
            string HHMMSS = DateTime.Now.ToString("HHmmss");
            string runningSQNum = random;
            return MMDD + HHMMSS + runningSQNum;
        }

        [Route("MCCMIDBlockingWSBean")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "MCCMIDBlockingWSBeanClient", typeof(ActiveResponseSucces<MCCMIDBlockingWSBeanClient>))]
        public async Task<IActionResult> MCCMIDBlockingWSBean([FromBody] MCCMIDBlockingRQModel Model)
        {
            DateTime StartTime = DateTime.Now;

            MCCMIDBlockingWSBeanClient client = new MCCMIDBlockingWSBeanClient();
            client.Endpoint.Address = new System.ServiceModel.EndpointAddress(_config["Euronet:BaseUrl"].ToString() + _config["Euronet:MCCMIDBlockingWSBeanPort"].ToString());

            MCCMIDBlockingRequest request = new MCCMIDBlockingRequest();
            request.ServiceHeader = new MCCMiDBlocking.ServiceHeader();
            request.ServiceHeader.ServiceID = Model.CommonModel.ServiceID;
            request.ServiceHeader.ChannelID = Model.CommonModel.ChannelID;
            request.ServiceHeader.CountryCode = Model.CommonModel.CountryCode;
            request.ServiceHeader.TxnSeqNum = GenerateSQN();
            request.ServiceHeader.SourceDate = DateTime.Now.ToString("yyyy-MM-dd");
            request.ServiceHeader.SourceTime = DateTime.Now.ToString("HH:mm:ss");
            request.ServiceHeader.UserID = Model.CommonModel.UserID;

            request.Request_MCCMIDBlocking = new Request_MCCMIDBlocking() { Function = Model.Function, MccBlkList = Model.MccBlkList, MidBlkList = Model.MidBlkList };
            MCCMIDBlockingResponse data = client.MCCMIDBlockingAsync(request).Result;

            MCCMIDBlockingRQResponse MCCMIDBlockingRQResponse = new MCCMIDBlockingRQResponse();
            MCCMIDBlockingRQResponse.ResponseCode = data.Response_MCCMIDBlocking.ResponseCode;
            MCCMIDBlockingRQResponse.ResponseText = data.Response_MCCMIDBlocking.ResponseText;

            ActiveResponseSucces<MCCMIDBlockingRQResponse> response = new ActiveResponseSucces<MCCMIDBlockingRQResponse>();
            InternalResult result = await APIRequestHandler.GetResponse( StartTime,
                                                                         MCCMIDBlockingRQResponse.ResponseCode,
                                                                         MCCMIDBlockingRQResponse.ResponseText,
                                                                         data,
                                                                         Model.SignOnRq,
                                                                         Model,
                                                                         level,
                                                                         _logs,
                                                                         _userChannels,
                                                                         this,
                                                                        _config);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;
            response.Status = result.Status;
            if (MCCMIDBlockingRQResponse.ResponseCode == "00")
            {
                MCCMIDBlockingRQResponse.MccList = data.Response_MCCMIDBlocking?.MccListDetail[0].MccList;
                MCCMIDBlockingRQResponse.MidList = data.Response_MCCMIDBlocking?.MidListDetail[0].MidList;
                response.Content = MCCMIDBlockingRQResponse;
                return Ok(response);
            }
            else
            {
                response.Status.Severity = Severity.Error;
                return BadRequest(response);
            }
        }

        [Route("AddressUpdate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "UpdateAddressModelResponse", typeof(ActiveResponseSucces<UpdateAddressModelResponse>))]
        public async Task<IActionResult> AddressUpdate([FromBody] UpdateAddressModel Model)
        {
            DateTime StartTime = DateTime.Now;
            UpdateAddressWSBeanClient client = new UpdateAddressWSBeanClient();
            client.Endpoint.Address = new System.ServiceModel.EndpointAddress(_config["Euronet:BaseUrl"].ToString() + _config["Euronet:UpdateAddressWSBeanPort"].ToString());

            UpdateAddressRequest request = new UpdateAddressRequest();
            request.ServiceHeader = new AddressUpdate.ServiceHeader();
            request.ServiceHeader.ServiceID = Model.CommonModel.ServiceID;
            request.ServiceHeader.ChannelID = Model.CommonModel.ChannelID;
            request.ServiceHeader.CountryCode = Model.CommonModel.CountryCode;
            request.ServiceHeader.TxnSeqNum = GenerateSQN();
            request.ServiceHeader.SourceDate = DateTime.Now.ToString("yyyy-MM-dd");
            request.ServiceHeader.SourceTime = DateTime.Now.ToString("HH:mm:ss");
            request.ServiceHeader.UserID = Model.CommonModel.UserID;

            request.Request_UpdateAddress = new Request_UpdateAddress();
            request.Request_UpdateAddress.Address1 = Model.Address1;
            request.Request_UpdateAddress.Address2 = Model.Address2;
            request.Request_UpdateAddress.AddType= Model.AddType;
            request.Request_UpdateAddress.CardSeqNum= Model.CardSeqNum;
            request.Request_UpdateAddress.City= Model.City;
            request.Request_UpdateAddress.Function= Model.Function;
            request.Request_UpdateAddress.PriOrSec= Model.PriOrSec;
            request.Request_UpdateAddress.Privacy= Model.Privacy;
            request.Request_UpdateAddress.SeqNum= Model.SeqNum;
            request.Request_UpdateAddress.State = Model.State;
            request.Request_UpdateAddress.TokenCardNumber= Model.TokenCardNumber;
            request.Request_UpdateAddress.ZipCode = Model.ZipCode;

            UpdateAddressResponse data = client.UpdateAddressAsync(request).Result;

            UpdateAddressModelResponse responseData = new UpdateAddressModelResponse();
            responseData.ResponseCode = data.Response_UpdateAddress.ResponseCode;
            responseData.ResponseText = data.Response_UpdateAddress.ResponseText;

            ActiveResponseSucces<UpdateAddressModelResponse> response = new ActiveResponseSucces<UpdateAddressModelResponse>();
            InternalResult result = await APIRequestHandler.GetResponse( StartTime,
                                                                         responseData.ResponseCode,
                                                                         responseData.ResponseText,
                                                                         data,
                                                                         Model.SignOnRq,
                                                                         Model,
                                                                         level,
                                                                         _logs,
                                                                         _userChannels,
                                                                         this,
                                                                        _config);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;
            response.Status = result.Status;
            if (responseData.ResponseCode == "00")
            {
                responseData.Address1 = data.Response_UpdateAddress.AddressDetail[0].Address1;
                responseData.Address2 = data.Response_UpdateAddress.AddressDetail[0].Address2;
                responseData.AddType = data.Response_UpdateAddress.AddressDetail[0].AddType;
                responseData.CardSeqNum = data.Response_UpdateAddress.CardSeqNum;
                responseData.City  = data.Response_UpdateAddress.AddressDetail[0].City;
                responseData.PriOrSec = data.Response_UpdateAddress.AddressDetail[0].PriOrSec;
                responseData.SeqNum = data.Response_UpdateAddress.AddressDetail[0].SeqNum;
                responseData.State = data.Response_UpdateAddress.AddressDetail[0].State;
                responseData.ZipCode = data.Response_UpdateAddress.AddressDetail[0].ZipCode;
                response.Content = responseData;
                return Ok(response);
            }
            else
            {
                response.Status.Severity = Severity.Error;
                return BadRequest(response);
            }
        }

        [Route("ChannelUnblocking")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ChannelRestrictionResponse", typeof(ActiveResponseSucces<ChannelRestrictionResponse>))]
        public async Task<IActionResult> ChannelUnblocking([FromBody] ChannelRestrictionModel Model)
        {
            DateTime StartTime = DateTime.Now;
            ChannelRestricationWSBeanClient client = new ChannelRestricationWSBeanClient();
            client.Endpoint.Address = new System.ServiceModel.EndpointAddress(_config["Euronet:BaseUrl"].ToString() + _config["Euronet:ChannelRestricationWSBeanPort"].ToString());

            ChannelRestricationRequest request = new ChannelRestricationRequest();
            request.ServiceHeader = new ChannelUnblocking.ServiceHeader();
            request.ServiceHeader.ServiceID = Model.CommonModel.ServiceID;
            request.ServiceHeader.ChannelID = Model.CommonModel.ChannelID;
            request.ServiceHeader.CountryCode = Model.CommonModel.CountryCode;
            request.ServiceHeader.TxnSeqNum = GenerateSQN();
            request.ServiceHeader.SourceDate = DateTime.Now.ToString("yyyy-MM-dd");
            request.ServiceHeader.SourceTime = DateTime.Now.ToString("HH:mm:ss");
            request.ServiceHeader.UserID = Model.CommonModel.UserID;

            request.Request_ChannelRestrication = new Request_ChannelRestrication();
            request.Request_ChannelRestrication.Function = Model.Function;
            request.Request_ChannelRestrication.CardSeqNum = Model.CardSeqNum;
            request.Request_ChannelRestrication.ContLess = Model.ContLess;
            request.Request_ChannelRestrication.DomCash = Model.DomCash;
            request.Request_ChannelRestrication.DomEcom = Model.DomEcom;
            request.Request_ChannelRestrication.DomPurchase = Model.DomPurchase;
            request.Request_ChannelRestrication.IntCash = Model.IntCash;
            request.Request_ChannelRestrication.IntEom = Model.IntEom;
            request.Request_ChannelRestrication.IntPurchase = Model.IntPurchase;
            request.Request_ChannelRestrication.Magstripe= Model.Magstripe;
            request.Request_ChannelRestrication.TokenCardNumber = Model.TokenCardNumber;

            ChannelRestricationResponse data = client.ChannelRestricationAsync(request).Result;

            ChannelRestrictionResponse responseData = new ChannelRestrictionResponse();
            responseData.ResponseCode = data.Response_ChannelRestrication.ResponseCode;
            responseData.ResponseText = data.Response_ChannelRestrication.ResponseText;

            ActiveResponseSucces<ChannelRestrictionResponse> response = new ActiveResponseSucces<ChannelRestrictionResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(StartTime,
                                                                         responseData.ResponseCode,
                                                                         responseData.ResponseText,
                                                                         data,
                                                                         Model.SignOnRq,
                                                                         Model,
                                                                         level,
                                                                         _logs,
                                                                         _userChannels,
                                                                         this,
                                                                        _config);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;
            response.Status = result.Status;
            if (responseData.ResponseCode == "00")
            {
                responseData.CardSeqNum = data.Response_ChannelRestrication.CardSeqNum;
                responseData.DomEcom = data.Response_ChannelRestrication.DomEcom;
                responseData.DomPur = data.Response_ChannelRestrication.DomPur;
                responseData.DomWit = data.Response_ChannelRestrication.DomWit;
                responseData.IntEom = data.Response_ChannelRestrication.IntEom;
                responseData.IntPur = data.Response_ChannelRestrication.IntPur;
                responseData.IntWit = data.Response_ChannelRestrication.IntWit;
                responseData.Magstripe = data.Response_ChannelRestrication.Magstripe;
                responseData.ContLess =  data.Response_ChannelRestrication.ContLess;
                response.Content = responseData;
                return Ok(response);
            }
            else
            {
                response.Status.Severity = Severity.Error;
                return BadRequest(response);
            }
        }

        [Route("CountryBlocking")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "CountryUnBlockingResponse", typeof(ActiveResponseSucces<CountryUnBlockingResponse>))]
        public async Task<IActionResult> CountryBlocking([FromBody] CountryUnBlockingModel Model)
        {
            DateTime StartTime = DateTime.Now;
            CountryBlockingWSBeanClient client = new CountryBlockingWSBeanClient();
            client.Endpoint.Address = new System.ServiceModel.EndpointAddress(_config["Euronet:BaseUrl"].ToString() + _config["Euronet:CountryBlockingWSBeanPort"].ToString());

            CountryBlockingRequest  request = new CountryBlockingRequest();
            request.ServiceHeader = new CountryBlocking.ServiceHeader();
            request.ServiceHeader.ServiceID = Model.CommonModel.ServiceID;
            request.ServiceHeader.ChannelID = Model.CommonModel.ChannelID;
            request.ServiceHeader.CountryCode = Model.CommonModel.CountryCode;
            request.ServiceHeader.TxnSeqNum = GenerateSQN();
            request.ServiceHeader.SourceDate = DateTime.Now.ToString("yyyy-MM-dd");
            request.ServiceHeader.SourceTime = DateTime.Now.ToString("HH:mm:ss");
            request.ServiceHeader.UserID = Model.CommonModel.UserID;

            request.Request_CountryBlocking= new Request_CountryBlocking();
            request.Request_CountryBlocking.CardSeqNum = Model.CardSeqNum;
            request.Request_CountryBlocking.CountryList = Model.CountryList;
            request.Request_CountryBlocking.Function = Model.Function;
            request.Request_CountryBlocking.TokenCardNumber = Model.TokenCardNumber;




            CountryBlockingResponse data = client.CountryBlockingAsync(request).Result;

            CountryUnBlockingResponse responseData = new CountryUnBlockingResponse();
            responseData.ResponseCode = data.Response_CountryBlocking.ResponseCode;
            responseData.ResponseText = data.Response_CountryBlocking.ResponseText;

            ActiveResponseSucces<CountryUnBlockingResponse> response = new ActiveResponseSucces<CountryUnBlockingResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(StartTime,
                                                                         responseData.ResponseCode,
                                                                         responseData.ResponseText,
                                                                         data,
                                                                         Model.SignOnRq,
                                                                         Model,
                                                                         level,
                                                                         _logs,
                                                                         _userChannels,
                                                                         this,
                                                                        _config);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;
            response.Status = result.Status;
            if (responseData.ResponseCode == "00")
            {
                responseData.CardSeqNum = data.Response_CountryBlocking.CardSeqNum;
                responseData.AllCntryLst = data.Response_CountryBlocking.AllCntryLst;
                responseData.TokenCardNumber = data.Response_CountryBlocking.TokenCardNumber;
                responseData.CntryLst = data.Response_CountryBlocking.CntryLst;
                response.Content = responseData;
                return Ok(response);
            }
            else
            {
                response.Status.Severity = Severity.Error;
                return BadRequest(response);
            }
        }

        [Route("PhoneUpdate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "UpdatePhoneModelResponse", typeof(ActiveResponseSucces<UpdatePhoneModelResponse>))]
        public async Task<IActionResult> PhoneUpdate([FromBody] UpdatePhoneModel Model)
        {
            DateTime StartTime = DateTime.Now;
            UpdatePhoneWSBeanClient client = new UpdatePhoneWSBeanClient();
            client.Endpoint.Address = new System.ServiceModel.EndpointAddress(_config["Euronet:BaseUrl"].ToString() + _config["Euronet:UpdatePhoneWSBeanPort"].ToString());

            UpdatePhoneRequest request = new UpdatePhoneRequest();
            request.ServiceHeader = new PhoneUpdate.ServiceHeader();
            request.ServiceHeader.ServiceID = Model.CommonModel.ServiceID;
            request.ServiceHeader.ChannelID = Model.CommonModel.ChannelID;
            request.ServiceHeader.CountryCode = Model.CommonModel.CountryCode;
            request.ServiceHeader.TxnSeqNum = GenerateSQN();
            request.ServiceHeader.SourceDate = DateTime.Now.ToString("yyyy-MM-dd");
            request.ServiceHeader.SourceTime = DateTime.Now.ToString("HH:mm:ss");
            request.ServiceHeader.UserID = Model.CommonModel.UserID;

            request.Request_UpdatePhone = new Request_UpdatePhone();
            request.Request_UpdatePhone.CardSeqNum= Model.CardSeqNum;
            request.Request_UpdatePhone.Function = Model.Function;
            request.Request_UpdatePhone.PhoneNum = Model.PhoneNum;
            request.Request_UpdatePhone.PhoneType = Model.PhoneType;
            request.Request_UpdatePhone.PriOrSec = Model.PriOrSec;
            request.Request_UpdatePhone.Privacy = Model.Privacy;
            request.Request_UpdatePhone.SeqNum = Model.SeqNum;
            request.Request_UpdatePhone.TokenCardNumber = Model.TokenCardNumber;

            UpdatePhoneResponse data = client.UpdatePhoneAsync(request).Result;

            UpdatePhoneModelResponse responseData = new UpdatePhoneModelResponse();
            responseData.ResponseCode = data.Response_UpdatePhone.ResponseCode;
            responseData.ResponseText = data.Response_UpdatePhone.ResponseText;

            ActiveResponseSucces<UpdatePhoneModelResponse> response = new ActiveResponseSucces<UpdatePhoneModelResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(StartTime,
                                                                         responseData.ResponseCode,
                                                                         responseData.ResponseText,
                                                                         data,
                                                                         Model.SignOnRq,
                                                                         Model,
                                                                         level,
                                                                         _logs,
                                                                         _userChannels,
                                                                         this,
                                                                        _config);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;
            response.Status = result.Status;
            if (responseData.ResponseCode == "00")
            {
                responseData.CardSeqNum = data.Response_UpdatePhone.CardSeqNum;
                responseData.TokenCardNumber = data.Response_UpdatePhone.TokenCardNumber;
                if (data.Response_UpdatePhone.PhoneDetail != null)
                {
                    responseData.PhnNum = data.Response_UpdatePhone.PhoneDetail[0].PhnNum;
                    responseData.PhnType = data.Response_UpdatePhone.PhoneDetail[0].PhnType;
                    responseData.PriOrSec = data.Response_UpdatePhone.PhoneDetail[0].PriOrSec;
                    responseData.SeqNum = data.Response_UpdatePhone.PhoneDetail[0].SeqNum;
                }
                response.Content = responseData;
                return Ok(response);
            }
            else
            {
                response.Status.Severity = Severity.Error;
                return BadRequest(response);
            }
        }

        [Route("TierUpdateLimitProfileNameUpdate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "LimitProfileUpdateResponse", typeof(ActiveResponseSucces<LimitProfileUpdateResponse>))]
        public async Task<IActionResult> TierUpdateLimitProfileNameUpdate([FromBody] LimitProfileUpdateModel Model)
        {
            DateTime StartTime = DateTime.Now;
            UpdateLimitProfileWSBeanClient client = new UpdateLimitProfileWSBeanClient();
            client.Endpoint.Address = new System.ServiceModel.EndpointAddress(_config["Euronet:BaseUrl"].ToString() + _config["Euronet:UpdateLimitProfileWSBeanPort"].ToString());

            UpdateLimitProfileRequest request = new UpdateLimitProfileRequest();
            request.ServiceHeader = new TierUpdateLimitProfileNameUpdate.ServiceHeader();
            request.ServiceHeader.ServiceID = Model.CommonModel.ServiceID;
            request.ServiceHeader.ChannelID = Model.CommonModel.ChannelID;
            request.ServiceHeader.CountryCode = Model.CommonModel.CountryCode;
            request.ServiceHeader.TxnSeqNum = GenerateSQN();
            request.ServiceHeader.SourceDate = DateTime.Now.ToString("yyyy-MM-dd");
            request.ServiceHeader.SourceTime = DateTime.Now.ToString("HH:mm:ss");
            request.ServiceHeader.UserID = Model.CommonModel.UserID;

            request.Request_UpdateLimitProfile = new Request_UpdateLimitProfile();
            request.Request_UpdateLimitProfile.CardMember = Model.CardMember;
            request.Request_UpdateLimitProfile.Function = Model.Function;
            request.Request_UpdateLimitProfile.Profile = Model.Profile;
            request.Request_UpdateLimitProfile.TokenCard = Model.TokenCard;

            UpdateLimitProfileResponse data = client.UpdateLimitProfileAsync(request).Result;

            LimitProfileUpdateResponse responseData = new LimitProfileUpdateResponse();
            responseData.ResponseCode = data.Response_UpdateLimitProfile.ResponseCode;
            responseData.ResponseText = data.Response_UpdateLimitProfile.ResponseText;

            ActiveResponseSucces<LimitProfileUpdateResponse> response = new ActiveResponseSucces<LimitProfileUpdateResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(StartTime,
                                                                         responseData.ResponseCode,
                                                                         responseData.ResponseText,
                                                                         data,
                                                                         Model.SignOnRq,
                                                                         Model,
                                                                         level,
                                                                         _logs,
                                                                         _userChannels,
                                                                         this,
                                                                        _config);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;
            response.Status = result.Status;
            if (responseData.ResponseCode == "00")
            {
                responseData.CurLimitProfile = data.Response_UpdateLimitProfile.CurLimitProfile;
                if (data.Response_UpdateLimitProfile.PosLimitProfileDetail != null)
                {

                    responseData.PosLimitProfile = data.Response_UpdateLimitProfile.PosLimitProfileDetail[0].PosLimitProfile;
                }
                response.Content = responseData;
                return Ok(response);
            }
            else
            {
                response.Status.Severity = Severity.Error;
                return BadRequest(response);
            }
        }

        [Route("CardIssuance")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "CardIssuanceResponse", typeof(ActiveResponseSucces<CardIssuanceResponse>))]
        public async Task<IActionResult> CardIssuance([FromBody] CardIssuanceModel Model)
        {
            DateTime StartTime = DateTime.Now;
            CardCreateWSBeanClient client = new CardCreateWSBeanClient();
            client.Endpoint.Address = new System.ServiceModel.EndpointAddress(_config["Euronet:BaseUrl"].ToString() + _config["Euronet:CardCreateWSBeanPort"].ToString());

            CardCreateRequest request = new CardCreateRequest();
            request.ServiceHeader = new CardIssuanceService.ServiceHeader();
            request.ServiceHeader.ServiceID = Model.CommonModel.ServiceID;
            request.ServiceHeader.ChannelID = Model.CommonModel.ChannelID;
            request.ServiceHeader.CountryCode = Model.CommonModel.CountryCode;
            request.ServiceHeader.TxnSeqNum = GenerateSQN();
            request.ServiceHeader.SourceDate = DateTime.Now.ToString("yyyy-MM-dd");
            request.ServiceHeader.SourceTime = DateTime.Now.ToString("HH:mm:ss");
            request.ServiceHeader.UserID = Model.CommonModel.UserID;

            request.RequestDt_CardCreate = new RequestDt_CardCreate();
            request.RequestDt_CardCreate.AccountList = new RequestDt_CardCreateAccountList[1];
            RequestDt_CardCreateAccountList address = new RequestDt_CardCreateAccountList();
            address.AccountBranch = Model.AccountBranch;
            address.AccountCurrency = Model.AccountCurrency;
            address.AccountNo = Model.AccountNumber;
            address.AccountType = Model.AcccountType;
            address.PrimaryFlag = Model.PrimaryFlag;
            request.RequestDt_CardCreate.AccountList[0] = address;
            request.RequestDt_CardCreate.Address1 = Model.Address1;
            request.RequestDt_CardCreate.Address2= Model.Address2;
            request.RequestDt_CardCreate.AWBRefNum= Model.AWBRefNum;
            request.RequestDt_CardCreate.CardActivateFlag= Model.CardActivateFlag;
            request.RequestDt_CardCreate.City= Model.City;
            request.RequestDt_CardCreate.CustID= Model.CustID;
            request.RequestDt_CardCreate.EmailID= Model.EmailID;
            request.RequestDt_CardCreate.EmbossLine2= Model.EmbossLine2;
            request.RequestDt_CardCreate.EmbossName = Model.EmbossName;
            request.RequestDt_CardCreate.PerHubLocation= Model.PerHubLocation;
            request.RequestDt_CardCreate.PhoneNum = Model.PhoneNum;
            request.RequestDt_CardCreate.PrefDate= Model.PrefDate;
            request.RequestDt_CardCreate.Preftime= Model.Preftime;
            request.RequestDt_CardCreate.State= Model.State;
            request.RequestDt_CardCreate.ZipCode= Model.ZipCode;
            request.RequestDt_CardCreate.CardType = Model.CardType;

            CardCreateResponse data = client.CardCreateAsync(request).Result;

            CardIssuanceResponse responseData = new CardIssuanceResponse();
            responseData.ResponseCode = data.ResponseHD_CardCreate.ResponseCode;
            responseData.ResponseText = data.ResponseHD_CardCreate.ResponseText;
          

            ActiveResponseSucces<CardIssuanceResponse> response = new ActiveResponseSucces<CardIssuanceResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(StartTime,
                                                                         responseData.ResponseCode,
                                                                         responseData.ResponseText,
                                                                         data,
                                                                         Model.SignOnRq,
                                                                         Model,
                                                                         level,
                                                                         _logs,
                                                                         _userChannels,
                                                                         this,
                                                                        _config);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;
            response.Status = result.Status;
            if (responseData.ResponseCode == "00")
            {
                responseData.NewTokenNumber = data.ResponseHD_CardCreate.NewTokenNumber;
                responseData.NewSeqNum = data.ResponseHD_CardCreate.NewSeqNum;
                response.Content = responseData;
                return Ok(response);
            }
            else
            {
                response.Status.Severity = Severity.Error;
                return BadRequest(response);
            }
        }

        [Route("CardStatusChangeAndReplacement")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "CardStatusAndReplacementResponse", typeof(ActiveResponseSucces<CardStatusAndReplacementResponse>))]
        public async Task<IActionResult> CardStatusChangeAndReplacement([FromBody] CardStatusAndReplacementModel Model)
        {
            DateTime StartTime = DateTime.Now;
            StatusChangeWithReplacementWSBeanClient client = new StatusChangeWithReplacementWSBeanClient();
            client.Endpoint.Address = new System.ServiceModel.EndpointAddress(_config["Euronet:BaseUrl"].ToString() + _config["Euronet:StatusChangeWithReplacementWSBeanPort"].ToString());

            StatusChangeWithReplacementRequest request = new StatusChangeWithReplacementRequest();
            request.ServiceHeader = new CardStatusChangeAndReplacement.ServiceHeader();
            request.ServiceHeader.ServiceID = Model.CommonModel.ServiceID;
            request.ServiceHeader.ChannelID = Model.CommonModel.ChannelID;
            request.ServiceHeader.CountryCode = Model.CommonModel.CountryCode;
            request.ServiceHeader.TxnSeqNum = GenerateSQN();
            request.ServiceHeader.SourceDate = DateTime.Now.ToString("yyyy-MM-dd");
            request.ServiceHeader.SourceTime = DateTime.Now.ToString("HH:mm:ss");
            request.ServiceHeader.UserID = Model.CommonModel.UserID;

            request.Request_StatusChangeWithReplacement = new Request_StatusChangeWithReplacement();
            request.Request_StatusChangeWithReplacement.Address1= Model.Address1;
            request.Request_StatusChangeWithReplacement.Address2 = Model.Address2;
            request.Request_StatusChangeWithReplacement.AWBRefNum = Model.AWBRefNum;
            request.Request_StatusChangeWithReplacement.CardActFlag = Model.CardActFlag;
            request.Request_StatusChangeWithReplacement.CardSeqNum = Model.CardSeqNum;
            request.Request_StatusChangeWithReplacement.City = Model.City;
            request.Request_StatusChangeWithReplacement.EmbossName = Model.EmbossName;
            request.Request_StatusChangeWithReplacement.PerHubLocation = Model.PerHubLocation;
            request.Request_StatusChangeWithReplacement.ReasonCode = Model.ReasonCode;
            request.Request_StatusChangeWithReplacement.State = Model.State;
            request.Request_StatusChangeWithReplacement.TokenCardNumber = Model.TokenCardNumber;
            request.Request_StatusChangeWithReplacement.ZipCode = Model.ZipCode;

            StatusChangeWithReplacementResponse data = client.StatusChangeWithReplacementAsync(request).Result;

            CardStatusAndReplacementResponse responseData = new CardStatusAndReplacementResponse();
            responseData.ResponseCode = data.Response_StatusChangeWithReplacement.ResponseCode;
            responseData.ResponseText = data.Response_StatusChangeWithReplacement.ResponseText;

            ActiveResponseSucces<CardStatusAndReplacementResponse> response = new ActiveResponseSucces<CardStatusAndReplacementResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(StartTime,
                                                                         responseData.ResponseCode,
                                                                         responseData.ResponseText,
                                                                         data,
                                                                         Model.SignOnRq,
                                                                         Model,
                                                                         level,
                                                                         _logs,
                                                                         _userChannels,
                                                                         this,
                                                                        _config);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;
            response.Status = result.Status;
            if (responseData.ResponseCode == "00")
            {
                responseData.TokenCardNumber = data.Response_StatusChangeWithReplacement.TokenCardNumber;
                responseData.CardSeqNum = data.Response_StatusChangeWithReplacement.CardSeqNum;
                responseData.NewCardSeqNum = data.Response_StatusChangeWithReplacement.NewCardSeqNum;
                responseData.NewTokenNumber = data.Response_StatusChangeWithReplacement.NewTokenNumber;
                response.Content = responseData;
                return Ok(response);
            }
            else
            {
                response.Status.Severity = Severity.Error;
                return BadRequest(response);
            }
        }

        [Route("LimitManagement")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "LimitManagementResponse", typeof(ActiveResponseSucces<EuronetHandler.LimitManagementResponse>))]
        public async Task<IActionResult> LimitManagement([FromBody] LimitManagementModel Model)
        {
            DateTime StartTime = DateTime.Now;
            LimitManagementWSBeanClient client = new LimitManagementWSBeanClient();
            client.Endpoint.Address = new System.ServiceModel.EndpointAddress(_config["Euronet:BaseUrl"].ToString() + _config["Euronet:LimitManagementWSBeanPort"].ToString());

            LimitManagementRequest request = new LimitManagementRequest();
            request.ServiceHeader = new LimitManagement.ServiceHeader();
            request.ServiceHeader.ServiceID = Model.CommonModel.ServiceID;
            request.ServiceHeader.ChannelID = Model.CommonModel.ChannelID;
            request.ServiceHeader.CountryCode = Model.CommonModel.CountryCode;
            request.ServiceHeader.TxnSeqNum = GenerateSQN();
            request.ServiceHeader.SourceDate = DateTime.Now.ToString("yyyy-MM-dd");
            request.ServiceHeader.SourceTime = DateTime.Now.ToString("HH:mm:ss");
            request.ServiceHeader.UserID = Model.CommonModel.UserID;

            request.Request_LimitManagement = new Request_LimitManagement();
            request.Request_LimitManagement.CardMember= Model.CardMember;
            request.Request_LimitManagement.DomATMAmount = Model.DomATMAmount;
            request.Request_LimitManagement.DomATMCount = Model.DomATMCount;
            request.Request_LimitManagement.DomATMSingle = Model.DomATMSingle;
            request.Request_LimitManagement.DomECOMAmount = Model.DomECOMAmount;
            request.Request_LimitManagement.DomECOMCount = Model.DomECOMCount;
            request.Request_LimitManagement.DomECOMSingle = Model.DomECOMSingle;
            request.Request_LimitManagement.DomPOSAmount = Model.DomPOSAmount;
            request.Request_LimitManagement.DomPOSCount = Model.DomPOSCount;
            request.Request_LimitManagement.DomPOSSingle = Model.DomPOSSingle;
            request.Request_LimitManagement.Function = Model.Function;
            request.Request_LimitManagement.IntATMAmount = Model.IntATMAmount;
            request.Request_LimitManagement.IntATMCount = Model.IntATMSingle;
            request.Request_LimitManagement.IntECOMAmount = Model.IntECOMAmount;
            request.Request_LimitManagement.IntECOMCount = Model.IntECOMCount;
            request.Request_LimitManagement.IntECOMSingle = Model.IntECOMSingle;
            request.Request_LimitManagement.IntPOSAmount = Model.IntPOSAmount;
            request.Request_LimitManagement.IntPOSCount = Model.IntPOSCount;
            request.Request_LimitManagement.IntPOSSingle = Model.IntPOSSingle;
            request.Request_LimitManagement.TokenCard = Model.TokenCard;

            LimitManagement.LimitManagementResponse data = client.LimitManagementAsync(request).Result;

            EuronetHandler.LimitManagementResponse responseData = new EuronetHandler.LimitManagementResponse();
            responseData.ResponseCode = data.Response_LimitManagement.ResponseCode;
            responseData.ResponseText = data.Response_LimitManagement.ResponseText;

            ActiveResponseSucces<EuronetHandler.LimitManagementResponse> response = new ActiveResponseSucces<EuronetHandler.LimitManagementResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(StartTime,
                                                                         responseData.ResponseCode,
                                                                         responseData.ResponseText,
                                                                         data,
                                                                         Model.SignOnRq,
                                                                         Model,
                                                                         level,
                                                                         _logs,
                                                                         _userChannels,
                                                                         this,
                                                                        _config);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;
            response.Status = result.Status;
            if (responseData.ResponseCode == "00")
            {
                responseData.DomATMAmount = data.Response_LimitManagement.DomATMAmount;
                responseData.DomATMCount = data.Response_LimitManagement.DomATMCount;
                responseData.DomATMSingle = data.Response_LimitManagement.DomATMSingle;
                responseData.DomECOMAmount = data.Response_LimitManagement.DomECOMAmount;
                responseData.DomECOMCount = data.Response_LimitManagement.DomECOMCount;
                responseData.DomECOMSingle = data.Response_LimitManagement.DomECOMSingle;
                responseData.DomPOSAmount = data.Response_LimitManagement.DomPOSAmount;
                responseData.DomPOSCount = data.Response_LimitManagement.DomPOSCount;
                responseData.DomPOSSingle = data.Response_LimitManagement.DomPOSSingle;
                responseData.IntATMAmount = data.Response_LimitManagement.IntATMAmount;
                responseData.IntATMCount = data.Response_LimitManagement.IntATMCount;
                responseData.IntATMSingle = data.Response_LimitManagement.IntATMSingle;
                responseData.IntECOMAmount = data.Response_LimitManagement.IntECOMAmount;
                responseData.IntECOMCount = data.Response_LimitManagement.IntECOMCount;
                responseData.IntECOMSingle = data.Response_LimitManagement.IntECOMSingle;
                responseData.IntPOSAmount = data.Response_LimitManagement.IntPOSAmount;
                responseData.IntPOSCount = data.Response_LimitManagement.IntPOSCount;
                responseData.IntPOSSingle = data.Response_LimitManagement.IntPOSSingle;
                responseData.MaxDomATMAmount = data.Response_LimitManagement.MaxDomATMAmount;
                responseData.MaxDomATMCount = data.Response_LimitManagement.MaxDomATMCount;
                responseData.MaxDomATMSingle = data.Response_LimitManagement.MaxDomATMSingle;
                responseData.MaxDomECOMAmount = data.Response_LimitManagement.MaxDomECOMAmount;
                responseData.MaxDomECOMCount = data.Response_LimitManagement.MaxDomECOMCount;
                responseData.MaxDomECOMSingle = data.Response_LimitManagement.MaxDomECOMSingle;
                responseData.MaxDomPOSAmount = data.Response_LimitManagement.MaxDomPOSAmount;
                responseData.MaxDomPOSCount = data.Response_LimitManagement.MaxDomPOSCount;
                responseData.MaxDomPOSSingle = data.Response_LimitManagement.MaxDomPOSSingle;
                responseData.MaxIntATMAmount = data.Response_LimitManagement.MaxIntATMAmount;
                responseData.MaxIntATMCount = data.Response_LimitManagement.MaxIntATMCount;
                responseData.MaxIntATMSingle = data.Response_LimitManagement.MaxIntATMSingle;
                responseData.MaxIntECOMAmount = data.Response_LimitManagement.MaxIntECOMAmount;
                responseData.MaxIntECOMCount = data.Response_LimitManagement.MaxIntECOMCount;
                responseData.MaxIntECOMSingle = data.Response_LimitManagement.MaxIntECOMSingle;
                responseData.MaxIntPOSAmount = data.Response_LimitManagement.MaxIntPOSAmount;
                responseData.MaxIntPOSCount = data.Response_LimitManagement.MaxIntPOSCount;
                responseData.MaxIntPOSSingle = data.Response_LimitManagement.MaxIntPOSSingle;
                response.Content = responseData;
                return Ok(response);
            }
            else
            {
                response.Status.Severity = Severity.Error;
                return BadRequest(response);
            }
        }

        [Route("UpdateCardStatus")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "CardStatusResponse", typeof(ActiveResponseSucces<CardStatusResponse>))]
        public async Task<IActionResult> UpdateCardStatus([FromBody] CardStatusModel Model)
        {
            DateTime StartTime = DateTime.Now;
            UpdateCardStatusWSBeanClient client = new UpdateCardStatusWSBeanClient();
            client.Endpoint.Address = new System.ServiceModel.EndpointAddress(_config["Euronet:BaseUrl"].ToString() + _config["Euronet:UpdateCardStatusWSBeanPort"].ToString());

            UpdateCardStatusRequest request = new UpdateCardStatusRequest();
            request.ServiceHeader = new UpdateCardStatus.ServiceHeader();
            request.ServiceHeader.ServiceID = Model.CommonModel.ServiceID;
            request.ServiceHeader.ChannelID = Model.CommonModel.ChannelID;
            request.ServiceHeader.CountryCode = Model.CommonModel.CountryCode;
            request.ServiceHeader.TxnSeqNum = GenerateSQN();
            request.ServiceHeader.SourceDate = DateTime.Now.ToString("yyyy-MM-dd");
            request.ServiceHeader.SourceTime = DateTime.Now.ToString("HH:mm:ss");
            request.ServiceHeader.UserID = Model.CommonModel.UserID;

            request.Request_UpdateCardStatus = new Request_UpdateCardStatus();
            request.Request_UpdateCardStatus.CardSeqNum = Model.CardSeqNum;
            request.Request_UpdateCardStatus.ReasonCode = Model.ReasonCode;
            request.Request_UpdateCardStatus.TokenCardNumber = Model.TokenCardNumber;

            UpdateCardStatusResponse data = client.UpdateCardStatusAsync(request).Result;

            CardStatusResponse responseData = new CardStatusResponse();
            responseData.ResponseCode = data.Response_UpdateCardStatus.ResponseCode;
            responseData.ResponseText = data.Response_UpdateCardStatus.ResponseText;

            ActiveResponseSucces<CardStatusResponse> response = new ActiveResponseSucces<CardStatusResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(StartTime,
                                                                         responseData.ResponseCode,
                                                                         responseData.ResponseText,
                                                                         data,
                                                                         Model.SignOnRq,
                                                                         Model,
                                                                         level,
                                                                         _logs,
                                                                         _userChannels,
                                                                         this,
                                                                        _config);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;
            response.Status = result.Status;
            if (responseData.ResponseCode == "00")
            {
                responseData.TokenCardNumber = data.Response_UpdateCardStatus.TokenCardNumber;
                responseData.CardSeqNum = data.Response_UpdateCardStatus.CardSeqNum;
                response.Content = responseData;
                return Ok(response);
            }
            else
            {
                response.Status.Severity = Severity.Error;
                return BadRequest(response);
            }
        }


        [Route("UpdateOTPService")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "OTPGenerationVerificationResponse", typeof(ActiveResponseSucces<OTPGenerationVerificationResponse>))]
        public async Task<IActionResult> UpdateOTPService([FromBody] OTPGenerationVerificationModel Model)
        {
            DateTime StartTime = DateTime.Now;
            UpdateOTPWSBeanClient client = new UpdateOTPWSBeanClient();
            client.Endpoint.Address = new System.ServiceModel.EndpointAddress(_config["Euronet:BaseUrl"].ToString() + _config["Euronet:UpdateOTPWSBeanPort"].ToString());

            UpdateOTPRequest request = new UpdateOTPRequest();
            request.ServiceHeader = new UpdateOTPService.ServiceHeader();
            request.ServiceHeader.ServiceID = Model.CommonModel.ServiceID;
            request.ServiceHeader.ChannelID = Model.CommonModel.ChannelID;
            request.ServiceHeader.CountryCode = Model.CommonModel.CountryCode;
            request.ServiceHeader.TxnSeqNum = GenerateSQN();
            request.ServiceHeader.SourceDate = DateTime.Now.ToString("yyyy-MM-dd");
            request.ServiceHeader.SourceTime = DateTime.Now.ToString("HH:mm:ss");
            request.ServiceHeader.UserID = Model.CommonModel.UserID;

            request.Request_UpdateOTP = new Request_UpdateOTP();
            request.Request_UpdateOTP.CardSeqNum = Model.CardSeqNum;
            request.Request_UpdateOTP.OTP = Model.OTP;
            request.Request_UpdateOTP.TokenCardNumber = Model.TokenCardNumber;
            UpdateOTPResponse data = client.UpdateOTPAsync(request).Result;

            OTPGenerationVerificationResponse responseData = new OTPGenerationVerificationResponse();
            responseData.ResponseCode = data.Response_UpdateOTP.ResponseCode;
            responseData.ResponseText = data.Response_UpdateOTP.ResponseText;

            ActiveResponseSucces<OTPGenerationVerificationResponse> response = new ActiveResponseSucces<OTPGenerationVerificationResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(StartTime,
                                                                         responseData.ResponseCode,
                                                                         responseData.ResponseText,
                                                                         data,
                                                                         Model.SignOnRq,
                                                                         Model,
                                                                         level,
                                                                         _logs,
                                                                         _userChannels,
                                                                         this,
                                                                        _config);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;
            response.Status = result.Status;
            if (responseData.ResponseCode == "00")
            {
                responseData.TokenCardNumber = data.Response_UpdateOTP.TokenCardNumber;
                responseData.CardSeqNum = data.Response_UpdateOTP.CardSeqNum;
                response.Content = responseData;
                return Ok(response);
            }
            else
            {
                response.Status.Severity = Severity.Error;
                return BadRequest(response);
            }
        }

        [Route("UpdatePINService")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "PINCreateVerifyResponse", typeof(ActiveResponseSucces<PINCreateVerifyResponse>))]
        public async Task<IActionResult> UpdatePINService([FromBody] PINCreateVerifyModel Model)
        {
            DateTime StartTime = DateTime.Now;
            UpdatePinWSBeanClient client = new UpdatePinWSBeanClient();
            client.Endpoint.Address = new System.ServiceModel.EndpointAddress(_config["Euronet:BaseUrl"].ToString() + _config["Euronet:UpdatePinWSBeanPort"].ToString());

            UpdatePinRequest request = new UpdatePinRequest();
            request.ServiceHeader = new UpdatePINService.ServiceHeader();
            request.ServiceHeader.ServiceID = Model.CommonModel.ServiceID;
            request.ServiceHeader.ChannelID = Model.CommonModel.ChannelID;
            request.ServiceHeader.CountryCode = Model.CommonModel.CountryCode;
            request.ServiceHeader.TxnSeqNum = GenerateSQN();
            request.ServiceHeader.SourceDate = DateTime.Now.ToString("yyyy-MM-dd");
            request.ServiceHeader.SourceTime = DateTime.Now.ToString("HH:mm:ss");
            request.ServiceHeader.UserID = Model.CommonModel.UserID;

            request.Request_UpdatePin = new Request_UpdatePin();
            request.Request_UpdatePin.CardSeqNum = Model.CardSeqNum;
            request.Request_UpdatePin.CardActivateFlag = Model.CardActivateFlag;
            request.Request_UpdatePin.CardExpiry = Model.CardExpiry;
            request.Request_UpdatePin.PIN = Model.PIN;
            request.Request_UpdatePin.TokenCardNumber = Model.TokenCardNumber;


            UpdatePinResponse data = client.UpdatePinAsync(request).Result;

            PINCreateVerifyResponse responseData = new PINCreateVerifyResponse();
            responseData.ResponseCode = data.Response_UpdatePin.ResponseCode;
            responseData.ResponseText = data.Response_UpdatePin.ResponseText;

            ActiveResponseSucces<PINCreateVerifyResponse> response = new ActiveResponseSucces<PINCreateVerifyResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(StartTime,
                                                                         responseData.ResponseCode,
                                                                         responseData.ResponseText,
                                                                         data,
                                                                         Model.SignOnRq,
                                                                         Model,
                                                                         level,
                                                                         _logs,
                                                                         _userChannels,
                                                                         this,
                                                                        _config);
            response.LogId = Model!=null?Model.SignOnRq.LogId:"";
             response.RequestDateTime = Model!= null ?Model.SignOnRq.DateTime:null;
            response.Status = result.Status;
            if (responseData.ResponseCode == "00")
            {
                responseData.TokenCardNumber = data.Response_UpdatePin.TokenCardNumber;
                responseData.CardSeqNum = data.Response_UpdatePin.CardSeqNum;
                response.Content = responseData;
                return Ok(response);
            }
            else
            {
                response.Status.Severity = Severity.Error;
                return BadRequest(response);
            }
        }
    }
}