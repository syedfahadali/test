﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CoreHandler;
using DBHandler.Helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using DBHandler.Repositories;
using DBHandler.Model;
using Microsoft.Extensions.Configuration;
using DBHandler.Model.Dtos;
using DBHandler.Enum;
using HttpHandler;
using VendorApi.Helper;
using CommonModel;
using Newtonsoft.Json.Linq;
using Swashbuckle.AspNetCore.Annotations;
using System;

namespace VendorApi.Controllers
{
   
    [Route("api/[controller]")]
    [ApiController]
    public class BankSmartController : ControllerBase
    {
        private readonly HttpHandler.HttpHandler httpHandler;
        private readonly IUserChannelsRepository _userChannels;
        private readonly ILogRepository _logs;
        private readonly IConfiguration _config;
        private Level level = Level.Info;
        private Exception myEx;

        private Exception excetionForLog;

        public BankSmartController(IConfiguration config, IUserChannelsRepository userChannels, IHttpClientFactory httpClientFactory, ILogRepository logs)
        {
            _config = config;
            _userChannels = userChannels;
            _logs = logs;
            httpHandler = new HttpHandler.HttpHandler(httpClientFactory);
        }

        #region Fahad Functions

        [Route("BalanceInquiry")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Balance>", typeof(ActiveResponseSucces<Balance>))]
        public async Task<IActionResult> BalanceInquiry([FromBody] BalanceInquiryModel Model)
        {
            ActiveResponseSucces<Balance> response = new ActiveResponseSucces<Balance>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:BalanceInquriy"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<Balance>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }
        
        [Route("GetCorporateCIFDetail")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.Dtos.CustomerCorporateView>", typeof(ActiveResponseSucces<DBHandler.Model.Dtos.CustomerCorporateView>))]
        public async Task<IActionResult> GetCorporateCIFDetail([FromBody] RimInquiry Model)
        {
            ActiveResponseSucces<DBHandler.Model.Dtos.CustomerCorporateView> response = new ActiveResponseSucces<DBHandler.Model.Dtos.CustomerCorporateView>();


            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:GetCorporateCIFDetail"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                string reaons = "";

                var temp = JObject.Parse(result.httpResult.httpResponse);
                if (temp["content"].Count() > 0)
                {
                    JObject channel = (JObject)temp["content"]["cust"];
                    reaons = channel.Property("reasonACorp").Value.ToString();
                    channel.Property("reasonACorp").Remove();
                }
                string json = temp.ToString(); // backing result to json
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.Dtos.CustomerCorporateView>>(json);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    response.Content.Cust.FATCANoReasonCorp = reaons;
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        //todo fix this as account model should take accountid and status only
       
        [Route("UpdateAccountStatus")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Account>", typeof(ActiveResponseSucces<Account>))]
        public async Task<IActionResult> UpdateAccountStatus([FromBody] AccountUpdateModel Model)
        {
            ActiveResponseSucces<Account> response = new ActiveResponseSucces<Account>();


            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:UpdateAccountStatus"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<Account>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

       
        [Route("CreateAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Content>", typeof(ActiveResponseSucces<Content>))]
        public async Task<IActionResult> CreateAccount([FromBody] CreateAccount Model)
        {
            ActiveResponseSucces<Content> response = new ActiveResponseSucces<Content>();
            if (Model != null)
            {
                //transforming 
                Model.Acc.OurBranchID = Model.SignOnRq.OurBranchId;
                Model.Acc.CreateBy = Model.SignOnRq.ChannelId;
            }

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:CreateAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<Content>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    if (string.IsNullOrEmpty(response.Content.status))
                    {
                        response.Content.status = "Active";
                    }
                    else if (response.Content.status == "I")
                    {
                        response.Content.status = "InActive";
                    }
                    else if (response.Content.status == "T")
                    {
                        response.Content.status = "Dorment";
                    }
                    else if (response.Content.status == "X")
                    {
                        response.Content.status = "Deceased";
                    }
                    else if (response.Content.status == "D")
                    {
                        response.Content.status = "Blocked";
                    }
                    else if (response.Content.status == "C")
                    {
                        response.Content.status = "Closed";
                    }
                    else
                    {
                        response.Content.status = "Undefined";
                    }

                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        //todo this need to fix as account model need to have validations
        [Route("CreateCorporateAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Content>", typeof(ActiveResponseSucces<Content>))]
        public async Task<IActionResult> CreateCorporateAccount([FromBody] CreateCorporateAccount Model)
        {
            ActiveResponseSucces<Content> response = new ActiveResponseSucces<Content>();

            if (Model != null)
            {
                //transforming 
                Model.Acc.OurBranchID = Model.SignOnRq.OurBranchId;
                Model.Acc.CreateBy = Model.SignOnRq.ChannelId;
            }

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:CreateCorporateAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<Content>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    if (string.IsNullOrEmpty(response.Content.status))
                    {
                        response.Content.status = "Active";
                    }
                    else if (response.Content.status == "I")
                    {
                        response.Content.status = "InActive";
                    }
                    else if (response.Content.status == "T")
                    {
                        response.Content.status = "Dorment";
                    }
                    else if (response.Content.status == "X")
                    {
                        response.Content.status = "Deceased";
                    }
                    else if (response.Content.status == "D")
                    {
                        response.Content.status = "Blocked";
                    }
                    else if (response.Content.status == "C")
                    {
                        response.Content.status = "Closed";
                    }
                    else
                    {
                        response.Content.status = "Undefined";
                    }

                    return Ok(response);
                }
            }
            return BadRequest(response);
        }


        [Route("UpdateCorporateAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Content>", typeof(ActiveResponseSucces<Content>))]
        public async Task<IActionResult> UpdateCorporateAccount([FromBody] UpdateCorporateAccountModel Model)
        {
            ActiveResponseSucces<Content> response = new ActiveResponseSucces<Content>();

            if (Model != null)
            {
                //transforming 
                Model.Acc.OurBranchID = Model.SignOnRq.OurBranchId;
                Model.Acc.CreateBy = Model.SignOnRq.ChannelId;
            }

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:UpdateCorporateAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<Content>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    if (string.IsNullOrEmpty(response.Content.status))
                    {
                        response.Content.status = "Active";
                    }
                    else if (response.Content.status == "I")
                    {
                        response.Content.status = "InActive";
                    }
                    else if (response.Content.status == "T")
                    {
                        response.Content.status = "Dorment";
                    }
                    else if (response.Content.status == "X")
                    {
                        response.Content.status = "Deceased";
                    }
                    else if (response.Content.status == "D")
                    {
                        response.Content.status = "Blocked";
                    }
                    else if (response.Content.status == "C")
                    {
                        response.Content.status = "Closed";
                    }
                    else
                    {
                        response.Content.status = "Undefined";
                    }

                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("CIFinquiryByIdno")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CustomerInfo>", typeof(ActiveResponseSucces<CustomerInfo>))]
        public async Task<IActionResult> CIFinquiryByIdno([FromBody] Riminquiry Model)
        {
            ActiveResponseSucces<CustomerInfo> response = new ActiveResponseSucces<CustomerInfo>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:riminquirybyIdno"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CustomerInfo>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
       
        [Route("CIFinquiryViaCIF")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CustomerInfo>", typeof(ActiveResponseSucces<CustomerInfo>))]
        public async Task<IActionResult> CIFinquiryViaCIF([FromBody] Riminquiryrim Model)
        {
            ActiveResponseSucces<CustomerInfo> response = new ActiveResponseSucces<CustomerInfo>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:riminquiryrim"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CustomerInfo>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        /// <summary>
        /// Rim Inquiry.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Todo
        ///     {
        ///         "signOnRq":{ 
        ///        "LogId": "e0ccbe1a-dfdd-4753-a4c7-acc6fb0665e3",
        ///          "DateTime":"03-06-2019",
        ///          "ChannelId":"Swagger",
        ///         "OurBranchId":"079"},
        ///       
        ///        "CprNumber": "3840366576395",
        ///     }
        ///
        /// </remarks>
        /// <response code="400">If any Error Occur</response>            
        
        [ProducesResponseType(typeof(ActiveResponseSucces<CustomerInfo>), 200)]
        [ProducesResponseType(400)]
        [Route("CreateRetailCIF")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CustomerInfo>", typeof(ActiveResponseSucces<CustomerInfo>))]
        public async Task<IActionResult> CreateRetailCIF([FromBody] CreateCIF Model)
        {
            ActiveResponseSucces<CustomerInfo> response = new ActiveResponseSucces<CustomerInfo>();

            if (Model != null)
            {
                //transforming 
                Model.Cust.OurBranchID = Model.SignOnRq.OurBranchId;
                Model.Cust.CreateBy = Model.SignOnRq.ChannelId;
                Model.Cust.NIC = Model.Cust.idno;
                Model.Cust.NicExpirydate = Model.Cust.IDExpirydate;
                Model.Cust.Email = Model.Cust.customerEmail;
            }

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:CreateCif"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CustomerInfo>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
       
        [Route("CreateCorporateCIF")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CorporateInfo> ", typeof(ActiveResponseSucces<CorporateInfo>))]
        public async Task<IActionResult> CreateCorporateCIF([FromBody] DBHandler.Model.Dtos.CustomerModel Model)
        {
            ActiveResponseSucces<CorporateInfo> response = new ActiveResponseSucces<CorporateInfo>();

            if (Model != null)
            {
                Model.Cust.CreateBy = Model.SignOnRq.ChannelId;
                Model.Cust.ReasonACorp = Model.Cust.FATCANoReasonCorp;
            }

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:CreateCorporateCIF"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CorporateInfo>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        
        //[Route("dotransfer")]
        //[Authorize(AuthenticationSchemes = "BasicAuthentication")]
        //[ProducesResponseType(typeof(ActiveResponseSucces<TransferResponse>), 200)]
        //[ProducesResponseType(typeof(ActiveResponse), 400)]
        //[HttpPost]
        //[SwaggerResponse(200, "ActiveResponseSucces<TransferResponse> ", typeof(ActiveResponseSucces<TransferResponse>))]
        //public async Task<IActionResult> transfer([FromBody] TransferModel Model)
        //{
        //    ActiveResponseSucces<TransferResponse> response = new ActiveResponseSucces<TransferResponse>();
        //    InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
        //                                                                _config["BankSmart:BaseUrl"].ToString(),
        //                                                                _config["BankSmart:transfer"].ToString(),
        //                                                                level,
        //                                                                _logs,
        //                                                                _userChannels,
        //                                                                this,
        //                                                                _config,
        //                                                                httpHandler);
        //    response.Status = result.Status;
        //    if (result.Status.Code == null)
        //    {
        //        response = JsonConvert.DeserializeObject<ActiveResponseSucces<TransferResponse>>(result.httpResult.httpResponse);
        //        if (result != null && result.httpResult?.severity == "Success")
        //        {
        //            return Ok(response);
        //        }
        //    }
        //    return BadRequest(response);
        //}

        
        [Route("GetPaymentStatus")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponseSucces<TransferResponse>), 200)]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<TransferStatusResponse> ", typeof(ActiveResponseSucces<TransferStatusResponse>))]
        public async Task<IActionResult> GetPaymentStatus([FromBody] TransferStatusModel Model)
        {
            ActiveResponseSucces<TransferStatusResponse> response = new ActiveResponseSucces<TransferStatusResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:GetTransferStatus"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<TransferStatusResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        
        [Route("GetNarrations")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponseSucces<List<TransactionDescription>>), 200)]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<TransactionDescription>>", typeof(ActiveResponseSucces<List<TransactionDescription>>))]
        public async Task<IActionResult> GetNarrations([FromBody] NarrationModel Model)
        {
            ActiveResponseSucces<List<TransactionDescription>> response = new ActiveResponseSucces<List<TransactionDescription>>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:GetNarations"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<TransactionDescription>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        
        [Route("AccountInquiry")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<AccountView>>", typeof(ActiveResponseSucces<List<AccountView>>))]
        public async Task<IActionResult> AccountInquiry([FromBody] AccountInquiryRequest Model)
        {
            ActiveResponseSucces<List<AccountView>> response = new ActiveResponseSucces<List<AccountView>>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:AccountInquiry"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<AccountView>>>(result.httpResult.httpResponse);
                List<AccountView> list = new List<AccountView>();
                if (response.Content != null && response.Content.Count> 0 )
                {
                    foreach (AccountView v in response.Content)
                    {
                        if (v.Status == "")
                        {
                            v.Status = "Active";
                        }
                        else if (v.Status == "I")
                        {
                            v.Status = "InActive";
                        }
                        else if (v.Status == "T")
                        {
                            v.Status = "Dorment";
                        }
                        else if (v.Status == "X")
                        {
                            v.Status = "Deceased";
                        }
                        else if (v.Status == "D")
                        {
                            v.Status = "Blocked";
                        }
                        else if (v.Status == "C")
                        {
                            v.Status = "Closed";
                        }
                        else
                        {
                            v.Status = "Undefined";
                        }
                        list.Add(v);
                    }
                    response.Content = list;
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        
        [Route("GetCurrency")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Currencies>", typeof(ActiveResponseSucces<Currencies>))]
        public async Task<IActionResult> GetCurrency([FromBody] CurrenciesInquiryRequest Model)
        {
            DateTime starTime = DateTime.Now;
            IActionResult Result = null;
            ActiveResponseSucces<Currencies> ResponseSuccess = new ActiveResponseSucces<Currencies>();
            DBHandler.Helper.APIHelper helper = new DBHandler.Helper.APIHelper(_userChannels, _logs);
            HttpContext ctx = Request.HttpContext;
            var id = HttpContext.User.Claims.First().Value;

            try
            {
                bool isModelValid = TryValidateModel(Model);
                if (!isModelValid || !helper.ValidateChannel(id, Model.SignOnRq.ChannelId, ctx))
                {
                    if (!isModelValid)
                    {
                        var errors = ModelState
                       .Where(x => x.Value.Errors.Count > 0)
                        .Select(x => new { x.Key, x.Value.Errors })
                        .ToArray();
                        ResponseSuccess.Status = new Status { Severity = Severity.Error, StatusMessage = errors[0].Key.ToUpper() + ":" + errors[0].Errors[0].ErrorMessage, Code = "ERROR-01" };
                    }
                    else
                    {
                        ResponseSuccess.Status = new Status { Severity = Severity.Error, StatusMessage = _config["Errors:InvalidChannel"].ToString(), Code = "ERROR-01" };
                    }

                    level = Level.Error;
                    excetionForLog = new Exception(ResponseSuccess.Status.StatusMessage);
                    Result = BadRequest(ResponseSuccess);
                }
                else
                {
                    var cur = _userChannels.GetCurrencies(Model.Currency);

                    if (cur != null)
                    {
                        ResponseSuccess.Content = cur;
                        Result = Ok(ResponseSuccess);
                        ResponseSuccess.Status = new Status { Severity = Severity.Success, StatusMessage = "Success", Code = "MSG-000000" };
                    }
                    else
                    {
                        Result = BadRequest(ResponseSuccess);
                        ResponseSuccess.Status = new Status { Severity = Severity.Error, StatusMessage = "No Currency found", Code = "ERROR-0101" };
                    }
                }
            }
            catch (Exception ex)
            {
                level = Level.Error;
                //ResponseSuccess.Status.Code =Status.ACTVETErroCode.Failed;
                ResponseSuccess.Content = null;
                ResponseSuccess.Status = new Status { Severity = Severity.Exception, StatusMessage = "Please contact Administrator to know more about this issues.", Code = "ERROR-03" };
                excetionForLog = ex;
                Result = BadRequest(ResponseSuccess);
            }

            ResponseSuccess.LogId = (Model == null || Model.SignOnRq == null) ? "" : Model.SignOnRq.LogId;
            ResponseSuccess.RequestDateTime = (Model == null || Model.SignOnRq == null) ? DateTime.Now : Model.SignOnRq.DateTime;
            string channelID = (Model == null || Model.SignOnRq == null) ? "" : Model.SignOnRq.ChannelId;

            //var userID = HttpContext.User.Claims.First().Value;
            var userID = id;
            string res = JsonConvert.SerializeObject(Model);
            string req = JsonConvert.SerializeObject(Result);
            DateTime Endtime = DateTime.Now;
            HttpContext Ctx = Request.HttpContext;
            _logs.Logs(level, ResponseSuccess.Status != null ? ResponseSuccess.Status.Code.ToString() : "", Ctx, _config, excetionForLog, starTime, Endtime, res, req, userID, channelID, ResponseSuccess.LogId, ResponseSuccess.RequestDateTime);
            return Result;
        }

        
        [Route("UpdateAccountMandate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateAccountMandate([FromBody] AccountMandateModel Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:AddUpdateAccountMandate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
       
        [Route("AddAccountMandate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> AddAccountMandate([FromBody] AccountMandateModel Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:AddUpdateAccountMandate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        
        [Route("GetShareHolders")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<ShareHolder>>", typeof(ActiveResponseSucces<List<ShareHolder>>))]
        public async Task<IActionResult> GetShareHolders([FromBody] CIFDetailInquiry Model)
        {
            ActiveResponseSucces<List<ShareHolder>> response = new ActiveResponseSucces<List<ShareHolder>>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:GetShareHolders"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<ShareHolder>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        
        [Route("GetAuthorisedSignatories")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<AuthorisedSignatoriesView>>", typeof(ActiveResponseSucces<List<AuthorisedSignatoriesView>>))]
        public async Task<IActionResult> GetAuthorisedSignatories([FromBody] CIFDetailInquiry Model)
        {
            ActiveResponseSucces<List<AuthorisedSignatoriesView>> response = new ActiveResponseSucces<List<AuthorisedSignatoriesView>>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:GetAuthorisedSignatories"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<AuthorisedSignatoriesView>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        
        [Route("GetBeneficialOwners")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<BeneficialOwners>>", typeof(ActiveResponseSucces<List<BeneficialOwners>>))]
        public async Task<IActionResult> GetBeneficialOwners([FromBody] CIFDetailInquiry Model)
        {
            ActiveResponseSucces<List<BeneficialOwners>> response = new ActiveResponseSucces<List<BeneficialOwners>>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:GetBeneficialOwners"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<BeneficialOwners>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        
        [Route("GetBoardDirectors")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<BoardDirectors>>", typeof(ActiveResponseSucces<List<BoardDirectors>>))]
        public async Task<IActionResult> GetBoardDirectors([FromBody] CIFDetailInquiry Model)
        {
            ActiveResponseSucces<List<BoardDirectors>> response = new ActiveResponseSucces<List<BoardDirectors>>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:GetBoardDirectors"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<BoardDirectors>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        
        [Route("DeleteShareHolders")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> DeleteShareHolders([FromBody] DeleteRequestForClientDetail Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:DeleteShareHolders"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        
        [Route("DeleteAuthSignatory")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> DeleteAuthSignatory([FromBody] DeleteRequestForClientDetail Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:DeleteAuthSignatory"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        
        [Route("DeleteBeneficialOwners")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> DeleteBeneficialOwners([FromBody] DeleteRequestForClientDetail Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:DeleteBeneficialOwners"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        
        [Route("DeleteBoardDirectors")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> DeleteBoardDirectors([FromBody] DeleteRequestForClientDetail Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:DeleteBoardDirectors"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        
        [Route("GetSignature")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<SignatureModel>", typeof(ActiveResponseSucces<SignatureModel>))]
        public async Task<IActionResult> GetSignature([FromBody] DeleteRequestForClientDetail Model)
        {
            ActiveResponseSucces<SignatureModel> response = new ActiveResponseSucces<SignatureModel>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:GetSignature"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<SignatureModel>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
       
        [Route("UpdateGroupAndSignature")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateGroupAndSignature([FromBody] UpdateSignatureModel Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:UpdateGroupAndSignature"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        
        [Route("AddShareHolder")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> AddShareHolder([FromBody] CIFShareHolderDetiilDTO Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:AddUpdateShareHolder"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
       
        [Route("AddAuthorisedSignatories")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> AddAuthorisedSignatories([FromBody] CIFAuthorisedSignatoriesDTO Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:AddUpdateAuthorisedSignatories"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
       
        [Route("AddBeneficialOwners")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> AddBeneficialOwners([FromBody] CIFBeneficialOwnersDTO Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:AddUpdateBeneficialOwners"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        
        [Route("AddBoardDirectors")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> AddBoardDirectors([FromBody] CIFBoardDirectorsDTO Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:AddUpdateBoardDirectors"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }


        
        [Route("UpdateShareHolder")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateShareHolder([FromBody] CIFShareHolderDetiilDTO Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:AddUpdateShareHolder"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
       
        [Route("UpdateAuthorisedSignatories")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateAuthorisedSignatories([FromBody] CIFAuthorisedSignatoriesDTO Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:AddUpdateAuthorisedSignatories"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
       
        [Route("UpdateBeneficialOwners")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateBeneficialOwners([FromBody] CIFBeneficialOwnersDTO Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:AddUpdateBeneficialOwners"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        
        [Route("UpdateBoardDirectors")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateBoardDirectors([FromBody] CIFBoardDirectorsDTO Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:AddUpdateBoardDirectors"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

       
        [Route("UpdateRetailClient")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateRetailClient([FromBody] UpdateCIF Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            if (Model != null)
            {
                //transforming 
                Model.Cust.OurBranchID = Model.SignOnRq.OurBranchId;
                Model.Cust.CreateBy = Model.SignOnRq.ChannelId;
                Model.Cust.Email = Model.Cust.customerEmail;
                Model.Cust.NicExpirydate = Model.Cust.IDExpirydate;
            }

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:UpdateCIF"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        
        [Route("UpdateCorporateClient")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateCorporateClient([FromBody] UpdateCorporateCIF Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            if (Model != null)
            {
                //transforming 
                Model.Cust.OurBranchID = Model.SignOnRq.OurBranchId;
                Model.Cust.CreateBy = Model.SignOnRq.ChannelId;
            }
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:UpdateCorporateClient"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }
        
        [Route("GetRetailClient")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<InsertCustomerRetail>", typeof(ActiveResponseSucces<InsertCustomerRetail>))]
        public async Task<IActionResult> GetRetailClient([FromBody] RetailCustomerInquiryModel Model)
        {
            ActiveResponseSucces<InsertCustomerRetail> response = new ActiveResponseSucces<InsertCustomerRetail>();

            ActiveResponseSucces<InsertCustomerRetailView> res = new ActiveResponseSucces<InsertCustomerRetailView>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:GetRetailClient"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<InsertCustomerRetail>>(result.httpResult.httpResponse);
                res = JsonConvert.DeserializeObject<ActiveResponseSucces<InsertCustomerRetailView>>(result.httpResult.httpResponse);
                res.Content.idno = response.Content.NIC;
                res.Content.IDExpirydate = response.Content.NicExpirydate;
                res.Content.customerEmail = response.Content.Email;

                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(res);
                }
            }
            return BadRequest(response);
        }

        //[ApiExplorerSettings(IgnoreApi = true)]
        [Route("GetAccountDetail")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<AccountViewDetail>", typeof(ActiveResponseSucces<AccountViewDetail>))]
        public async Task<IActionResult> GetAccountDetail([FromBody] BalanceIBANInquiryModel Model)
        {
            ActiveResponseSucces<AccountViewDetail> response = new ActiveResponseSucces<AccountViewDetail>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:GetAccountDetail"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<AccountViewDetail>>(result.httpResult.httpResponse);

                if (response.Content != null)
                {
                    if (string.IsNullOrEmpty(response.Content.status))
                    {
                        response.Content.status = "Active";
                    }
                    else if (response.Content.status == "I")
                    {
                        response.Content.status = "InActive";
                    }
                    else if (response.Content.status == "T")
                    {
                        response.Content.status = "Dorment";
                    }
                    else if (response.Content.status == "X")
                    {
                        response.Content.status = "Deceased";
                    }
                    else if (response.Content.status == "D")
                    {
                        response.Content.status = "Blocked";
                    }
                    else if (response.Content.status == "C")
                    {
                        response.Content.status = "Closed";
                    }
                    else
                    {
                        response.Content.status = "Undefined";
                    }
                }

                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }


        [Route("GetCIFAccountsBalanceInq")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<Balance>>", typeof(ActiveResponseSucces<List<Balance>>))]
        public async Task<IActionResult> GetCIFAccountsBalanceInq([FromBody] RetailCustomerInquiryModel Model)
        {
            ActiveResponseSucces<List<Balance>> response = new ActiveResponseSucces<List<Balance>>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:GetCIFAccountsBalanceInq"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<Balance>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("CloseAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<Balance>>", typeof(ActiveResponseSucces<List<Balance>>))]
        public async Task<IActionResult> CloseAccount([FromBody] BalanceInquiryModel Model)
        {
            ActiveResponseSucces<List<Balance>> response = new ActiveResponseSucces<List<Balance>>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:CloseAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<Balance>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("UpdateRetailAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateRetailAccount([FromBody] UpdateAccountModel Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            if (Model != null)
            {
                Model.Acc.OurBranchID = Model.SignOnRq.OurBranchId;
                Model.Acc.CreateBy = Model.SignOnRq.ChannelId;
            }

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:UpdateRetailAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }
       
        [Route("FreezeAccountAmountInquiryRetail")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List< AccountFreeze>>", typeof(ActiveResponseSucces<List<AccountFreeze>>))]
        public async Task<IActionResult> FreezeAccountAmountInquiryRetail([FromBody] BalanceInquiryModel Model)
        {
            ActiveResponseSucces<List< AccountFreeze>> response = new ActiveResponseSucces<List<AccountFreeze>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:FreezeAccountInquiryRetail"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<AccountFreeze>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        
        [Route("FreezeAccountAmountInquiryCorporate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<AccountFreeze>>", typeof(ActiveResponseSucces<List<AccountFreeze>>))]
        public async Task<IActionResult> FreezeAccountAmountInquiryCorporate([FromBody] BalanceInquiryModel Model)
        {
            ActiveResponseSucces<List<AccountFreeze>> response = new ActiveResponseSucces<List<AccountFreeze>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:FreezeAccountInquiryCorporate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<AccountFreeze>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

       
        [Route("FreezeAccountAmountRetail")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> FreezeAccountAmountRetail([FromBody] FreezeAccountModel Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:FreezeAccountRetail"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }
       
        [Route("FreezeAccountAmountCorporate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> FreezeAccountAmountCorporate([FromBody] FreezeAccountModel Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:FreezeAccountCorporate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        
        [Route("UnFreezeAccountAmountRetail")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UnFreezeAccountAmountRetail([FromBody] UpdateAccountFreezeModel Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:UnFreezeAccountRetail"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }


        
        [Route("UnFreezeAccountAmountCorporate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UnFreezeAccountAmountCorporate([FromBody] UpdateAccountFreezeModel Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:UnFreezeAccountCorporate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetTransactionListRetail")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<TransactionView>>", typeof(ActiveResponseSucces<List<TransactionView>>))]
        public async Task<IActionResult> GetTransactionListRetail([FromBody] GetTransactionModel Model)
        {
            ActiveResponseSucces<List<TransactionView>> response = new ActiveResponseSucces<List<TransactionView>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:GetTransactionListRetail"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<TransactionView>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetTransactionListCorporate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<TransactionView>>", typeof(ActiveResponseSucces<List<TransactionView>>))]
        public async Task<IActionResult> GetTransactionListCorporate([FromBody] GetTransactionModel Model)
        {
            ActiveResponseSucces<List<TransactionView>> response = new ActiveResponseSucces<List<TransactionView>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:GetTransactionListCorporate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<TransactionView>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }


        [Route("LinkDebitCardStatusUpdate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> LinkDebitCardStatusUpdate([FromBody] LinkDebitCardStatusUpdateView Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:LinkDebitCardStatusUpdate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }
      
        [Route("LinkDebitCard")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> LinkDebitCard([FromBody] LinkDebitCardStatusInsertView Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:LinkDebitCard"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }
       
        [Route("DebitCardInquiry")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DebitCards>>", typeof(ActiveResponseSucces<List<DebitCards>>))]
        public async Task<IActionResult> DebitCardInquiry([FromBody] DebitCardInquiryView Model)
        {
            ActiveResponseSucces<List<DebitCards>> response = new ActiveResponseSucces<List<DebitCards>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:DebitCardInquiry"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<DebitCards>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        
        [Route("DebitCardCategoryInquiry")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DebitCardCategory>>", typeof(ActiveResponseSucces<List<DebitCardCategory>>))]
        public async Task<IActionResult> DebitCardCategoryInquiry([FromBody] DebitCardCategoryInquiryView Model)
        {
            ActiveResponseSucces<List<DebitCardCategory>> response = new ActiveResponseSucces<List<DebitCardCategory>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:DebitCardCategoryInquiry"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<DebitCardCategory>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

       
        [Route("CurrencyPairsinquiry")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<CurrencyPairing>>", typeof(ActiveResponseSucces<List<CurrencyPairing>>))]
        public async Task<IActionResult> CurrencyPairsinquiry([FromBody] DebitCardCategoryInquiryView Model)
        {
            ActiveResponseSucces<List<CurrencyPairing>> response = new ActiveResponseSucces<List<CurrencyPairing>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:CurrencyPairsinquiry"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<CurrencyPairing>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }
        
        [Route("GetStatement")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<CustomerStatement>>", typeof(ActiveResponseSucces<List<CustomerStatement>>))]
        public async Task<IActionResult> GetStatement([FromBody] GetStatementView Model)
        {
            ActiveResponseSucces<List<CustomerStatement>> response = new ActiveResponseSucces<List<CustomerStatement>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:GetStatement"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<CustomerStatement>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("AddRealTimeSweeps")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> AddRealTimeSweeps([FromBody] RealTimeSweepView1 Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:AddRealTimeSweeps"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("ModifyRealTimeSweeps")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> ModifyRealTimeSweeps([FromBody] RealTimeSweepView1 Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:ModifyRealTimeSweeps"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("CancelRealTimeSweeps")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> CancelRealTimeSweeps([FromBody] CancelRealTimeSweepView Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:CancelRealTimeSweeps"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetRealTimeSweep")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<RealTimeSweepResponse1>))]
        public async Task<IActionResult> GetRealTimeSweep([FromBody] CancelRealTimeSweepView Model)
        {
            ActiveResponseSucces<RealTimeSweepResponse1> response = new ActiveResponseSucces<RealTimeSweepResponse1>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:GetRealTimeSweep"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<RealTimeSweepResponse1>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }


        [Route("AddInternalAccountExcessSweeps")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> AddInternalAccountExcessSweeps([FromBody] InternalSweepView Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:AddInternalAccountExcessSweeps"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("ModifyInternalAccountExcessSweeps")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> ModifyInternalAccountExcessSweeps([FromBody] InternalSweepView Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:ModifyInternalAccountExcessSweeps"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("CancelInternalAccountExcessSweeps")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> CancelInternalAccountExcessSweeps([FromBody] CancelRealTimeSweepView Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:CancelInternalAccountExcessSweeps"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetInternalAccountExcessSweeps")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<RealTimeSweepResponse>))]
        public async Task<IActionResult> GetInternalAccountExcessSweeps([FromBody] CancelRealTimeSweepView Model)
        {
            ActiveResponseSucces<RealTimeSweepResponse> response = new ActiveResponseSucces<RealTimeSweepResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:GetInternalAccountExcessSweeps"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<RealTimeSweepResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }


        [Route("AddInternalAccountShortageSweeps")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> AddInternalAccountShortageSweeps([FromBody] InternalSweepView Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:AddInternalAccountShortageSweeps"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("ModifyInternalAccountShortageSweeps")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> ModifyInternalAccountShortageSweeps([FromBody] InternalSweepView Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:ModifyInternalAccountShortageSweeps"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("CancelInternalAccountShortageSweeps")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> CancelInternalAccountShortageSweeps([FromBody] CancelRealTimeSweepView Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:CancelInternalAccountShortageSweeps"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetInternalAccountShortageSweeps")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<RealTimeSweepResponse>))]
        public async Task<IActionResult> GetInternalAccountShortageSweeps([FromBody] CancelRealTimeSweepView Model)
        {
            ActiveResponseSucces<RealTimeSweepResponse> response = new ActiveResponseSucces<RealTimeSweepResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:GetInternalAccountShortageSweeps"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<RealTimeSweepResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("AddMandate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Dictionary<string, string>>", typeof(ActiveResponseSucces<Dictionary<string, string>>))]
        public async Task<IActionResult> AddMandate([FromBody] DDMandateInsertView Model)
        {
            ActiveResponseSucces<Dictionary<string, string>> response = new ActiveResponseSucces<Dictionary<string, string>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:AddMandate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<Dictionary<string, string>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetAllMandate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DDMandateView>>", typeof(ActiveResponseSucces<List<DDMandateView>>))]
        public async Task<IActionResult> GetAllMandate([FromBody] BalanceInquiryModel Model)
        {
            ActiveResponseSucces<List<DDMandateView>> response = new ActiveResponseSucces<List<DDMandateView>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:GetAllMandate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<DDMandateView>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("CancelMandate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> CancelMandate([FromBody] DDMandateCancelView Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:CancelMandate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetMandate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "Dictionary<string, string>", typeof(ActiveResponseSucces<Dictionary<string, string>>))]
        public async Task<IActionResult> GetMandate([FromBody] DDMandateCancelView Model)
        {
            ActiveResponseSucces<Dictionary<string, string>> response = new ActiveResponseSucces<Dictionary<string, string>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:GetMandate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<Dictionary<string, string>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetCorporateAccountDetail")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<AccountCorporateView>", typeof(ActiveResponseSucces<AccountCorporateView>))]
        public async Task<IActionResult> GetCorporateAccountDetail([FromBody] BalanceIBANInquiryModel Model)
        {
            ActiveResponseSucces<AccountCorporateView> response = new ActiveResponseSucces<AccountCorporateView>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:GetCorporateAccountDetail"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                try
                {
                    response = JsonConvert.DeserializeObject<ActiveResponseSucces<AccountCorporateView>>(result.httpResult.httpResponse);
                }
                catch (Exception ex)
                {
                    int i = 0;
                }
                    if (result != null && result.httpResult?.severity == "Success")
                {

                    if (string.IsNullOrEmpty(response.Content.status))
                    {
                        response.Content.status = "Active";
                    }
                    else if (response.Content.status == "I")
                    {
                        response.Content.status = "InActive";
                    }
                    else if (response.Content.status == "T")
                    {
                        response.Content.status = "Dorment";
                    }
                    else if (response.Content.status == "X")
                    {
                        response.Content.status = "Deceased";
                    }
                    else if (response.Content.status == "D")
                    {
                        response.Content.status = "Blocked";
                    }
                    else if (response.Content.status == "C")
                    {
                        response.Content.status = "Closed";
                    }
                    else
                    {
                        response.Content.status = "Undefined";
                    }
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("IssuanceOfChequeBooks")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ChequeResponseModel>", typeof(ActiveResponseSucces<ChequeResponseModel>))]
        public async Task<IActionResult> IssuanceOfChequeBooks([FromBody] ChequeModel Model)
        {
            ActiveResponseSucces<ChequeResponseModel> response = new ActiveResponseSucces<ChequeResponseModel>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:IssuanceOfChequeBooks"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<ChequeResponseModel>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetChequeBookType")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ChequeResponseModel>", typeof(ActiveResponseSucces<ChqBookTypes>))]
        public async Task<IActionResult> GetChequeBookType([FromBody] ChequeTypeModel Model)
        {
            ActiveResponseSucces<ChqBookTypes> response = new ActiveResponseSucces<ChqBookTypes>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:GetChequeBookType"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<ChqBookTypes>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("InquiryNOSTRO")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "string", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> InquiryNOSTRO([FromBody] InquiryNOSTROModel Model)
        {
            ActiveResponseSucces<List<NostroAccounts>> response = new ActiveResponseSucces<List<NostroAccounts>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:InquiryNOSTRO"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<NostroAccounts>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("UpdateCharity")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "string", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateCharity([FromBody] CharityModel Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:UpdateCharity"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetFixDepositProducts")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<ProductView>>", typeof(ActiveResponseSucces<List<ProductView>>))]
        public async Task<IActionResult> GetFixDepositProducts([FromBody] SignOnRq SignOnRq)
        {
            ActiveResponseSucces<List<ProductView>> response = new ActiveResponseSucces<List<ProductView>>();

            InternalResult result = await APIRequestHandler.GetResponse(SignOnRq == null ? null : SignOnRq, SignOnRq,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:GetFixDepositProducts"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<ProductView>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetDepositRates")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DepositRatesDTO>", typeof(ActiveResponseSucces<DepositRatesDTO>))]
        public async Task<IActionResult> GetDepositRates([FromBody] DepositRatesModel Model)
        {
            ActiveResponseSucces<DepositRatesDTO> response = new ActiveResponseSucces<DepositRatesDTO>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:GetDepositRates"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DepositRatesDTO>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }


        [Route("OpenFDAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "string", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> OpenFDAccount([FromBody] OpenFDAccountModel Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:OpenFDAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("OpenFixDeposit")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<IssueFixDepositResponse>", typeof(ActiveResponseSucces<IssueFixDepositResponse>))]
        public async Task<IActionResult> OpenFixDeposit([FromBody] OpenFixDepositModel Model)
        {
            ActiveResponseSucces<IssueFixDepositResponse> response = new ActiveResponseSucces<IssueFixDepositResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:OpenFixDeposit"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<IssueFixDepositResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("DepositsListAccountwise")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<TDRIssuance>>", typeof(ActiveResponseSucces<List<TDRIssuance>>))]
        public async Task<IActionResult> DepositsListAccountwise([FromBody] DepositsListAccountwiseModel Model)
        {
            ActiveResponseSucces<List<TDRIssuance>> response = new ActiveResponseSucces<List<TDRIssuance>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:DepositsListAccountwise"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<TDRIssuance>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }


        [Route("DepositsListClientWise")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<TDRIssuance>>", typeof(ActiveResponseSucces<List<TDRIssuance>>))]
        public async Task<IActionResult> DepositsListClientWise([FromBody] DepositsListClientWiseModel Model)
        {
            ActiveResponseSucces<List<TDRIssuance>> response = new ActiveResponseSucces<List<TDRIssuance>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:DepositsListClientWise"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<TDRIssuance>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("LienUnlienMarking")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> LienUnlienMarking([FromBody] LienUnlienMarkingModel Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:LienUnlienMarking"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }


        [Route("DepositsDetails")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<TDRIssuanceModel>))]
        public async Task<IActionResult> DepositsDetails([FromBody] DepositsDetailsModel Model)
        {
            ActiveResponseSucces<TDRIssuanceModel> response = new ActiveResponseSucces<TDRIssuanceModel>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:DepositsDetails"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<TDRIssuanceModel>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetAccrualRates")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> GetAccrualRates([FromBody] AccrualRatesModel Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:GetAccrualRates"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetDailyAccrual")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<RptSavAccruals>", typeof(ActiveResponseSucces<List<RptSavAccruals>>))]
        public async Task<IActionResult> GetDailyAccrual([FromBody] AccrualRatesModel Model)
        {
            ActiveResponseSucces<List<RptSavAccruals>> response = new ActiveResponseSucces<List<RptSavAccruals>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:GettDailyAccrual"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<RptSavAccruals>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    List<RptSavAccruals> newlist = new List<RptSavAccruals>();
                    List<RptSavAccruals> list = response.Content;
                    foreach(RptSavAccruals l in list )
                    {
                        if (l.Days == 1)
                        {
                            newlist.Add(l);
                        }
                        else
                        {
                            decimal amount = l.Amount / l.Days;
                            for (int i = 0; i < l.Days; i++)
                            {
                                RptSavAccruals r = new RptSavAccruals();
                                r.AccountID = l.AccountID;
                                r.AccrualDate = l.AccrualDate;
                                r.Amount = amount;
                                r.Balance = l.Balance;
                                r.CurrencyID = l.CurrencyID;
                                r.Days = l.Days;
                                r.Rate = l.Rate;
                                r.ValueDate = l.ValueDate.AddDays(i);
                                newlist.Add(r);
                            }
                        }
                    }
                    response.Content = newlist;
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("CalculateMaturityDate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<MaturityValues>", typeof(ActiveResponseSucces<MaturityValues>))]
        public async Task<IActionResult> CalculateMaturityDate([FromBody] CalculateMaturityDateModel Model)
        {
            ActiveResponseSucces<MaturityValues> response = new ActiveResponseSucces<MaturityValues>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:CalculateMaturityDate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<MaturityValues>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("DepositsList")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<TDRIssuance>>", typeof(ActiveResponseSucces<List<TDRIssuance>>))]
        public async Task<IActionResult> DepositsList([FromBody] DepositsListModel Model)
        {
            ActiveResponseSucces<List<TDRIssuance>> response = new ActiveResponseSucces<List<TDRIssuance>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:DepositsList"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<TDRIssuance>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("CloseFixedDeposit")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CloseFXResponse>", typeof(ActiveResponseSucces<CloseFXResponse>))]
        public async Task<IActionResult> CloseFixedDeposit([FromBody] CloseFixedDepositModel Model)
        {
            ActiveResponseSucces<CloseFXResponse> response = new ActiveResponseSucces<CloseFXResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:CloseFixedDeposit"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CloseFXResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("FDPartialWithdrawal")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<FDPartialWithdrawalResponse>", typeof(ActiveResponseSucces<FDPartialWithdrawalResponse>))]
        public async Task<IActionResult> FDPartialWithdrawal([FromBody] FDPartialWithdrawalModel Model)
        {
            ActiveResponseSucces<FDPartialWithdrawalResponse> response = new ActiveResponseSucces<FDPartialWithdrawalResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:FDPartialWithdrawal"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<FDPartialWithdrawalResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("AddCustomerCardData")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> AddCustomerCardData([FromBody] CustomerCardDataModel Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:AddCustomerCardData"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("AddCountryLimits")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> AddCountryLimits([FromBody] CountryLimitsModel Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:AddCountryLimits"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }


        [Route("UpdateCountryLimits")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateCountryLimits([FromBody] CountryLimitsModel Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:UpdateCountryLimits"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }


        [Route("CountryLimitInquire")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<CountryLimits>))]
        public async Task<IActionResult> CountryLimitInquire([FromBody] InquirCountryLimitsModel Model)
        {
            ActiveResponseSucces<CountryLimits> response = new ActiveResponseSucces<CountryLimits>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:CountryLimitInquire"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CountryLimits>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetVirtualAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<List<VAResponse>>))]
        public async Task<IActionResult> GetVirtualAccount([FromBody] GetVirtualAccountModel Model)
        {
            ActiveResponseSucces<List<VAResponse>> response = new ActiveResponseSucces<List<VAResponse>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:GetVirtualAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<VAResponse>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }


        [Route("CreateVirtualAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<VirtualAccountResponse>))]
        public async Task<IActionResult> CreateVirtualAccount([FromBody] VirtualAccountModel Model)
        {
            ActiveResponseSucces<VirtualAccountResponse> response = new ActiveResponseSucces<VirtualAccountResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:CreateVirtualAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<VirtualAccountResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }


        [Route("ModifyVirtualAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> ModifyVirtualAccount([FromBody] ModifyVirtualAccountModel Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:ModifyVirtualAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }


        [Route("GetTotalCharity")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<GetCharity>", typeof(ActiveResponseSucces<GetCharity>))]
        public async Task<IActionResult> GetTotalCharity([FromBody] GetTotalCharityDto Model)
        {
            ActiveResponseSucces<GetCharity> response = new ActiveResponseSucces<GetCharity>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:GetTotalCharity"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<GetCharity>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetPenaltyMatrix")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<PenaltyMatrix>>", typeof(ActiveResponseSucces<List<PenaltyMatrix>>))]
        public async Task<IActionResult> GetPenaltyMatrix([FromBody] PenaltyMatrixModel Model)
        {
            ActiveResponseSucces<List<PenaltyMatrix>> response = new ActiveResponseSucces<List<PenaltyMatrix>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:GetPenaltyMatrix"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<PenaltyMatrix>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }


        [Route("GetRelationshipManagers")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<RelationshipOfficer>>", typeof(ActiveResponseSucces<List<RelationshipOfficer>>))]
        public async Task<IActionResult> GetRelationshipManagers([FromBody] SignOnRq Model)
        {
            ActiveResponseSucces<List<RelationshipOfficer>> response = new ActiveResponseSucces<List<RelationshipOfficer>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model, Model,
                                                                        _config["BankSmart:BaseUrl"].ToString(),
                                                                        _config["BankSmart:GetRelationshipManagers"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            response.LogId = Model.LogId;
            response.RequestDateTime = Model.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<RelationshipOfficer>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        #endregion

        #region Ramiz Functions

        [Route("transfer")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Account>", typeof(ActiveResponseSucces<TransferRs>))]
        public async Task<IActionResult> DoTransfer([FromBody] TransferRq Model)
        {
            ActiveResponseSucces<TransferRs> response = new ActiveResponseSucces<TransferRs>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmartLimit:BaseUrl"].ToString(),
                                                                        _config["BankSmartLimit:DoTransfer"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            response.Content = new TransferRs() { ChannelRefId = Model.ChannelRefId };
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<TransferRs>>(result.httpResult.httpResponse);
                if(response.Content == null )
                {
                    response.Content = new TransferRs() { ChannelRefId = Model.ChannelRefId };
                }
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("CollateralInquiry")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Account>", typeof(ActiveResponseSucces<InquireCollateralOut>))]
        public async Task<IActionResult> CollateralInquiry([FromBody] CollateralInquiry Model)
        {
            ActiveResponseSucces<InquireCollateralOut> response = new ActiveResponseSucces<InquireCollateralOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmartLimit:BaseUrl"].ToString(),
                                                                        _config["BankSmartLimit:CollateralInquiry"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<InquireCollateralOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("CollateralSearch")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Account>", typeof(ActiveResponseSucces<dynamic>))]
        public async Task<IActionResult> CollateralSearch([FromBody] SearchCollateral Model)
        {
            ActiveResponseSucces<dynamic> response = new ActiveResponseSucces<dynamic>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmartLimit:BaseUrl"].ToString(),
                                                                        _config["BankSmartLimit:CollateralSearch"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<dynamic>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("CollateralCreate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Account>", typeof(ActiveResponseSucces<CreateCollateralOut>))]
        public async Task<IActionResult> CollateralCreate([FromBody] CreateCollateralDto Model)
        {
            ActiveResponseSucces<CreateCollateralOut> response = new ActiveResponseSucces<CreateCollateralOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmartLimit:BaseUrl"].ToString(),
                                                                        _config["BankSmartLimit:CollateralCreate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CreateCollateralOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("CollateralUpdate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Account>", typeof(ActiveResponseSucces<UpdateCollateralOut>))]
        public async Task<IActionResult> CollateralUpdate([FromBody] UpdateCollateralDto Model)
        {
            ActiveResponseSucces<UpdateCollateralOut> response = new ActiveResponseSucces<UpdateCollateralOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmartLimit:BaseUrl"].ToString(),
                                                                        _config["BankSmartLimit:CollateralUpdate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<UpdateCollateralOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        //cif
        [Route("CIFExists")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Account>", typeof(ActiveResponseSucces<dynamic>))]
        public async Task<IActionResult> CIFExists([FromBody] CIFExist Model)
        {
            ActiveResponseSucces<dynamic> response = new ActiveResponseSucces<dynamic>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmartLimit:BaseUrl"].ToString(),
                                                                        _config["BankSmartLimit:CIFExists"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<dynamic>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("CIFSearch")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Account>", typeof(ActiveResponseSucces<dynamic>))]
        public async Task<IActionResult> CIFSearch([FromBody] SearchCIF Model)
        {
            ActiveResponseSucces<dynamic> response = new ActiveResponseSucces<dynamic>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmartLimit:BaseUrl"].ToString(),
                                                                        _config["BankSmartLimit:CIFSearch"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<dynamic>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("CIFExposure")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Account>", typeof(ActiveResponseSucces<CIFExposure>))]
        public async Task<IActionResult> CIFExposure([FromBody] CIFExist Model)
        {
            ActiveResponseSucces<List<CIFExposure>> response = new ActiveResponseSucces<List<CIFExposure>>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmartLimit:BaseUrl"].ToString(),
                                                                        _config["BankSmartLimit:CIFExposure"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<CIFExposure>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("CIFHierarchy")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Account>", typeof(ActiveResponseSucces<CIFHierarchy>))]
        public async Task<IActionResult> CIFHierarchy([FromBody] CIFExist Model)
        {
            ActiveResponseSucces<List<CIFHierarchy>> response = new ActiveResponseSucces<List<CIFHierarchy>>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmartLimit:BaseUrl"].ToString(),
                                                                        _config["BankSmartLimit:CIFHierarchy"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<CIFHierarchy>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        //Account Limit
        [Route("AccountLimitInquiry")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Account>", typeof(ActiveResponseSucces<FacilityAccInquiryOut>))]
        public async Task<IActionResult> FacilityAccInquiry([FromBody] FacilityAccInquiry Model)
        {
            ActiveResponseSucces<FacilityAccInquiryOut> response = new ActiveResponseSucces<FacilityAccInquiryOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmartLimit:BaseUrl"].ToString(),
                                                                        _config["BankSmartLimit:FacilityAccInquiry"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<FacilityAccInquiryOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("AccountLimitSearch")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Account>", typeof(ActiveResponseSucces<SearchAccountLimitOut>))]
        public async Task<IActionResult> FacilityAccSearch([FromBody] SearchAccountLimitDto Model)
        {
            ActiveResponseSucces<SearchAccountLimitOut> response = new ActiveResponseSucces<SearchAccountLimitOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmartLimit:BaseUrl"].ToString(),
                                                                        _config["BankSmartLimit:FacilityAccSearch"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<SearchAccountLimitOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("AccountLimitCreate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Account>", typeof(ActiveResponseSucces<CreateAccountLimitOut>))]
        public async Task<IActionResult> FacilityAccCreate([FromBody] CreateAccountLimitDto Model)
        {
            ActiveResponseSucces<CreateAccountLimitOut> response = new ActiveResponseSucces<CreateAccountLimitOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmartLimit:BaseUrl"].ToString(),
                                                                        _config["BankSmartLimit:FacilityAccCreate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CreateAccountLimitOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("AccountLimitModify")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Account>", typeof(ActiveResponseSucces<CreateAccountLimitOut>))]
        public async Task<IActionResult> FacilityAccModify([FromBody] ModifyAccountLimitDto Model)
        {
            ActiveResponseSucces<CreateAccountLimitOut> response = new ActiveResponseSucces<CreateAccountLimitOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmartLimit:BaseUrl"].ToString(),
                                                                        _config["BankSmartLimit:FacilityAccModify"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CreateAccountLimitOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("AccountLimitCancel")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Account>", typeof(ActiveResponseSucces<CancelAccountLimitOut>))]
        public async Task<IActionResult> FacilityAccCancel([FromBody] CancelAccountLimitDto Model)
        {
            ActiveResponseSucces<CancelAccountLimitOut> response = new ActiveResponseSucces<CancelAccountLimitOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmartLimit:BaseUrl"].ToString(),
                                                                        _config["BankSmartLimit:FacilityAccCancel"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CancelAccountLimitOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        //Facility
        [Route("FacilityInquiry")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Account>", typeof(ActiveResponseSucces<FacilityOut>))]
        public async Task<IActionResult> FacilityInquiry([FromBody] FacilityInquiry Model)
        {
            ActiveResponseSucces<FacilityOut> response = new ActiveResponseSucces<FacilityOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmartLimit:BaseUrl"].ToString(),
                                                                        _config["BankSmartLimit:FacilityInquiry"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<FacilityOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("FacilityCreate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Account>", typeof(ActiveResponseSucces<CreateFacilityOut>))]
        public async Task<IActionResult> FacilityCreate([FromBody] CreateFacilityDto Model)
        {
            ActiveResponseSucces<CreateFacilityOut> response = new ActiveResponseSucces<CreateFacilityOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmartLimit:BaseUrl"].ToString(),
                                                                        _config["BankSmartLimit:FacilityCreate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CreateFacilityOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("FacilityModify")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Account>", typeof(ActiveResponseSucces<ModifyFacilityOut>))]
        public async Task<IActionResult> FacilityModify([FromBody] ModifyFacilityDto Model)
        {
            ActiveResponseSucces<ModifyFacilityOut> response = new ActiveResponseSucces<ModifyFacilityOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmartLimit:BaseUrl"].ToString(),
                                                                        _config["BankSmartLimit:FacilityModify"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<ModifyFacilityOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("FacilitySearch")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Account>", typeof(ActiveResponseSucces<SearchFacilityOut>))]
        public async Task<IActionResult> FacilitySearch([FromBody] SearchFacilityDto Model)
        {
            ActiveResponseSucces<SearchFacilityOut> response = new ActiveResponseSucces<SearchFacilityOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmartLimit:BaseUrl"].ToString(),
                                                                        _config["BankSmartLimit:FacilitySearch"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<SearchFacilityOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("FacilityCancel")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Account>", typeof(ActiveResponseSucces<CancelFacilityOut>))]
        public async Task<IActionResult> FacilityCancel([FromBody] CancelFacilityDto Model)
        {
            ActiveResponseSucces<CancelFacilityOut> response = new ActiveResponseSucces<CancelFacilityOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmartLimit:BaseUrl"].ToString(),
                                                                        _config["BankSmartLimit:FacilityCancel"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CancelFacilityOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        //Loan
        [Route("LoanSimulator")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Account>", typeof(ActiveResponseSucces<LoanSimulatorOut>))]
        public async Task<IActionResult> LoanSimulator([FromBody] LoanSimulatorDto Model)
        {
            ActiveResponseSucces<LoanSimulatorOut> response = new ActiveResponseSucces<LoanSimulatorOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmartLimit:BaseUrl"].ToString(),
                                                                        _config["BankSmartLimit:LoanSimulator"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<LoanSimulatorOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("LoanCreateAccounts")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Account>", typeof(ActiveResponseSucces<LoanAccountOut>))]
        public async Task<IActionResult> LoanCreateAccounts([FromBody] CreateLoanAccountDto Model)
        {
            ActiveResponseSucces<LoanAccountOut> response = new ActiveResponseSucces<LoanAccountOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmartLimit:BaseUrl"].ToString(),
                                                                        _config["BankSmartLimit:LoanCreateAccounts"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<LoanAccountOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("LoanDisbursement")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Account>", typeof(ActiveResponseSucces<DisbursementOut>))]
        public async Task<IActionResult> LoanDisbursement([FromBody] DisbursementDto Model)
        {
            ActiveResponseSucces<DisbursementOut> response = new ActiveResponseSucces<DisbursementOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmartLimit:BaseUrl"].ToString(),
                                                                        _config["BankSmartLimit:LoanDisbursement"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DisbursementOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        //CreditProgram
        [Route("CreditProgramInquiry")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Account>", typeof(ActiveResponseSucces<CreditProgramOut>))]
        public async Task<IActionResult> CreditProgramInquiry([FromBody] CreditProgramInquiry Model)
        {
            ActiveResponseSucces<CreditProgramOut> response = new ActiveResponseSucces<CreditProgramOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmartLimit:BaseUrl"].ToString(),
                                                                        _config["BankSmartLimit:CreditProgramInquiry"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CreditProgramOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("CreditProgramCreate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Account>", typeof(ActiveResponseSucces<CreateCreditProgramOut>))]
        public async Task<IActionResult> CreditProgramCreate([FromBody] CreateCreditProgramDto Model)
        {
            ActiveResponseSucces<CreateCreditProgramOut> response = new ActiveResponseSucces<CreateCreditProgramOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model == null ? null : Model.SignOnRq, Model,
                                                                        _config["BankSmartLimit:BaseUrl"].ToString(),
                                                                        _config["BankSmartLimit:CreditProgramCreate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler);
            response.Status = result.Status;
            response.LogId = Model?.SignOnRq?.LogId;
            response.RequestDateTime = Model?.SignOnRq?.DateTime;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CreateCreditProgramOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        #endregion
    }
}