﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FirebaseHandler
{
    public class Result
    {
        public string error { get; set; }
    }

    public class RootObject
    {
        public long multicast_id { get; set; }
        public int success { get; set; }
        public int failure { get; set; }
        public int canonical_ids { get; set; }
        public List<Result> results { get; set; }
    }

    public class FireBaseMessage
    {
        public NotificationClass Body { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string title { get; set; }
        public string deviceID { get; set; }
        //[Required(ErrorMessage = " is a mandatory field.")]
        //public string CIF { get; set; }
        [Required(ErrorMessage = " is a mandatory field.")]
        public string typeID { get; set; }
        public string Platform { get; set; }
    }

    public class NotificationClass
    {
        public string Path { get; set; }
        public string TextToSend { get; set; }
        public bool? ProcessRequest { get; set; }
    }
}
