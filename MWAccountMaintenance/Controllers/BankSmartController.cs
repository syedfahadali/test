﻿using System.Collections.Generic;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using DBHandler.Helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using DBHandler.Repositories;
using Microsoft.Extensions.Configuration;
using DBHandler.Enum;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Globalization;
using DigiBancMiddleware.Helper;

namespace VendorApi.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class BankSmartController : ControllerBase
    {
        private readonly HttpHandler.HttpHandler httpHandler;
        private readonly IUserChannelsRepository _userChannels;
        private readonly ILogRepository _logs;
        private readonly IConfiguration _config;
        private Level level = Level.Info;
        public BankSmartController(IConfiguration config, IUserChannelsRepository userChannels, IHttpClientFactory httpClientFactory, ILogRepository logs, ControllerBase controllerbase = null)
        {
            _config = config;
            _userChannels = userChannels;
            _logs = logs;
            httpHandler = new HttpHandler.HttpHandler(httpClientFactory);
        }


        [Route("UpdateSOCID")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<UpdateSOCIDResponse>", typeof(ActiveResponseSucces<UpdateSOCIDResponse>))]
        public async Task<IActionResult> UpdateSOCID([FromBody] UpdateSOCIDModel Model)
        {


            ActiveResponseSucces<UpdateSOCIDResponse> response = new ActiveResponseSucces<UpdateSOCIDResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:UpdateSOCID"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<UpdateSOCIDResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("AccountInquiry")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<AccountView>>", typeof(ActiveResponseSucces<List<AccountView>>))]
        public async Task<IActionResult> AccountInquiry([FromBody] AccountInquiryRequest Model)
        {


            ActiveResponseSucces<List<AccountView>> response = new ActiveResponseSucces<List<AccountView>>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:AccountInquiry"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<AccountView>>>(result.httpResult.httpResponse);
                List<AccountView> list = new List<AccountView>();
                if (response.Content != null && response.Content.Count > 0)
                {
                    foreach (AccountView v in response.Content)
                    {
                        if (v.Status == "")
                        {
                            v.Status = "Active";
                        }
                        else if (v.Status == "I")
                        {
                            v.Status = "InActive";
                        }
                        else if (v.Status == "T")
                        {
                            v.Status = "Dormant";
                        }
                        else if (v.Status == "X")
                        {
                            v.Status = "Deceased";
                        }
                        else if (v.Status == "D")
                        {
                            v.Status = "Blocked";
                        }
                        else if (v.Status == "C")
                        {
                            v.Status = "Closed";
                        }
                        else
                        {
                            v.Status = "Undefined";
                        }
                        list.Add(v);
                    }
                    response.Content = list;
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("CloseAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<Balance>>", typeof(ActiveResponseSucces<List<Balance>>))]
        public async Task<IActionResult> CloseAccount([FromBody] BalanceInquiryModel Model)
        {


            ActiveResponseSucces<List<Balance>> response = new ActiveResponseSucces<List<Balance>>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:CloseAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<Balance>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("UpdateCharity")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "string", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateCharity([FromBody] CharityModel Model)
        {


            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:UpdateCharity"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetAccountMandate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<AccountMandate>>", typeof(ActiveResponseSucces<List<AccountMandate>>))]
        public async Task<IActionResult> GetAccountMandate([FromBody] AccountMandateViewModel Model)
        {


            ActiveResponseSucces<List<AccountMandate>> response = new ActiveResponseSucces<List<AccountMandate>>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:GetAccountMandate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<AccountMandate>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("GetAccountStatusMarking")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<Accountstatusmarking>>", typeof(ActiveResponseSucces<List<Accountstatusmarking>>))]
        public async Task<IActionResult> GetAccountStatusMarking([FromBody] GetAccountStatusMarkingModel Model)
        {


            ActiveResponseSucces<List<Accountstatusmarking>> response = new ActiveResponseSucces<List<Accountstatusmarking>>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:GetAccountStatusMarking"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<Accountstatusmarking>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("AccountStatusMarking")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> AccountStatusMarking([FromBody] MarkAccountStatusModel Model)
        {


            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:AccountStatusMarking"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("UnMarkAccountStatus")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UnMarkAccountStatus([FromBody] UnMarkAccountStatusModel Model)
        {


            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:UnMarkAccountStatus"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("GetAccountDetail")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<AccountViewDetail>", typeof(ActiveResponseSucces<AccountViewDetail>))]
        public async Task<IActionResult> GetAccountDetail([FromBody] BalanceIBANInquiryModel Model)
        {


            ActiveResponseSucces<AccountViewDetail> response = new ActiveResponseSucces<AccountViewDetail>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:GetAccountDetail"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<AccountViewDetail>>(result.httpResult.httpResponse);

                if (response.Content != null)
                {
                    if (string.IsNullOrEmpty(response.Content.status))
                    {
                        response.Content.status = "Active";
                    }
                    else if (response.Content.status == "I")
                    {
                        response.Content.status = "InActive";
                    }
                    else if (response.Content.status == "T")
                    {
                        response.Content.status = "Dormant";
                    }
                    else if (response.Content.status == "X")
                    {
                        response.Content.status = "Deceased";
                    }
                    else if (response.Content.status == "D")
                    {
                        response.Content.status = "Blocked";
                    }
                    else if (response.Content.status == "C")
                    {
                        response.Content.status = "Closed";
                    }
                    else
                    {
                        response.Content.status = "Undefined";
                    }
                }

                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("LinkDebitCardStatusUpdate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> LinkDebitCardStatusUpdate([FromBody] LinkDebitCardStatusUpdateView Model)
        {


            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:LinkDebitCardStatusUpdate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("LinkDebitCard")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> LinkDebitCard([FromBody] LinkDebitCardStatusInsertView Model)
        {


            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:LinkDebitCard"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("DebitCardInquiry")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DebitCards>>", typeof(ActiveResponseSucces<List<DebitCards>>))]
        public async Task<IActionResult> DebitCardInquiry([FromBody] DebitCardInquiryView Model)
        {


            ActiveResponseSucces<List<DebitCards>> response = new ActiveResponseSucces<List<DebitCards>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:DebitCardInquiry"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<DebitCards>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("DebitCardCategoryInquiry")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DebitCardCategory>>", typeof(ActiveResponseSucces<List<DebitCardCategory>>))]
        public async Task<IActionResult> DebitCardCategoryInquiry([FromBody] DebitCardCategoryInquiryView Model)
        {


            ActiveResponseSucces<List<DebitCardCategory>> response = new ActiveResponseSucces<List<DebitCardCategory>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:DebitCardCategoryInquiry"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<DebitCardCategory>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("AddMandate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Dictionary<string, string>>", typeof(ActiveResponseSucces<Dictionary<string, string>>))]
        public async Task<IActionResult> AddMandate([FromBody] DDMandateInsertView Model)
        {


            ActiveResponseSucces<Dictionary<string, string>> response = new ActiveResponseSucces<Dictionary<string, string>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:AddMandate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<Dictionary<string, string>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetAllMandate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DDMandateView>>", typeof(ActiveResponseSucces<List<DDMandateView>>))]
        public async Task<IActionResult> GetAllMandate([FromBody] BalanceInquiryModel Model)
        {


            ActiveResponseSucces<List<DDMandateView>> response = new ActiveResponseSucces<List<DDMandateView>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:GetAllMandate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<DDMandateView>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("CancelMandate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> CancelMandate([FromBody] DDMandateCancelView Model)
        {


            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:CancelMandate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetMandate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "Dictionary<string, string>", typeof(ActiveResponseSucces<Dictionary<string, string>>))]
        public async Task<IActionResult> GetMandate([FromBody] DDMandateCancelView Model)
        {


            ActiveResponseSucces<Dictionary<string, string>> response = new ActiveResponseSucces<Dictionary<string, string>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:GetMandate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<Dictionary<string, string>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetCorporateAccountDetail")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<AccountCorporateView>", typeof(ActiveResponseSucces<AccountCorporateView>))]
        public async Task<IActionResult> GetCorporateAccountDetail([FromBody] BalanceIBANInquiryModel Model)
        {


            ActiveResponseSucces<AccountCorporateView> response = new ActiveResponseSucces<AccountCorporateView>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSCorpOnboardingAPI:BaseUrl"].ToString(),
                                                                        _config["BSCorpOnboardingAPI:GetCorporateAccountDetail"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                try
                {
                    response = JsonConvert.DeserializeObject<ActiveResponseSucces<AccountCorporateView>>(result.httpResult.httpResponse);
                }
                catch (Exception ex)
                {
                    int i = 0;
                }
                if (result != null && result.httpResult?.severity == "Success")
                {

                    if (string.IsNullOrEmpty(response.Content.status))
                    {
                        response.Content.status = "Active";
                    }
                    else if (response.Content.status == "I")
                    {
                        response.Content.status = "InActive";
                    }
                    else if (response.Content.status == "T")
                    {
                        response.Content.status = "Dormant";
                    }
                    else if (response.Content.status == "X")
                    {
                        response.Content.status = "Deceased";
                    }
                    else if (response.Content.status == "D")
                    {
                        response.Content.status = "Blocked";
                    }
                    else if (response.Content.status == "C")
                    {
                        response.Content.status = "Closed";
                    }
                    else
                    {
                        response.Content.status = "Undefined";
                    }
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("IssuanceOfChequeBooks")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ChequeResponseModel>", typeof(ActiveResponseSucces<ChequeResponseModel>))]
        public async Task<IActionResult> IssuanceOfChequeBooks([FromBody] ChequeModel Model)
        {


            ActiveResponseSucces<ChequeResponseModel> response = new ActiveResponseSucces<ChequeResponseModel>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:IssuanceOfChequeBooks"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<ChequeResponseModel>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetChequeBookType")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ChequeResponseModel>", typeof(ActiveResponseSucces<ChqBookTypes>))]
        public async Task<IActionResult> GetChequeBookType([FromBody] ChequeTypeModel Model)
        {


            ActiveResponseSucces<ChqBookTypes> response = new ActiveResponseSucces<ChqBookTypes>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:GetChequeBookType"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<ChqBookTypes>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("InquiryNOSTRO")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "string", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> InquiryNOSTRO([FromBody] InquiryNOSTROModel Model)
        {


            ActiveResponseSucces<List<NostroAccounts>> response = new ActiveResponseSucces<List<NostroAccounts>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:InquiryNOSTRO"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<NostroAccounts>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }




        [Route("GettDailyAccrual")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<RptSavAccruals>", typeof(ActiveResponseSucces<List<RptSavAccruals>>))]
        public async Task<IActionResult> GettDailyAccrual([FromBody] AccrualRatesModel Model)
        {


            ActiveResponseSucces<List<RptSavAccruals>> response = new ActiveResponseSucces<List<RptSavAccruals>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:GettDailyAccrual"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<RptSavAccruals>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    List<RptSavAccruals> newlist = new List<RptSavAccruals>();
                    List<RptSavAccruals> list = response.Content;
                    foreach (RptSavAccruals l in list)
                    {
                        if (l.Days == 1)
                        {
                            newlist.Add(l);
                        }
                        else
                        {
                            decimal amount = l.Amount / l.Days;
                            for (int i = 0; i < l.Days; i++)
                            {
                                RptSavAccruals r = new RptSavAccruals();
                                r.AccountID = l.AccountID;
                                r.AccrualDate = l.AccrualDate;
                                r.Amount = amount;
                                r.Balance = l.Balance;
                                r.CurrencyID = l.CurrencyID;
                                r.Days = l.Days;
                                r.Rate = l.Rate;
                                r.ValueDate = l.ValueDate.AddDays(i);
                                newlist.Add(r);
                            }
                        }
                    }
                    response.Content = newlist;
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("AddCountryLimits")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> AddCountryLimits([FromBody] CountryLimitsModel Model)
        {


            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:AddCountryLimits"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("UpdateCountryLimits")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateCountryLimits([FromBody] CountryLimitsModel Model)
        {


            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:UpdateCountryLimits"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("CountryLimitInquire")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<CountryLimits>))]
        public async Task<IActionResult> CountryLimitInquire([FromBody] InquirCountryLimitsModel Model)
        {


            ActiveResponseSucces<CountryLimits> response = new ActiveResponseSucces<CountryLimits>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:CountryLimitInquire"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CountryLimits>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("CreateVirtualAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<VirtualAccountResponse>))]
        public async Task<IActionResult> CreateVirtualAccount([FromBody] VirtualAccountModel Model)
        {


            ActiveResponseSucces<VirtualAccountResponse> response = new ActiveResponseSucces<VirtualAccountResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:CreateVirtualAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<VirtualAccountResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }


        [Route("ModifyVirtualAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> ModifyVirtualAccount([FromBody] ModifyVirtualAccountModel Model)
        {


            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:ModifyVirtualAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("CancelVirtualAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> CancelVirtualAccount([FromBody] CancelVirtualAccountModel Model)
        {


            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:CancelVirtualAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetVirtualAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<List<VAResponse>>))]
        public async Task<IActionResult> GetVirtualAccount([FromBody] GetVirtualAccountModel Model)
        {


            ActiveResponseSucces<List<VAResponse>> response = new ActiveResponseSucces<List<VAResponse>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:GetVirtualAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<VAResponse>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }


        [Route("GetTotalCharity")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<GetCharity>", typeof(ActiveResponseSucces<GetCharity>))]
        public async Task<IActionResult> GetTotalCharity([FromBody] GetTotalCharityDto Model)
        {


            ActiveResponseSucces<GetCharity> response = new ActiveResponseSucces<GetCharity>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:GetTotalCharity"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<GetCharity>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetBasicAccountInquiry")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<GetBasicAccountInquiryResponse>", typeof(ActiveResponseSucces<GetBasicAccountInquiryResponse>))]
        public async Task<IActionResult> GetBasicAccountInquiry([FromBody] GetBasicAccountInquiryModel Model)
        {


            ActiveResponseSucces<GetBasicAccountInquiryResponse> response = new ActiveResponseSucces<GetBasicAccountInquiryResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:GetBasicAccountInquiry"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<GetBasicAccountInquiryResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    string specialChar = _config["GlobalSettings:SpecialChar"];
                    if (HttpContext.Request.Headers["ChannelId"] == _config["GlobalSettings:ECSCannel"])
                    {
                        response.Content.ClientName = response.Content.ClientName != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.ClientName, specialChar) : null;
                        response.Content.AccountTitle = response.Content.AccountTitle != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.AccountTitle, specialChar) : null;
                        response.Content.CustomerAddress = response.Content.CustomerAddress != null ? CommonModel.Common.RemoveSpeicalChar(response.Content.CustomerAddress, specialChar) : null;
                    }
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }


        [Route("GetAccountBalance")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<AccountBalance>", typeof(ActiveResponseSucces<AccountBalance>))]
        public async Task<IActionResult> GetAccountBalance([FromBody] GetTotalCharityDto Model)
        {


            ActiveResponseSucces<AccountBalance> response = new ActiveResponseSucces<AccountBalance>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:GetAccountBalance"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<AccountBalance>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }




        [Route("AddCustomerCardData")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> AddCustomerCardData([FromBody] CustomerCardDataModel Model)
        {



            if (!string.IsNullOrEmpty(Model.publicdata.IssueDate))
            {
                string stringDate = Convert.ToDateTime(Model.publicdata.IssueDate, new CultureInfo("en-GB")).ToString("MM/dd/yyyy");
                Model.publicdata.IssueDate = stringDate;
            }
            if (!string.IsNullOrEmpty(Model.publicdata.ExpiryDate))
            {
                string stringDate = Convert.ToDateTime(Model.publicdata.ExpiryDate, new CultureInfo("en-GB")).ToString("MM/dd/yyyy"); ;
                Model.publicdata.ExpiryDate = stringDate;
            }
            if (!string.IsNullOrEmpty(Model.publicdata.DOB))
            {
                string stringDate = Convert.ToDateTime(Model.publicdata.DOB, new CultureInfo("en-GB")).ToString("MM/dd/yyyy"); ;
                Model.publicdata.DOB = stringDate;
            }
            if (!string.IsNullOrEmpty(Model.publicdata.PassportIssueDate))
            {
                string stringDate = Convert.ToDateTime(Model.publicdata.PassportIssueDate, new CultureInfo("en-GB")).ToString("MM/dd/yyyy"); ;
                Model.publicdata.PassportIssueDate = stringDate;
            }
            if (!string.IsNullOrEmpty(Model.publicdata.PassportExpiryDate))
            {
                string stringDate = Convert.ToDateTime(Model.publicdata.PassportExpiryDate, new CultureInfo("en-GB")).ToString("MM/dd/yyyy"); ;
                Model.publicdata.PassportExpiryDate = stringDate;
            }
            if (!string.IsNullOrEmpty(Model.publicdata.DateOfGraduation))
            {
                string stringDate = Convert.ToDateTime(Model.publicdata.DateOfGraduation, new CultureInfo("en-GB")).ToString("MM/dd/yyyyy"); ;
                Model.publicdata.DateOfGraduation = stringDate;
            }
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:AddCustomerCardData"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }



        [Route("GetCIFBasicDetails")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CustomerBasicDetailModel>", typeof(ActiveResponseSucces<CustomerBasicDetailModel>))]
        public async Task<IActionResult> GetCIFBasicDetails([FromBody] RetailCustomerInquiryModel Model)
        {


            ActiveResponseSucces<CustomerBasicDetailModel> res = new ActiveResponseSucces<CustomerBasicDetailModel>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:GetCIFBasicDetails"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            res.Status = result.Status;
            if (result.Status.Code == null)
            {
                res = JsonConvert.DeserializeObject<ActiveResponseSucces<CustomerBasicDetailModel>>(result.httpResult.httpResponse);

                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(res);
                }
            }
            return BadRequest(res);
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("UpdateKYCDateCorporate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateKYCDateCorporate([FromBody] UpdateKYCDateModel Model)
        {


            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:UpdateKYCDateCorporate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("UpdateKYCDateRetail")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateKYCDateRetail([FromBody] UpdateKYCDateModel Model)
        {


            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:UpdateKYCDateRetail"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("CheckIDForCIF")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> CheckIDForCIF([FromBody] CheckIDForCIFModel Model)
        {


            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:CheckIDForCIF"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetPenaltyMatrix")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<PenaltyMatrix>>", typeof(ActiveResponseSucces<List<PenaltyMatrix>>))]
        public async Task<IActionResult> GetPenaltyMatrix([FromBody] PenaltyMatrixModel Model)
        {

            ActiveResponseSucces<List<PenaltyMatrix>> response = new ActiveResponseSucces<List<PenaltyMatrix>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:GetPenaltyMatrix"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<PenaltyMatrix>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("UpdateAccountStatus")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Account>", typeof(ActiveResponseSucces<Account>))]
        public async Task<IActionResult> UpdateAccountStatus([FromBody] AccountUpdateModel Model)
        {
            ActiveResponseSucces<Account> response = new ActiveResponseSucces<Account>();


            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:UpdateAccountStatus"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<Account>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("AddAccountMandate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> AddAccountMandate([FromBody] AccountMandateModel Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:AddUpdateAccountMandate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("UpdateAccountMandate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UpdateAccountMandate([FromBody] AccountMandateModel Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:AddUpdateAccountMandate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("FreezeAccountAmountInquiryRetail")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List< AccountFreeze>>", typeof(ActiveResponseSucces<List<AccountFreeze>>))]
        public async Task<IActionResult> FreezeAccountAmountInquiryRetail([FromBody] BalanceInquiryModel Model)
        {
            ActiveResponseSucces<List<AccountFreeze>> response = new ActiveResponseSucces<List<AccountFreeze>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:FreezeAccountInquiryRetail"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<AccountFreeze>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("FreezeAccountAmountInquiryCorporate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<AccountFreeze>>", typeof(ActiveResponseSucces<List<AccountFreeze>>))]
        public async Task<IActionResult> FreezeAccountAmountInquiryCorporate([FromBody] BalanceInquiryModel Model)
        {
            ActiveResponseSucces<List<AccountFreeze>> response = new ActiveResponseSucces<List<AccountFreeze>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:FreezeAccountInquiryCorporate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<AccountFreeze>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("FreezeAccountAmountRetail")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> FreezeAccountAmountRetail([FromBody] FreezeAccountModel Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:FreezeAccountRetail"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("FreezeAccountAmountCorporate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> FreezeAccountAmountCorporate([FromBody] FreezeAccountModel Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:FreezeAccountCorporate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("UnFreezeAccountAmountRetail")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UnFreezeAccountAmountRetail([FromBody] UpdateAccountFreezeModel Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:UnFreezeAccountRetail"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("UnFreezeAccountAmountCorporate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> UnFreezeAccountAmountCorporate([FromBody] UpdateAccountFreezeModel Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:UnFreezeAccountCorporate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetAccrualRates")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> GetAccrualRates([FromBody] AccrualRatesModel Model)
        {
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:GetAccrualRates"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("AddCustomerCardDataInBankSmart")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> AddCustomerCardDataInBankSmart([FromBody] CustomerCardDataModel Model)
        {


            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:AddCustomerCardData"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("UpdateFloatingRates")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<bool>", typeof(ActiveResponseSucces<bool>))]
        public async Task<IActionResult> UpdateFloatingRates([FromBody] DBHandler.Model.UpdateFloatingRatesModel Model)
        {
            ActiveResponseSucces<bool> response = new ActiveResponseSucces<bool>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:UpdateFloatingRates"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<bool>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("UpdateFXRates")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<bool>", typeof(ActiveResponseSucces<bool>))]
        public async Task<IActionResult> UpdateFXRates([FromBody] DBHandler.Model.UpdateFXRatesModel Model)
        {
            ActiveResponseSucces<bool> response = new ActiveResponseSucces<bool>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:UpdateFXRates"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<bool>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("AccountAccrualSettlement")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.GetCorporateAccountDetailEBosModel.GetCorporateAccountDetailEBosModelResponse>", typeof(ActiveResponseSucces<DBHandler.AccountAccrualSettlementModel.AccountAccrualSettlementResponse>))]
        public async Task<IActionResult> AccountAccrualSettlement([FromBody] DBHandler.AccountAccrualSettlementModel.AccountAccrualSettlementModel Model)
        {
            ActiveResponseSucces<DBHandler.AccountAccrualSettlementModel.AccountAccrualSettlementResponse> response = new ActiveResponseSucces<DBHandler.AccountAccrualSettlementModel.AccountAccrualSettlementResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:AccountAccrualSettlement"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.AccountAccrualSettlementModel.AccountAccrualSettlementResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetPreAccountClosure")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<Account>", typeof(ActiveResponseSucces<GetPreAccountClosureResponse>))]
        public async Task<IActionResult> GetPreAccountClosure([FromBody] GetPreAccountClosure Model)
        {
            ActiveResponseSucces<GetPreAccountClosureResponse> response = new ActiveResponseSucces<GetPreAccountClosureResponse>();


            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:GetPreAccountClosure"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<GetPreAccountClosureResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("UpdateFXRatesV2")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<bool>", typeof(ActiveResponseSucces<bool>))]
        public async Task<IActionResult> UpdateFXRatesV2([FromBody] UpdateFXRatesV2DTO Model)
        {
            ActiveResponseSucces<bool> response = new ActiveResponseSucces<bool>();


            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:UpdateFXRatesV2"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<bool>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetPublicData")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [ProducesResponseType(typeof(ActiveResponse), 400)]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<GetPublicDataResponse>", typeof(ActiveResponseSucces<GetPublicDataResponse>))]
        public async Task<IActionResult> GetPublicData([FromBody] GetPublicDataModel Model)
        {
            ActiveResponseSucces<GetPublicDataResponse> response = new ActiveResponseSucces<GetPublicDataResponse>();


            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSAccountMaintenanceAPI:BaseUrl"].ToString(),
                                                                        _config["BSAccountMaintenanceAPI:GetPublicData"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<GetPublicDataResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }
    }
}
