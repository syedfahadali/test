﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWAccountMaintenance.Swagger.BankSmart.UpdateFXRatesV2
{
    
    //REQUEST
    public class UpdateFXRatesV2Request : IExamplesProvider<UpdateFXRatesV2DTO>
    {
        public UpdateFXRatesV2DTO GetExamples()
        {
            return new UpdateFXRatesV2DTO()
            {
                //AccountId = "0315100003348801",
                //MobileNo = "12312312312",
                //IBAN = "AL47 2121 1009 0000 0002 3569 87411"
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
