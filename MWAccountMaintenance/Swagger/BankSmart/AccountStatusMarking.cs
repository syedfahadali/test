﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWAccountMaintenance.Swagger.BankSmart.AccountStatusMarking
{
    //REQUEST
    public class AccountStatusMarkingRequest : IExamplesProvider<MarkAccountStatusModel>
    {
        public MarkAccountStatusModel GetExamples()
        {
            return new MarkAccountStatusModel()
            {
                AccountStatus = new InsertAccountStatus()
                {

                    AccountID = "0310100000045601",
                    Status = "D",
                    Comments = "Block account",
                    WorkingDate = DateTime.Now
                    

                }
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}

}
