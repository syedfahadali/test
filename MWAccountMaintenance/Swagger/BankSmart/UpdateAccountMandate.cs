﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWAccountMaintenance.Swagger.BankSmart.UpdateAccountMandate
{
    //REQUEST
    public class UpdateAccountMandateRequest : IExamplesProvider<AccountMandateModel>
    {
        public AccountMandateModel GetExamples()
        {
            return new AccountMandateModel()
            {
                AccountMandate = new AccountMandate()
                {

                    AccountID = "0310100000013701",
                    MinLimit = 1,
                    MaxLimit = 6,
                    AuthGroupID1 = "",
                    AuthGroupID2 = "",
                    AuthGroupID3 = "",
                    AuthGroupID4 = ""

                }
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}


}
