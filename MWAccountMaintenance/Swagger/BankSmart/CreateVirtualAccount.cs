﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWAccountMaintenance.Swagger.BankSmart.CreateVirtualAccount
{

    //REQUEST
    public class CreateVirtualAccountRequest : IExamplesProvider<VirtualAccountModel>
    {
        public VirtualAccountModel GetExamples()
        {
            return new VirtualAccountModel()
            {
                VirtualAccounts = new VirtualAccounts()
                {
                    AccountID = "0310100013896301",
                    Address = "asdasd",
                    AccountTitle = "asdas",
                    ContactNo = "",
                    EmailID = "asas@hotmail.com"
                }
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
