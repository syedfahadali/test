﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWAccountMaintenance.Swagger.BankSmart.FreezeAccountAmountCorporate
{
    
    //REQUEST
    public class FreezeAccountAmountCorporateRequest : IExamplesProvider<FreezeAccountModel>
    {
        public FreezeAccountModel GetExamples()
        {
            return new FreezeAccountModel()
            {
                Freeze = new InsertAccountFreeze()
                {
                    AccountID = "0191000000028715",
                    Amount =100,
                    Comments = "test data",
                    Date = DateTime.Now,
                    ExpiredOn = DateTime.Now

                }
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}

}
