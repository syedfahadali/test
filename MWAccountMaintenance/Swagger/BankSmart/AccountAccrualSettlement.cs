﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWAccountMaintenance.Swagger.BankSmart.AccountAccrualSettlement
{

    //REQUEST
    public class AccountAccrualSettlementRequest : IExamplesProvider<DBHandler.AccountAccrualSettlementModel.AccountAccrualSettlementModel>
    {
        public DBHandler.AccountAccrualSettlementModel.AccountAccrualSettlementModel GetExamples()
        {
            return new DBHandler.AccountAccrualSettlementModel.AccountAccrualSettlementModel()
            {
                AccountId = "0210100022327901"

            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
