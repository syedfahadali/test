﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWAccountMaintenance.Swagger.BankSmart.LinkDebitCard
{
    //REQUEST
    public class LinkDebitCardRequest : IExamplesProvider<LinkDebitCardStatusInsertView>
    {
        public LinkDebitCardStatusInsertView GetExamples()
        {
            return new LinkDebitCardStatusInsertView()
            {
                DebitCards = new DebitCards()
                {
                    AccountID = "0210100000138601",
                    CardName = "Fahad",
                    CardType = "Supplementary",
                    CreateDateTime = DateTime.Now,
                    DebitCardCategory = "001",
                    DebitCardToken = "9999995390608623",
                    ExpiryDate = DateTime.Now,
                    Status = "A"
                }
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}


}
