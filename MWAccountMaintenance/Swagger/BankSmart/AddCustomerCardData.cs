﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWAccountMaintenance.Swagger.BankSmart.AddCustomerCardData
{

    //REQUEST
    public class AddCustomerCardDataRequest : IExamplesProvider<CustomerCardDataModel>
    {
        public CustomerCardDataModel GetExamples()
        {
            return new CustomerCardDataModel()
            {
                publicdata = new Publicdata()
                {
                    IDNumber = "580519577906474",
                    cardNo = "999999999",
                    IssueDate = "12/12/2011",
                    ExpiryDate = "12/12/2001",
                    FullNameAr = "fahad",
                    TitleEn = "fahad",
                    FullNameEn = "fahad",
                    Sex = "M",
                    DOB = "12/12/2011",
                    MotherFullNameAr = "",
                    MotherFullNameEn = "",
                    CardholderPhotography = "",
                    OccupationCode = "",
                    MaritalStatusCode = "",
                    HusbandIDN = "",
                    SponsorTypeCode = "",
                    SponsorUnifiedNumber = "",
                    SponsorName = "",
                    ResidencyTypeCode = "",
                    ResidencyNumber = "",
                    IDTypeofDocument = "",
                    FamilyId = "",
                    PassportNumber = "",
                    PassportType = "",
                    PassportCountryDescrAr = "",
                    PassportCountryDescEn = "",
                    PassportIssueDate = "",
                    PassportExpiryDate = "",
                    PlaceOfBirthAr = "",
                    PlaceOfBirthEn = "",
                    QualificationLevel = "",
                    QualificationLevelDescrAr = "",
                    QualificationLevelDescrptionEn = "",
                    DegreeDescriptionAr = "",
                    DegreeDescriptionEn = "",
                    FieldOfStudy = "",
                    FieldOfStudyAr = "",
                    FieldOfStudyEn = "",
                    PlaceOfStudyAr = "",
                    PlaceOfStudyEn = "",
                    DateOfGraduation = "",
                    OccupationAr = "",
                    OccupationEn = "",
                    OccupationFieldCode = "",
                    OccupationTypeAr = "",
                    OccupationTypeEn = "",
                    CompanyNameAr = "",
                    CompanyNameEn = "",
                    OccupationCodeOcc = "",
                    OccupationTypeOccEn = "",
                    MotherFullNameOthAr = "",
                    MotherFullNameOthEn = "",
                    HolderSignatureImage = "",
                    SponsorUnifiedNumberOth = "",
                    EmiratesDesc = "",
                    EmiratesDescAr = "",
                    EmirateCode = "",
                    CityCode = "",
                    CityDescEn = "",
                    CityDescAr = "",
                    LabelOfTheField = "",
                    Street = "",
                    StreetAr = "",
                    AreaDesc = "",
                    AreaDescAr = "",
                    Building = "",
                    BuildingAr = "",
                    AreaCode = "",
                    FlatNo = "",
                    AddressTypeCode = "",
                    LocationCode = "",
                    ResidentPhoneNumber = "",
                    MobilePhoneNumber = "",
                    POBox = "",
                    Email = ""

                }
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
