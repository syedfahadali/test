﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWAccountMaintenance.Swagger.BankSmart.AccountInquiry
{
    //REQUEST
    public class AccountInquiryRequest : IExamplesProvider<DBHandler.Model.AccountInquiryRequest>
    {
        public DBHandler.Model.AccountInquiryRequest GetExamples()
        {
            return new DBHandler.Model.AccountInquiryRequest()
            {
                AccountId = "0315100003348801",
                MobileNo="12312312312",
                IBAN= "AL47 2121 1009 0000 0002 3569 87411",
                ClientId = "000033488"
                
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
