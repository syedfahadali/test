﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWAccountMaintenance.Swagger.BankSmart.IssuanceOfChequeBooks
{

    //REQUEST
    public class IssuanceOfChequeBooksRequest : IExamplesProvider<ChequeModel>
    {
        public ChequeModel GetExamples()
        {
            return new ChequeModel()
            {
                AccountID = "0150100000003701",
                ChequeStart = "55",
                ApplyCharges = 1,
                ChqBookTypeID = "001",
                NoOfLeaves = 5

            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}

}
