﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWAccountMaintenance.Swagger.BankSmart.LinkDebitCardStatusUpdate
{
    
    //REQUEST
    public class LinkDebitCardStatusUpdateRequest : IExamplesProvider<LinkDebitCardStatusUpdateView>
    {
        public LinkDebitCardStatusUpdateView GetExamples()
        {
            return new LinkDebitCardStatusUpdateView()
            {
                AccountID = "0210100000138601",
                status="B",
                DebitCardToken= "9999995390608623"
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}


}
