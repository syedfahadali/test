using System;
using System.IO;
using System.Text;
using DBHandler.Enum;
using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Repositories;
using DigiBancMiddleware.Helper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.ResponseCompression;
using System.IO.Compression;
using Swashbuckle.AspNetCore.Filters;

namespace DigiBancMiddleware.config
{
    public class Startup
    {
        public Startup(IConfiguration configuration/*, IConfiguration config*/)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        // This method gets called by the runtime. Use this method to add services to the container.

        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddResponseCompression();

            services.Configure<BrotliCompressionProviderOptions>(options =>
            {
                options.Level = CompressionLevel.Fastest;
            });

            DBHandler.Common.Decrypt d = new DBHandler.Common.Decrypt(Configuration);
            //services.AddDbContext<VendorMWContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            //services.AddDbContext<VendorMWContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")), ServiceLifetime.Transient);
            services.AddDbContextPool<VendorMWContext>(options => options.UseSqlServer(d.DecryptString(Configuration.GetConnectionString("DefaultConnection"))));
            services.AddIdentity<ApplicationUser, IdentityRole>(options =>
            {
                options.User.RequireUniqueEmail = false;
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 8;
                options.Password.RequireNonAlphanumeric = true;
                options.Password.RequireUppercase = true;
                options.Password.RequireLowercase = true;
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                options.Lockout.MaxFailedAccessAttempts = 5;
            })
              .AddEntityFrameworkStores<VendorMWContext>()
              .AddDefaultTokenProviders();

            #region Cors

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader();
                        //.AllowCredentials();
                    });
            });

            #endregion

            //services.AddScoped<IDocumentsRepository, DocumentsRepository>();
            services.AddScoped<ILogRepository, LogRepository>();
            services.AddScoped<ILogRqChRepository, LogRqChRepository>();
            services.AddScoped<ILogRepository, LogRepository>();
            services.AddScoped<IUserChannelsRepository, UserChannelsRepository>();
            services.AddScoped<ITransactionImageRepository, TransactionImageRepository>();
            services.AddScoped<ILinkAccountRepository, LinkAccountRepository>();
            services.AddScoped<IVkeyRegisterdUserRepository, VkeyRegisterdUserRepo>();

            services.AddSwaggerGen(c =>
            {
                c.ExampleFilters();
            });

            services.AddSwaggerExamplesFromAssemblyOf<Startup>();

            services.AddMvc(option => option.EnableEndpointRouting = false).AddNewtonsoftJson();
            services.AddAuthentication("BasicAuthentication")
              .AddScheme<AuthenticationSchemeOptions, BasicAuthenticationHandler>("BasicAuthentication", o =>
              {
              });

            services.Configure<IISServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });
            services.Configure<KestrelServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });

            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });

            services.AddHttpClient();
            services.AddMvc().AddNewtonsoftJson();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILogRepository lg)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });
            app.UseExceptionHandler(a => a.Run(async context =>
            {
                var starttime = DateTime.Now;

                string clientip = Utility.GetIP(context.Request.HttpContext);
                string currentController = "";
                string currentAction = "";
                var RouteInfo = context.Request.HttpContext.GetRouteData().Values;
                if (RouteInfo.Keys.Count > 0)
                {
                    currentController = RouteInfo["controller"].ToString() == null ? "" : RouteInfo["controller"].ToString();
                    currentAction = RouteInfo["action"].ToString() == null ? "" : RouteInfo["action"].ToString();
                }

                var feature = context.Features.Get<IExceptionHandlerPathFeature>();
                var logId = Guid.NewGuid().ToString();


                DBHandler.Helper.ActiveResponseSucces<string> res = new DBHandler.Helper.ActiveResponseSucces<string>()
                {
                    LogId = logId,
                    Content = null,
                    RequestDateTime = DateTime.Now
                };

                try
                {

                    DBHandler.Common.Decrypt d = new DBHandler.Common.Decrypt(Configuration);
                    SqlConnection con = new SqlConnection(d.DecryptString(Configuration.GetConnectionString("DefaultConnection")));
                    con.Open();
                    con.Close();
                    string bodyStr = string.Empty;
                    using (StreamReader reader = new StreamReader(context.Request.Body, Encoding.UTF8, true, 1024, true))
                    {
                        bodyStr = reader.ReadToEnd();
                    }
                    lg.Logs(Level.Error, "", context, feature.Error, starttime, DateTime.Now, bodyStr, "Request or response is not as expected.", "", "", logId, DateTime.Now, null, "", "", DateTime.Now, DateTime.Now, clientip, currentController, currentAction, Configuration["ErrorLog"], Configuration["SuccessLog"], Configuration["InfoLog"], "", "", "");
                    res.Status = new DBHandler.Helper.Status
                    {
                        Severity = Severity.Error,
                        StatusMessage = "Request is not as expected.",
                        Code = "ERROR-01"
                    };
                    context.Response.StatusCode = 400;
                }
                catch (Exception ex)
                {
                    res.Status = new DBHandler.Helper.Status
                    {
                        Severity = Severity.Error,
                        StatusMessage = $"Connection could not be made with database",
                        Code = "ERROR-500"
                    };
                    context.Response.StatusCode = 500;
                }

                var originalBodyStream = context.Response.Body;
                var requestResponse = JsonConvert.SerializeObject(res);
                context.Response.ContentType = "application/json";
                using (var responseBody = new MemoryStream())
                {
                    try
                    {
                        byte[] b = System.Text.ASCIIEncoding.ASCII.GetBytes(requestResponse);
                        originalBodyStream.Write(b);
                        await responseBody.CopyToAsync(originalBodyStream);
                    }
                    catch (Exception ex)
                    { }
                }
            }));

            app.UseCors("AllowAll");
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            //app.UseLogIdMiddleWare();
            app.UseChannelAuthorizationMiddleware();
            //app.VkeyMiddleware();
            app.UseMiddleware<RequestMiddleware>();
            app.UseMvc();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}