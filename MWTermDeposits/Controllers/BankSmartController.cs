﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CoreHandler;
using DBHandler.Helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using DBHandler.Repositories;
using Microsoft.Extensions.Configuration;
using DBHandler.Enum;
using Swashbuckle.AspNetCore.Annotations;
using System;
using DigiBancMiddleware.Helper;

namespace VendorApi.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class BankSmartController : ControllerBase
    {
        private readonly HttpHandler.HttpHandler httpHandler;
        private readonly IUserChannelsRepository _userChannels;
        private readonly ILogRepository _logs;
        private readonly IConfiguration _config;
        private Level level = Level.Info;
        private Exception myEx;
        private ControllerBase _controllerBase;
        private Exception excetionForLog;

        public BankSmartController(IConfiguration config, IUserChannelsRepository userChannels, IHttpClientFactory httpClientFactory, ILogRepository logs, ControllerBase controllerbase = null)
        {
            _controllerBase = controllerbase;
            _config = config;
            _userChannels = userChannels;
            _logs = logs;
            httpHandler = new HttpHandler.HttpHandler(httpClientFactory);
        }


        [Route("DeleteDepositRate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DeleteDepositRateResponse>", typeof(ActiveResponseSucces<DeleteDepositRateResponse>))]
        public async Task<IActionResult> DeleteDepositRate([FromBody] DeleteDepositRateModel Model)
        {
            
            
            ActiveResponseSucces<DeleteDepositRateResponse> response = new ActiveResponseSucces<DeleteDepositRateResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:DeleteDepositRate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DeleteDepositRateResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("AddUpdateDepositRate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<AddUpdateDepositRateResponse>", typeof(ActiveResponseSucces<AddUpdateDepositRateResponse>))]
        public async Task<IActionResult> AddUpdateDepositRate([FromBody] AddUpdateDepositRateModel Model)
        {
            
            
            ActiveResponseSucces<AddUpdateDepositRateResponse> response = new ActiveResponseSucces<AddUpdateDepositRateResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:AddUpdateDepositRate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<AddUpdateDepositRateResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }
        [Route("GetDepositRate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<GetDepositRateResponsee>>", typeof(ActiveResponseSucces<List<GetDepositRateResponse>>))]
        public async Task<IActionResult> GetDepositRate([FromBody] GetDepositRateModel Model)
        {
            
            
            ActiveResponseSucces<List<GetDepositRateResponse>> response = new ActiveResponseSucces<List<GetDepositRateResponse>>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:GetDepositRate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<GetDepositRateResponse>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }
        [Route("GetFixDepositProducts")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<ProductView>>", typeof(ActiveResponseSucces<List<ProductView>>))]
        public async Task<IActionResult> GetFixDepositProducts([FromBody] RelationshipOfficerModel Model)
        {
            
            
            ActiveResponseSucces<List<ProductView>> response = new ActiveResponseSucces<List<ProductView>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:GetFixDepositProducts"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            try
            {
                response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
                response.LogId = HttpContext.Request.Headers["LogId"];
                response.Status = result.Status;
                if (result.Status.Code == null)
                {
                    response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<ProductView>>>(result.httpResult.httpResponse);
                    if (result != null && result.httpResult?.severity == "Success")
                    {
                        return CreatedAtAction(null, response);
                    }
                }

                return BadRequest(response);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [Route("GetDepositRates")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DepositRatesDTO>", typeof(ActiveResponseSucces<DepositRatesDTO>))]
        public async Task<IActionResult> GetDepositRates([FromBody] DepositRatesModel Model)
        {
            
            
            ActiveResponseSucces<DepositRatesDTO> response = new ActiveResponseSucces<DepositRatesDTO>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:GetDepositRates"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DepositRatesDTO>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }


        [Route("OpenFDAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "string", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> OpenFDAccount([FromBody] OpenFDAccountModel Model)
        {
            
            
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:OpenFDAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("OpenFixDeposit")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<IssueFixDepositResponse>", typeof(ActiveResponseSucces<IssueFixDepositResponse>))]
        public async Task<IActionResult> OpenFixDeposit([FromBody] OpenFixDepositModel Model)
        {
            
            
            ActiveResponseSucces<IssueFixDepositResponse> response = new ActiveResponseSucces<IssueFixDepositResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:OpenFixDeposit"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<IssueFixDepositResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("DepositsListAccountwise")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<TDRIssuance>>", typeof(ActiveResponseSucces<List<DepositsListAccountwiseTDRIssuance>>))]
        public async Task<IActionResult> DepositsListAccountwise([FromBody] DepositsListAccountwiseModel Model)
        {
            
            
            ActiveResponseSucces<List<DepositsListAccountwiseTDRIssuance>> response = new ActiveResponseSucces<List<DepositsListAccountwiseTDRIssuance>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:DepositsListAccountwise"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<DepositsListAccountwiseTDRIssuance>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetFDAccountsProductwise")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<FDAccountsProductwise>>", typeof(ActiveResponseSucces<List<FDAccountsProductwise>>))]
        public async Task<IActionResult> GetFDAccountsProductwise([FromBody] FDAccountsProductwiseModel Model)
        {
            
            
            ActiveResponseSucces<List<FDAccountsProductwise>> response = new ActiveResponseSucces<List<FDAccountsProductwise>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:GetFDAccountsProductwise"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<FDAccountsProductwise>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetFDCollectionAccounts")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<FDCollectionAccounts>>", typeof(ActiveResponseSucces<List<FDCollectionAccounts>>))]
        public async Task<IActionResult> GetFDCollectionAccounts([FromBody] GetFDCollectionAccountsModel Model)
        {
            
            
            ActiveResponseSucces<List<FDCollectionAccounts>> response = new ActiveResponseSucces<List<FDCollectionAccounts>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:GetFDCollectionAccounts"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<FDCollectionAccounts>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetFDCharges")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<FDChargesArray>>", typeof(ActiveResponseSucces<List<FDChargesArray>>))]
        public async Task<IActionResult> GetFDCharges([FromBody] FDChargesModel Model)
        {
            
            
            ActiveResponseSucces<List<FDChargesArray>> response = new ActiveResponseSucces<List<FDChargesArray>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:GetFDCharges"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<FDChargesArray>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("MarkFDLost")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> MarkFDLost([FromBody] TDRLostModel Model)
        {
            
            
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:MarkFDLost"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }
        [Route("GetCustomerPostingAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        public async Task<IActionResult> GetCustomerPostingAccount([FromBody] CustomerPostingAccountModel Model)
        {
            
            
            ActiveResponseSucces<List<GetCustPostingAccountNew>> response = new ActiveResponseSucces<List<GetCustPostingAccountNew>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:GetCustomerPostingAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<GetCustPostingAccountNew>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("ModifyFixedDeposit")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> ModifyFixedDeposit([FromBody] ModifyFDModel Model)
        {
            
            
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:ModifyFixedDeposit"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }


        [Route("DepositsListClientWise")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<TDRIssuance>>", typeof(ActiveResponseSucces<List<DepositsListClientWiseTDRIssuance>>))]
        public async Task<IActionResult> DepositsListClientWise([FromBody] DepositsListClientWiseModel Model)
        {
            
            
            ActiveResponseSucces<List<DepositsListClientWiseTDRIssuance>> response = new ActiveResponseSucces<List<DepositsListClientWiseTDRIssuance>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:DepositsListClientWise"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<DepositsListClientWiseTDRIssuance>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("LienUnlienMarking")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<string>))]
        public async Task<IActionResult> LienUnlienMarking([FromBody] LienUnlienMarkingModel Model)
        {
            
            
            ActiveResponseSucces<string> response = new ActiveResponseSucces<string>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:LienUnlienMarking"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<string>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }


        [Route("InquireLien")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<InquireLien>>", typeof(ActiveResponseSucces<List<InquireLien>>))]
        public async Task<IActionResult> InquireLien([FromBody] InquireLienModel Model)
        {
            
            
            ActiveResponseSucces<List<InquireLien>> response = new ActiveResponseSucces<List<InquireLien>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:InquireLien"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<InquireLien>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("InquireOutstandingAmt")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<InquireOutstandingAmt>", typeof(ActiveResponseSucces<InquireOutstandingAmt>))]
        public async Task<IActionResult> InquireOutstandingAmt([FromBody] BalanceInquiryModel Model)
        {
            
            
            ActiveResponseSucces<InquireOutstandingAmt> response = new ActiveResponseSucces<InquireOutstandingAmt>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:InquireOutstandingAmt"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<InquireOutstandingAmt>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("DepositsDetails")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<TDRIssuanceModel>))]
        public async Task<IActionResult> DepositsDetails([FromBody] DepositsDetailsModel Model)
        {
            
            
            ActiveResponseSucces<TDRIssuanceModel> response = new ActiveResponseSucces<TDRIssuanceModel>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:DepositsDetails"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<TDRIssuanceModel>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("DepositsList")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<TDRIssuance>>", typeof(ActiveResponseSucces<List<DepositsListTDRIssuance>>))]
        public async Task<IActionResult> DepositsList([FromBody] DepositsListModel Model)
        {
            
            
            ActiveResponseSucces<List<DepositsListTDRIssuance>> response = new ActiveResponseSucces<List<DepositsListTDRIssuance>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:DepositsList"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<DepositsListTDRIssuance>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("CalculateMaturityDate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<MaturityValues>", typeof(ActiveResponseSucces<MaturityValues>))]
        public async Task<IActionResult> CalculateMaturityDate([FromBody] CalculateMaturityDateModel Model)
        {
            
            
            ActiveResponseSucces<MaturityValues> response = new ActiveResponseSucces<MaturityValues>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:CalculateMaturityDate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<MaturityValues>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("CalculateReceiptMaturityDate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.CalculateReceiptMaturityDate.CalculateReceiptMaturityDateResponse>", typeof(ActiveResponseSucces<DBHandler.Model.CalculateReceiptMaturityDate.CalculateReceiptMaturityDateResponse>))]
        public async Task<IActionResult> CalculateReceiptMaturityDate([FromBody] DBHandler.Model.CalculateReceiptMaturityDate.CalculateReceiptMaturityDateModel Model)
        {
            
            
            ActiveResponseSucces<DBHandler.Model.CalculateReceiptMaturityDate.CalculateReceiptMaturityDateResponse> response = new ActiveResponseSucces<DBHandler.Model.CalculateReceiptMaturityDate.CalculateReceiptMaturityDateResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:CalculateReceiptMaturityDate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.CalculateReceiptMaturityDate.CalculateReceiptMaturityDateResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("CalculatePenaltyAmount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CalculatePenaltyAmountResponse>", typeof(ActiveResponseSucces<CalculatePenaltyAmountResponse>))]
        public async Task<IActionResult> CalculatePenaltyAmount([FromBody] CalculatePenaltyAmountModel Model)
        {
            
            
            ActiveResponseSucces<CalculatePenaltyAmountResponse> response = new ActiveResponseSucces<CalculatePenaltyAmountResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:CalculatePenaltyAmount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CalculatePenaltyAmountResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("CloseFixedDeposit")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CloseFXResponse>", typeof(ActiveResponseSucces<CloseFXResponse>))]
        public async Task<IActionResult> CloseFixedDeposit([FromBody] CloseFixedDepositModel Model)
        {
            
            
            ActiveResponseSucces<CloseFXResponse> response = new ActiveResponseSucces<CloseFXResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:CloseFixedDeposit"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CloseFXResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("FDPartialWithdrawal")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<FDPartialWithdrawalResponse>", typeof(ActiveResponseSucces<FDPartialWithdrawalResponse>))]
        public async Task<IActionResult> FDPartialWithdrawal([FromBody] FDPartialWithdrawalModel Model)
        {
            
            
            ActiveResponseSucces<FDPartialWithdrawalResponse> response = new ActiveResponseSucces<FDPartialWithdrawalResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:FDPartialWithdrawal"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<FDPartialWithdrawalResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        


        [Route("DepositAdvice")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DepositAdviceResponse>>", typeof(ActiveResponseSucces<DepositAdviceResponse>))]
        public async Task<IActionResult> DepositAdvice([FromBody] DepositAdviceModel Model)
        {
            
            
            ActiveResponseSucces<DepositAdviceResponse> response = new ActiveResponseSucces<DepositAdviceResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:DepositAdvice"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        _controllerBase != null ? _controllerBase : this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DepositAdviceResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("DepositReceiptwise")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DepositReceiptwiseTDRIssuance>", typeof(ActiveResponseSucces<DepositReceiptwiseTDRIssuance>))]
        public async Task<IActionResult> DepositReceiptwise([FromBody] DepositReceiptwiseModel Model)
        {
            ActiveResponseSucces<DepositReceiptwiseTDRIssuance> response = new ActiveResponseSucces<DepositReceiptwiseTDRIssuance>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:DepositReceiptwise"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DepositReceiptwiseTDRIssuance>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetRollOverReciept")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DBHandler.Model.GetRollOverRecieptResponse>>", typeof(ActiveResponseSucces<List<DBHandler.Model.GetRollOverRecieptResponse>>))]
        public async Task<IActionResult> GetRollOverReciept([FromBody] DBHandler.Model.GetRollOverRecieptModel Model)
        {
            ActiveResponseSucces<List<DBHandler.Model.GetRollOverRecieptResponse>> response = new ActiveResponseSucces<List<DBHandler.Model.GetRollOverRecieptResponse>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:GetRollOverReciept"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<DBHandler.Model.GetRollOverRecieptResponse>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("DepositAdviceV2")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DepositAdviceResponseV2>>", typeof(ActiveResponseSucces<DepositAdviceResponseV2>))]
        public async Task<IActionResult> DepositAdviceV2([FromBody] DepositAdviceModelV2 Model)
        {
            ActiveResponseSucces<DepositAdviceResponseV2> response = new ActiveResponseSucces<DepositAdviceResponseV2>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSTermDepositsAPI:BaseUrl"].ToString(),
                                                                        _config["BSTermDepositsAPI:DepositAdviceV2"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        _controllerBase != null ? _controllerBase : this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DepositAdviceResponseV2>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

    }
}
