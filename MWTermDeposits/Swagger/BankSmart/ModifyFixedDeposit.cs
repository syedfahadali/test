﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWTermDeposits.Swagger.BankSmart.ModifyFixedDeposit
{
    public class ModifyFixedDeposit
    {
    }
    //REQUEST
    public class ModifyFixedDepositRequest : IExamplesProvider<ModifyFDModel>
    {
        public ModifyFDModel GetExamples()
        {
            return new ModifyFDModel()
            {
                ReceiptID = "155",
                AutoProfit="",
                AutoRollover="",
                OptionsAtMaturity="",
                PaymentPeriod="",
                Treatment="",
                

            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
