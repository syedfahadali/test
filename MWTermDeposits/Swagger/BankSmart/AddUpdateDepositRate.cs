﻿using DBHandler.Helper;
using DBHandler.Model;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWTermDeposits.Swagger.BankSmart.AddUpdateDepositRate
{
    //REQUEST
    public class AddUpdateDepositRateRequest : IExamplesProvider<AddUpdateDepositRateModel>
    {
        public AddUpdateDepositRateModel GetExamples()
        {
            return new AddUpdateDepositRateModel()
            {
                ProductId = "TRDM1",
                SerialId = 75,
                EffectiveDate = DateTime.Now,
                EffectiveRate = 1000,
                //MaxSlabAmount = "",
                //MinSlabAmount = "",
                //PreMatureRate = ""
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
