﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.CalculateReceiptMaturityDate;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWTermDeposits.Swagger.BankSmart.CalculateReceiptMaturityDate
{
    //REQUEST
    public class CalculateReceiptMaturityDateRequest : IExamplesProvider<CalculateReceiptMaturityDateModel>
    {
        public CalculateReceiptMaturityDateModel GetExamples()
        {
            return new CalculateReceiptMaturityDateModel()
            {
              ReceiptID="" 
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
