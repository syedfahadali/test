﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWTermDeposits.Swagger.BankSmart.CalculateMaturityDate
{

    //REQUEST
    public class CalculateMaturityDateRequest : IExamplesProvider<CalculateMaturityDateModel>
    {
        public CalculateMaturityDateModel GetExamples()
        {
            return new CalculateMaturityDateModel()
            {
                ProductID = "TDR1M",
                Amount=100,
                NoOfDays=20,
                Rate=12,
                ValueDate=DateTime.Now
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
