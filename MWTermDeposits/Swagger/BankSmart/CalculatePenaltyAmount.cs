﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWTermDeposits.Swagger.BankSmart.CalculatePenaltyAmount
{
    //REQUEST
    public class CalculatePenaltyAmountRequest : IExamplesProvider<CalculatePenaltyAmountModel>
    {
        public CalculatePenaltyAmountModel GetExamples()
        {
            return new CalculatePenaltyAmountModel()
            {
                ReceiptID= "2131",
                PreMatureRate=Convert.ToDecimal(3.5)
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
