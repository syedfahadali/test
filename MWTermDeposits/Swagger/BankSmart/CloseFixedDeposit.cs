﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWTermDeposits.Swagger.BankSmart.CloseFixedDeposit
{
    
    //REQUEST
    public class CloseFixedDepositRequest : IExamplesProvider<CloseFixedDepositModel>
    {
        public CloseFixedDepositModel GetExamples()
        {
            return new CloseFixedDepositModel()
            {
                ReceiptID = "1730",
                ClientId = "0000000489",
                AccountID = "01012000000h70901",
                PenaltyAmount =10,
                Penalty = "N",
                PenaltyRate =000000,
                PenaltyType = "R",
                PreMatureRate=0000000,
                ZeroInterest="N"
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
