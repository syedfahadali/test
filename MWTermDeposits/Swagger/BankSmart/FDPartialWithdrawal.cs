﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWTermDeposits.Swagger.BankSmart.FDPartialWithdrawal
{
    
    //REQUEST
    public class FDPartialWithdrawalRequest : IExamplesProvider<FDPartialWithdrawalModel>
    {
        public FDPartialWithdrawalModel GetExamples()
        {
            return new FDPartialWithdrawalModel()
            {
                ReceiptID = "999",
                AccountID= "0325600014032601",
                PartialAmount=5000,
                PenaltyAmount=0,
                NewReceiptRate=40,
                Penalty="N",
                PenaltyRate=0,
                PenaltyType="R",
                PreMatureRate=2
            
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
