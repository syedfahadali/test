﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWTermDeposits.Swagger.BankSmart.LienUnlienMarking
{

    //REQUEST
    public class LienUnlienMarkingRequest : IExamplesProvider<LienUnlienMarkingModel>
    {
        public LienUnlienMarkingModel GetExamples()
        {
            return new LienUnlienMarkingModel()
            {
                AccountID = "0325100025887101",
                ReceiptID = "19472",
                LienAccountID = "0325100025887101",
                LienAmount = 1000,
                IsLien = "1",
                Remarks = ""
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
