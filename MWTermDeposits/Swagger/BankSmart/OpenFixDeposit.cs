﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWTermDeposits.Swagger.BankSmart.OpenFixDeposit
{

    //REQUEST
    public class OpenFixDepositRequest : IExamplesProvider<OpenFixDepositModel>
    {
        public OpenFixDepositModel GetExamples()
        {
            return new OpenFixDepositModel()
            {
                NoOfIssues = 1,
                TDRIssuance = new TDRIssuanceModel()
                {
                    AccountID = "0315100000016601",
                    Amount = Convert.ToDecimal(10000.0),
                    AutoProfit = "No",
                    AutoRollover = "No",
                    NAccount = "0100200000300001",
                    IssueDate = DateTime.Now,
                    MaturityDate = DateTime.Now,
                    ParentReceiptID = "",
                    PaymentPeriod = "Monthly",
                    ProductID = "TDR1Y",
                    Rate = 5,
                    StartDate = DateTime.Now,
                    Status = "",
                    Treatment = "Principal Plus Profit",
                    ClientId= "000000166",
                    
                }
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
