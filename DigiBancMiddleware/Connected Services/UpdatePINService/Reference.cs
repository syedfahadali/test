﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UpdatePINService
{
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.1-preview-30422-0661")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://cms.ws.eeft.com", ConfigurationName="UpdatePINService.UpdatePinWSBean")]
    public interface UpdatePinWSBean
    {
        
        // CODEGEN: Generating message contract since the operation has multiple return values.
        [System.ServiceModel.OperationContractAttribute(Action="UpdatePin", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Threading.Tasks.Task<UpdatePINService.UpdatePinResponse> UpdatePinAsync(UpdatePINService.UpdatePinRequest request);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.1-preview-30422-0661")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://core.ws.eeft.com")]
    public partial class ServiceHeader
    {
        
        private string serviceIDField;
        
        private string channelIDField;
        
        private string countryCodeField;
        
        private string txnSeqNumField;
        
        private string sourceDateField;
        
        private string sourceTimeField;
        
        private string userIDField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public string ServiceID
        {
            get
            {
                return this.serviceIDField;
            }
            set
            {
                this.serviceIDField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=1)]
        public string ChannelID
        {
            get
            {
                return this.channelIDField;
            }
            set
            {
                this.channelIDField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=2)]
        public string CountryCode
        {
            get
            {
                return this.countryCodeField;
            }
            set
            {
                this.countryCodeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=3)]
        public string TxnSeqNum
        {
            get
            {
                return this.txnSeqNumField;
            }
            set
            {
                this.txnSeqNumField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=4)]
        public string SourceDate
        {
            get
            {
                return this.sourceDateField;
            }
            set
            {
                this.sourceDateField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=5)]
        public string SourceTime
        {
            get
            {
                return this.sourceTimeField;
            }
            set
            {
                this.sourceTimeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=6)]
        public string UserID
        {
            get
            {
                return this.userIDField;
            }
            set
            {
                this.userIDField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.1-preview-30422-0661")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://cms.ws.eeft.com")]
    public partial class Request_UpdatePin
    {
        
        private string tokenCardNumberField;
        
        private string cardSeqNumField;
        
        private string cardExpiryField;
        
        private string cardActivateFlagField;
        
        private string pINField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public string TokenCardNumber
        {
            get
            {
                return this.tokenCardNumberField;
            }
            set
            {
                this.tokenCardNumberField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=1)]
        public string CardSeqNum
        {
            get
            {
                return this.cardSeqNumField;
            }
            set
            {
                this.cardSeqNumField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=2)]
        public string CardExpiry
        {
            get
            {
                return this.cardExpiryField;
            }
            set
            {
                this.cardExpiryField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=3)]
        public string CardActivateFlag
        {
            get
            {
                return this.cardActivateFlagField;
            }
            set
            {
                this.cardActivateFlagField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=4)]
        public string PIN
        {
            get
            {
                return this.pINField;
            }
            set
            {
                this.pINField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.1-preview-30422-0661")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://cms.ws.eeft.com")]
    public partial class Response_UpdatePin
    {
        
        private string tokenCardNumberField;
        
        private string cardSeqNumField;
        
        private string responseCodeField;
        
        private string responseTextField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public string TokenCardNumber
        {
            get
            {
                return this.tokenCardNumberField;
            }
            set
            {
                this.tokenCardNumberField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=1)]
        public string CardSeqNum
        {
            get
            {
                return this.cardSeqNumField;
            }
            set
            {
                this.cardSeqNumField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=2)]
        public string ResponseCode
        {
            get
            {
                return this.responseCodeField;
            }
            set
            {
                this.responseCodeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=3)]
        public string ResponseText
        {
            get
            {
                return this.responseTextField;
            }
            set
            {
                this.responseTextField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.1-preview-30422-0661")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="UpdatePinRequest", WrapperNamespace="http://cms.ws.eeft.com", IsWrapped=true)]
    public partial class UpdatePinRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://core.ws.eeft.com", Order=0)]
        [System.Xml.Serialization.XmlElementAttribute(Namespace="http://core.ws.eeft.com")]
        public UpdatePINService.ServiceHeader ServiceHeader;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://cms.ws.eeft.com", Order=1)]
        public UpdatePINService.Request_UpdatePin Request_UpdatePin;
        
        public UpdatePinRequest()
        {
        }
        
        public UpdatePinRequest(UpdatePINService.ServiceHeader ServiceHeader, UpdatePINService.Request_UpdatePin Request_UpdatePin)
        {
            this.ServiceHeader = ServiceHeader;
            this.Request_UpdatePin = Request_UpdatePin;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.1-preview-30422-0661")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="UpdatePinResponse", WrapperNamespace="http://cms.ws.eeft.com", IsWrapped=true)]
    public partial class UpdatePinResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://core.ws.eeft.com", Order=0)]
        [System.Xml.Serialization.XmlElementAttribute(Namespace="http://core.ws.eeft.com")]
        public UpdatePINService.ServiceHeader ServiceHeader;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://cms.ws.eeft.com", Order=1)]
        public UpdatePINService.Response_UpdatePin Response_UpdatePin;
        
        public UpdatePinResponse()
        {
        }
        
        public UpdatePinResponse(UpdatePINService.ServiceHeader ServiceHeader, UpdatePINService.Response_UpdatePin Response_UpdatePin)
        {
            this.ServiceHeader = ServiceHeader;
            this.Response_UpdatePin = Response_UpdatePin;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.1-preview-30422-0661")]
    public interface UpdatePinWSBeanChannel : UpdatePINService.UpdatePinWSBean, System.ServiceModel.IClientChannel
    {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.1-preview-30422-0661")]
    public partial class UpdatePinWSBeanClient : System.ServiceModel.ClientBase<UpdatePINService.UpdatePinWSBean>, UpdatePINService.UpdatePinWSBean
    {
        
        /// <summary>
        /// Implement this partial method to configure the service endpoint.
        /// </summary>
        /// <param name="serviceEndpoint">The endpoint to configure</param>
        /// <param name="clientCredentials">The client credentials</param>
        static partial void ConfigureEndpoint(System.ServiceModel.Description.ServiceEndpoint serviceEndpoint, System.ServiceModel.Description.ClientCredentials clientCredentials);
        
        public UpdatePinWSBeanClient() : 
                base(UpdatePinWSBeanClient.GetDefaultBinding(), UpdatePinWSBeanClient.GetDefaultEndpointAddress())
        {
            this.Endpoint.Name = EndpointConfiguration.UpdatePinWSBeanPort.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public UpdatePinWSBeanClient(EndpointConfiguration endpointConfiguration) : 
                base(UpdatePinWSBeanClient.GetBindingForEndpoint(endpointConfiguration), UpdatePinWSBeanClient.GetEndpointAddress(endpointConfiguration))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public UpdatePinWSBeanClient(EndpointConfiguration endpointConfiguration, string remoteAddress) : 
                base(UpdatePinWSBeanClient.GetBindingForEndpoint(endpointConfiguration), new System.ServiceModel.EndpointAddress(remoteAddress))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public UpdatePinWSBeanClient(EndpointConfiguration endpointConfiguration, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(UpdatePinWSBeanClient.GetBindingForEndpoint(endpointConfiguration), remoteAddress)
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public UpdatePinWSBeanClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress)
        {
        }
        
        public System.Threading.Tasks.Task<UpdatePINService.UpdatePinResponse> UpdatePinAsync(UpdatePINService.UpdatePinRequest request)
        {
            return base.Channel.UpdatePinAsync(request);
        }
        
        public virtual System.Threading.Tasks.Task OpenAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginOpen(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndOpen));
        }
        
        public virtual System.Threading.Tasks.Task CloseAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginClose(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndClose));
        }
        
        private static System.ServiceModel.Channels.Binding GetBindingForEndpoint(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.UpdatePinWSBeanPort))
            {
                System.ServiceModel.BasicHttpBinding result = new System.ServiceModel.BasicHttpBinding();
                result.MaxBufferSize = int.MaxValue;
                result.ReaderQuotas = System.Xml.XmlDictionaryReaderQuotas.Max;
                result.MaxReceivedMessageSize = int.MaxValue;
                result.AllowCookies = true;
                return result;
            }
            throw new System.InvalidOperationException(string.Format("Could not find endpoint with name \'{0}\'.", endpointConfiguration));
        }
        
        private static System.ServiceModel.EndpointAddress GetEndpointAddress(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.UpdatePinWSBeanPort))
            {
                return new System.ServiceModel.EndpointAddress("http://10.13.139.89:9080/ITMWS/services/UpdatePinWSBeanPort");
            }
            throw new System.InvalidOperationException(string.Format("Could not find endpoint with name \'{0}\'.", endpointConfiguration));
        }
        
        private static System.ServiceModel.Channels.Binding GetDefaultBinding()
        {
            return UpdatePinWSBeanClient.GetBindingForEndpoint(EndpointConfiguration.UpdatePinWSBeanPort);
        }
        
        private static System.ServiceModel.EndpointAddress GetDefaultEndpointAddress()
        {
            return UpdatePinWSBeanClient.GetEndpointAddress(EndpointConfiguration.UpdatePinWSBeanPort);
        }
        
        public enum EndpointConfiguration
        {
            
            UpdatePinWSBeanPort,
        }
    }
}
