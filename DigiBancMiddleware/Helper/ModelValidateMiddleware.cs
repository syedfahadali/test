﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CommonModel;
using DBHandler.Helper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;

namespace DigiBancMiddleware.Helper
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class ModelValidateMiddleware
    {
        private readonly RequestDelegate _next;

        public ModelValidateMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task<ActiveResponseSucces<Object>> Invoke(HttpContext httpContext)
        {
            string data = GetRequestBodyInString(httpContext);
            ActiveResponseSucces<Object> OBJ = new ActiveResponseSucces<Object>();

            if (data == "")
            {
                await _next(httpContext).ConfigureAwait(false);          
            }
            else
            {
                var originalBodyStream = httpContext.Response.Body;
               // DTO model = JsonConvert.DeserializeObject<DTO>(data);
               // var context = new ValidationContext(model, serviceProvider: null, items: null);
                var validationResults = new List<ValidationResult>();
               //bool isValid = Validator.TryValidateObject(model, context, validationResults, true);
                //var ModelState = httpContext.Features.Get<ModelStateFeature>()?.ModelState;   
                //bool isModelValid = Controller.TryValidateModel(model);
                //if (!isModelValid)
                //{
                //    var errors = ModelState
                //    .Where(x => x.Value.Errors.Count > 0)
                //    .Select(x => new { x.Key, x.Value.Errors })
                //    .ToArray();

                //    string errormessage = "";
                //    foreach (var e in errors) 
                //    {
                //        errormessage += e.Key.ToUpper() + ":" + e.Errors[0].ErrorMessage + ",  ";
                //    }
                //    //status = new DBHandler.Helper.Status { Severity = Severity.Error, StatusMessage = errormessage, Code = "ERROR-02" };

                //    //level = Level.Error;
                //    //excetionForLog = new Exception(status.StatusMessage);
                ////}

                return OBJ;
            }
            return OBJ;

        }

        




        private static async Task MockRequestBody(HttpContext context, string jsonOriginalParamModel)
        {
            var requestContent = new StringContent(jsonOriginalParamModel, Encoding.UTF8);
            var stream = context.Request.Body;
            stream = await requestContent.ReadAsStreamAsync().ConfigureAwait(false);
            context.Request.Body = stream;
        }

        private static string GetRequestBodyInString(HttpContext context)
        {
            //context.Request.EnableRewind();
            string bodyStr = string.Empty;
            using (StreamReader reader = new StreamReader(context.Request.Body, Encoding.UTF8, true, 1024, true))
            {
                bodyStr = reader.ReadToEnd();
            }
            //context.Request.Body.Position = 0;
            return bodyStr;
        }

        private async Task<string> FormatResponse(HttpResponse response)
        {
            //We need to read the response stream from the beginning...
            response.Body.Seek(0, SeekOrigin.Begin);

            //...and copy it into a string
            string text = await new StreamReader(response.Body).ReadToEndAsync();

            //We need to reset the reader for the response so that the client can read it.
            response.Body.Seek(0, SeekOrigin.Begin);

            //Return the string for the response, including the status code (e.g. 200, 404, 401, etc.)
            return $"{response.StatusCode}: {text}";
        }


        private async Task<string> GetResponseBodyInString(HttpContext context, Stream originalBodyStream)
        {
            string bodyStr = string.Empty;
            //var originalBodyStream = context.Response.Body;

            try
            {
                //var originalBodyStream = context.Response.Body;

                //Create a new memory stream...
                using (var responseBody = new MemoryStream())
                {
                    //...and use that for the temporary response body
                    context.Response.Body = responseBody;

                    //Continue down the Middleware pipeline, eventually returning to this class
                    //await _next(context);

                    //Format the response from the server
                    var response = await FormatResponse(context.Response);

                    //TODO: Save log to chosen datastore

                    //Copy the contents of the new memory stream (which contains the response) to the original stream, which is then returned to the client.
                    await responseBody.CopyToAsync(originalBodyStream);
                }


                //context.Response.en
                ////context.Request.EnableRewind();
                //using (StreamReader reader = new StreamReader(context.Response.Body, Encoding.UTF8, true, 1024, true))
                //{
                //    bodyStr = reader.ReadToEnd();
                //}
                //context.Request.Body.Position = 0;
            }
            catch (Exception ex)
            {

            }
            return bodyStr;
        }

        private string GetRequestData(HttpContext context)
        {
            throw new NotImplementedException();
        }

        public class ErrorResponse
        {
            //[DataMember(Name = "Message")]
            public string Message { get; set; }

            public ErrorResponse(string message)
            {
                Message = message;
            }
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class ModelValidateMiddlewareExtensions
    {
        public static IApplicationBuilder UseModelValidateMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ModelValidateMiddleware>();
        }
    }

    public class ValidateModelFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                context.Result = new BadRequestObjectResult(context.ModelState);
            }
        }
    }
}



