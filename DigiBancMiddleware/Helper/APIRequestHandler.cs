﻿using DBHandler.Enum;
using DBHandler.Model;
using DBHandler.Repositories;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DBHandler.Helper;
using HttpHandler;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using CommonModel;
using System.Text;
using System.Net.Http;
using DBHandler.Common;
using Microsoft.AspNetCore.Routing;
using System.IO;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;
using System.Net.Mail;
using CommonModel.SMS;

namespace VendorApi.Helper
{
    public class APIRequestHandler
    {
        public static async Task<InternalResult> GetResponse(   object Model,
                                                               string baseurl,
                                                               string url,
                                                               Level level,
                                                               ILogRepository _logs,
                                                               IUserChannelsRepository _userChannels,
                                                               ControllerBase controllerBase,
                                                               IConfiguration _config,
                                                               HttpHandler.HttpHandler httpHandler,
                                                               int type = 1,
                                                               int isGet = 1,
                                                               string newBody = "",
                                                               string toke = "",
                                                               string StartTime = "")
        {
          
            DBHandler.Helper.ActiveResponseSucces<string> res = new DBHandler.Helper.ActiveResponseSucces<string>()
            {
                LogId = controllerBase.Request.Headers["LogId"],
                Content = null,
                RequestDateTime = Convert.ToDateTime(controllerBase.Request.Headers["DateTime"])
            };

            Exception excetionForLog = null;
            DBHandler.Helper.Status status;
            DateTime starTime = StartTime == "" ? DateTime.Now : DateTime.Parse(StartTime);
            DBHandler.Helper.APIHelper helper = new DBHandler.Helper.APIHelper(_userChannels, _logs);
            HttpContext ctx = controllerBase.Request?.HttpContext;
            var id = controllerBase.HttpContext.User.Claims.First().Value;
            RequestResponse httpResult = null;
            CommonHelper common = new CommonHelper(_logs,_config);
            string body = "";
            DateTime ApiRequestStartTime = DateTime.Now;
            DateTime ApiRequestEndTime = DateTime.Now;

            string httpMethods = "";

            status = ValidateModel(controllerBase, Model);
            if (status != null)
            {
                level = Level.Error;
                excetionForLog = new Exception(status.StatusMessage);
            }
            else
            {
                status = new Status() { Code = null };

                Dictionary<string, string> header = null;

                switch (type)
                {
                    case 1:
                        header = common.GetHeaderForBankSmart(ctx);
                        break;
                 
                    default:
                        header = common.GetHeader();
                        break;
                }

                body = JsonConvert.SerializeObject(Model);
                body = string.IsNullOrEmpty(newBody) ? body : newBody;
                ApiRequestStartTime = DateTime.Now;
                switch (isGet)
                {
                    case 23:
                        httpResult = await httpHandler.PostJsonData(baseurl, url, header, body, _config).ConfigureAwait(false);
                        httpMethods = "Post";
                        break;
                    case 2:
                        httpResult = await httpHandler.GetJsonData(baseurl, url, header, _config).ConfigureAwait(false);
                        httpMethods = "Get";
                        break;
                    case 3:
                        httpResult = await httpHandler.PutJsonData(baseurl, url, header, body, _config).ConfigureAwait(false);
                        httpMethods = "Put";
                        break;
                    case 33:
                        httpResult = await httpHandler.DeleteJsonData(baseurl, url, header, body, _config).ConfigureAwait(false);
                        httpMethods = "Delete";
                        break;
                    case 44:
                        header.Remove("Content-Type");
                        header.Add("Content-Type", "multipart/form-data; boundary=--------------------------845235239682787180694175");
                        httpResult = await httpHandler.PostFormData(baseurl, url, header, (Dictionary<string, string>)Model, _config).ConfigureAwait(false);
                        httpMethods = "PostFromData";
                        break;
                    case 55:
                        httpResult = await httpHandler.PostFormData(baseurl, url, header, (MultipartFormDataContent)Model, _config).ConfigureAwait(false);
                        httpMethods = "PostFromData";
                        break;
                    case 66:
                        httpResult = await httpHandler.PostFormData(baseurl, url, header, (Dictionary<string, string>)Model, _config).ConfigureAwait(false);
                        httpMethods = "PostFormData";
                        break;
                    case 77:
                        httpResult = await httpHandler.PostJsonDataDigest(baseurl, url, header, body, getTmsCredentials(_logs), _config).ConfigureAwait(false);
                        httpMethods = "PostJsonDataDigest";
                        break;
                    case 88:
                        httpResult = await httpHandler.GetJsonDataDigest(baseurl, url, header, getTmsCredentials(_logs), _config).ConfigureAwait(false);
                        httpMethods = "GetJsonDataDigest";
                        break;
                    case 99:
                        httpResult = await httpHandler.PutKafkaData(baseurl, url, body, _config).ConfigureAwait(false);
                        httpMethods = "PutFromData";
                        break;
                    case 111:
                        body = JsonConvert.SerializeObject(Model);
                        httpResult = new RequestResponse();
                        httpResult.httpResponse = newBody;
                        break;
                    case 222:
                        try {
                            ActiveResponseSucces<object> smtp_response = new ActiveResponseSucces<object>();
                            CommonModel.SMTP_Email.Email.EmailResponse emailResponse = new CommonModel.SMTP_Email.Email.EmailResponse();

                            CommonModel.SMTP_Email.Email.EmailRequest emailRequest = (CommonModel.SMTP_Email.Email.EmailRequest)Model;
                            MailMessage mailMessage = new MailMessage();

                            mailMessage.From = new MailAddress(emailRequest.FromEmail);
                            mailMessage.Body = emailRequest.EmailBody;

                            SmtpClient client = new SmtpClient("smtp.gmail.com");
                            foreach (CommonModel.SMTP_Email.Email.Recipient ToEMailId in emailRequest.Recipient)
                            {
                                mailMessage.To.Add(new MailAddress(ToEMailId.Email, ToEMailId.Name));
                            }
                            foreach (CommonModel.SMTP_Email.Email.CC CCEMailId in emailRequest.CC)
                            {
                                mailMessage.CC.Add(new MailAddress(CCEMailId.Email, CCEMailId.Name));
                            }
                            foreach (CommonModel.SMTP_Email.Email.BCC BCCEMailId in emailRequest.BCC)
                            {
                                mailMessage.Bcc.Add(new MailAddress(BCCEMailId.Email, BCCEMailId.Name));
                            }

                            foreach (CommonModel.SMTP_Email.Email.Attachment file in emailRequest.Attachments)
                            {
                                //byte[] bytes = System.Text.ASCIIEncoding.ASCII.GetBytes(file.File);
                                //MemoryStream stream = new MemoryStream(bytes);
                                //mailMessage.Attachments.Add(new Attachment(stream, file.FileName));
                                var bytes1 = Convert.FromBase64String(file.File);
                                //string v = System.Convert.ToBase64String(bytes1);
                                MemoryStream stream = new MemoryStream(bytes1);
                                mailMessage.Attachments.Add(new Attachment(stream, file.FileName));
                            }

                            client.Port = 587;
                            client.UseDefaultCredentials = false;
                            client.Credentials = new System.Net.NetworkCredential("it-admin@masriacard.com", "cP&$YcmJJXhe");
                            client.EnableSsl = true;
                            client.Send(mailMessage);

                            smtp_response.Content = emailResponse.Success = "Success";
                            smtp_response.Status = new Status() { Code = "MSG-000000", Severity = Severity.Success, StatusMessage = "Success" };

                            httpResult = new RequestResponse() { StatusCode = System.Net.HttpStatusCode.BadRequest, severity = Severity.Success, httpResponse = "Success" };

                        }

                        catch (Exception ex)
                        {
                            httpResult = new RequestResponse() { StatusCode = System.Net.HttpStatusCode.BadRequest, severity = "Error", httpResponse = ex.ToString() };
                        }

                        httpMethods = "Post";
                        break;

                    default:
                        if (type != 13)
                        {
                            httpResult = await httpHandler.PostJsonData(baseurl, url, header, body, false, _config).ConfigureAwait(false);
                            httpMethods = "Post";

                        }
                        else
                        {
                            httpResult = await httpHandler.PostJsonData(baseurl, url, header, body, true, _config).ConfigureAwait(false);
                            httpMethods = "Post";
                        }
                        break;
                }
                ApiRequestEndTime = DateTime.Now;
            }

            if (httpResult! != null && (httpResult.StatusCode != System.Net.HttpStatusCode.OK && httpResult.StatusCode != System.Net.HttpStatusCode.BadRequest))
            {
                status = new DBHandler.Helper.Status() { Severity = Severity.Error, Code = "ERROR-"+ ((int)httpResult.StatusCode), StatusMessage = httpResult.StatusCode.ToString()};
            }


            string severity = httpResult != null ? httpResult.severity : "";
            string response = httpResult == null ? JsonConvert.SerializeObject(status) : httpResult.httpResponse;
            System.Net.HttpStatusCode code = httpResult != null ? httpResult.StatusCode : System.Net.HttpStatusCode.OK;

            string clientip = Utility.GetIP(controllerBase.Request.HttpContext);
            string currentController = "";
            string currentAction = "";
            var RouteInfo = controllerBase.Request.HttpContext.GetRouteData().Values;
            if (RouteInfo.Keys.Count > 0)
            {
                currentController = RouteInfo["controller"].ToString() == null ? "" : RouteInfo["controller"].ToString();
                currentAction = RouteInfo["action"].ToString() == null ? "" : RouteInfo["action"].ToString();
            }
            if (bool.Parse(_config["GlobalSettings:IsLogEnabled"]))
            {


                //Task.Run(() =>
                _logs.Logs(level, severity,
                            controllerBase.Request.HttpContext,
                            excetionForLog,
                            starTime,
                            DateTime.Now,
                            body,
                            response,
                            id,
                            controllerBase.Request.Headers["ChannelId"],
                            controllerBase.Request.Headers["LogId"],
                            // Guid.NewGuid().ToString(),
                            Convert.ToDateTime(controllerBase.Request.Headers["DateTime"]), code, baseurl, url, ApiRequestStartTime,
                            ApiRequestEndTime, clientip, currentController, currentAction, _config["ErrorLog"], _config["SuccessLog"], _config["InfoLog"], "","","",httpMethods);//);
            }

            return new InternalResult() { Status = status, httpResult = httpResult };
        }

        private static Dictionary<string, string> getTmsCredentials(ILogRepository _logs)
        {
            var credentials = _logs.GetHeaders("TMSCredentials", 3);
            Dictionary<string, string> credential = new Dictionary<string, string>();
            foreach (var item in credentials)
            {
                credential.Add(item.HeaderName, item.HeaderValue);
            }
            return credential;
        }

        public static DBHandler.Helper.Status ValidateModel(ControllerBase controllerBase, object Model)
        {
            DBHandler.Helper.Status status;

            bool isModelValid = controllerBase.TryValidateModel(Model);
            if (!isModelValid)
            {
                var errors = controllerBase.ModelState
                .Where(x => x.Value.Errors.Count > 0)
                .Select(x => new { x.Key, x.Value.Errors })
                .ToArray();

                string errormessage = "";
                foreach (var e in errors)
                {
                    errormessage += e.Key.ToUpper() + ":" + e.Errors[0].ErrorMessage + ",  ";
                }
                status = new DBHandler.Helper.Status { Severity = Severity.Error, StatusMessage = errormessage, Code = "ERROR-02" };
                return status;
            }
            return null;
        }
    }
}
