﻿
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using DBHandler.Repositories;
using DBHandler.Model;
using CommonModel;
using CoreHandler.Model;
using DBHandler.Helper;
using DBHandler.Enum;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.Controllers;
using DBHandler.Model.LoanAdvice;
using Newtonsoft.Json.Linq;
using System.Web;
using Microsoft.AspNetCore.Http.Extensions;

namespace DigiBancMiddleware.Helper
{
    public class RequestMiddleware : ControllerBase
    {
        private readonly RequestDelegate next;
        private IConfiguration _config;

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseMiddleware"/> class.
        /// </summary>
        /// <param name="next"></param>
        /// <param name="executor"></param>
        /// <param name="loggerFactory"></param>
        public RequestMiddleware(RequestDelegate next, IActionResultExecutor<ObjectResult> executor, ILoggerFactory loggerFactory, IConfiguration config)
        {
            this.next = next;
           
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="context"></param>
        /// <param name="log"></param>
        /// <param name="config"></param>
        /// <returns>A <see cref="Task"/> representing the result of the asynchronous operation.</returns>
        public async Task Invoke(HttpContext context, ILogRqChRepository log, ILogRepository mainlog, IConfiguration config, IUserChannelsRepository _userChannels, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
        {
             string data = GetRequestBodyInString(context);
            _config = config;

            if (string.IsNullOrEmpty(data))
            {
                data = context.Request.GetEncodedUrl();
            }
            
            DBHandler.Helper.APIHelper helper = new DBHandler.Helper.APIHelper(_userChannels, mainlog);
            var authHeader = AuthenticationHeaderValue.Parse(context.Request.Headers["Authorization"]);
            var credentialBytes = Convert.FromBase64String(authHeader.Parameter);
            var credentials = Encoding.UTF8.GetString(credentialBytes).Split(':');
            var username = credentials[0];
            var user = await userManager.FindByNameAsync(username);
            var id = "";
            if (user != null)
            {
                id = user.Id;
            }
          //  DTO dto = new DTO();
            var originalBodyStream = context.Response.Body;
            //dto = Utility.ReadFromHeaders(context);
            DBHandler.Helper.ActiveResponseSucces<string> result = new DBHandler.Helper.ActiveResponseSucces<string>()
            {
                LogId = context.Request.Headers["LogId"],
                Content = null,
                RequestDateTime = Convert.ToDateTime(context.Request.Headers["DateTime"])
            };
            await MockRequestBody(context, data);
                //await MockRequestBody(context, JsonConvert.SerializeObject(jobject));

                using (var responseBody = new MemoryStream())
                {
                try
                {
                    context.Response.Body = responseBody;
                    DBHandler.Helper.ActiveResponseSucces<string> res = new DBHandler.Helper.ActiveResponseSucces<string>()
                    {
                        LogId = context.Request.Headers["LogId"],
                        Content = null,
                        RequestDateTime = Convert.ToDateTime(context.Request.Headers["DateTime"])
                    };

                    await next(context).ConfigureAwait(false);
                    var response = await FormatResponse(context.Response);
                    context.Response.Body = responseBody;

                    string logId = "";
                    string channelID = "";
                    DateTime requestDateTime = DateTime.Now;
                    if (res != null && !string.IsNullOrEmpty(context.Request.Headers["LogId"]))
                    {
                        logId = context.Request.Headers["LogId"];
                        channelID = context.Request.Headers["ChannelId"];
                        requestDateTime = Convert.ToDateTime(context.Request.Headers["DateTime"]);
                    }
                    else
                    {
                        logId = Guid.NewGuid().ToString();
                        channelID = "";
                    }
                    string actionName = context.GetEndpoint().Metadata.GetMetadata<ControllerActionDescriptor>().ActionName;
                    string controllerName = context.GetEndpoint().Metadata.GetMetadata<ControllerActionDescriptor>().ControllerName;

                    if (bool.Parse(config["GlobalSettings:IsLogEnabled"]))
                    {
                        //if (context.Response.Headers.ContainsKey("Authorization"))
                        //{
                        //    context.Response.Headers.Remove("Authorization");
                        //}
                        //return Task.FromResult(0);
                        //authHeader = context.Request.Headers.Remove("Authorization");
                        string header = context.Request.Headers.ContainsKey("Authorization").ToString();
                        if (context.Request.Headers.ContainsKey("Authorization"))
                        {
                            context.Request.Headers.Remove("Authorization");
                        }
                        header = JsonConvert.SerializeObject(context.Request.Headers);

                        //string var = context.Request.Headers.ContainsKey("Authorization").ToString();
                        //if (context.Request.Headers.ContainsKey("Authorization"))
                        //{
                        //    context.Request.Headers.Remove("Authorization");
                        //}
                        log.LogData(logId, channelID, requestDateTime, data, response, actionName, controllerName, header);//);
                    }
                    await responseBody.CopyToAsync(originalBodyStream);
                }
                catch (Exception ex)
                {
                    var starttime = DateTime.Now;
                    string clientip = Utility.GetIP(context.Request.HttpContext);
                    string currentController = "";
                    string currentAction = "";
                    var RouteInfo = context.Request.HttpContext.GetRouteData().Values;
                    if (RouteInfo.Keys.Count > 0)
                    {
                        currentController = RouteInfo["controller"].ToString() == null ? "" : RouteInfo["controller"].ToString();
                        currentAction = RouteInfo["action"].ToString() == null ? "" : RouteInfo["action"].ToString();
                    }

                    string logId = "";
                    string channelID = "";
                    DateTime requestDateTime = DateTime.Now;
                    if (result != null && !string.IsNullOrEmpty(context.Request.Headers["LogId"]))
                    {
                        logId = context.Request.Headers["LogId"];
                        channelID = context.Request.Headers["ChannelId"];
                        requestDateTime = Convert.ToDateTime(context.Request.Headers["DateTime"]);
                    }
                    else
                    {
                        logId = Guid.NewGuid().ToString();
                        channelID = "";
                    }
                    DBHandler.Helper.ActiveResponseSucces<string> res = new DBHandler.Helper.ActiveResponseSucces<string>();
                    if (ex != null && ex.Message != null &&
                        (ex.Message == "One or more errors occurred. (No connection could be made because the target machine actively refused it.)" ||
                        ex.Message == "No connection could be made because the target machine actively refused it." ||
                        ex.Message.Contains("A connection attempt failed because the connected party") ||
                        ex.Message.Contains("No such host is known")))
                    {
                        res.Status = new DBHandler.Helper.Status { Severity = Severity.Error, StatusMessage = "One or more errors occurred. (No connection could be made because the target machine actively refused it.)", Code = "ERROR-01" };
                    }
                    else if (ex != null && ex.Message != null && (ex.Message == "A task was canceled." || ex.Message == "The operation was canceled."))
                    {
                        res.Status = new DBHandler.Helper.Status { Severity = Severity.Error, StatusMessage = "Request has been timedout, 3 mints are default timeout.", Code = "ERROR-01" };

                    }
                    else
                    {
                        res.Status = new DBHandler.Helper.Status { Severity = Severity.Error, StatusMessage = "Response is not as expected in " + currentAction + " function.", Code = "ERROR-01" };

                    }

                    res.Content = null;
                    res.LogId = logId;
                    res.RequestDateTime = requestDateTime;
                    context.Response.ContentType = "application/json";
                    context.Response.StatusCode = 400;

                    var requestResponse = JsonConvert.SerializeObject(res);
                    Log model = new Log();
                    model.Application = "Global Handler Exception";
                    model.FunctionName = currentAction;
                    model.ControllerName = currentController;
                    model.DeviceID = "Exception DeviceID";
                    model.IP = "Exception ip";
                    model.StartTime = starttime;
                    model.EndTime = DateTime.Now;
                    model.Exception = ex.ToString();
                    model.Message = ex.Message;
                    model.Level = Level.Error.ToString();
                    model.Status = "Exception";
                    model.RequestResponse = requestResponse;
                    model.RequestParameters = data;
                    model.LogId = logId;
                    model.RequestDateTime = DateTime.Now;
                    model.ErrorCode = "Error-01";
                    model.Channel = channelID;

                    if (bool.Parse(config["GlobalSettings:IsLogEnabled"]))
                    {

                        mainlog.Logs(Level.Error, "", context, ex, DateTime.Now, DateTime.Now,
                               model.RequestParameters, model.RequestResponse, model.UserID, model.Channel, model.LogId, model.RequestDateTime, System.Net.HttpStatusCode.InternalServerError, "", "", DateTime.Now, DateTime.Now, clientip, currentController, currentAction
                               , _config["ErrorLog"], _config["SuccessLog"], _config["InfoLog"], "", "", "");
                    }

                    if (bool.Parse(config["GlobalSettings:IsLogEnabled"]))
                    {
                        string header = JsonConvert.SerializeObject(context.Request.Headers);

                        if (data == "")
                        {
                            Uri myUri = new Uri(context.Request.GetEncodedUrl());
                            //string param1 = HttpUtility.ParseQueryString(myUri.Query).Get("[0]").ToString();
                            string getQueryParameter = HttpUtility.ParseQueryString(myUri.Query).ToString();
                            data = getQueryParameter;
                        }

                        log.LogData(logId, channelID, requestDateTime, data, requestResponse, currentAction, currentController, header);
                    }
                    byte[] b = System.Text.ASCIIEncoding.ASCII.GetBytes(requestResponse);
                    originalBodyStream.Write(b);
                    await responseBody.CopyToAsync(originalBodyStream);
                }
            }
        }

        private static async Task MockRequestBody(HttpContext context, string jsonOriginalParamModel)
        {
            var requestContent = new StringContent(jsonOriginalParamModel, Encoding.UTF8);
            var stream = context.Request.Body;
            stream = await requestContent.ReadAsStreamAsync().ConfigureAwait(false);
            context.Request.Body = stream;
        }

        private static string GetRequestBodyInString(HttpContext context)
        {
            //context.Request.EnableRewind();
            string bodyStr = string.Empty;
            using (StreamReader reader = new StreamReader(context.Request.Body, Encoding.UTF8, true, 1024, true))
            {
                bodyStr = reader.ReadToEnd();
            }
            //context.Request.Body.Position = 0;
            return bodyStr;
        }


        private async Task<string> FormatResponse(HttpResponse response)
        {
            //We need to read the response stream from the beginning...
            response.Body.Seek(0, SeekOrigin.Begin);

            //...and copy it into a string
            string text = await new StreamReader(response.Body).ReadToEndAsync();

            //We need to reset the reader for the response so that the client can read it.
            response.Body.Seek(0, SeekOrigin.Begin);

            //Return the string for the response, including the status code (e.g. 200, 404, 401, etc.)
            return $"{text}";
        }

        private async Task<string> GetResponseBodyInString(HttpContext context, Stream originalBodyStream)
        {
            string bodyStr = string.Empty;
            //var originalBodyStream = context.Response.Body;

            try
            {
                //var originalBodyStream = context.Response.Body;

                //Create a new memory stream...
                using (var responseBody = new MemoryStream())
                {
                    //...and use that for the temporary response body
                    context.Response.Body = responseBody;

                    //Continue down the Middleware pipeline, eventually returning to this class
                    //await _next(context);

                    //Format the response from the server
                    var response = await FormatResponse(context.Response);

                    //TODO: Save log to chosen datastore

                    //Copy the contents of the new memory stream (which contains the response) to the original stream, which is then returned to the client.
                    await responseBody.CopyToAsync(originalBodyStream);
                }


                //context.Response.en
                ////context.Request.EnableRewind();
                //using (StreamReader reader = new StreamReader(context.Response.Body, Encoding.UTF8, true, 1024, true))
                //{
                //    bodyStr = reader.ReadToEnd();
                //}
                //context.Request.Body.Position = 0;
            }
            catch (Exception ex)
            {

            }
            return bodyStr;
        }

        private string GetRequestData(HttpContext context)
        {
            throw new NotImplementedException();
        }

        public class ErrorResponse
        {
            //[DataMember(Name = "Message")]
            public string Message { get; set; }

            public ErrorResponse(string message)
            {
                Message = message;
            }
        }
    }
}
