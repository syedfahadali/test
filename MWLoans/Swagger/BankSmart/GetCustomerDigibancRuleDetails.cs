﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWLoans.Swagger.BankSmart.GetCustomerDigibancRuleDetails
{
    
    //REQUEST
    public class GetCustomerDigibancRuleDetailsRequest : IExamplesProvider<CustomerDigibancRuleDetails>
    {
        public CustomerDigibancRuleDetails GetExamples()
        {
            return new CustomerDigibancRuleDetails()
            {
                cif ="",
                dob = DateTime.Now,
                LastMonthSalary = "",
                LastMonthSalaryCount = "",
                WorkingTenureWithCurrentEmployer = 123
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
