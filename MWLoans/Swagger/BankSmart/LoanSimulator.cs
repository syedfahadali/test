﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace MWLoans.Swagger.BankSmart.LoanSimulator
{
    //REQUEST
    public class LoanSimulatorRequest : IExamplesProvider<LoanSimulatorDto>
    {
        public LoanSimulatorDto GetExamples()
        {
            return new LoanSimulatorDto()
            {
                LoanAmount = 10000,
                InterestRate = 5,
                Method = "B",
                RePaymentMode = "M",
                ManualSchedule = new List<ManualSchedule>()
                {
                    new ManualSchedule()
                    {

                    }
                },
                MaturityDate = DateTime.Now,
                StartDate = DateTime.Now
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
