﻿using DBHandler.Helper;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWLoans.Swagger.BankSmart.FacilityInquiry
{ 
    //REQUEST
    public class FacilityInquiryRequest : IExamplesProvider<DBHandler.Model.Dtos.FacilityInquiry>
    {
        public DBHandler.Model.Dtos.FacilityInquiry GetExamples()
        {
            return new DBHandler.Model.Dtos.FacilityInquiry()
            {
                FacilityID = "zHviypbJdeyvqNTuO9TbbjS2c",
                
                
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}

}
