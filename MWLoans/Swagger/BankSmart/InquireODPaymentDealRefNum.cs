﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using DBHandler.Model.Loan;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWLoans.Swagger.BankSmart.InquireODPaymentDealRefNum
{

    //REQUEST
    public class InquireODPaymentDealRefNumRequest : IExamplesProvider<InquireODPaymentDealRefNumModel>
    {
        public InquireODPaymentDealRefNumModel GetExamples()
        {
            return new InquireODPaymentDealRefNumModel()
            {
                ClientID = "123123123",
                dealRefNo = "123"
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
