﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWLoans.Swagger.BankSmart.FeeChargesReversal
{

    //REQUEST
    public class FeeChargesReversalRequest : IExamplesProvider<DBHandler.Model.FeeChargesReversal.FeeChargesReversalModel>
    {
        public DBHandler.Model.FeeChargesReversal.FeeChargesReversalModel GetExamples()
        {
            return new DBHandler.Model.FeeChargesReversal.FeeChargesReversalModel()
            {
                ReversalAmt =1234,
                ClientId = "",
                FacId = "",
                FeeType = "",
                ProdId = "",
                RefNo = "",
                ReversalReason = "",
                ReversalRemark = "",
                ReversalType = ""
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
