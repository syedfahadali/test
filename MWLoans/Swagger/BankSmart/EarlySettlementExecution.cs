﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWLoans.Swagger.BankSmart.EarlySettlementExecution
{

    //REQUEST
    public class EarlySettlementExecutionRequest : IExamplesProvider<DBHandler.Model.EarlySettlementExecution.EarlySettlementExecutionModel>
    {
        public DBHandler.Model.EarlySettlementExecution.EarlySettlementExecutionModel GetExamples()
        {
            return new DBHandler.Model.EarlySettlementExecution.EarlySettlementExecutionModel()
            {
                clientId = "000024147",
                facId = "12",
                prodId = "21",
                dealRefNo = "123456",
                settlementDate = DateTime.Now,
                PaymentSource = "asd",
                CurrAccId = "ad",
                principalAmt = 1
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
