﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWLoans.Swagger.BankSmart.DebitInstruction
{

    //REQUEST
    public class DebitInstructionRequest : IExamplesProvider<DBHandler.Model.DebitInstruction.DebitInstructionModel>
    {
        public DBHandler.Model.DebitInstruction.DebitInstructionModel GetExamples()
        {
            return new DBHandler.Model.DebitInstruction.DebitInstructionModel()
            {
               ClientId = "",
               FeeAmt = 20000,
               DebitAccNo = "",
               FacId = "",
               ApplyVAT = "",
               FeeType = "",
               miscFeePurpose = "",
               ProdId = "",
               RefNo = "",
               ReversalReason = "",
               ReversalType = ""

            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
