﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using DBHandler.Model.LoanSimulatorV2;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace MWLoans.Swagger.BankSmart.LoanSimulatorV2
{

    //REQUEST
    public class LoanSimulatorV2Request : IExamplesProvider<LoanSimulatorDtoV2>
    {
        public LoanSimulatorDtoV2 GetExamples()
        {
            return new LoanSimulatorDtoV2()
            {
                LoanAmount = 150,
                InterestRate=4,
                Method="B",
                RePaymentMode="M",
                StartDate=DateTime.Now,
                MaturityDate=DateTime.Now,
                ManualSchedule= new List<ManualScheduleD>() 
                {
                    new ManualScheduleD()
                    {
                        Percentage=0,
                        DueDate=DateTime.Now
                    }
                },
                MoretoreumPeriod=0

            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
