﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWLoans.Swagger.BankSmart.CollateralInquiry
{
    //REQUEST
    public class CollateralInquiryRequest : IExamplesProvider<DBHandler.Model.Dtos.CollateralInquiry>
    {
        public DBHandler.Model.Dtos.CollateralInquiry GetExamples()
        {
            return new DBHandler.Model.Dtos.CollateralInquiry()
            {
                CollateralID= "o6UxqIbw7SKmkFmJ0puiZ1q9",
                
            };
        }
    }

    //RESPONSE
    //public class CollateralInquiryResponse : IExamplesProvider<ActiveResponseSucces<InquireCollateralOut>>
    //{
    //    public ActiveResponseSucces<InquireCollateralOut> GetExamples()
    //    {
    //        return new ActiveResponseSucces<InquireCollateralOut>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new InquireCollateralOut()
    //            {


    //            }

    //        };
    //    }
    //}
}
