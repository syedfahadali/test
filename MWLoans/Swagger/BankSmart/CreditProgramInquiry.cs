﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWLoans.Swagger.BankSmart.CreditProgramInquiry
{
    //REQUEST
    public class CreditProgramInquiryRequest : IExamplesProvider<DBHandler.Model.Dtos.CreditProgramInquiry>
    {
        public DBHandler.Model.Dtos.CreditProgramInquiry GetExamples()
        {
            return new DBHandler.Model.Dtos.CreditProgramInquiry()
            {
                CreditProgramID = "7MRyXXFyirHa9nN2d4vNTuI35"
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
