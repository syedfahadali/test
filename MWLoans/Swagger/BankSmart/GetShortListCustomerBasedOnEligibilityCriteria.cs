﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWLoans.Swagger.BankSmart.GetShortListCustomerBasedOnEligibilityCriteria
{
    //REQUEST
    public class GetShortListCustomerBasedOnEligibilityCriteriaRequest : IExamplesProvider<DBHandler.Model.SelectSQLModel>
    {
        public DBHandler.Model.SelectSQLModel GetExamples()
        {
            return new DBHandler.Model.SelectSQLModel()
            {
                IsAllColumn = new bool()
                {

                },
                ColumnToSelect = new List<string>()
                {
                    
                },
                TableToSelectAlias = "",
                TableToSelect = "",
                WhereClause = new List<WhereSQLModel>()
                {
                    new WhereSQLModel()
                    {

                    }
                }

            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}

}
