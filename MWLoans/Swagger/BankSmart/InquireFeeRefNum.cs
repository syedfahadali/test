﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using DBHandler.Model.Loan;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWLoans.Swagger.BankSmart.InquireFeeRefNum
{

    //REQUEST
    public class InquireFeeRefNumRequest : IExamplesProvider<InquireFeeRefNumModel>
    {
        public InquireFeeRefNumModel GetExamples()
        {
            return new InquireFeeRefNumModel()
            {
                refNo = "asd",
                feeType="asds"
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
