﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWLoans.Swagger.BankSmart.LoanSchedule
{

    //REQUEST
    public class FacilityProductBlockInquireLoanScheduleRequest : IExamplesProvider<GetLoanScheduleDetailsModel>
    {
        public GetLoanScheduleDetailsModel GetExamples()
        {
            return new GetLoanScheduleDetailsModel()
            {
                
                DealReferenceNo = "g2lkKvJi9c1aXqC0p5vv1ClQHLSIQo"
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
