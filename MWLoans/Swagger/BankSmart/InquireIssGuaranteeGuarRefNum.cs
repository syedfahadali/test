﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using DBHandler.Model.Loan;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWLoans.Swagger.BankSmart.InquireIssGuaranteeGuarRefNum
{

    //REQUEST
    public class InquireIssGuaranteeGuarRefNumRequest : IExamplesProvider<InquireIssGuaranteeGuarRefNumModel>
    {
        public InquireIssGuaranteeGuarRefNumModel GetExamples()
        {
            return new InquireIssGuaranteeGuarRefNumModel()
            {
                GuarRefNo = ""
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
