﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using DBHandler.Model.Loan;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWLoans.Swagger.BankSmart.InquireDealRefNumODPayCIF
{

    //REQUEST
    public class InquireDealRefNumODPayCIFRequest : IExamplesProvider<InquireIssGuaranteeAmedmentRefNumModel>
    {
        public InquireIssGuaranteeAmedmentRefNumModel GetExamples()
        {
            return new InquireIssGuaranteeAmedmentRefNumModel()
            {
                ClientID = "123123123"
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
