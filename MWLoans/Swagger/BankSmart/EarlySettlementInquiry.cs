﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWLoans.Swagger.BankSmart.EarlySettlementInquiry
{
    
    //REQUEST
    public class EarlySettlementInquiryRequest : IExamplesProvider<DBHandler.Model.EarlySettlementExecution.EarlySettlementExecutionModel>
    {
        public DBHandler.Model.EarlySettlementExecution.EarlySettlementExecutionModel GetExamples()
        {
            return new DBHandler.Model.EarlySettlementExecution.EarlySettlementExecutionModel()
            {
                clientId = "000000004",
                facId = "HkjBAQmS9zLLAaJGJOdSijuGN",
                prodId = "21",
                dealRefNo = "v",
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
