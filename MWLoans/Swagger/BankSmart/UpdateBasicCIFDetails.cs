﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWLoans.Swagger.BankSmart.UpdateBasicCIFDetails
{

    //REQUEST
    public class UpdateBasicCIFDetailsRequest : IExamplesProvider<DBHandler.Model.UpdateBasicCIFDetails>
    {
        public DBHandler.Model.UpdateBasicCIFDetails GetExamples()
        {
            return new DBHandler.Model.UpdateBasicCIFDetails()
            {
                ClientId = "000222797",
                RiskProfile = "Medium",
                BlackList = "Y",
                PEP = "Y",
                KYCReviewDate = DateTime.Now,
                Period = null
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
