﻿using DBHandler.Helper;
using DBHandler.Model;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWLoans.Swagger.BankSmart.DisbursementV2
{

    //REQUEST
    public class DisbursementV2Request : IExamplesProvider<DBHandler.Model.DisbursementV2.DisbursementV2Model>
    {
        public DBHandler.Model.DisbursementV2.DisbursementV2Model GetExamples()
        {
            return new DBHandler.Model.DisbursementV2.DisbursementV2Model()
            {
                AccountID = "0330100000013901",
                FacilityID = "",
                LoanCategoryID = "001",
                TenureType = "M",
                Tenure = 8,
                Method = "B",
                RePaymentMode = "M",
                GracePeriod = 0,
                RateType = "F",
                InterestRate = 4,
                RateID = "",
                Margin = 0,
                LoanAmount = 1,
                esFeeType = "Percentage",
                MinEsFee = 2000,
                EsFeePercentage = 1,
                Remarks = "asdfsa",
                DisbursementMode = "S",
                DisbAccountID = "0310100000013905",
                FeesAccountID = "0310100000013905",
                TrxAccountID = "0310100000013905",
                RecAccountID = "0335100000013901",
                interestAccountID = "0310100000013905",
                Charges= new List<DBHandler.Model.DisbursementV2.DisbChargesD>()
                {

                }
                
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}

}
