﻿using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWLoans.Swagger.BankSmart.LoanRepayment
{

    //REQUEST
    public class LoanRepaymentRequest : IExamplesProvider<DBHandler.Model.LoanRepayment.LoanRepaymentModel>
    {
        public DBHandler.Model.LoanRepayment.LoanRepaymentModel GetExamples()
        {
            return new DBHandler.Model.LoanRepayment.LoanRepaymentModel()
            {
                ClientId = "",
                FacId = "",
                ProdId = "",
                DealRefNo = "",
                RePaymentType = "Partial",
                RepaymentAmt = 0,
                CurrAccID = "",
                PaymentMode = "",
                PaymentSource = "",
                TransactionDate = DateTime.Now,
                ForceDebit = "Y"
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
