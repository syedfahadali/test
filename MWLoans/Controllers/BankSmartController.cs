﻿using System.Collections.Generic;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CoreHandler;
using DBHandler.Helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using DBHandler.Model;
using DBHandler.Model.Dtos;
using DBHandler.Repositories;
using Microsoft.Extensions.Configuration;
using DBHandler.Enum;
using CommonModel;
using Newtonsoft.Json.Linq;
using Swashbuckle.AspNetCore.Annotations;
using System;
//using RuleEngineHandler;
using DBHandler.CreditProgramSearchByCIFModel;
using DBHandler.CreditProgramUpdateModel;
using DBHandler.PostChargesModel;
using DBHandler.Model.Loan;
using DBHandler.Model.Gurantee;
using System.Globalization;
using DigiBancMiddleware.Helper;

namespace VendorApi.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class BankSmartController : ControllerBase
    {
        private readonly HttpHandler.HttpHandler httpHandler;
        private readonly IUserChannelsRepository _userChannels;
        private readonly ILogRepository _logs;
        private readonly IConfiguration _config;
        private Level level = Level.Info;
        private Exception myEx;
        private ControllerBase _controllerBase;
        private Exception excetionForLog;

        public BankSmartController(IConfiguration config, IUserChannelsRepository userChannels, IHttpClientFactory httpClientFactory, ILogRepository logs, ControllerBase controllerbase = null)
        {
            _controllerBase = controllerbase;
            _config = config;
            _userChannels = userChannels;
            _logs = logs;
            httpHandler = new HttpHandler.HttpHandler(httpClientFactory);
        }

        [Route("GetCASAAccounts")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<CIFAccounts>>", typeof(ActiveResponseSucces<List<CIFAccounts>>))]
        public async Task<IActionResult> GetCASAAccounts([FromBody] CIFExist Model)
        {
            
           
            ActiveResponseSucces<List<CIFAccounts>> response = new ActiveResponseSucces<List<CIFAccounts>>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:CIFAccounts"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<CIFAccounts>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("CIFExists")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<dynamic>", typeof(ActiveResponseSucces<dynamic>))]
        public async Task<IActionResult> CIFExists([FromBody] CIFExist Model)
        {
            
           
            ActiveResponseSucces<dynamic> response = new ActiveResponseSucces<dynamic>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:CIFExists"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<dynamic>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("UpdateBasicCIFDetails")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<bool>", typeof(ActiveResponseSucces<bool>))]
        public async Task<IActionResult> UpdateBasicCIFDetails([FromBody] UpdateBasicCIFDetails Model)
        {
            
           
            ActiveResponseSucces<bool> response = new ActiveResponseSucces<bool>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:UpdateBasicCIFDetails"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<bool>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("CIFExposure")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<CIFExposure>>", typeof(ActiveResponseSucces<List<CIFExposure>>))]
        public async Task<IActionResult> CIFExposure([FromBody] CIFExist Model)
        {
            
           
            ActiveResponseSucces<List<CIFExposure>> response = new ActiveResponseSucces<List<CIFExposure>>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:CIFExposure"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<CIFExposure>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("CIFHierarchy")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<CIFHierarchy>>", typeof(ActiveResponseSucces<List<CIFHierarchy>>))]
        public async Task<IActionResult> CIFHierarchy([FromBody] CIFExist Model)
        {
            ActiveResponseSucces<List<CIFHierarchy>> response = new ActiveResponseSucces<List<CIFHierarchy>>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:CIFHierarchy"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<CIFHierarchy>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("CollateralInquiry")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<InquireCollateralOut>", typeof(ActiveResponseSucces<InquireCollateralOut>))]
        public async Task<IActionResult> CollateralInquiry([FromBody] CollateralInquiry Model)
        {
            
           
            ActiveResponseSucces<InquireCollateralOut> response = new ActiveResponseSucces<InquireCollateralOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:CollateralInquiry"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<InquireCollateralOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("FacilityOverview")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DBHandler.Model.FacilityOverview.FacilityOverviewResponse>>", typeof(ActiveResponseSucces<List<DBHandler.Model.FacilityOverview.FacilityOverviewResponse>>))]
        public async Task<IActionResult> FacilityOverview([FromBody] DBHandler.Model.FacilityOverview.FacilityOverviewModel Model)
        {
            
           
            ActiveResponseSucces<List<DBHandler.Model.FacilityOverview.FacilityOverviewResponse>> response = new ActiveResponseSucces<List<DBHandler.Model.FacilityOverview.FacilityOverviewResponse>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FacilityOverview"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<DBHandler.Model.FacilityOverview.FacilityOverviewResponse>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("LoanOverview")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.LoanOverview.LoanOverviewResponse>", typeof(ActiveResponseSucces<DBHandler.Model.LoanOverview.LoanOverviewResponse>))]
        public async Task<IActionResult> LoanOverview([FromBody] DBHandler.Model.LoanOverview.LoanOverviewModel Model)
        {
            
           
            ActiveResponseSucces<DBHandler.Model.LoanOverview.LoanOverviewResponse> response = new ActiveResponseSucces<DBHandler.Model.LoanOverview.LoanOverviewResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:LoanOverview"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.LoanOverview.LoanOverviewResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }


        [Route("LoanSchedule")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.LoanSchedule.LoanScheduleResponse>", typeof(ActiveResponseSucces<DBHandler.Model.LoanSchedule.LoanScheduleResponse>))]
        public async Task<IActionResult> LoanSchedule([FromBody] DBHandler.Model.LoanSchedule.LoanScheduleModel Model)
        {
            
           
            ActiveResponseSucces<DBHandler.Model.LoanSchedule.LoanScheduleResponse> response = new ActiveResponseSucces<DBHandler.Model.LoanSchedule.LoanScheduleResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:LoanSchedule"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.LoanSchedule.LoanScheduleResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("LoanDetails")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DBHandler.Model.LoanDetail.LoanDetailsResponse>>", typeof(ActiveResponseSucces<List<DBHandler.Model.LoanDetail.LoanDetailsResponse>>))]
        public async Task<IActionResult> LoanDetails([FromBody] DBHandler.Model.LoanDetail.LoanDetailsModel Model)
        {
            
           
            ActiveResponseSucces<List<DBHandler.Model.LoanDetail.LoanDetailsResponse>> response = new ActiveResponseSucces<List<DBHandler.Model.LoanDetail.LoanDetailsResponse>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:LoanDetails"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<DBHandler.Model.LoanDetail.LoanDetailsResponse>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("BuyerCCP")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DBHandler.Model.BuyerCCP.BuyerCCPsResponse>>", typeof(ActiveResponseSucces<List<DBHandler.Model.BuyerCCP.BuyerCCPsResponse>>))]
        public async Task<IActionResult> BuyerCCP([FromBody] DBHandler.Model.BuyerCCP.BuyerCCPsModel Model)
        {
            
           
            ActiveResponseSucces<List<DBHandler.Model.BuyerCCP.BuyerCCPsResponse>> response = new ActiveResponseSucces<List<DBHandler.Model.BuyerCCP.BuyerCCPsResponse>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:BuyerCCP"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<DBHandler.Model.BuyerCCP.BuyerCCPsResponse>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("BuyerCCPDetails")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DBHandler.Model.BuyerCCPDetails.BuyerCCPDetailsResponse>>", typeof(ActiveResponseSucces<List<DBHandler.Model.BuyerCCPDetails.BuyerCCPDetailsResponse>>))]
        public async Task<IActionResult> BuyerCCPDetails([FromBody] DBHandler.Model.BuyerCCPDetails.BuyerCCPDetailsModel Model)
        {
            
           
            ActiveResponseSucces<List<DBHandler.Model.BuyerCCPDetails.BuyerCCPDetailsResponse>> response = new ActiveResponseSucces<List<DBHandler.Model.BuyerCCPDetails.BuyerCCPDetailsResponse>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:BuyerCCPDetails"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<DBHandler.Model.BuyerCCPDetails.BuyerCCPDetailsResponse>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("CreditProgramInquiry")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CreditProgramOut>", typeof(ActiveResponseSucces<CreditProgramOut>))]
        public async Task<IActionResult> CreditProgramInquiry([FromBody] CreditProgramInquiry Model)
        {
            
           
            ActiveResponseSucces<CreditProgramOut> response = new ActiveResponseSucces<CreditProgramOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:CreditProgramInquiry"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CreditProgramOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }
        
        [Route("GetShortListCustomerBasedOnEligibilityCriteria")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<Customer>>", typeof(ActiveResponseSucces<List<CustomerDTOFor321>>))]
        public async Task<IActionResult> GetShortListCustomerBasedOnEligibilityCriteria(SelectSQLModel Model)
        {
            
           
            ActiveResponseSucces<List<CustomerDTOFor321>> response = new ActiveResponseSucces<List<CustomerDTOFor321>>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:GetShortListCustomerBasedOnEligibilityCriteria"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<CustomerDTOFor321>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        ///// <summary>
        ///// getting all cp for rule engine web portal
        ///// </summary>
        ///// <param name = "Model" ></ param >
        ///// < returns ></ returns >
        [Route("GetCustomerDigibancRuleDetails")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CustomerDigibancRuleDetails>", typeof(ActiveResponseSucces<CustomerDigibancRuleDetails>))]

        public async Task<IActionResult> GetCustomerDigibancRuleDetails([FromBody] CustomerDigibancRuleDetails Model)
        {
            ActiveResponseSucces<CustomerDigibancRuleDetails> response = new ActiveResponseSucces<CustomerDigibancRuleDetails>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:GetCustomerDigibancRuleDetails"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CustomerDigibancRuleDetails>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetDynamicData")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CreateCreditProgramOut>", typeof(ActiveResponseSucces<dynamic>))]
        public async Task<IActionResult> GetDynamicData([FromBody] DynamicParametersDTO Model)
        {
            
           
            ActiveResponseSucces<dynamic> response = new ActiveResponseSucces<dynamic>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:GetDynamicData"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<dynamic>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("SaveDynamicData")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CreateCreditProgramOut>", typeof(ActiveResponseSucces<dynamic>))]
        public async Task<IActionResult> SaveDynamicData([FromBody] SaveDynamicParametersDTO Model)
        {
            
           
            ActiveResponseSucces<dynamic> response = new ActiveResponseSucces<dynamic>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:SaveDynamicData"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<dynamic>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }
        [Route("FacilityInquiry")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<FacilityOut>", typeof(ActiveResponseSucces<FacilityOut>))]
        public async Task<IActionResult> FacilityInquiry([FromBody] FacilityInquiry Model)
        {
            
           
            ActiveResponseSucces<FacilityOut> response = new ActiveResponseSucces<FacilityOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FacilityInquiry"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<FacilityOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [Route("FacilityBlockInquire")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<FacilityBlockInquiry>", typeof(ActiveResponseSucces<FacilityBlockInquiry>))]
        public async Task<IActionResult> FacilityBlockInquire([FromBody] FacilityInquiry Model)
        {
            
           
            ActiveResponseSucces<FacilityBlockInquiry> response = new ActiveResponseSucces<FacilityBlockInquiry>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FacilityBlockInquire"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<FacilityBlockInquiry>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }
        [Route("FacilityProductBlock")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<FacilityProductBlockOut>", typeof(ActiveResponseSucces<FacilityProductBlockOut>))]
        public async Task<IActionResult> FacilityProductBlock([FromBody] FacInquireIssGuaranteeAmedmentRefNumModelilityProductInquiry Model)
        {
            
           
            ActiveResponseSucces<FacilityProductBlockOut> response = new ActiveResponseSucces<FacilityProductBlockOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FacilityProductBlock"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<FacilityProductBlockOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }
        [Route("FacilityProductUnBlock")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<FacilityProductUnBlockOut>", typeof(ActiveResponseSucces<FacilityProductUnBlockOut>))]
        public async Task<IActionResult> FacilityProductUnBlock([FromBody] FacInquireIssGuaranteeAmedmentRefNumModelilityProductInquiry Model)
        {
            
           
            ActiveResponseSucces<FacilityProductUnBlockOut> response = new ActiveResponseSucces<FacilityProductUnBlockOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FacilityProductUnBlock"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<FacilityProductUnBlockOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }


        [Route("FacilityProductBlockInquire")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<FacilityProductBlockInquiry>", typeof(ActiveResponseSucces<FacilityProductBlockInquiry>))]
        public async Task<IActionResult> FacilityProductBlockInquire([FromBody] FacInquireIssGuaranteeAmedmentRefNumModelilityProductInquiry Model)
        {
            
           
            ActiveResponseSucces<FacilityProductBlockInquiry> response = new ActiveResponseSucces<FacilityProductBlockInquiry>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FacilityProductBlockInquire"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<FacilityProductBlockInquiry>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }
        [Route("InquireGuaranteeProductAvalIssuance")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<InquireGuaranteeProductAvalIssuanceResponse>", typeof(ActiveResponseSucces<InquireGuaranteeProductAvalIssuanceResponse>))]
        public async Task<IActionResult> InquireGuaranteeProductAvalIssuance([FromBody] InquireRefNumDebitInstCIFModel Model)
        {
            
           
            ActiveResponseSucces<InquireGuaranteeProductAvalIssuanceResponse> response = new ActiveResponseSucces<InquireGuaranteeProductAvalIssuanceResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:InquireGuaranteeProductAvalIssuance"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<InquireGuaranteeProductAvalIssuanceResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("InquireIssGuaranteeAmedmentRefNum")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<InquireIssGuaranteeAmedmentRefNumResponse>", typeof(ActiveResponseSucces<InquireIssGuaranteeAmedmentRefNumResponse>))]
        public async Task<IActionResult> InquireIssGuaranteeAmedmentRefNum([FromBody] InquireIssGuaranteeAmedmentRefNumModel Model)
        {
            
           
            ActiveResponseSucces<InquireIssGuaranteeAmedmentRefNumResponse> response = new ActiveResponseSucces<InquireIssGuaranteeAmedmentRefNumResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:InquireIssGuaranteeAmedmentRefNum"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<InquireIssGuaranteeAmedmentRefNumResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }


        [Route("InquireIssGuaranteeGuarRefNum")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<InquireIssGuaranteeAmedmentRefNumResponse>", typeof(ActiveResponseSucces<InquireIssGuaranteeAmedmentRefNumResponse>))]
        public async Task<IActionResult> InquireIssGuaranteeGuarRefNum([FromBody] InquireIssGuaranteeGuarRefNumModel Model)
        {
            
           
            ActiveResponseSucces<InquireIssGuaranteeAmedmentRefNumResponse> response = new ActiveResponseSucces<InquireIssGuaranteeAmedmentRefNumResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:InquireIssGuaranteeGuarRefNum"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<InquireIssGuaranteeAmedmentRefNumResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("InquireAvailableLimit")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<InquireAvailableLimitResponse>", typeof(ActiveResponseSucces<InquireAvailableLimitResponseFinal>))]
        public async Task<IActionResult> InquireAvailableLimit([FromBody] InquireAvailableLimitModel Model)
        {
            
           
            ActiveResponseSucces<InquireAvailableLimitResponseFinal> response = new ActiveResponseSucces<InquireAvailableLimitResponseFinal>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:InquireAvailableLimit"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<InquireAvailableLimitResponseFinal>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }


        [Route("InquireFeeRefNum")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<InquireFeeRefNumResponse>", typeof(ActiveResponseSucces<InquireFeeRefNumResponse>))]
        public async Task<IActionResult> InquireFeeRefNum([FromBody] InquireFeeRefNumModel Model)
        {
            
           
            ActiveResponseSucces<InquireFeeRefNumResponse> response = new ActiveResponseSucces<InquireFeeRefNumResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:InquireFeeRefNum"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<InquireFeeRefNumResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("InquireDealRefNumODPayCIF")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<InquireDealRefNumODPayCIFResponse>", typeof(ActiveResponseSucces<InquireDealRefNumODPayCIFResponse>))]
        public async Task<IActionResult> InquireDealRefNumODPayCIF([FromBody] InquireIssGuaranteeAmedmentRefNumModel Model)
        {
            
           
            ActiveResponseSucces<InquireDealRefNumODPayCIFResponse> response = new ActiveResponseSucces<InquireDealRefNumODPayCIFResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:InquireDealRefNumODPayCIF"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<InquireDealRefNumODPayCIFResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }


        [Route("InquireODPaymentDealRefNum")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<InquireODPaymentDealRefNumResponse>", typeof(ActiveResponseSucces<InquireODPaymentDealRefNumResponse>))]
        public async Task<IActionResult> InquireODPaymentDealRefNum([FromBody] InquireODPaymentDealRefNumModel Model)
        {
            
           
            ActiveResponseSucces<InquireODPaymentDealRefNumResponse> response = new ActiveResponseSucces<InquireODPaymentDealRefNumResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:InquireODPaymentDealRefNum"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<InquireODPaymentDealRefNumResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }


        [Route("InquireRefNumDebitInstCIF")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<InquireRefNumDebitInstCIFResponse>", typeof(ActiveResponseSucces<InquireRefNumDebitInstCIFResponse>))]
        public async Task<IActionResult> InquireRefNumDebitInstCIF([FromBody] InquireRefNumDebitInstCIFModel Model)
        {
            
           
            ActiveResponseSucces<InquireRefNumDebitInstCIFResponse> response = new ActiveResponseSucces<InquireRefNumDebitInstCIFResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:InquireRefNumDebitInstCIF"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<InquireRefNumDebitInstCIFResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("InquireRefNumFeeChargesCIF")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<InquireRefNumFeeChargesCIFResponse>", typeof(ActiveResponseSucces<InquireRefNumFeeChargesCIFResponse>))]
        public async Task<IActionResult> InquireRefNumFeeChargesCIF([FromBody] InquireRefNumDebitInstCIFModel Model)
        {
            
           
            ActiveResponseSucces<InquireRefNumFeeChargesCIFResponse> response = new ActiveResponseSucces<InquireRefNumFeeChargesCIFResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:InquireRefNumFeeChargesCIF"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<InquireRefNumFeeChargesCIFResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }


        [Route("InquireDealRefNumEarlySettleCIF")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<InquireDealRefNumEarlySettleCIFResponse>", typeof(ActiveResponseSucces<InquireDealRefNumEarlySettleCIFResponse>))]
        public async Task<IActionResult> InquireDealRefNumEarlySettleCIF([FromBody] InquireRefNumDebitInstCIFModel Model)
        {
            
           
            ActiveResponseSucces<InquireDealRefNumEarlySettleCIFResponse> response = new ActiveResponseSucces<InquireDealRefNumEarlySettleCIFResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:InquireDealRefNumEarlySettleCIF"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<InquireDealRefNumEarlySettleCIFResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }
        [Route("EarlySettlementExecution")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DBHandler.Model.EarlySettlementExecution.EarlySettlementExecutionResponse>>", typeof(ActiveResponseSucces<DBHandler.Model.EarlySettlementExecution.EarlySettlementExecutionResponse>))]
        public async Task<IActionResult> EarlySettlementExecution([FromBody] DBHandler.Model.EarlySettlementExecution.EarlySettlementExecutionModel Model)
        {
            
           
            ActiveResponseSucces<DBHandler.Model.EarlySettlementExecution.EarlySettlementExecutionResponse> response = new ActiveResponseSucces<DBHandler.Model.EarlySettlementExecution.EarlySettlementExecutionResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:EarlySettlementExecution"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.EarlySettlementExecution.EarlySettlementExecutionResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }


        [Route("EarlySettlementInquiry")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DBHandler.Model.EarlySettlementInquiry.EarlySettlementInquiryResponse>>", typeof(ActiveResponseSucces<DBHandler.Model.EarlySettlementInquiry.EarlySettlementInquiryResponse>))]
        public async Task<IActionResult> EarlySettlementInquiry([FromBody] DBHandler.Model.EarlySettlementExecution.EarlySettlementExecutionModel Model)
        {
            
           
            ActiveResponseSucces<DBHandler.Model.EarlySettlementInquiry.EarlySettlementInquiryResponse> response = new ActiveResponseSucces<DBHandler.Model.EarlySettlementInquiry.EarlySettlementInquiryResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:EarlySettlementExecution"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.EarlySettlementInquiry.EarlySettlementInquiryResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("FeeChargesReversal")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DBHandler.Model.FeeChargesReversal.FeeChargesReversalResponse>>", typeof(ActiveResponseSucces<DBHandler.Model.FeeChargesReversal.FeeChargesReversalResponse>))]
        public async Task<IActionResult> FeeChargesReversal([FromBody] DBHandler.Model.FeeChargesReversal.FeeChargesReversalModel Model)
        {
            
           
            ActiveResponseSucces<DBHandler.Model.FeeChargesReversal.FeeChargesReversalResponse> response = new ActiveResponseSucces<DBHandler.Model.FeeChargesReversal.FeeChargesReversalResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FeeChargesReversal"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.FeeChargesReversal.FeeChargesReversalResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }


        [Route("DebitInstruction")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DBHandler.Model.DebitInstruction.DebitInstructionResponse>>", typeof(ActiveResponseSucces<DBHandler.Model.DebitInstruction.DebitInstructionResponse>))]
        public async Task<IActionResult> DebitInstruction([FromBody] DBHandler.Model.DebitInstruction.DebitInstructionModel Model)
        {
            
           
            ActiveResponseSucces<DBHandler.Model.DebitInstruction.DebitInstructionResponse> response = new ActiveResponseSucces<DBHandler.Model.DebitInstruction.DebitInstructionResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FeeChargesReversal"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.DebitInstruction.DebitInstructionResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("LoanRepayment")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DBHandler.Model.LoanRepayment.LoanRepaymentResponse>>", typeof(ActiveResponseSucces<DBHandler.Model.LoanRepayment.LoanRepaymentResponse>))]
        public async Task<IActionResult> LoanRepayment([FromBody] DBHandler.Model.LoanRepayment.LoanRepaymentModel Model)
        {
            
           
            ActiveResponseSucces<DBHandler.Model.LoanRepayment.LoanRepaymentResponse> response = new ActiveResponseSucces<DBHandler.Model.LoanRepayment.LoanRepaymentResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FeeChargesReversal"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.LoanRepayment.LoanRepaymentResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }



        [Route("GetZXDetails")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<GetZXDetailsOut>", typeof(ActiveResponseSucces<GetZXDetailsOut>))]
        public async Task<IActionResult> GetZXDetails([FromBody] GetZXDetailsDTO Model)
        {
            
           
            ActiveResponseSucces<GetZXDetailsOut> response = new ActiveResponseSucces<GetZXDetailsOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:GetZXDetails"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<GetZXDetailsOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }



        [Route("LoanSimulator")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<LoanSimulatorOut>", typeof(ActiveResponseSucces<LoanSimulatorOut>))]
        public async Task<IActionResult> LoanSimulator([FromBody] LoanSimulatorDto Model)
        {
            
           
            ActiveResponseSucces<LoanSimulatorOut> response = new ActiveResponseSucces<LoanSimulatorOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:LoanSimulator"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<LoanSimulatorOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }


        [Route("LoanSimulatorV2")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.LoanSimulatorV2.LoanSimulatorOutV2>", typeof(ActiveResponseSucces<DBHandler.Model.LoanSimulatorV2.LoanSimulatorOutV2>))]
        public async Task<IActionResult> LoanSimulatorV2([FromBody] DBHandler.Model.LoanSimulatorV2.LoanSimulatorDtoV2 Model)
        {
            
           
            ActiveResponseSucces<DBHandler.Model.LoanSimulatorV2.LoanSimulatorOutV2> response = new ActiveResponseSucces<DBHandler.Model.LoanSimulatorV2.LoanSimulatorOutV2>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:LoanSimulatorV2"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.LoanSimulatorV2.LoanSimulatorOutV2>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }
            return BadRequest(response);
        }

        [NonAction]
        [Route("DisbursementV2")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.DisbursementV2.DisbursementV2Response>", typeof(ActiveResponseSucces<DBHandler.Model.DisbursementV2.DisbursementV2Response>))]
        public async Task<IActionResult> DisbursementV2([FromBody] DBHandler.Model.DisbursementV2.DisbursementV2Model Model)
        {
            
           
            ActiveResponseSucces<DBHandler.Model.DisbursementV2.DisbursementV2Response> response = new ActiveResponseSucces<DBHandler.Model.DisbursementV2.DisbursementV2Response>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:DisbursementV2"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.DisbursementV2.DisbursementV2Response>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }


































        [Route("GetLoanAccountsPerCIF")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<LoanAccountsPerCIFResponse>>", typeof(ActiveResponseSucces<List<LoanAccountsPerCIFResponse>>))]
        public async Task<IActionResult> GetLoanAccountsPerCIF([FromBody] GetLoanAccountsPerCIFModel Model)
        {
            
           
            ActiveResponseSucces<List<LoanAccountsPerCIFResponse>> response = new ActiveResponseSucces<List<LoanAccountsPerCIFResponse>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:GetLoanAccountsPerCIF"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<LoanAccountsPerCIFResponse>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }


        [Route("GetLoanDetailsPerAccount")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<LoanDetailsPerAccountResponse>>", typeof(ActiveResponseSucces<List<LoanDetailsPerAccountResponse>>))]
        public async Task<IActionResult> GetLoanDetailsPerAccount([FromBody] GetLoanDetailsPerAccountModel Model)
        {
            
           
            ActiveResponseSucces<List<LoanDetailsPerAccountResponse>> response = new ActiveResponseSucces<List<LoanDetailsPerAccountResponse>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:GetLoanDetailsPerAccount"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<LoanDetailsPerAccountResponse>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("GetLoanScheduleDetails")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<List<LoanScheduleDetailsResponse>>>", typeof(ActiveResponseSucces<List<LoanScheduleDetailsResponse>>))]
        public async Task<IActionResult> GetLoanScheduleDetails([FromBody] GetLoanScheduleDetailsModel Model)
        {
            
           
            ActiveResponseSucces<List<LoanScheduleDetailsResponse>> response = new ActiveResponseSucces<List<LoanScheduleDetailsResponse>>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:GetLoanScheduleDetails"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<LoanScheduleDetailsResponse>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("LoanAdvice")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<List<LoanAdviceResponse>>>", typeof(ActiveResponseSucces<LoanAdviceResponse>))]
        public async Task<IActionResult> LoanAdvice([FromBody] LoanAdviceModel Model)
        {
            
           
            ActiveResponseSucces<LoanAdviceResponse> response = new ActiveResponseSucces<LoanAdviceResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:LoanAdvice"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        _controllerBase != null ? _controllerBase : this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<LoanAdviceResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("NoLiabilityLetter")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<NoLiabilityLetterResponse>", typeof(ActiveResponseSucces<NoLiabilityLetterResponse>))]
        public async Task<IActionResult> NoLiabilityLetter([FromBody] LiabilityLetterModel Model)
        {
            
           
            ActiveResponseSucces<NoLiabilityLetterResponse> response = new ActiveResponseSucces<NoLiabilityLetterResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:NoLiabilityLetter"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        _controllerBase != null ? _controllerBase : this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<NoLiabilityLetterResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("LiabilityLetter")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<LiabilityLetterResponse>", typeof(ActiveResponseSucces<LiabilityLetterResponse>))]
        public async Task<IActionResult> LiabilityLetter([FromBody] LiabilityLetterModel Model)
        {
            
           
            ActiveResponseSucces<LiabilityLetterResponse> response = new ActiveResponseSucces<LiabilityLetterResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:LiabilityLetter"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        _controllerBase != null ? _controllerBase : this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<LiabilityLetterResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }

        [Route("BalanceConfirmation")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<BalanceConfirmationResponse>", typeof(ActiveResponseSucces<BalanceConfirmationResponse>))]
        public async Task<IActionResult> BalanceConfirmation([FromBody] LiabilityLetterModel Model)
        {
            
           
            ActiveResponseSucces<BalanceConfirmationResponse> response = new ActiveResponseSucces<BalanceConfirmationResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:BalanceConfirmation"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        _controllerBase != null ? _controllerBase : this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<BalanceConfirmationResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }
        [Route("GuaranteeIssue")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<IssueResponse>", typeof(ActiveResponseSucces<IssueResponse>))]
        public async Task<IActionResult> GuaranteeIssue([FromBody] IssueRequest Model)
        {
            
           
            ActiveResponseSucces<IssueResponse> response = new ActiveResponseSucces<IssueResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:GuaranteeIssue"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<IssueResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return CreatedAtAction(null, response);
                }
            }

            return BadRequest(response);
        }


        [Route("CIFSearch")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<dynamic>", typeof(ActiveResponseSucces<dynamic>))]
        public async Task<IActionResult> CIFSearch([FromBody] SearchCIF Model)
        {
            ActiveResponseSucces<dynamic> response = new ActiveResponseSucces<dynamic>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:CIFSearch"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<dynamic>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }


        [Route("CollateralCancel")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<UpdateCollateralOut>", typeof(ActiveResponseSucces<UpdateCollateralOut>))]
        public async Task<IActionResult> CollateralCancel([FromBody] CollateralInquiry Model)
        {
            ActiveResponseSucces<UpdateCollateralOut> response = new ActiveResponseSucces<UpdateCollateralOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:CollateralCancel"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<UpdateCollateralOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("CollateralSearch")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<dynamic>", typeof(ActiveResponseSucces<dynamic>))]
        public async Task<IActionResult> CollateralSearch([FromBody] SearchCollateral Model)
        {
            ActiveResponseSucces<dynamic> response = new ActiveResponseSucces<dynamic>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:CollateralSearch"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<dynamic>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("CollateralCreate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CreateCollateralOut>", typeof(ActiveResponseSucces<CreateCollateralOut>))]
        public async Task<IActionResult> CollateralCreate([FromBody] CreateCollateralDto Model)
        {
            ActiveResponseSucces<CreateCollateralOut> response = new ActiveResponseSucces<CreateCollateralOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:CollateralCreate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CreateCollateralOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("CollateralUpdate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<UpdateCollateralOut>", typeof(ActiveResponseSucces<UpdateCollateralOut>))]
        public async Task<IActionResult> CollateralUpdate([FromBody] UpdateCollateralDto Model)
        {
            ActiveResponseSucces<UpdateCollateralOut> response = new ActiveResponseSucces<UpdateCollateralOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:CollateralUpdate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<UpdateCollateralOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("CreditProgramCreate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CreateCreditProgramOut>", typeof(ActiveResponseSucces<CreateCreditProgramOut>))]
        public async Task<IActionResult> CreditProgramCreate([FromBody] CreateCreditProgramDto Model)
        {
            ActiveResponseSucces<CreateCreditProgramOut> response = new ActiveResponseSucces<CreateCreditProgramOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:CreditProgramCreate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CreateCreditProgramOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("CreditProgramUpdate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CreditProgramUpdateResponse>", typeof(ActiveResponseSucces<CreditProgramUpdateResponse>))]
        public async Task<IActionResult> CreditProgramUpdate([FromBody] CreditProgramUpdateModel Model)
        {
            ActiveResponseSucces<CreditProgramUpdateResponse> response = new ActiveResponseSucces<CreditProgramUpdateResponse>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:CreditProgramUpdate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CreditProgramUpdateResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("CreditProgramSearchByCIF")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CreditProgramSearchByCIFResponse>", typeof(ActiveResponseSucces<CreditProgramSearchByCIFResponse>))]
        public async Task<IActionResult> CreditProgramSearchByCIF([FromBody] CreditProgramSearchByCIFModel Model)
        {
            ActiveResponseSucces<List<CreditProgramSearchByCIFResponse>> response = new ActiveResponseSucces<List<CreditProgramSearchByCIFResponse>>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:CreditProgramSearchByCIF"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<CreditProgramSearchByCIFResponse>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("LoanDisbReversal")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DisbursementReversalOut>", typeof(ActiveResponseSucces<DisbursementReversalOut>))]
        public async Task<IActionResult> LoanDisbReversal([FromBody] LoanDisbReversalDto Model)
        {
            ActiveResponseSucces<DisbursementReversalOut> response = new ActiveResponseSucces<DisbursementReversalOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:LoanDisbursementReversal"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DisbursementReversalOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("LoanDisbursement")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DisbursementOut>", typeof(ActiveResponseSucces<DisbursementOut>))]
        public async Task<IActionResult> LoanDisbursement([FromBody] DisbursementDto Model)
        {
            ActiveResponseSucces<DisbursementOut> response = new ActiveResponseSucces<DisbursementOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:LoanDisbursement"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DisbursementOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("LoanCreateAccounts")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<LoanAccountOut>", typeof(ActiveResponseSucces<LoanAccountOut>))]
        public async Task<IActionResult> LoanCreateAccounts([FromBody] CreateLoanAccountDto Model)
        {
            ActiveResponseSucces<LoanAccountOut> response = new ActiveResponseSucces<LoanAccountOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:LoanCreateAccounts"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<LoanAccountOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("InqDisbDetails")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<InquireDisbursDetailsOut>", typeof(ActiveResponseSucces<InquireDisbursDetailsFinal>))]
        public async Task<IActionResult> InqDisbDetails([FromBody] InquireDisbursDetailsDTO Model)
        {
            ActiveResponseSucces<InquireDisbursDetailsFinal> response = new ActiveResponseSucces<InquireDisbursDetailsFinal>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:InqDisbDetails"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<InquireDisbursDetailsFinal>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("GuaranteeinvokeRev")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<InvokeRevResponse>", typeof(ActiveResponseSucces<InvokeRevResponse>))]
        public async Task<IActionResult> GuaranteeinvokeRev([FromBody] InvokeRevRequest Model)
        {
            ActiveResponseSucces<InvokeRevResponse> response = new ActiveResponseSucces<InvokeRevResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:GuaranteeinvokeRev"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<InvokeRevResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("GuaranteeClose")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CloseResponse>", typeof(ActiveResponseSucces<CloseResponse>))]
        public async Task<IActionResult> GuaranteeClose([FromBody] CloseRequest Model)
        {
            ActiveResponseSucces<CloseResponse> response = new ActiveResponseSucces<CloseResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:GuaranteeClose"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CloseResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("GuaranteeInvoke")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<InvokeResponse>", typeof(ActiveResponseSucces<InvokeResponse>))]
        public async Task<IActionResult> GuaranteeInvoke([FromBody] InvokeRequest Model)
        {
            ActiveResponseSucces<InvokeResponse> response = new ActiveResponseSucces<InvokeResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:GuaranteeInvoke"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<InvokeResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("GuaranteeCancel")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CancelResponse>", typeof(ActiveResponseSucces<CancelResponse>))]
        public async Task<IActionResult> GuaranteeCancel([FromBody] CancelRequest Model)
        {
            ActiveResponseSucces<CancelResponse> response = new ActiveResponseSucces<CancelResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:GuaranteeCancel"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CancelResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }
        [Route("GuaranteeAmend")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<AmendResponse>", typeof(ActiveResponseSucces<AmendResponse>))]
        public async Task<IActionResult> GuaranteeAmend([FromBody] AmendRequest Model)
        {
            ActiveResponseSucces<AmendResponse> response = new ActiveResponseSucces<AmendResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:GuaranteeAmend"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<AmendResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }
        

        [Route("FacilityUnBlock")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<FacilityUnBlockOut>", typeof(ActiveResponseSucces<FacilityUnBlockOut>))]
        public async Task<IActionResult> FacilityUnBlock([FromBody] FacilityInquiry Model)
        {
            ActiveResponseSucces<FacilityUnBlockOut> response = new ActiveResponseSucces<FacilityUnBlockOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FacilityUnBlock"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<FacilityUnBlockOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("FacilityBlock")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<FacilityBlockOut>", typeof(ActiveResponseSucces<FacilityBlockOut>))]
        public async Task<IActionResult> FacilityBlock([FromBody] FacilityInquiry Model)
        {
            ActiveResponseSucces<FacilityBlockOut> response = new ActiveResponseSucces<FacilityBlockOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FacilityBlock"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<FacilityBlockOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("FacilityCancel")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CancelFacilityOut>", typeof(ActiveResponseSucces<CancelFacilityOut>))]
        public async Task<IActionResult> FacilityCancel([FromBody] CancelFacilityDto Model)
        {
            ActiveResponseSucces<CancelFacilityOut> response = new ActiveResponseSucces<CancelFacilityOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FacilityCancel"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CancelFacilityOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("FacilitySearch")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<SearchFacilityOut>", typeof(ActiveResponseSucces<SearchFacilityOut>))]
        public async Task<IActionResult> FacilitySearch([FromBody] SearchFacilityDto Model)
        {
            ActiveResponseSucces<SearchFacilityOut> response = new ActiveResponseSucces<SearchFacilityOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FacilitySearch"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<SearchFacilityOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("FacilityModify")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<ModifyFacilityOut>", typeof(ActiveResponseSucces<ModifyFacilityOut>))]
        public async Task<IActionResult> FacilityModify([FromBody] ModifyFacilityDto Model)
        {
            ActiveResponseSucces<ModifyFacilityOut> response = new ActiveResponseSucces<ModifyFacilityOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FacilityModify"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<ModifyFacilityOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("FacilityCreate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CreateFacilityOut>", typeof(ActiveResponseSucces<CreateFacilityOut>))]
        public async Task<IActionResult> FacilityCreate([FromBody] CreateFacilityDto Model)
        {
            ActiveResponseSucces<CreateFacilityOut> response = new ActiveResponseSucces<CreateFacilityOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FacilityCreate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CreateFacilityOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("AccountLimitCancel")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CancelAccountLimitOut>", typeof(ActiveResponseSucces<CancelAccountLimitOut>))]
        public async Task<IActionResult> AccountLimitCancel([FromBody] CancelAccountLimitDto Model)
        {
            ActiveResponseSucces<CancelAccountLimitOut> response = new ActiveResponseSucces<CancelAccountLimitOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FacilityAccCancel"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CancelAccountLimitOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("AccountLimitModify")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CreateAccountLimitOut>", typeof(ActiveResponseSucces<CreateAccountLimitOut>))]
        public async Task<IActionResult> AccountLimitModify([FromBody] ModifyAccountLimitDto Model)
        {
            ActiveResponseSucces<CreateAccountLimitOut> response = new ActiveResponseSucces<CreateAccountLimitOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FacilityAccModify"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CreateAccountLimitOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("AccountLimitCreate")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<CreateAccountLimitOut>", typeof(ActiveResponseSucces<CreateAccountLimitOut>))]
        public async Task<IActionResult> AccountLimitCreate([FromBody] CreateAccountLimitDto Model)
        {
            ActiveResponseSucces<CreateAccountLimitOut> response = new ActiveResponseSucces<CreateAccountLimitOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FacilityAccCreate"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<CreateAccountLimitOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("AccountLimitSearch")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<SearchAccountLimitOut>", typeof(ActiveResponseSucces<SearchAccountLimitOut>))]
        public async Task<IActionResult> AccountLimitSearch([FromBody] SearchAccountLimitDto Model)
        {
            ActiveResponseSucces<SearchAccountLimitOut> response = new ActiveResponseSucces<SearchAccountLimitOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FacilityAccSearch"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<SearchAccountLimitOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("AccountLimitInquiry")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<FacilityAccInquiryOut>", typeof(ActiveResponseSucces<FacilityAccInquiryOut>))]
        public async Task<IActionResult> AccountLimitInquiry([FromBody] FacilityAccInquiry Model)
        {
            ActiveResponseSucces<FacilityAccInquiryOut> response = new ActiveResponseSucces<FacilityAccInquiryOut>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:FacilityAccInquiry"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<FacilityAccInquiryOut>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }
            return BadRequest(response);
        }

        [Route("ProductDetails")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<DBHandler.Model.ProductDetailsModelResponse>>", typeof(ActiveResponseSucces<List<DBHandler.Model.ProductDetailsModelResponse>>))]
        public async Task<IActionResult> ProductDetails([FromBody] DBHandler.Model.ProductDetailsModel Model)
        {
            ActiveResponseSucces<List<DBHandler.Model.ProductDetailsModelResponse>> response = new ActiveResponseSucces<List<DBHandler.Model.ProductDetailsModelResponse>>();
            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:ProductDetails"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<DBHandler.Model.ProductDetailsModelResponse>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("BuyerCCPInvoices")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<string>", typeof(ActiveResponseSucces<List<DBHandler.Model.BuyerCCPInvoicesResponse>>))]
        public async Task<IActionResult> BuyerCCPInvoices([FromBody] DBHandler.Model.BuyerCCPInvoicesModel Model)
        {
            ActiveResponseSucces<List<DBHandler.Model.BuyerCCPInvoicesResponse>> response = new ActiveResponseSucces<List<DBHandler.Model.BuyerCCPInvoicesResponse>>();
            InternalResult result = await APIRequestHandler.GetResponse( Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:BuyerCCPInvoices"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.Status = result.Status;
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<List<DBHandler.Model.BuyerCCPInvoicesResponse>>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("BuyerCCPJC")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<DBHandler.Model.BuyerCCPJCModel.BuyerCCPJCResponse>", typeof(ActiveResponseSucces<DBHandler.Model.BuyerCCPJCModel.BuyerCCPJCResponse>))]
        public async Task<IActionResult> BuyerCCPJC([FromBody] DBHandler.Model.BuyerCCPJCModel.BuyerCCPJCModel Model)
        {
            ActiveResponseSucces<DBHandler.Model.BuyerCCPJCModel.BuyerCCPJCResponse> response = new ActiveResponseSucces<DBHandler.Model.BuyerCCPJCModel.BuyerCCPJCResponse>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:BuyerCCPJC"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<DBHandler.Model.BuyerCCPJCModel.BuyerCCPJCResponse>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("LoanAdviceV2")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<List<List<LoanAdviceResponseV2>>>", typeof(ActiveResponseSucces<LoanAdviceResponseV2>))]
        public async Task<IActionResult> LoanAdviceV2([FromBody] LoanAdviceModelV2 Model)
        {
            ActiveResponseSucces<LoanAdviceResponseV2> response = new ActiveResponseSucces<LoanAdviceResponseV2>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:LoanAdviceV2"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        _controllerBase != null ? _controllerBase : this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<LoanAdviceResponseV2>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("NoLiabilityLetterV2")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<NoLiabilityLetterResponseV2>", typeof(ActiveResponseSucces<NoLiabilityLetterResponseV2>))]
        public async Task<IActionResult> NoLiabilityLetterV2([FromBody] NoLiabilityLetterModelV2 Model)
        {
            ActiveResponseSucces<NoLiabilityLetterResponseV2> response = new ActiveResponseSucces<NoLiabilityLetterResponseV2>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:NoLiabilityLetterv2"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        _controllerBase != null ? _controllerBase : this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<NoLiabilityLetterResponseV2>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("LiabilityLetterV2")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<LiabilityLetterResponseV2>", typeof(ActiveResponseSucces<LiabilityLetterResponseV2>))]
        public async Task<IActionResult> LiabilityLetterV2([FromBody] LiabilityLetterModelV2 Model)
        {
            ActiveResponseSucces<LiabilityLetterResponseV2> response = new ActiveResponseSucces<LiabilityLetterResponseV2>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:LiabilityLetterV2"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        _controllerBase != null ? _controllerBase : this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<LiabilityLetterResponseV2>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }

        [Route("BalanceConfirmationV2")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        [HttpPost]
        [SwaggerResponse(200, "ActiveResponseSucces<BalanceConfirmationResponseV2>", typeof(ActiveResponseSucces<BalanceConfirmationResponseV2>))]
        public async Task<IActionResult> BalanceConfirmationV2([FromBody] BalanceConfirmationModelV2 Model)
        {
            ActiveResponseSucces<BalanceConfirmationResponseV2> response = new ActiveResponseSucces<BalanceConfirmationResponseV2>();

            InternalResult result = await APIRequestHandler.GetResponse(Model,
                                                                        _config["BSLoansAPI:BaseUrl"].ToString(),
                                                                        _config["BSLoansAPI:BalanceConfirmationV2"].ToString(),
                                                                        level,
                                                                        _logs,
                                                                        _userChannels,
                                                                        _controllerBase != null ? _controllerBase : this,
                                                                        _config,
                                                                        httpHandler, 1);
            response.Status = result.Status;
            response.LogId = HttpContext.Request.Headers["LogId"];
            response.RequestDateTime = DateTime.Parse(HttpContext.Request.Headers["DateTime"]);
            if (result.Status.Code == null)
            {
                response = JsonConvert.DeserializeObject<ActiveResponseSucces<BalanceConfirmationResponseV2>>(result.httpResult.httpResponse);
                if (result != null && result.httpResult?.severity == "Success")
                {
                    return Ok(response);
                }
            }

            return BadRequest(response);
        }



    }
}
