﻿using System;
using System.IO;
using System.Threading.Tasks;
using DBHandler.Enum;
using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Repositories;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Routing;

namespace DigiBancMiddleware.Helper
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class ChannelAuthorizationMiddleware
    {
        private readonly RequestDelegate _next;
        public static long count = 1;


        public ChannelAuthorizationMiddleware(RequestDelegate next/*, IConfiguration config*/)
        {
            _next = next;
        }
        
        public async Task Invoke(HttpContext context, ILogRepository mainlog, IConfiguration config, IUserChannelsRepository _userChannels, UserManager<ApplicationUser> userManager)
        {
           string data = Utilities.GetRequestBodyInString(context);
           await Utilities.MockRequestBody(context, data);

            var id = context.Request.Headers["UserID"];
            var originalBodyStream = context.Response.Body;

            DBHandler.Helper.ActiveResponseSucces<string> resULT = new DBHandler.Helper.ActiveResponseSucces<string>()
            {
                LogId = context.Request.Headers["LogId"],
                Content = null,
                RequestDateTime = Convert.ToDateTime(context.Request.Headers["DateTime"])
            };

            using (var responseBody = new MemoryStream())
            {
                try
                {
                    var endpoint = context.GetEndpoint();
                    if (endpoint == null)
                    {

                        context.Request.Headers.Add("LogId", Guid.NewGuid().ToString());
                        context.Request.Headers.Add("DateTime", DateTime.Now.ToString());

                        resULT = new DBHandler.Helper.ActiveResponseSucces<string>()
                        {
                            LogId = context.Request.Headers["LogId"],
                            Content = null,
                            RequestDateTime = Convert.ToDateTime(context.Request.Headers["DateTime"])
                        };
                        string clientip = Utility.GetIP(context.Request.HttpContext);
                        
                        context.Response.Body = responseBody;
                        ErrorMessages mes = mainlog.GetErrorMessage("ERROR-04");
                        var path = context.Features.Get<IExceptionHandlerPathFeature>();
                        resULT.Status = new DBHandler.Helper.Status
                        {
                            Severity = Severity.Error,
                            StatusMessage = mes.ErrorMessage,
                            Code = mes.ErrorCode
                        };
                        context.Response.ContentType = "application/json";
                        context.Response.StatusCode = 404;
                        var requestResponse = JsonConvert.SerializeObject(resULT);
                        byte[] b = System.Text.ASCIIEncoding.ASCII.GetBytes(requestResponse);
                        originalBodyStream.Write(b);
                        string req = requestResponse == null ? JsonConvert.SerializeObject(resULT.Status) : requestResponse;
                        DateTime starTime = DateTime.Now;
                        Exception excetionForLog = new Exception("URL not connfigured, Not found.");
                        mainlog.Logs(Level.Error, resULT.Status.Code != null ? resULT.Status.Code.ToString() : "",
                        context,
                        excetionForLog,
                        starTime,
                        DateTime.Now,
                        data,
                        requestResponse,
                        id,
                        context.Request.Headers["ChannelId"],
                        context.Request.Headers["LogId"],
                        Convert.ToDateTime(context.Request.Headers["DateTime"]), System.Net.HttpStatusCode.NotFound, "", "", DateTime.Now, DateTime.Now, clientip, "", ""
                        , config["ErrorLog"], config["SuccessLog"], config["InfoLog"], "", "", "");
                        await responseBody.CopyToAsync(originalBodyStream);
                    }
                    else
                    {
                        //dto = Utility.ReadFromHeaders(context);
                        var controllerActionDescriptor = endpoint.Metadata.GetMetadata<ControllerActionDescriptor>();


                        string OurbranchId = context.Request.Headers["BranchId"];
                        string OurbankId = context.Request.Headers["BankId"];

                        DBHandler.Helper.APIHelper helper = new DBHandler.Helper.APIHelper(_userChannels, mainlog);

                        string chanelId = helper.ValidateChannel(id, controllerActionDescriptor.ControllerName, controllerActionDescriptor.ActionName, OurbankId, OurbranchId);
                        if (string.IsNullOrEmpty(chanelId))
                        {
                            string clientip = Utility.GetIP(context.Request.HttpContext);
                            string currentController = "";
                            string currentAction = "";
                            var RouteInfo = context.Request.HttpContext.GetRouteData().Values;
                            if (RouteInfo.Keys.Count > 0)
                            {
                                currentController = RouteInfo["controller"].ToString() == null ? "" : RouteInfo["controller"].ToString();
                                currentAction = RouteInfo["action"].ToString() == null ? "" : RouteInfo["action"].ToString();
                            }

                            context.Response.Body = responseBody;
                            ErrorMessages mes = mainlog.GetErrorMessage("ERROR-01");
                            resULT.Status = new DBHandler.Helper.Status
                            {
                                Severity = Severity.Error,
                                StatusMessage = $"Channel {context.Request.Headers["ChannelId"]} is not allowed access to {controllerActionDescriptor.ActionName} function.",
                                Code = mes.ErrorMessage
                            };
                            context.Response.ContentType = "application/json";
                            context.Response.StatusCode = 401;
                            var requestResponse = JsonConvert.SerializeObject(resULT);
                            byte[] b = System.Text.ASCIIEncoding.ASCII.GetBytes(requestResponse);
                            originalBodyStream.Write(b);
                            string req = requestResponse == null ? JsonConvert.SerializeObject(resULT.Status) : requestResponse;
                            DateTime starTime = DateTime.Now;
                            Exception excetionForLog = null;

                            mainlog.Logs(Level.Error, resULT.Status.Code != null ? resULT.Status.Code.ToString() : "",
                            context,
                            excetionForLog,
                            starTime,
                            DateTime.Now,
                            data,
                            requestResponse,
                            id,
                            context.Request.Headers["ChannelId"],
                            context.Request.Headers["LogId"],
                            Convert.ToDateTime(context.Request.Headers["DateTime"]), System.Net.HttpStatusCode.BadRequest, "", "", DateTime.Now, DateTime.Now, clientip, currentController, currentAction,
                            config["ErrorLog"], config["SuccessLog"], config["InfoLog"]);
                            await responseBody.CopyToAsync(originalBodyStream);
                        }

                        else
                        {
                            context.Request.Headers.Add("ChannelId", chanelId);
                            // context.Request.Headers.Add(")
                            await _next(context).ConfigureAwait(false);
                        }
                    }

                }
                catch (Exception ex)
                {
                    string clientip = Utility.GetIP(context.Request.HttpContext);
                    string currentController = "";
                    string currentAction = "";
                    string LogId = Guid.NewGuid().ToString();
                    var RouteInfo = context.Request.HttpContext.GetRouteData().Values;
                    if (RouteInfo.Keys.Count > 0)
                    {
                        currentController = RouteInfo["controller"].ToString() == null ? "" : RouteInfo["controller"].ToString();
                        currentAction = RouteInfo["action"].ToString() == null ? "" : RouteInfo["action"].ToString();
                    }

                    context.Response.Body = responseBody;
                    ErrorMessages mes = mainlog.GetErrorMessage("ERROR-01");

                    resULT.Status = new DBHandler.Helper.Status
                    {
                        Severity = Severity.Error,
                        StatusMessage = "Request is not as expected.",
                        Code = "ERROR-01"
                    };
                    context.Response.ContentType = "application/json";
                    context.Response.StatusCode = 400;
                    var requestResponse = JsonConvert.SerializeObject(resULT);
                    byte[] b = System.Text.ASCIIEncoding.ASCII.GetBytes(requestResponse);
                    originalBodyStream.Write(b);
                    string req = requestResponse == null ? JsonConvert.SerializeObject(resULT.Status) : requestResponse;
                    DateTime starTime = DateTime.Now;
                    Exception excetionForLog = ex;

                    mainlog.Logs(Level.Error, resULT.Status.Code != null ? resULT.Status.Code.ToString() : "",
                    context,
                    excetionForLog,
                    starTime,
                    DateTime.Now,
                    data,
                    requestResponse,
                    id,
                    context.Request.Headers["ChannelId"],
                            context.Request.Headers["LogId"],
                            Convert.ToDateTime(context.Request.Headers["DateTime"]), System.Net.HttpStatusCode.BadRequest, "", "", DateTime.Now, DateTime.Now, clientip, currentController, currentAction
                    , config["ErrorLog"], config["SuccessLog"], config["InfoLog"]);
                    await responseBody.CopyToAsync(originalBodyStream);
                }
            }
        }
    }
}
