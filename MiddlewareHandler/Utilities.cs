﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DigiBancMiddleware.Helper
{
    public static class Utilities
    {
        public static bool IsDMSError(string result)
        {
            List<string> errorlist = new List<string> { "400", "401", "404", "405", "500", "501", "502", "503", "415" };

            if (result.Length>3)
            {
                if(errorlist.Contains(result.Substring(0, 3)) && result.Substring(3, 1) == " ")
                {
                    return true;
                }
                return false;
            }

            return false;
        }

        public static bool IsBase64(this string base64String)
        {
            if (string.IsNullOrEmpty(base64String) || base64String.Length % 4 != 0
            || base64String.Contains(" ") || base64String.Contains("\t") || base64String.Contains("\r") || base64String.Contains("\n"))
            {
                return false;
            }
            try
            {
                Convert.FromBase64String(base64String);
                return true;
            }
            catch (Exception exception)
            {
                // Handle the exception
            }
            return false;
        }

        public static async Task MockRequestBody(HttpContext context, string jsonOriginalParamModel)
        {
            var requestContent = new StringContent(jsonOriginalParamModel, Encoding.UTF8);
            var stream = context.Request.Body;
            stream = await requestContent.ReadAsStreamAsync().ConfigureAwait(false);
            context.Request.Body = stream;
        }

        public  static string GetRequestBodyInString(HttpContext context)
        {
            //context.Request.EnableRewind();
            string bodyStr = string.Empty;
            using (StreamReader reader = new StreamReader(context.Request.Body, Encoding.UTF8, true, 1024, true))
            {
                bodyStr = reader.ReadToEnd();
            }
            //context.Request.Body.Position = 0;
            return bodyStr;
        }


        public  static async Task<string> FormatResponse(HttpResponse response)
        {
            //We need to read the response stream from the beginning...
            response.Body.Seek(0, SeekOrigin.Begin);

            //...and copy it into a string
            string text = await new StreamReader(response.Body).ReadToEndAsync();

            //We need to reset the reader for the response so that the client can read it.
            response.Body.Seek(0, SeekOrigin.Begin);

            //Return the string for the response, including the status code (e.g. 200, 404, 401, etc.)
            return $"{text}";
        }

        public static  async Task<string> GetResponseBodyInString(HttpContext context, Stream originalBodyStream)
        {
            string bodyStr = string.Empty;
            //var originalBodyStream = context.Response.Body;

            try
            {
                //var originalBodyStream = context.Response.Body;

                //Create a new memory stream...
                using (var responseBody = new MemoryStream())
                {
                    //...and use that for the temporary response body
                    context.Response.Body = responseBody;

                    //Continue down the Middleware pipeline, eventually returning to this class
                    //await _next(context);

                    //Format the response from the server
                    var response = await FormatResponse(context.Response);

                    //TODO: Save log to chosen datastore

                    //Copy the contents of the new memory stream (which contains the response) to the original stream, which is then returned to the client.
                    await responseBody.CopyToAsync(originalBodyStream);
                }
            }
            catch (Exception ex)
            {

            }
            return bodyStr;
        }

        public  static string GetRequestData(HttpContext context)
        {
            throw new NotImplementedException();
        }
    }

    public class ErrorResponse
    {
        //[DataMember(Name = "Message")]
        public string Message { get; set; }

        public ErrorResponse(string message)
        {
            Message = message;
        }
    }

    public static class ChannelAuthorizationMiddlewareExtensions
    {
        public static IApplicationBuilder UseChannelAuthorizationMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ChannelAuthorizationMiddleware>();
        }
    }
}
