﻿
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using DBHandler.Repositories;
using DBHandler.Model;
using DBHandler.Helper;
using DBHandler.Enum;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Controllers;
using System.Web;
using Microsoft.AspNetCore.Http.Extensions;

namespace DigiBancMiddleware.Helper
{
    public class RequestMiddleware : ControllerBase
    {
        private readonly RequestDelegate next;

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseMiddleware"/> class.
        /// </summary>
        /// <param name="next"></param>
        /// <param name="executor"></param>
        /// <param name="loggerFactory"></param>
        public RequestMiddleware(RequestDelegate next, IActionResultExecutor<ObjectResult> executor, ILoggerFactory loggerFactory, IConfiguration config)
        {
            this.next = next;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="context"></param>
        /// <param name="log"></param>
        /// <param name="config"></param>
        /// <returns>A <see cref="Task"/> representing the result of the asynchronous operation.</returns>
        public async Task Invoke(HttpContext context, ILogRqChRepository log, ILogRepository mainlog, IConfiguration config, IUserChannelsRepository _userChannels, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
        {
            string logId = context.Request.Headers["LogId"];
            string channelID = context.Request.Headers["ChannelId"];
            DateTime requestDateTime = Convert.ToDateTime(context.Request.Headers["DateTime"]);
            var id = context.Request.Headers["UserID"];

            string data = Utilities.GetRequestBodyInString(context);
            var originalBodyStream = context.Response.Body;
            DBHandler.Helper.ActiveResponseSucces<string> res = new DBHandler.Helper.ActiveResponseSucces<string>()
            {
                LogId = context.Request.Headers["LogId"],
                Content = null,
                RequestDateTime = Convert.ToDateTime(context.Request.Headers["DateTime"])
            };
            await Utilities.MockRequestBody(context, data);
            using (var responseBody = new MemoryStream())
            {
                try
                {
                    context.Response.Body = responseBody;

                    await next(context).ConfigureAwait(false);
                    var response = await Utilities.FormatResponse(context.Response);
                    context.Response.Body = responseBody;

                    string actionName = context.GetEndpoint().Metadata.GetMetadata<ControllerActionDescriptor>().ActionName;
                    string controllerName = context.GetEndpoint().Metadata.GetMetadata<ControllerActionDescriptor>().ControllerName;

                    string header = context.Request.Headers.ContainsKey("Authorization").ToString();
                    if (context.Request.Headers.ContainsKey("Authorization"))
                    {
                        context.Request.Headers.Remove("Authorization");
                    }
                    header = JsonConvert.SerializeObject(context.Request.Headers);

                    log.LogData(logId, channelID, requestDateTime, data, response, actionName, controllerName, header);//);
                    await responseBody.CopyToAsync(originalBodyStream);
                }
                catch (Exception ex)
                {
                    var starttime = DateTime.Now;
                    string clientip = Utility.GetIP(context.Request.HttpContext);
                    string currentController = "";
                    string currentAction = "";
                    var RouteInfo = context.Request.HttpContext.GetRouteData().Values;
                    if (RouteInfo.Keys.Count > 0)
                    {
                        currentController = RouteInfo["controller"].ToString() == null ? "" : RouteInfo["controller"].ToString();
                        currentAction = RouteInfo["action"].ToString() == null ? "" : RouteInfo["action"].ToString();
                    }

                    if (ex != null && ex.Message != null &&
                        (ex.Message == "One or more errors occurred. (No connection could be made because the target machine actively refused it.)" ||
                        ex.Message == "No connection could be made because the target machine actively refused it." ||
                        ex.Message.Contains("A connection attempt failed because the connected party") ||
                        ex.Message.Contains("No such host is known") ||
                        ex.Message.Contains("No connection could be made")))
                        
                    {
                        res.Status = new DBHandler.Helper.Status { Severity = Severity.Error, StatusMessage = "One or more errors occurred. (No connection could be made because the target machine actively refused it.)", Code = "ERROR-01" };
                    }

                    else if (ex != null && ex.Message != null && (ex.Message.Contains( "Value cannot be null")))
                    {
                        res.Status = new DBHandler.Helper.Status { Severity = Severity.Error, StatusMessage = "Request is not as expected.", Code = "ERROR-01" };
                    }
                    else if (ex != null && ex.Message != null && (ex.Message == "A task was canceled." || ex.Message == "The operation was canceled."))
                    {
                        res.Status = new DBHandler.Helper.Status { Severity = Severity.Error, StatusMessage = "Request has been timedout, 3 mints are default timeout.", Code = "ERROR-01" };
                    }
                    else
                    {
                        res.Status = new DBHandler.Helper.Status { Severity = Severity.Error, StatusMessage = "Response is not as expected in " + currentAction + " function.", Code = "ERROR-01" };
                    }

                    res.LogId = logId;
                    res.RequestDateTime = requestDateTime;
                    context.Response.ContentType = "application/json";
                    context.Response.StatusCode = 400;

                    var requestResponse = JsonConvert.SerializeObject(res);
                    Log model = new Log();
                    model.Application = "Global Handler Exception";
                    model.FunctionName = currentAction;
                    model.ControllerName = currentController;
                    model.DeviceID = "Exception DeviceID";
                    model.IP = "Exception ip";
                    model.StartTime = starttime;
                    model.EndTime = DateTime.Now;
                    model.Exception = ex.ToString();
                    model.Message = ex.Message;
                    model.Level = Level.Error.ToString();
                    model.Status = "Exception";
                    model.RequestResponse = requestResponse;
                    model.RequestParameters = data;
                    model.LogId = logId;
                    model.RequestDateTime = DateTime.Now;
                    model.ErrorCode = "Error-01";
                    model.Channel = channelID;
                    model.UserID = context.Request.Headers["UserID"];

                    mainlog.Logs(Level.Error, "", context, ex, DateTime.Now, DateTime.Now,
                            model.RequestParameters, model.RequestResponse, model.UserID, model.Channel, model.LogId, model.RequestDateTime, System.Net.HttpStatusCode.InternalServerError, "", "", DateTime.Now, DateTime.Now, clientip, currentController, currentAction
                            ,config["ErrorLog"],config["SuccessLog"],config["InfoLog"], "", "", "");

                    string header = JsonConvert.SerializeObject(context.Request.Headers);

                    if (data == "")
                    {
                        Uri myUri = new Uri(context.Request.GetEncodedUrl());
                        string getQueryParameter = HttpUtility.ParseQueryString(myUri.Query).ToString();
                        data = getQueryParameter;
                    }

                    log.LogData(logId, channelID, requestDateTime, data, requestResponse, currentAction, currentController, header);
                    byte[] b = System.Text.ASCIIEncoding.ASCII.GetBytes(requestResponse);
                    originalBodyStream.Write(b);
                    await responseBody.CopyToAsync(originalBodyStream);
                }
            }
        }
    }
}
