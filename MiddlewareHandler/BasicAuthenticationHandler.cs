﻿using CommonModel;
using DBHandler.Enum;
using DBHandler.Helper;
using DBHandler.Model;
using DBHandler.Repositories;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Routing;


namespace DigiBancMiddleware.Helper
{
    public class BasicAuthenticationHandler : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        private UserManager<ApplicationUser> _userManager;
        private SignInManager<ApplicationUser> _signInManager;
        public ILogRepository mainlog { get; set; }
        public IConfiguration _config { get; set; }

        //  private string a="Invalid";            
        public BasicAuthenticationHandler(IOptionsMonitor<AuthenticationSchemeOptions> options, IConfiguration config, ILoggerFactory logger, ILogRepository mainlog, UrlEncoder encoder,
            ISystemClock clock, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager) : base(options, logger, encoder, clock)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            this.mainlog = mainlog;
            this._config = config;

        }

        protected override async Task HandleChallengeAsync(AuthenticationProperties properties)
        {
            Response.Headers["WWW-Authenticate"] = $"Basic realm=\"VendorApi\", charset=\"UTF-8\"";
            Response.ContentType = "application/json";
            Response.StatusCode = 401;

            //this.Context.Request.Headers.Add("LogId", Guid.NewGuid().ToString());
            //this.Context.Request.Headers.Add("DateTime", DateTime.Now.ToString());


            string data = GetRequestBodyInString();
            //DTO dto = new DTO();
            //dto = Utility.ReadFromHeaders(this.Context);
            DBHandler.Helper.ActiveResponseSucces<string> res = new DBHandler.Helper.ActiveResponseSucces<string>()
            {
                //LogId = dto.SignOnRq.LogId,
                LogId = this.Context.Request.Headers["LogId"],
                Content = null,
                //RequestDateTime = dto.SignOnRq.DateTime
                RequestDateTime = Convert.ToDateTime(this.Context.Request.Headers["DateTime"])
            };

            res.Status = new DBHandler.Helper.Status
            {
                Severity = Severity.Error,
                StatusMessage = $"Please check your username and password. If you still cant log in, contact your administrator.",
                Code = "ERROR-401"
            };

            var requestResponse = JsonConvert.SerializeObject(res);
            Response.Body.WriteAsync(Encoding.ASCII.GetBytes(requestResponse), 0, Encoding.ASCII.GetBytes(requestResponse).Length);

            string req = requestResponse == null ? JsonConvert.SerializeObject(res.Status) : requestResponse;
            DateTime starTime = DateTime.Now;
            Exception excetionForLog = null;


            string clientip = Utility.GetIP(this.Context.Request.HttpContext);
            string currentController = "";
            string currentAction = "";
            var RouteInfo = this.Context.Request.HttpContext.GetRouteData().Values;
            if (RouteInfo.Keys.Count > 0)
            {
                currentController = RouteInfo["controller"].ToString() == null ? "" : RouteInfo["controller"].ToString();
                currentAction = RouteInfo["action"].ToString() == null ? "" : RouteInfo["action"].ToString();
            }

            mainlog.Logs(Level.Error, res.Status.Code != null ? res.Status.Code.ToString() : "",
            this.Context,
            excetionForLog,
            starTime,
            DateTime.Now,
            data,
            requestResponse,
            "",
            this.Context.Request.Headers["ChannelId"],
            this.Context.Request.Headers["LogId"],
            Convert.ToDateTime(this.Context.Request.Headers["DateTime"]), System.Net.HttpStatusCode.InternalServerError, "",
                "", DateTime.Now, DateTime.Now, clientip, currentController, currentAction
                , _config["ErrorLog"], _config["SuccessLog"], _config["InfoLog"],"", "", "");//);
            await base.HandleChallengeAsync(properties);
        }

        private void NewMethod()
        {
            Task.FromResult(Response);
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            this.Context.Request.Headers.Add("LogId", Guid.NewGuid().ToString());
            this.Context.Request.Headers.Add("DateTime", DateTime.Now.ToString());

            AuthenticateResult result = null;
            if (!Request.Headers.ContainsKey("Authorization"))
                result = AuthenticateResult.Fail("Missing Authorization Header");
            try
            {
                var authHeader = AuthenticationHeaderValue.Parse(Request.Headers["Authorization"]);
                var credentialBytes = Convert.FromBase64String(authHeader.Parameter);
                var credentials = Encoding.UTF8.GetString(credentialBytes).Split(':');
                var username = credentials[0];
                var password = credentials[1];
                //user = await _userService.Authenticate(username, password);
                var user = await _userManager.FindByNameAsync(username);

                if (user == null)
                {
                    result = AuthenticateResult.Fail("Invalid Username or Password");
                }
                else
                {
                    this.Context.Request.Headers.Add("UserID", user.Id);
                }
                DateTime nowTime = DateTime.Now;
                if (user.LockoutEnd != null)
                {
                    DateTime? lockTime = DateTime.Parse(user.LockoutEnd.ToString());

                    if (lockTime > nowTime)
                    {

                        result = AuthenticateResult.Fail($"Account is lockedout until {lockTime}");
                    }
                }
                var _password = user.Id + password;
                var _result = await _signInManager.CheckPasswordSignInAsync(user, _password, true);
                if (_result.Succeeded)
                {
                    var claims = new[] {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
            };
                    var identity = new ClaimsIdentity(claims, Scheme.Name);
                    var principal = new ClaimsPrincipal(identity);
                    var ticket = new AuthenticationTicket(principal, Scheme.Name);

                    result = AuthenticateResult.Success(ticket);
                }
                else
                {
                    result = AuthenticateResult.Fail("Invalid Username or Password");
                }
            }
            catch (Exception ex)
            {
                result = AuthenticateResult.Fail("Invalid Authorization Header");
            }

            return await Task.FromResult(result);
        }

        private string GetRequestBodyInString()
        {
          
            string bodyStr = string.Empty;
            using (StreamReader reader = new StreamReader(this.Request.Body, Encoding.UTF8, true, 1024, true))
            {
                bodyStr = reader.ReadToEnd();
            }
            //context.Request.Body.Position = 0;
            return bodyStr;
        }
    }
}
